Main steps to execute the IMT benchmark:

- Clone the Git repository [mt-bench-pulic](https://gitlab.telecom-paris.fr/mbe-tools/mt-bench-public.git).
- To prepare your eclipse:
    - install the OSATE, Gradle for YAMTL, MoTE, VIATRA and eMoflon tools starting from the Eclipse [Modeling Tools package](https://www.eclipse.org/downloads/packages/).
    Or
    - use the [provided eclipse (for Linux)  into which all these tools have already been installed](https://gitlab.telecom-paris.fr/mbe-tools/mt-bench-public/-/blob/master/eclipse/imtBench-eclipse.tar.xz)

- In a first workspace, import /viatra/_fr.tpt.mem4csd.mtbench.aadl2aadl.trace.*_ and /required/_org.osate.aadl2_  and /required/_org.eclipse.xtext_ and /MoTE/_fr.tpt.mem4csd.mtbench.aadl2aadl.mote[.copying]_ projects.

- Run an Eclipse Application from the workspace.

- In the second workspace, import /required/_fr.tpt.mem4csd.utils.osate.standalone_ and 
/required/_fr.tpt.mem4csd.mtbench.aadl2aadl.tests_.  (with _fr.tpt.mem4csd.utils.compare_, _fr.tpt.mem4csd.utils.compare.aadl_ and _fr.tpt.mem4csd.utils.compare.emf_). Then, import the MoTE, eMoflon, VIATRA and YAMTL projects.

- In the _fr.tpt.mem4csd.mtbench.aadl2aadl.*.test_ project, run the launches/**TestSuiteRefinement.launch** or launches/**TestSuiteCopying.launch**
file to execute the benchmark model transformation.

- Update the **TestSuiteRefinement/TestSuiteCopying** classes to execute the model transformation
of different scenarios and different tests (they may be executed independently).

- As result, an excel file will be generated for each scenario, containing for each test: the AADL model name, the number of the port connections and the execution time in ns.

