package aadl2aadl

import java.util.List
import org.osate.aadl2.ComponentCategory
import org.osate.aadl2.ComponentPrototypeActual
import org.osate.aadl2.ComponentTypeRename
import org.osate.aadl2.Element
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.ConnectionInstance
import org.osate.aadl2.instance.ConnectionInstanceEnd
import org.osate.aadl2.instance.ConnectionKind
import org.osate.aadl2.instance.ConnectionReference
import org.osate.aadl2.instance.FeatureCategory
import org.osate.aadl2.instance.FeatureInstance
import org.osate.aadl2.instance.InstancePackage
import org.osate.aadl2.instance.SystemInstance
import yamtl.dsl.Rule

import java.util.Map

class Aadl2Aadl extends AbstractTransformation {
	val INSTANCE = InstancePackage.eINSTANCE

	new() {
		header().in('in', INSTANCE).out('out', INSTANCE)
		ruleStore(#[
			new Rule('connection2connection').priority(0).in('connection_in', INSTANCE.connectionInstance).filter [
				val connection_in = 'connection_in'.fetch as ConnectionInstance
				var boolean matches = (
					connection_in.kind != ConnectionKind.PORT_CONNECTION ||
					(connection_in.kind == ConnectionKind.PORT_CONNECTION && connection_in.is_portconnection_device
					)
				)
				if (matches) {
					for (refin : connection_in.connectionReferences) {
						if (connection_in.componentInstance.category == ComponentCategory.SYSTEM) {
							val old_element_source = connectionRef_to_source.get(refin)
							if (old_element_source !== null && refin.source != old_element_source) {

								removeFromListIs_in_portconnection_system_source(old_element_source, refin)
								connectionRef_to_source.remove(refin)
							}
							putInIs_in_portconnection_system_source(refin.source, refin)
							connectionRef_to_source.put(refin, refin.source)

							val old_element = connectionRef_to_destination.get(refin)
							if (old_element !== null && refin.destination != old_element) {

								removeFromListIs_in_portconnection_system_destination(old_element, refin)
								connectionRef_to_destination.remove(refin)
							}
							putInIs_in_portconnection_system_destination(refin.destination, refin)
							connectionRef_to_destination.put(refin, refin.destination)
						}
					}
				}
				matches
			].out('connection_out', INSTANCE.connectionInstance, [
				val connection_out = 'connection_out'.fetch as ConnectionInstance
				val connection_in = 'connection_in'.fetch as ConnectionInstance

				connection_out.name = connection_in.name
				connection_out.kind = connection_in.kind
				connection_out.bidirectional = connection_in.bidirectional
				connection_out.complete = connection_in.complete;
				connection_out.source = connection_in.source.fetch as ConnectionInstanceEnd
				connection_out.destination = connection_in.destination.fetch as ConnectionInstanceEnd

				connection_out.connectionReferences +=
					(connection_in.connectionReferences.fetch('refout', 'conRef2conRef') as List<ConnectionReference>)

				val component_instance = connection_in.componentInstance
				val target_component_instance = component_instance.fetch() as ComponentInstance
				target_component_instance.connectionInstances += connection_out

			]).undo [
				val connection_in = 'connection_in'.fetch as ConnectionInstance
				for (refin : connection_in.connectionReferences) {
					removeFromListIs_in_portconnection_system_source(refin.source, refin)
					connectionRef_to_source.remove(refin)
					removeFromListIs_in_portconnection_system_destination(refin.destination, refin)
					connectionRef_to_destination.remove(refin)

					val comp_list = getComponentList(refin)
					comp_list.forEach [ pair |
						insertDependency(pair.key, pair.value)
					]

				}
			],
			new Rule('connection2connection_complement').transient.priority(0).in('connection_in',
				INSTANCE.connectionInstance).filter [
				val connection_in = 'connection_in'.fetch as ConnectionInstance
				var boolean matches = (
					connection_in.kind != ConnectionKind.PORT_CONNECTION ||
					(connection_in.kind == ConnectionKind.PORT_CONNECTION && connection_in.is_portconnection_device
					)
				)
				if (!matches) {
					for (refin : connection_in.connectionReferences) {
						if (connection_in.componentInstance.category == ComponentCategory.SYSTEM) {
							val old_element_source = connectionRef_to_source.get(refin)
							if (old_element_source !== null && refin.source != old_element_source) {

								removeFromListIs_in_portconnection_system_source(old_element_source, refin)
								connectionRef_to_source.remove(refin)
							}
							putInIs_in_portconnection_system_source(refin.source, refin)
							connectionRef_to_source.put(refin, refin.source)

							val old_element = connectionRef_to_destination.get(refin)
							if (old_element !== null && refin.destination != old_element) {
								removeFromListIs_in_portconnection_system_destination(old_element, refin)
								connectionRef_to_destination.remove(refin)
							}
							putInIs_in_portconnection_system_destination(refin.destination, refin)
							connectionRef_to_destination.put(refin, refin.destination)

						}
					}
				}
				!matches
			].out('connection_out', INSTANCE.connectionInstance).undo [
				val connection_in = 'connection_in'.fetch as ConnectionInstance
				for (refin : connection_in.connectionReferences) {
					removeFromListIs_in_portconnection_system_source(refin.source, refin)
					connectionRef_to_source.remove(refin)
					removeFromListIs_in_portconnection_system_destination(refin.destination, refin)
					connectionRef_to_destination.remove(refin)

					val comp_list = getComponentList(refin)
					comp_list.forEach [ pair |
						insertDependency(pair.key, pair.value)
					]
				}
			],
			new Rule('conRef2conRef').isUniqueLazy.in('refin', INSTANCE.connectionReference).out('refout',
				INSTANCE.connectionReference, [
					val refin = 'refin'.fetch as ConnectionReference
					val refout = 'refout'.fetch as ConnectionReference
					refout.source = refin.source.fetch as ConnectionInstanceEnd
					refout.destination = refin.destination.fetch as ConnectionInstanceEnd
					refout.context = refin.context.fetch as ComponentInstance
				// refout.name = refin.name
				]),
			new Rule('feature2feature').in('feature_in', INSTANCE.featureInstance).filter [
				val feat_in = 'feature_in'.fetch as FeatureInstance
				var Boolean matches = false
				matches = (feat_in.category != FeatureCategory.DATA_PORT &&
					feat_in.category != FeatureCategory.EVENT_DATA_PORT &&
					feat_in.category != FeatureCategory.EVENT_PORT)
				if (matches)
					return matches
				else {
					val c_source = getListIs_in_portconnection_system_source(feat_in)
					if (!c_source.isEmpty)
						for (ConnectionReference cr : c_source) {
							matches = is_portconnection_device(cr.eContainer as ConnectionInstance)
							if(matches) return matches
						}
					else {
						val c_destination = getListIs_in_portconnection_system_destination(feat_in)
						if (!c_destination.isEmpty)
							for (ConnectionReference cr : c_destination) {
								matches = is_portconnection_device(cr.eContainer as ConnectionInstance)
								if(matches) return matches
							}
						else {
							val listRef = allInstances(INSTANCE.connectionReference) as List<ConnectionReference>
							matches = feat_in.srcConnectionInstances.isEmpty &&
								feat_in.dstConnectionInstances.isEmpty && !listRef.exists [ r |
									r.source == feat_in || r.destination == feat_in
								]
						}
					}
				}
				matches ?: false
			].out('feature_out', INSTANCE.featureInstance, [

				val feat_out = 'feature_out'.fetch as FeatureInstance
				val feat_in = 'feature_in'.fetch as FeatureInstance

				feat_out.category = feat_in.category
				feat_out.name = feat_in.name
				feat_out.direction = feat_in.direction
			]),
			new Rule('component2component').in('comp_in', INSTANCE.componentInstance).filter [
				val comp_in = 'comp_in'.fetch as ComponentInstance
				(comp_in.eClass.name == 'ComponentInstance')
			].out('comp_out', INSTANCE.componentInstance, [
				val comp_out = 'comp_out'.fetch as ComponentInstance
				val comp_in = 'comp_in'.fetch as ComponentInstance
				comp_out.category = comp_in.category
				comp_out.name = comp_in.name

				comp_out.componentInstances += (comp_in.componentInstances.fetch as List<ComponentInstance>)
				comp_out.featureInstances += (comp_in.featureInstances.fetch as List<FeatureInstance>)
			]),
			new Rule('thread2thread').inheritsFrom(#['component2component']).in('comp_in', INSTANCE.componentInstance).
				filter [
					val comp_in = 'comp_in'.fetch as ComponentInstance
					comp_in.category == ComponentCategory.THREAD

				].out('comp_out', INSTANCE.componentInstance, [
					val comp_out = 'comp_out'.fetch as ComponentInstance
					val comp_in = 'comp_in'.fetch as ComponentInstance
					val process = comp_in.owner as ComponentInstance
					for (f : comp_in.featureInstances) {
						val c_source = getListIs_in_portconnection_system_source(f)
						var boolean source_test = false
						var boolean destination_test = false
						if (!c_source.isEmpty)
							for (ConnectionReference cr : c_source) {
								if (is_portconnection_device(cr.eContainer as ConnectionInstance)) {
									source_test = true
									return
								}else
								insertComponent(cr, 'thread2thread', comp_in)
							}
						val c_destination = getListIs_in_portconnection_system_destination(f)
						if (!c_destination.isEmpty)
							for (ConnectionReference cr : c_destination) {
								if (is_portconnection_device(cr.eContainer as ConnectionInstance)) {
									destination_test = true
									return
								}else
								insertComponent(cr, 'thread2thread', comp_in)
							}

						if (!source_test && !destination_test) {
							if (!c_source.isEmpty || !c_destination.isEmpty) {
								comp_out.featureInstances += f.fetch('feat_out', 'feat2featplus') as FeatureInstance
							}
						}
						if (process.is_in_portconnection_process(f)) {
							comp_out.featureInstances += f.fetch('feat_out', 'feat2featplus') as FeatureInstance
						}
					}
				]),
			new Rule('process2process').inheritsFrom(#['component2component']).in('comp_in',
				INSTANCE.componentInstance).filter [
				val comp_in = 'comp_in'.fetch as ComponentInstance
				comp_in.category == ComponentCategory.PROCESS
			].out('comp_out', INSTANCE.componentInstance, [
				val comp_in = 'comp_in'.fetch as ComponentInstance
				val comp_out = 'comp_out'.fetch as ComponentInstance

				for (e : comp_in.componentInstances) {
					if (e.category == ComponentCategory.THREAD) {
						for (f : e.featureInstances) {
							val c_source = getListIs_in_portconnection_system_source(f)
							if (!c_source.isEmpty) {
								for (ConnectionReference cr : c_source) {
									if (!is_portconnection_device(cr.eContainer as ConnectionInstance)) {
										insertComponent(cr, 'process2process', comp_in)
										comp_out.componentInstances +=
											#{'featthread' -> f,
												'portconnection' -> cr.eContainer as ConnectionInstance}.fetch(
												'newdata', 'portconnection2data') as ComponentInstance
										comp_out.connectionInstances +=
											#{'featthread' -> f,
												'portconnection' -> cr.eContainer as ConnectionInstance}.fetch(
												'accessconnection',
												'portconnection2accessconnectionsource') as ConnectionInstance
									}
								}
							}

							val c_destination = getListIs_in_portconnection_system_destination(f)
							if (!c_destination.isEmpty) {
								for (ConnectionReference cr : c_destination) {
									if (!is_portconnection_device(cr.eContainer as ConnectionInstance)) {
										insertComponent(cr, 'process2process', comp_in)
										comp_out.componentInstances +=
											#{'featthread' -> f,
												'portconnection' -> cr.eContainer as ConnectionInstance}.fetch(
												'newdata', 'portconnection2data') as ComponentInstance
										comp_out.connectionInstances +=
											#{'featthread' -> f,
												'portconnection' -> cr.eContainer as ConnectionInstance}.fetch(
												'accessconnection',
												'portconnection2accessconnectiondestination') as ConnectionInstance
									}

								}
							}
						}
					}
				}
				for (e : comp_in.connectionInstances) {
					if ((e.kind == ConnectionKind.PORT_CONNECTION) && !is_portconnection_device(e)) {
						comp_out.componentInstances +=
							e.fetch('newdata', 'portconnection2dataprocess') as ComponentInstance
						comp_out.connectionInstances +=
							e.fetch('accessconnectionsource',
								'portconnection2accessconnectionatprocesssource') as ConnectionInstance
						comp_out.connectionInstances +=
							e.fetch('accessconnectiondestination',
								'portconnection2accessconnectionatprocessdestination') as ConnectionInstance
					}
				}
			]),
			new Rule('sys2sys').in('sys_in', INSTANCE.systemInstance).out('sys_out', INSTANCE.systemInstance, [
				val sys_out = 'sys_out'.fetch as SystemInstance
				val sys_in = 'sys_in'.fetch as SystemInstance
				sys_out.name = sys_in.name
				sys_out.featureInstances += (sys_in.featureInstances.fetch as List<FeatureInstance>)
				sys_out.componentInstances += (sys_in.componentInstances.fetch as List<ComponentInstance>)
				sys_out.category = sys_in.category
			]),
			/**
			 * UNIQUE LAZY new RuleS
			 */
			new Rule('portconnection2accessconnectionatprocesssource').uniqueLazy.in('portconnection',
				INSTANCE.connectionInstance).out('accessconnectionsource', INSTANCE.connectionInstance, [
				val accessconnection = 'accessconnectionsource'.fetch as ConnectionInstance
				val portconnection = 'portconnection'.fetch as ConnectionInstance

				accessconnection.name = portconnection.name
				accessconnection.kind = ConnectionKind.ACCESS_CONNECTION
				accessconnection.complete = true

				accessconnection.source = portconnection.source.fetch('feat_out',
					'feat2featplus') as ConnectionInstanceEnd
				accessconnection.destination = portconnection.fetch('newdata',
					'portconnection2dataprocess') as ConnectionInstanceEnd
				accessconnection.connectionReferences +=
					portconnection.fetch('newref', 'refconnection2refconnectionplussource') as ConnectionReference
			]),
			new Rule('portconnection2accessconnectionatprocessdestination').uniqueLazy.in('portconnection',
				INSTANCE.connectionInstance).out('accessconnectiondestination', INSTANCE.connectionInstance, [
				val accessconnection = 'accessconnectiondestination'.fetch as ConnectionInstance
				val portconnection = 'portconnection'.fetch as ConnectionInstance

				accessconnection.name = portconnection.name
				accessconnection.kind = ConnectionKind.ACCESS_CONNECTION
				accessconnection.complete = true

				accessconnection.destination = portconnection.destination.fetch('feat_out',
					'feat2featplus') as ConnectionInstanceEnd
				accessconnection.source = portconnection.fetch('newdata',
					'portconnection2dataprocess') as ConnectionInstanceEnd
				accessconnection.connectionReferences +=
					portconnection.fetch('newref', 'refconnection2refconnectionplusdestination') as ConnectionReference

			]),
			new Rule('refconnection2refconnectionplussource').uniqueLazy.in('portconnection',
				INSTANCE.connectionInstance).out('newref', INSTANCE.connectionReference, [
				val newref = 'newref'.fetch as ConnectionReference
				val portconnection = 'portconnection'.fetch as ConnectionInstance
				newref.context = (portconnection.owner).fetch as ComponentInstance
				newref.source = portconnection.source.fetch('feat_out', 'feat2featplus') as ConnectionInstanceEnd
				newref.destination = portconnection.fetch('newdata',
					'portconnection2dataprocess') as ConnectionInstanceEnd
			]),
			new Rule('refconnection2refconnectionplusdestination').uniqueLazy.in('portconnection',
				INSTANCE.connectionInstance).out('newref', INSTANCE.connectionReference, [
				val newref = 'newref'.fetch as ConnectionReference
				val portconnection = 'portconnection'.fetch as ConnectionInstance
				newref.context = (portconnection.owner).fetch as ComponentInstance

				newref.destination = portconnection.destination.fetch('feat_out',
					'feat2featplus') as ConnectionInstanceEnd
				newref.source = portconnection.fetch('newdata', 'portconnection2dataprocess') as ConnectionInstanceEnd
			]),
			new Rule('feat2featplus').uniqueLazy.in('feat_in', INSTANCE.featureInstance).out('feat_out',
				INSTANCE.featureInstance, [
					val feat_out = 'feat_out'.fetch as FeatureInstance
					val feat_in = 'feat_in'.fetch as FeatureInstance
					feat_out.category = FeatureCategory.DATA_ACCESS
					feat_out.name = feat_in.name
					feat_out.direction = feat_in.direction
				]),
			new Rule('portconnection2dataprocess').uniqueLazy.in('portconnection', INSTANCE.connectionInstance).out(
				'newdata', INSTANCE.componentInstance, [
					val newdata = 'newdata'.fetch as ComponentInstance
					val portconnection = 'portconnection'.fetch as ConnectionInstance
					newdata.name = portconnection.name
					newdata.category = ComponentCategory.DATA
				]),
			new Rule('portconnection2accessconnectionsource').uniqueLazy.in('featthread', INSTANCE.featureInstance).in(
				'portconnection', INSTANCE.connectionInstance).out('accessconnection', INSTANCE.connectionInstance, [
				val portconnection = 'portconnection'.fetch as ConnectionInstance
				val accessconnection = 'accessconnection'.fetch as ConnectionInstance
				val featthread = 'featthread'.fetch as FeatureInstance

				accessconnection.name = portconnection.name
				accessconnection.kind = ConnectionKind.ACCESS_CONNECTION
				accessconnection.complete = true

				accessconnection.destination = #{'featthread' -> featthread, 'portconnection' -> portconnection}.fetch(
					'newdata', 'portconnection2data') as ConnectionInstanceEnd
				// TODO change to  featthread vs portconnection.source
				accessconnection.source = featthread.fetch('feat_out', 'feat2featplus') as ConnectionInstanceEnd

				accessconnection.connectionReferences +=
					#{'featthread' -> featthread, 'portconnection' -> portconnection}.fetch('newref',
						'refconnection2refconnectionplussourcesystem') as ConnectionReference

			]),
			new Rule('portconnection2accessconnectiondestination').uniqueLazy.in('featthread',
				INSTANCE.featureInstance).in('portconnection', INSTANCE.connectionInstance).out('accessconnection',
				INSTANCE.connectionInstance, [
					val portconnection = 'portconnection'.fetch as ConnectionInstance
					val accessconnection = 'accessconnection'.fetch as ConnectionInstance

					val featthread = 'featthread'.fetch as FeatureInstance

					accessconnection.name = portconnection.name
					accessconnection.kind = ConnectionKind.ACCESS_CONNECTION
					accessconnection.complete = true

					accessconnection.source = #{'featthread' -> featthread, 'portconnection' -> portconnection}.fetch(
						'newdata', 'portconnection2data') as ConnectionInstanceEnd
					// TODO change to  featthread vs portconnection.source
					accessconnection.destination = featthread.fetch('feat_out',
						'feat2featplus') as ConnectionInstanceEnd

					accessconnection.connectionReferences +=
						#{'featthread' -> featthread, 'portconnection' -> portconnection}.fetch('newref',
							'refconnection2refconnectionplusdestinationsystem') as ConnectionReference
				]),
			new Rule('portconnection2data').uniqueLazy.in('featthread', INSTANCE.featureInstance).in('portconnection',
				INSTANCE.connectionInstance).out('newdata', INSTANCE.componentInstance, [
				val newdata = 'newdata'.fetch as ComponentInstance
				val portconnection = 'portconnection'.fetch as ConnectionInstance

				newdata.name = portconnection.name
				newdata.category = ComponentCategory.DATA
			]),
			new Rule('refconnection2refconnectionplussourcesystem').uniqueLazy.in('featthread',
				INSTANCE.featureInstance).in('portconnection', INSTANCE.connectionInstance).out('newref',
				INSTANCE.connectionReference, [
					val newref = 'newref'.fetch as ConnectionReference
					val portconnection = 'portconnection'.fetch as ConnectionInstance
					val featthread = 'featthread'.fetch as FeatureInstance

					newref.context = (featthread.owner.owner).fetch as ComponentInstance

					newref.destination = #{'featthread' -> featthread, 'portconnection' -> portconnection}.fetch(
						'newdata', 'portconnection2data') as ComponentInstance

					newref.source = featthread.fetch('feat_out', 'feat2featplus') as ConnectionInstanceEnd

				]),
			new Rule('refconnection2refconnectionplusdestinationsystem').uniqueLazy.in('featthread',
				INSTANCE.featureInstance).in('portconnection', INSTANCE.connectionInstance).out('newref',
				INSTANCE.connectionReference, [
					val newref = 'newref'.fetch as ConnectionReference
					val portconnection = 'portconnection'.fetch as ConnectionInstance
					val featthread = 'featthread'.fetch as FeatureInstance

					newref.context = (featthread.owner.owner).fetch as ComponentInstance

					newref.destination = featthread.fetch('feat_out', 'feat2featplus') as ConnectionInstanceEnd
					newref.source = #{'featthread' -> featthread, 'portconnection' -> portconnection}.fetch('newdata',
						'portconnection2data') as ComponentInstance
				])
		])

	}

	/**
	 * HELPERS
	 */
	def is_portconnection_device(ConnectionInstance c) {
		c.connectionReferences.exists [ r |
			r.source.getOwner.is_device || r.destination.getOwner.is_device
		]
	}

	def Boolean is_device(Element elem) {
		switch (elem) {
			ComponentPrototypeActual: elem.category == ComponentCategory.DEVICE
			ComponentTypeRename: elem.category == ComponentCategory.DEVICE
			ComponentInstance: elem.category == ComponentCategory.DEVICE
			default: false
		}
	}

	def Boolean is_in_portconnection_process(ComponentInstance c, FeatureInstance f) {
		c.connectionInstances.exists[cc|cc.source == f] || c.connectionInstances.exists[cc|cc.destination == f]
	}

//	val ListMultimap<Element, ConnectionReference> is_in_portconnection_system_source = ArrayListMultimap.create()
//	val ListMultimap<Element, ConnectionReference> is_in_portconnection_system_destination = ArrayListMultimap.create()
	val Map<Element, List<ConnectionReference>> is_in_portconnection_system_source = newHashMap
	val Map<Element, List<ConnectionReference>> is_in_portconnection_system_destination = newHashMap
	val Map<ConnectionReference, Element> connectionRef_to_source = newHashMap
	val Map<ConnectionReference, Element> connectionRef_to_destination = newHashMap
	val Map<ConnectionReference, List<Pair<String, ComponentInstance>>> connectionToComponentList = newHashMap

	def getComponentList(ConnectionReference conn) {
		val list = connectionToComponentList.get(conn)
		list ?: newArrayList
	}

	def insertComponent(ConnectionReference conn, String ruleName, ComponentInstance comp) {
		var list = connectionToComponentList.get(conn)
		if (list === null) {
			list = newArrayList(ruleName -> comp)
			connectionToComponentList.put(conn, list)
		} else {
			list.add(ruleName -> comp)
		}
	}

	def putInIs_in_portconnection_system_source(Element elt, ConnectionReference conn) {
		var list = is_in_portconnection_system_source.get(elt)
		if (list === null) {
			list = newArrayList(conn)
			is_in_portconnection_system_source.put(elt, list)
		} else {
			list.add(conn)
		}
	}

	def getListIs_in_portconnection_system_source(Element elt) {
		val list = is_in_portconnection_system_source.get(elt)
		list ?: newArrayList
	}

	def removeFromListIs_in_portconnection_system_source(Element elt, ConnectionReference conn) {
		val list = is_in_portconnection_system_source.get(elt)
		list.removeAll(conn)
	}

	def putInIs_in_portconnection_system_destination(Element elt, ConnectionReference conn) {
		var list = is_in_portconnection_system_destination.get(elt)
		if (list === null) {
			list = newArrayList(conn)
			is_in_portconnection_system_destination.put(elt, list)
		} else {
			list.add(conn)
		}
	}

	def getListIs_in_portconnection_system_destination(Element elt) {
		val list = is_in_portconnection_system_destination.get(elt)
		list ?: newArrayList
	}

	def removeFromListIs_in_portconnection_system_destination(Element elt, ConnectionReference conn) {
		val list = is_in_portconnection_system_destination.get(elt)
		list.removeAll(conn)
	}

}
