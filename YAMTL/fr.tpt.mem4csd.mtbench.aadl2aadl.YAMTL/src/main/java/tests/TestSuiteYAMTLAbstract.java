package tests;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.TestSuiteAbstract;

public abstract class TestSuiteYAMTLAbstract extends TestSuiteAbstract {

	protected static void runAllTests(final String[] args, final TestSuiteYAMTLAbstract testSuite) {
		for (String arg : args) {
			System.out.println("arguments: " + arg);
		}

		try {
			if (args.length > 0 && args[0].equals("--batch"))
				testSuite.testBatch();
			else {
				testSuite.testAddition();
				testSuite.testUpdateAttribute();
				testSuite.testUpdateReference();
				testSuite.testDeletion();
			}

			System.exit(0);
		} catch (final Throwable ex) {
			ex.printStackTrace();

			System.exit(-1);
		}
	}

}
