package tests;

import java.util.Map;

import org.eclipse.emf.ecore.resource.Resource;

import aadl2aadl.AbstractTransformation;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.AbstractScenario;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.ResourcesUtil;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.Result;

public abstract class AbstractScenarioBatch extends AbstractScenario {

	protected AbstractScenarioBatch() {
		setup();
	}

	protected long[] scenarioSteps(int runtimes, AbstractTransformation xform) {
		long t0, t1;
		long[] runs = new long[runtimes];
		for (int i = 0; i < runtimes; i++) {
			t0 = System.nanoTime();
			xform.execute();
			t1 = System.nanoTime();
			runs[i] = t1 - t0;

		}
		return runs;
	}

	public abstract String getExtension();

	public abstract AbstractTransformation getSpecification();

	@Override
	public Resource executeScenario(int testNumber, Result res, String inputModelName) throws Exception {

		long[] runsOfUp = new long[res.getRuntimes()];

		final String inputModelPath = ResourcesUtil.createAadlInstanceInputURI(inputModelName).toFileString();
		final String outputModelPath = ResourcesUtil.createAadlInstanceOutputURI(inputModelName + getExtension())
				.toFileString();

		System.out.println("Test " + testNumber + "" + outputModelPath + " Performing transformation:");

		long t0, t1;
		t0 = System.nanoTime();
		AbstractTransformation xform = getSpecification();
		xform.fromRoots = false;
		xform.loadInputModels(Map.of("in", inputModelPath));
		t1 = System.nanoTime();

		System.out.println("Test " + testNumber + " Initialization in " + Long.toString(t1 - t0));

		runsOfUp = scenarioSteps(res.getRuntimes(), xform);

		res.setExecutionTimes(inputModelName, testNumber, res.getMedian(runsOfUp));

		System.out.println("Test " + testNumber + " in " + res.getMedian(runsOfUp) + " ns");

		xform.saveOutputModels(Map.of("out", outputModelPath));

		System.out.println("Transformation finished.");

		return xform.getModelResource("out");
	}

}
