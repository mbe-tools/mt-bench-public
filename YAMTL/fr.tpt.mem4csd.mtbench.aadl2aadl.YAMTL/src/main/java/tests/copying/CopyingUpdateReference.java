package tests.copying;

import aadl2aadl.AbstractTransformation;
import aadl2aadl.Copying;
import tests.AbstractScenarioSync;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class CopyingUpdateReference extends AbstractScenarioSync {

	public CopyingUpdateReference() {
	}

	@Override
	public ScenarioKind getScenario() {
		return ScenarioKind.UPDATE_REF;
	}

	@Override
	public String getExtension() {
		return BenchmarkConstants.COPY_UPDATE_REF_SUFFIX;
	}

	@Override
	public AbstractTransformation getSpecification() {
		return new Copying();
	}
}
