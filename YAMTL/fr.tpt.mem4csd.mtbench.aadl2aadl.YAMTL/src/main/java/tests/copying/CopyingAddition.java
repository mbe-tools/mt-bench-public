package tests.copying;

import aadl2aadl.AbstractTransformation;
import aadl2aadl.Copying;
import tests.AbstractScenarioSync;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;


public class CopyingAddition extends AbstractScenarioSync {

	public CopyingAddition() {
		super();
	}

	@Override
	public ScenarioKind getScenario() {
		return ScenarioKind.ADDITION;
	}

	@Override
	public String getExtension() {
		return BenchmarkConstants.COPY_ADD_SUFFIX;
	}

	@Override
	public AbstractTransformation getSpecification() {
		return new Copying();
	}

}