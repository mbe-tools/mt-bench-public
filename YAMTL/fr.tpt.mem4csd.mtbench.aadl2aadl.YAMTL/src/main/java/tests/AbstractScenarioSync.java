package tests;

import java.util.Map;

import org.eclipse.emf.ecore.resource.Resource;

import aadl2aadl.AbstractTransformation;
import yamtl.core.YAMTLModule.ExecutionMode;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.AbstractScenario;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.ModelModif;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.ResourcesUtil;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.Result;
import yamtl.core.YAMTLModule.ExtentTypeModifier;

public abstract class AbstractScenarioSync extends AbstractScenario {

	protected AbstractScenarioSync() {
		setup();
	}

	protected long[] scenarioSteps(ScenarioKind scenarioKind, ModelModif modif, int runtimes,
			AbstractTransformation xform) {
		long t0, t1;
		long[] runs = new long[runtimes];

		xform.execute();

		for (int i = 0; i < runtimes; i++) {
			Resource a = xform.getModelResource("in");
			modif.applyModif(scenarioKind, a);
			t0 = System.nanoTime();
			xform.propagateDelta("in");
			t1 = System.nanoTime();
			runs[i] = t1 - t0;

		}
		return runs;
	}

	public abstract ScenarioKind getScenario();

	public abstract String getExtension();

	public abstract AbstractTransformation getSpecification();

	@Override
	public Resource executeScenario(int testNumber, Result res, String inputModelName) throws Exception {

		ModelModif modif = new ModelModif();
		long[] runsOfUp = new long[res.getRuntimes()];

		final String inputModelPath = ResourcesUtil.createAadlInstanceInputURI(inputModelName).toFileString();
		final String outputModelPath = ResourcesUtil.createAadlInstanceOutputURI(inputModelName + getExtension())
				.toFileString();

		System.out.println("Test " + testNumber + " " + outputModelPath + " Performing transformation:");

		long t0, t1;
		t0 = System.nanoTime();

		AbstractTransformation xform = getSpecification();

		xform.fromRoots = false;
		xform.extentTypeModifier = ExtentTypeModifier.LIST;
		xform.executionMode = ExecutionMode.INCREMENTAL;
		xform.initLocationsWhenLoading = false;

		xform.loadInputModels(Map.of("in", inputModelPath));
		xform.adaptInputModel("in");
		t1 = System.nanoTime();
		System.out.println("Test " + testNumber + " Initialization in " + Long.toString(t1 - t0));

		runsOfUp = scenarioSteps(getScenario(), modif, res.getRuntimes(), xform);

		res.setExecutionTimes(inputModelName, testNumber, res.getMedian(runsOfUp));

		System.out.println("Test " + testNumber + " in " + res.getMedian(runsOfUp) + " ns");

		xform.saveOutputModels(Map.of("out", outputModelPath));

		System.out.println("Transformation finished.");

		return xform.getModelResource("out");
	}

}
