package tests.refinement;

import aadl2aadl.Aadl2Aadl;
import aadl2aadl.AbstractTransformation;
import tests.AbstractScenarioSync;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class RefinementAddition extends AbstractScenarioSync {

	public RefinementAddition() {
	}

	@Override
	public ScenarioKind getScenario() {
		return ScenarioKind.ADDITION;
	}

	@Override
	public String getExtension() {
		return BenchmarkConstants.REFINEMENT_ADD_SUFFIX;
	}

	@Override
	public AbstractTransformation getSpecification() {
		return new Aadl2Aadl();
	}

}
