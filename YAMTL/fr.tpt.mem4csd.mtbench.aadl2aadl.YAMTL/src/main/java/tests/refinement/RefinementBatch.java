package tests.refinement;

import aadl2aadl.Aadl2Aadl;
import aadl2aadl.AbstractTransformation;
import tests.AbstractScenarioBatch;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;

public class RefinementBatch extends AbstractScenarioBatch {

	public RefinementBatch() {
	}

	@Override
	public AbstractTransformation getSpecification() {
		return new Aadl2Aadl();
	}

	@Override
	public String getExtension() {
		return BenchmarkConstants.REFINEMENT_BATCH_SUFFIX;
	}
}
