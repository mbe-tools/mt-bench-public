package tests.refinement;

import aadl2aadl.Aadl2Aadl;
import aadl2aadl.AbstractTransformation;
import tests.AbstractScenarioSync;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class RefinementUpdateReference extends AbstractScenarioSync {

	public RefinementUpdateReference() {
	}

	@Override
	public ScenarioKind getScenario() {
		return ScenarioKind.UPDATE_REF;
	}

	@Override
	public String getExtension() {
		return BenchmarkConstants.REFINEMENT_UPDATE_REF_SUFFIX;
	}

	@Override
	public AbstractTransformation getSpecification() {
		return new Aadl2Aadl();
	}
}
