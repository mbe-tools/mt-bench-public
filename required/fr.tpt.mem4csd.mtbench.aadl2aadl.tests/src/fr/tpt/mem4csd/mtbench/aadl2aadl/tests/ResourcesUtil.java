package fr.tpt.mem4csd.mtbench.aadl2aadl.tests;

import java.io.File;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.modelsupport.FileNameConstants;

import fr.tpt.mem4csd.utils.osate.standalone.OsateStandaloneSetup;

public class ResourcesUtil {

	private ResourcesUtil() {
	}

	private static final URI BASE_ABS_URI = URI.createFileURI(new File(".").getAbsolutePath());

	private static final String BASE_RESOURCES_AADL_MODELS = "../../required/fr.tpt.mem4csd.mtbench.aadl2aadl.tests/resources/";

	private static final String BASE_RESOURCES_OUTPUT_MODELS = "resources/outputs/";

	public static ResourceSet createResourceSet() {
		return new OsateStandaloneSetup( BASE_ABS_URI ).createResourceSet();
	}

	public static Resource loadAadlInstanceInputResource(	final String modelName,
															final ResourceSet resSet ) {
		final URI modelUri = createAadlInstanceInputURI( modelName );

		return resSet.getResource(modelUri, true);
	}

	public static SystemInstance getExpectedSystemInstance(	final String modelName,
															final ResourceSet resSet ) {
		final URI modelUri = createAadlInstanceURI( BASE_RESOURCES_AADL_MODELS + "expected/" + modelName);

		final Resource res = resSet.getResource(modelUri, true);

		return (SystemInstance) res.getContents().get(0);
	}

	public static URI createAadlInstanceInputURI( final String modelName ) {
		return createAadlInstanceURI( BASE_RESOURCES_AADL_MODELS + "inputs/instances/" + modelName );
	}

	public static URI createAadlInstanceOutputURI( final String modelName ) {
		return createAadlInstanceURI( BASE_RESOURCES_OUTPUT_MODELS + modelName );
	}

	public static URI createOutputURI( 	final String modelName,
										final String fileExtension ) {
		return createURI( BASE_RESOURCES_OUTPUT_MODELS + modelName, fileExtension );
	}

	public static URI createAadlInstanceURI( final String modelName ) {
		return createURI( modelName, FileNameConstants.INSTANCE_FILE_EXT );
	}
	
	public static URI createURI( 	final String modelName,
									final String fileExtension ) {
		final URI uri = URI.createFileURI( modelName ).resolve( BASE_ABS_URI );
		
		return fileExtension == null ? uri : uri.appendFileExtension(fileExtension);
	}
}
