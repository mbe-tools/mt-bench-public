package fr.tpt.mem4csd.mtbench.aadl2aadl.tests;

public interface BenchmarkConstants {

	enum ScenarioKind {
		BATCH, ADDITION, UPDATE_ATT, UPDATE_REF, DELETE
	};

	enum SpecificationKind {
		COPYING, REFINEMENT
	};

	String[] TEST_CASES = { "1_PC_Simple_Native_Instance", "2_PC_Simple_Native_Instance", "4_PC_Simple_Native_Instance",
			"8_PC_Simple_Native_Instance", "16_PC_Simple_Native_Instance", "32_PC_Simple_Native_Instance",
			"64_PC_Simple_Native_Instance", "128_PC_Simple_Native_Instance", "256_PC_Simple_Native_Instance",
			"512_PC_Simple_Native_Instance", "1024_PC_Simple_Native_Instance", "topology_test_i_Instance",
			"hierarchy1_PC_Complete_level1_Instance", "hierarchy2_Top_level2_Instance",
			"hierarchy3_TopTop_level3_Instance" };

	String COPY_ADD_SUFFIX = ".copy.add";
	String COPY_BATCH_SUFFIX = ".copy.batch";
	String COPY_DELETION_SUFFIX = ".copy.delete";
	String COPY_UPDATE_ATT_SUFFIX = ".copy.update";
	String COPY_UPDATE_REF_SUFFIX = ".copy.updateref";

	String REFINEMENT_ADD_SUFFIX = ".refine.add";
	String REFINEMENT_BATCH_SUFFIX = ".refine.batch";
	String REFINEMENT_DELETION_SUFFIX = ".refine.delete";
	String REFINEMENT_UPDATE_ATT_SUFFIX = ".refine.update";
	String REFINEMENT_UPDATE_REF_SUFFIX = ".refine.updateref";
}
