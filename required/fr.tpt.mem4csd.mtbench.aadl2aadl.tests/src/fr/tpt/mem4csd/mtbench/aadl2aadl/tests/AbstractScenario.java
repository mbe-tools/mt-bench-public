package fr.tpt.mem4csd.mtbench.aadl2aadl.tests;

import org.eclipse.emf.ecore.resource.Resource;

public abstract class AbstractScenario {

	protected AbstractScenario() {
		setup();
	}

	public abstract Resource executeScenario(	int testNumber, 
												Result res, 
												String inputModelName )
	throws Exception;

	protected void setup() {
	}
}
