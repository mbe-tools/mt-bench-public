package fr.tpt.mem4csd.mtbench.aadl2aadl.tests;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.UsageCrossReferencer;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.SystemInstance;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

import org.osate.aadl2.instance.ConnectionKind;

public class ModelModif {

	public ModelModif() {
	}

	public void applyModif(final ScenarioKind scenarioKind, Resource inputModel) {
		switch (scenarioKind) {
		case ADDITION:
			addConnectionInstance(inputModel);
			break;
		case UPDATE_ATT:
			updateConnectionInstanceName(inputModel);
			break;
		case UPDATE_REF:
			updateConnectionInstanceSource(inputModel);
			break;
		case DELETE:
			deleteConnectionInstance(inputModel);
			break;
		case BATCH:
			break;
		default:
			throw new IllegalArgumentException("Unknown scenario kind: '" + scenarioKind + "'.");
		}
	}

	/**
	 * Update the source of the first connection at the system level
	 * 
	 * @param inputModel
	 */
	public void updateConnectionInstanceSource(Resource inputModel) {
		SystemInstance systemRoot = (SystemInstance) inputModel.getContents().get(0);

		ListIterator<ConnectionInstance> allConnections = systemRoot.getConnectionInstances().listIterator();

		while (allConnections.hasNext()) {
			ConnectionInstance firstConnection = allConnections.next();
			if (firstConnection.getKind() == ConnectionKind.PORT_CONNECTION) {

				for (Iterator<ConnectionReference> i = firstConnection.getConnectionReferences().iterator(); i
						.hasNext();) {
					ConnectionReference ref = (ConnectionReference) i.next();
					if ((ref.getSource().getName().equals("Data_Source"))
							&& (ref.getContext().getCategory() == ComponentCategory.PROCESS)) {
						ComponentInstance thread = (ComponentInstance) ref.getSource().getOwner();
						for (Iterator<FeatureInstance> j = thread.getAllFeatureInstances().iterator(); j.hasNext();) {

							FeatureInstance feat = (FeatureInstance) j.next();
							if (feat.getName().equals("Extra_Data_Source")) {
								ref.setSource(feat);
								firstConnection.setSource(feat);
								break;
							}
						}
					}
				}
				break;
			}
			// save the updated connection
//		try {
//			inputModel.save(null);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

		}
	}

	/**
	 * Update the name of the first connection at the system level
	 * 
	 * @param inputModel
	 */
	public void updateConnectionInstanceName(Resource inputModel) {
		SystemInstance systemRoot = (SystemInstance) inputModel.getContents().get(0);
		EList<ConnectionInstance> allConnections = systemRoot.getConnectionInstances();
		if (!allConnections.isEmpty()) {
			ConnectionInstance firstConnection = allConnections.get(0);

			firstConnection.setName("updatedConnection");
		}
		// save the updated connection
//		try {
//			inputModel.save(null);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

	}

	/**
	 * Copy oldFeat feature
	 * 
	 * @param oldFeat
	 * @return addedFeat
	 */
	FeatureInstance addFeature(FeatureInstance oldFeat) {
		FeatureInstance addedFeat = oldFeat.getContainingComponentInstance().createFeatureInstance();
		addedFeat.setCategory(oldFeat.getCategory());
		addedFeat.setDirection(oldFeat.getDirection());
		addedFeat.setName("addedFeat_" + oldFeat.getName());
		return addedFeat;
	}

	/**
	 * Copy the first connection at the system level
	 * 
	 * @param inputModel
	 */
	public void addConnectionInstance(Resource inputModel) {
		SystemInstance systemRoot = (SystemInstance) inputModel.getContents().get(0);

		ListIterator<ConnectionInstance> allConnections = systemRoot.getConnectionInstances().listIterator();

		while (allConnections.hasNext()) {
			ConnectionInstance firstConnection = allConnections.next();

			if (firstConnection.getKind() == ConnectionKind.PORT_CONNECTION) {

				ConnectionInstance newConnection = firstConnection.getContainingComponentInstance()
						.createConnectionInstance();

				newConnection.setKind(firstConnection.getKind());

				ListIterator<ConnectionReference> it = firstConnection.getConnectionReferences().listIterator();
				List<FeatureInstance> featurelist = new ArrayList<FeatureInstance>();
				List<FeatureInstance> addedfeaturelist = new ArrayList<FeatureInstance>();

				while (it.hasNext()) {
					ConnectionReference ref = it.next();

					if (!featurelist.contains(ref.getSource())) {
						featurelist.add((FeatureInstance) ref.getSource());
						addedfeaturelist.add(addFeature((FeatureInstance) ref.getSource()));
					}
					if (!featurelist.contains(ref.getDestination())) {
						featurelist.add((FeatureInstance) ref.getDestination());
						addedfeaturelist.add(addFeature((FeatureInstance) ref.getDestination()));
					}
				}

				newConnection.setSource(addedfeaturelist.get(featurelist.indexOf(firstConnection.getSource())));
				newConnection
						.setDestination(addedfeaturelist.get(featurelist.indexOf(firstConnection.getDestination())));

				newConnection.setName("addedConnection_" + newConnection.getSource().getName() + "_"
						+ newConnection.getDestination().getName());
				newConnection.setBidirectional(false);
				newConnection.setComplete(true);

				ListIterator<ConnectionReference> references = firstConnection.getConnectionReferences().listIterator();

				while (references.hasNext()) {
					ConnectionReference reference = references.next();
					ConnectionReference newref = newConnection.createConnectionReference();
					newref.setContext(reference.getContext());

					newref.setSource(addedfeaturelist.get(featurelist.indexOf(reference.getSource())));
					newref.setDestination(addedfeaturelist.get(featurelist.indexOf(reference.getDestination())));

				}

				// save the added connection
//			try {
//				inputModel.save(null);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
				break;
			}
		}

	}

	/**
	 * Delete the first connection at the system level
	 * 
	 * @param inputModel
	 */
	public void deleteConnectionInstance(Resource inputModel) {
		SystemInstance systemRoot = (SystemInstance) inputModel.getContents().get(0);

		EList<ConnectionInstance> allConnections = systemRoot.getConnectionInstances();
		if (!allConnections.isEmpty()) {
			ConnectionInstance firstConnection = allConnections.get(0);

			delete(firstConnection, true);

		}
		// save
//		try {
//			inputModel.save(null);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	public void delete(EObject eObject, boolean recursive) {
		if (recursive) {
			EObject rootEObject = EcoreUtil.getRootContainer(eObject);

			Set<EObject> eObjects = new HashSet<EObject>();
			Set<EObject> crossResourceEObjects = new HashSet<EObject>();
			eObjects.add(eObject);
			for (@SuppressWarnings("unchecked")
			TreeIterator<InternalEObject> j = (TreeIterator<InternalEObject>) (TreeIterator<?>) eObject
					.eAllContents(); j.hasNext();) {
				InternalEObject childEObject = j.next();
				if (childEObject.eDirectResource() != null) {
					crossResourceEObjects.add(childEObject);
					j.prune();
				} else {
					eObjects.add(childEObject);
				}
			}

			Map<EObject, Collection<EStructuralFeature.Setting>> usages;

			usages = UsageCrossReferencer.findAll(eObjects, rootEObject);

			for (Map.Entry<EObject, Collection<EStructuralFeature.Setting>> entry : usages.entrySet()) {
				EObject deletedEObject = entry.getKey();
				Collection<EStructuralFeature.Setting> settings = entry.getValue();
				for (EStructuralFeature.Setting setting : settings) {
					if (!eObjects.contains(setting.getEObject()) && setting.getEStructuralFeature().isChangeable()) {
						EcoreUtil.remove(setting, deletedEObject);
					}
				}
			}

			EcoreUtil.remove(eObject);

			for (EObject crossResourceEObject : crossResourceEObjects) {
				EcoreUtil.remove(crossResourceEObject.eContainer(), crossResourceEObject.eContainmentFeature(),
						crossResourceEObject);
			}
		} else {
			EcoreUtil.delete(eObject);
		}
	}

}