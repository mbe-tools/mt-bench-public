package fr.tpt.mem4csd.mtbench.aadl2aadl.tests;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.emf.ecore.resource.Resource;

public class Result {

	String testName;

	int runtimes; // number of runs
	int testSize; // number of AADL models

	double[] executionTimes; // medians of execution times
	long[] modelSizes; // number of port connection at system level
	String[] modelsNames;
	Resource[] outputInstanceModels;

	public Result(String testName, int testsize, int runtimes) {
		this.testName = testName;
		this.testSize = testsize;
		this.runtimes = runtimes;
		executionTimes = new double[testSize];
		modelSizes = new long[testSize];
		modelsNames = new String[testSize];
		outputInstanceModels = new Resource[testSize];

	}

	public long getMedian(long[] runs) {
		Arrays.sort(runs);
		return runs[runtimes / 2];
	}

	/**
	 * Create an excel file from the executionTimes table
	 * 
	 * @param testtime
	 */
	@SuppressWarnings("resource")
	public void testExcel(long testtime) {
		// Blank workbook
		XSSFWorkbook workbook = new XSSFWorkbook();

		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet("Data");

		Map<String, Object[]> data = new TreeMap<String, Object[]>();
		data.put("1", new Object[] { "File", "Number of port connections", "Execution Time (s)" });

		for (int i = 0; i < testSize; i++) {
			data.put(Integer.toString(i + 2),
					new Object[] { modelsNames[i], Long.toString(modelSizes[i]), Double.toString(executionTimes[i]) });
		}

		Set<String> keyset = data.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Integer)
					cell.setCellValue((Integer) obj);
			}
		}
		try {
			// Write the workbook in file system
			FileOutputStream out = new FileOutputStream(new File(testName + "_" + testtime + ".xlsx"));
			workbook.write(out);
			out.close();
			System.out.println(testName + ".xlsx written successfully on disk in " + testtime + " ms");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public double[] getExecutionTimes() {
		return executionTimes;
	}

	public void setExecutionTimes(String inputModelName, int i, long executionTime) {
		this.executionTimes[i - 1] = (double) executionTime / 1_000_000_000;
		this.modelsNames[i - 1] = inputModelName;
	}

	public Resource getOutputInstanceModels(int index) {
		return outputInstanceModels[index];
	}

	public void setOutputInstanceModels(int index, Resource outputInstanceModel) {
		this.outputInstanceModels[index] = outputInstanceModel;
	}

	public long[] getModelSizes() {
		return modelSizes;
	}

	public void setModelSizes(long[] modelSizes) {
		this.modelSizes = modelSizes;
	}

	public int getRuntimes() {
		return runtimes;
	}

	public void setRuntimes(int runtimes) {
		this.runtimes = runtimes;
	}

	public int getTestSize( ) {
		return this.testSize;
	}
	public void setTestName(String extension) {
		this.testName = this.testName +"-"+extension;
	}
	public String getTestName() {
		return testName;
	}	
}