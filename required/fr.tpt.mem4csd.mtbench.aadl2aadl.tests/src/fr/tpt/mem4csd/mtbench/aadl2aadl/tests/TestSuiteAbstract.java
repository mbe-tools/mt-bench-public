package fr.tpt.mem4csd.mtbench.aadl2aadl.tests;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.resource.Resource;
import org.junit.Test;
import org.osate.aadl2.instance.SystemInstance;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import fr.tpt.mem4csd.utils.compare.IComparisonReport;
import fr.tpt.mem4csd.utils.compare.aadl.DefaultComparatorAADL;
import fr.tpt.mem4csd.utils.compare.emf.IComparatorEMF;

public abstract class TestSuiteAbstract {

	final boolean withComparisontmp = false;
	protected static boolean jUnitRunning = isJUnitRunning();

	protected static void runAllTests(final String[] args, final TestSuiteAbstract testSuite) {

		try {
//			testSuite.testBatch();
//			testSuite.testAddition();
//			testSuite.testUpdateAttribute();
			testSuite.testUpdateReference();
//			testSuite.testDeletion();

			System.exit(0);

		} catch (final Throwable ex) {
			ex.printStackTrace();

			System.exit(-1);
		}
	}

	private final IComparatorEMF comparator;

	protected TestSuiteAbstract() {
		comparator = createModelComparator();
	}

	protected abstract String getTestSuiteName();

	protected abstract AbstractScenario getScenario(ScenarioKind scenarioKind);

	protected void executeScenario(final ScenarioKind scenarioKind, final boolean withComparison) throws Throwable {
		Result res = new Result(getTestSuiteName(), 15, 1);
		long[] sizes = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 3, 1, 1, 1 };
		res.setModelSizes(sizes);
		res.setTestName(scenarioKind.name());

		long t0 = System.currentTimeMillis();

		final AbstractScenario scenario = getScenario(scenarioKind);

		System.out.println("Start Execution " + res.getTestName());

		for (int testNum = 0; testNum < res.getTestSize(); testNum++) {
			res.setOutputInstanceModels(testNum,
					scenario.executeScenario(testNum + 1, res, BenchmarkConstants.TEST_CASES[testNum]));

			if (withComparison) {
				checkResults(res.getOutputInstanceModels(testNum));
			}
		}
		long t1 = System.currentTimeMillis();
		res.testExcel(t1 - t0);
	}

	@Test
	public void testAddition() throws Throwable {
		executeScenario(ScenarioKind.ADDITION, withComparisontmp);
	}

	@Test
	public void testUpdateAttribute() throws Throwable {
		executeScenario(ScenarioKind.UPDATE_ATT, withComparisontmp);
	}

	@Test
	public void testUpdateReference() throws Throwable {
		executeScenario(ScenarioKind.UPDATE_REF, withComparisontmp);
	}

	@Test
	public void testDeletion() throws Throwable {
		executeScenario(ScenarioKind.DELETE, withComparisontmp);
	}

	@Test
	public void testBatch() throws Throwable {
		executeScenario(ScenarioKind.BATCH, withComparisontmp);
	}

	protected void checkResults(final SystemInstance outputModel) throws IOException {
		checkResults(outputModel.eResource());
	}

	protected void checkResults(final Resource outputModelRes) throws IOException {
		System.out.println(
				"Comparing transformation result with input file '" + outputModelRes.getURI().lastSegment() + "'...");

		final URI expModelUri = outputModelRes.getURI().trimFileExtension();

		try {
			final SystemInstance expectedModel = ResourcesUtil.getExpectedSystemInstance(expModelUri.lastSegment(),
					outputModelRes.getResourceSet());
			final Resource expectedModelResource = expectedModel.eResource();
			final IComparisonReport report = comparator.compare(expectedModelResource, outputModelRes);

			if (report.containsDiff()) {
				// report.print();

				final StringBuilder message = new StringBuilder("Fail: Result model '");
				message.append(outputModelRes);
				message.append("' is different from expected model '");
				message.append(expectedModelResource.getURI());
				message.append("'.");
				message.append(System.lineSeparator());
				message.append(report.getMessage());

				writeActualOutputResults(outputModelRes);

				jUnitFail(message.toString());
			} else {
				System.out.println("Identical");
			}
		} catch (final WrappedException ex) {
			ex.printStackTrace();
			final StringBuilder message = new StringBuilder("Fail: Expected result model '");
			message.append(expModelUri.lastSegment());
			message.append("' could not be loaded: ");
			message.append(ex.getLocalizedMessage());

			writeActualOutputResults(outputModelRes);

			jUnitFail(message.toString());
		}
	}

	protected static void jUnitFail(final String message) {
		System.err.println(message);

		if (jUnitRunning) {
			fail(message);
		}
	}

	private static boolean isJUnitRunning() {
		for (final StackTraceElement element : Thread.currentThread().getStackTrace()) {
			if (element.getClassName().startsWith("org.junit.")) {
				return true;
			}
		}

		return false;
	}

	private static void writeActualOutputResults(final Resource outputModelRes) throws IOException {
		outputModelRes.save(null);
	}

	protected IComparatorEMF createModelComparator() {
		return new DefaultComparatorAADL(null, false);
	}
}
