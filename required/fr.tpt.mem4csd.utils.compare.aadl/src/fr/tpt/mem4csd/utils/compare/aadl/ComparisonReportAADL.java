package fr.tpt.mem4csd.utils.compare.aadl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.EcoreUtil2;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyValue;
import org.osate.aadl2.instance.provider.InstanceItemProviderAdapterFactory;
import org.osate.aadl2.provider.Aadl2ItemProviderAdapterFactory;

import fr.tpt.mem4csd.utils.compare.emf.ComparisonReportEMF;

public class ComparisonReportAADL extends ComparisonReportEMF {

	public ComparisonReportAADL(	final Resource p_leftResource,
									final Resource p_rightResource,
									final List<Diff> p_differences ) {
		super( p_leftResource, p_rightResource, p_differences );
	}
	
	@Override
	protected List<AdapterFactory> createAdditionalAdapterFactories() {
		final List<AdapterFactory> factories = new ArrayList<AdapterFactory>( super.createAdditionalAdapterFactories() );
		factories.add( new Aadl2ItemProviderAdapterFactory() );
		factories.add( new InstanceItemProviderAdapterFactory() );

		return factories;
	}

	@Override
	protected String getText( final Object value ) {
		if ( value instanceof PropertyValue ) {
			return getText( (PropertyValue) value );
		}
		
		if ( value instanceof NamedElement ) {
			final NamedElement element = (NamedElement) value;
			final String name = element.getQualifiedName();
			
			if ( name != null && !name.isEmpty() ) {
				return element.eClass().getName() + " " + name;
			}
		}
		
		return super.getText( value );
	}

	protected String getText( final PropertyValue value ) {
		final StringBuilder text = new StringBuilder( "property " );
		
		final PropertyAssociation propAss = EcoreUtil2.getContainerOfType( value, PropertyAssociation.class );
		text.append( getText( propAss.getProperty() ) );

		return  text.toString();
	}
}
