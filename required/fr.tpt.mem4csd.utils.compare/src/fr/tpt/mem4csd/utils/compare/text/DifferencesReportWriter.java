package fr.tpt.mem4csd.utils.compare.text;

import java.io.PrintStream;

import fr.tpt.mem4csd.utils.compare.IEditCommand;

public class DifferencesReportWriter extends DefaultReportWriter {

	public DifferencesReportWriter( final PrintStream p_tream ) {
		super( p_tream );
	}

    @Override
    public void report( final IEditCommand command ) {
    	if ( !( command instanceof MatchCommand ) ) {
    		super.report( command );
    	}
    }
}
