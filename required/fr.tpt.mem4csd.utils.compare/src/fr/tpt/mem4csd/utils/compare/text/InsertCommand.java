package fr.tpt.mem4csd.utils.compare.text;

/**
 * Insert a block new lines into the old file.
 */
public class InsertCommand extends EditCommand {
	
    public InsertCommand(	final FileInfo oldFileInfo, 
    						final FileInfo newFileInfo ) {
        super();
        
        command = "Insert before";
        oldLines = oldFileInfo.getBlockAt( oldFileInfo.getLineNum() );
        newLines = newFileInfo.nextBlock();
        newLines.reportable = true;
    }
}
