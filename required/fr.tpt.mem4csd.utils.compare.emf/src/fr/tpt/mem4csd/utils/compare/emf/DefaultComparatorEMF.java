package fr.tpt.mem4csd.utils.compare.emf;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.diff.DefaultDiffEngine;
import org.eclipse.emf.compare.diff.DiffBuilder;
import org.eclipse.emf.compare.diff.FeatureFilter;
import org.eclipse.emf.compare.diff.IDiffEngine;
import org.eclipse.emf.compare.diff.IDiffProcessor;
import org.eclipse.emf.compare.match.DefaultComparisonFactory;
import org.eclipse.emf.compare.match.DefaultEqualityHelperFactory;
import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.PackageNotFoundException;
import org.xml.sax.SAXParseException;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import fr.tpt.mem4csd.utils.compare.IComparisonReport;
import fr.tpt.mem4csd.utils.compare.text.DefaultComparatorText;

public class DefaultComparatorEMF extends DefaultComparatorText implements IComparatorEMF {
	
	private final EMFCompare comparator;
	
	private final ResourceSet resourceSet;
	
	private final Collection<Predicate<? super Diff>> diffFilteringPredicates;
	
	private class CustomMatchEngineFactory extends MatchEngineFactoryImpl {
		
		private CustomMatchEngineFactory( 	final IEObjectMatcher matcher,
											final IComparisonFactory comparisonFactory,
											final IMatchEngine matchEngine ) {
			super( matcher, comparisonFactory );
			
			if ( matchEngine != null ) {
				this.matchEngine = matchEngine;
			}
		}
	}
	
	public DefaultComparatorEMF( 	final ResourceSet resSet,
									final boolean p_useIds ) {
		this( resSet, p_useIds, Collections.<Predicate<? super Diff>>emptyList(), Collections.<String>emptyList() );
	}

	public DefaultComparatorEMF( 	final ResourceSet resSet,
									final boolean useIds,
									final Collection<Predicate<? super Diff>> diffFilteringPredicates,
									final Collection<String> ignoredFileExtensions ) {
		super( ignoredFileExtensions );
		
		resourceSet = resSet == null? new ResourceSetImpl() : resSet;
		
		this.diffFilteringPredicates = diffFilteringPredicates;
		
		final IEObjectMatcher matcher = createEObjectMatcher( useIds );
		final IComparisonFactory comparisonFactory = new DefaultComparisonFactory( new DefaultEqualityHelperFactory() );
		 
		final IMatchEngine.Factory matchEngineFactory = new CustomMatchEngineFactory( matcher, comparisonFactory, createMatchEngine( matcher, comparisonFactory ) );
		matchEngineFactory.setRanking( 20 );

		final IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
		matchEngineRegistry.add( matchEngineFactory );
		
		final IDiffProcessor diffProcessor = new DiffBuilder();
		final IDiffEngine diffEngine = new DefaultDiffEngine(diffProcessor) {
			
			@Override
			protected FeatureFilter createFeatureFilter() {
				return DefaultComparatorEMF.this.createFeatureFilter();
			}
		};
		
		comparator = EMFCompare.builder().setDiffEngine(diffEngine).setMatchEngineFactoryRegistry( matchEngineRegistry ).build();
	}
	
	protected IEObjectMatcher createEObjectMatcher( final boolean useIds ) {
		return DefaultMatchEngine.createDefaultEObjectMatcher( useIds ? UseIdentifiers.WHEN_AVAILABLE : UseIdentifiers.NEVER );
	}
	
	protected FeatureFilter createFeatureFilter() {
		return new FeatureFilter();
	}
	
	protected IMatchEngine createMatchEngine( 	final IEObjectMatcher matcher,
												final IComparisonFactory comparisonFactory ) {
		return null;
	}

	@Override
	public IComparisonReport compare(	final File fileLeft,
										final File fileRight,
										final FilenameFilter filter,
										final boolean ignoreExtraFilesRightSide )
	throws IOException {
		if ( fileLeft.isFile() && !ignore( fileLeft, filter) && fileRight.isFile() && !ignore( fileRight, filter ) ) {
			final Resource resourceLeft = convertToEMFResource( fileLeft );
			
			if ( resourceLeft != null ) {
				checkErrors( resourceLeft );

				final Resource resourceRight = convertToEMFResource( fileRight );
			
				if ( resourceRight != null ) {
					checkErrors( resourceRight );

					return compare( resourceLeft, resourceRight );
				}
			}
		}

		return super.compare( fileLeft, fileRight, filter, ignoreExtraFilesRightSide );
	}
	
	private void checkErrors( final Resource resource ) {
		if ( !resource.getErrors().isEmpty() ) {
			final StringBuilder strBuild = new StringBuilder( "Resource " );
			strBuild.append( resource.getURI() );
			strBuild.append( " has errors!" );
			strBuild.append( System.lineSeparator() );
			strBuild.append( resource.getErrors().toString() );
			
			throw new IllegalStateException( strBuild.toString() );
		}
	}

	@Override
	public IComparisonReport compare(	final URI uriLeft,
										final URI uriRight )
	throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public IComparisonReport compare(	final Resource resourceLeft,
										final Resource resourceRight )
	throws IOException {
		final IComparisonScope scope = new ResourceComparisonScope( resourceLeft, resourceRight );
		final Comparison result = comparator.compare( scope );
		final List<Diff> filteredDifferences;
		
		if ( diffFilteringPredicates == null || diffFilteringPredicates.isEmpty() ) {
			filteredDifferences = new ArrayList<Diff>( result.getDifferences() );
		}
		else {
			filteredDifferences = new ArrayList<Diff>();
			
			for ( final Predicate<? super Diff> predicate : diffFilteringPredicates ) {
				final Iterable<Diff> filteredDiffIterable = Iterables.filter( result.getDifferences(), predicate );
				
				final Iterator<Diff> filteredDifferencesIt = filteredDiffIterable.iterator();
				
				while ( filteredDifferencesIt.hasNext() ) {
					filteredDifferences.add( filteredDifferencesIt.next() );
				}
			}
		}
			
		return createComparisonReport( 	result.getMatchedResources().get( 0 ).getLeft(),
										result.getMatchedResources().get( 0 ).getRight(),
										filteredDifferences );
	}
	
	protected IComparisonReport createComparisonReport( final Resource leftResource,
														final Resource rightResource,
														final List<Diff> differences ) {
		return new ComparisonReportEMF( leftResource, rightResource, differences );
	}
	
	// TODO: Move to EMF Utilities class
	protected Resource convertToEMFResource( final File file ) {
		assert file != null : "File cannot be null.";
		final URI uri;
		
		if ( file.isAbsolute() ) {
			uri = URI.createFileURI( file.getPath() );
		}
		else {
			uri = URI.createURI( file.getPath() );
		}

		try {
			if ( resourceSet.getURIConverter().exists( uri, null ) ) {
				return resourceSet.getResource( uri, true );
			}
			
			return null;
		}
		catch ( final RuntimeException ex ) {
			//p_ex.printStackTrace();
			if ( 	ex.getCause() instanceof SAXParseException || ex.getCause() instanceof PackageNotFoundException ||
					( ex.getMessage() != null && ex.getMessage().endsWith( "a registered resource factory is needed" ) ) ) {
				return null;
			}
			
			throw ex;
		}
	}
}
