package fr.tpt.mem4csd.utils.compare.emf;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

import fr.tpt.mem4csd.utils.compare.IComparator;
import fr.tpt.mem4csd.utils.compare.IComparisonReport;

public interface IComparatorEMF extends IComparator {

	IComparisonReport compare(	URI file1, 
								URI file2 )
	throws IOException;

	IComparisonReport compare(	Resource resource1, 
								Resource resource2 ) 
	throws IOException;
}
