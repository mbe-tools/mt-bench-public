package fr.tpt.mem4csd.utils.compare.emf;

import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.ecore.resource.Resource;

public class ResourceComparisonScope extends DefaultComparisonScope {

	public ResourceComparisonScope(	final Resource resourceLeft, 
										final Resource resourceRight ) {
		super( resourceLeft, resourceRight, null );
	}

	@Override
	public Resource getLeft() {
		return (Resource) super.getLeft();
	}

	@Override
	public Resource getRight() {
		return (Resource) super.getRight();
	}
}
