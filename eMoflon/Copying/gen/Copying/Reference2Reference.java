/**
 */
package Copying;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.instance.ConnectionReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference2 Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Copying.Reference2Reference#getSource <em>Source</em>}</li>
 *   <li>{@link Copying.Reference2Reference#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see Copying.CopyingPackage#getReference2Reference()
 * @model
 * @generated
 */
public interface Reference2Reference extends EObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(ConnectionReference)
	 * @see Copying.CopyingPackage#getReference2Reference_Source()
	 * @model
	 * @generated
	 */
	ConnectionReference getSource();

	/**
	 * Sets the value of the '{@link Copying.Reference2Reference#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(ConnectionReference value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ConnectionReference)
	 * @see Copying.CopyingPackage#getReference2Reference_Target()
	 * @model
	 * @generated
	 */
	ConnectionReference getTarget();

	/**
	 * Sets the value of the '{@link Copying.Reference2Reference#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ConnectionReference value);

} // Reference2Reference
