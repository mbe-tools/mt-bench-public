/**
 */
package Copying;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionInstanceEnd;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection Instance To Connection Instance Marker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_destination <em>CONTEXT SRC sdestination</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__TRG__connection_target <em>CREATE TRG connection target</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_destination <em>CONTEXT TRG tdestination</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__CORR__c2c <em>CREATE CORR c2c</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e1 <em>CONTEXT CORR e2e1</em>}</li>
 *   <li>{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}</li>
 * </ul>
 *
 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker()
 * @model
 * @generated
 */
public interface ConnectionInstanceToConnectionInstance__Marker extends EObject, TGGRuleApplication {
	/**
	 * Returns the value of the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC component source</em>' reference.
	 * @see #setCONTEXT__SRC__component_source(ComponentInstance)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__component_source()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__component_source();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC component source</em>' reference.
	 * @see #getCONTEXT__SRC__component_source()
	 * @generated
	 */
	void setCONTEXT__SRC__component_source(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC connection source</em>' reference.
	 * @see #setCREATE__SRC__connection_source(ConnectionInstance)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CREATE__SRC__connection_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCREATE__SRC__connection_source();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC connection source</em>' reference.
	 * @see #getCREATE__SRC__connection_source()
	 * @generated
	 */
	void setCREATE__SRC__connection_source(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC sdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC sdestination</em>' reference.
	 * @see #setCONTEXT__SRC__s_destination(ConnectionInstanceEnd)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_destination()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstanceEnd getCONTEXT__SRC__s_destination();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_destination <em>CONTEXT SRC sdestination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC sdestination</em>' reference.
	 * @see #getCONTEXT__SRC__s_destination()
	 * @generated
	 */
	void setCONTEXT__SRC__s_destination(ConnectionInstanceEnd value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC ssource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC ssource</em>' reference.
	 * @see #setCONTEXT__SRC__s_source(ConnectionInstanceEnd)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstanceEnd getCONTEXT__SRC__s_source();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC ssource</em>' reference.
	 * @see #getCONTEXT__SRC__s_source()
	 * @generated
	 */
	void setCONTEXT__SRC__s_source(ConnectionInstanceEnd value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG component target</em>' reference.
	 * @see #setCONTEXT__TRG__component_target(ComponentInstance)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__component_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__component_target();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG component target</em>' reference.
	 * @see #getCONTEXT__TRG__component_target()
	 * @generated
	 */
	void setCONTEXT__TRG__component_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG connection target</em>' reference.
	 * @see #setCREATE__TRG__connection_target(ConnectionInstance)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CREATE__TRG__connection_target()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCREATE__TRG__connection_target();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__TRG__connection_target <em>CREATE TRG connection target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG connection target</em>' reference.
	 * @see #getCREATE__TRG__connection_target()
	 * @generated
	 */
	void setCREATE__TRG__connection_target(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG tdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG tdestination</em>' reference.
	 * @see #setCONTEXT__TRG__t_destination(ConnectionInstanceEnd)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_destination()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstanceEnd getCONTEXT__TRG__t_destination();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_destination <em>CONTEXT TRG tdestination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG tdestination</em>' reference.
	 * @see #getCONTEXT__TRG__t_destination()
	 * @generated
	 */
	void setCONTEXT__TRG__t_destination(ConnectionInstanceEnd value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG tsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG tsource</em>' reference.
	 * @see #setCONTEXT__TRG__t_source(ConnectionInstanceEnd)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstanceEnd getCONTEXT__TRG__t_source();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG tsource</em>' reference.
	 * @see #getCONTEXT__TRG__t_source()
	 * @generated
	 */
	void setCONTEXT__TRG__t_source(ConnectionInstanceEnd value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR c2c</em>' reference.
	 * @see #setCREATE__CORR__c2c(ConnectionToConnection)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CREATE__CORR__c2c()
	 * @model required="true"
	 * @generated
	 */
	ConnectionToConnection getCREATE__CORR__c2c();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__CORR__c2c <em>CREATE CORR c2c</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR c2c</em>' reference.
	 * @see #getCREATE__CORR__c2c()
	 * @generated
	 */
	void setCREATE__CORR__c2c(ConnectionToConnection value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR comp2comp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR comp2comp</em>' reference.
	 * @see #setCONTEXT__CORR__comp2comp(CompToComp)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__comp2comp()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__comp2comp();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR comp2comp</em>' reference.
	 * @see #getCONTEXT__CORR__comp2comp()
	 * @generated
	 */
	void setCONTEXT__CORR__comp2comp(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR e2e1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR e2e1</em>' reference.
	 * @see #setCONTEXT__CORR__e2e1(End2End)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e1()
	 * @model required="true"
	 * @generated
	 */
	End2End getCONTEXT__CORR__e2e1();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e1 <em>CONTEXT CORR e2e1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR e2e1</em>' reference.
	 * @see #getCONTEXT__CORR__e2e1()
	 * @generated
	 */
	void setCONTEXT__CORR__e2e1(End2End value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR e2e2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR e2e2</em>' reference.
	 * @see #setCONTEXT__CORR__e2e2(End2End)
	 * @see Copying.CopyingPackage#getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e2()
	 * @model required="true"
	 * @generated
	 */
	End2End getCONTEXT__CORR__e2e2();

	/**
	 * Sets the value of the '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR e2e2</em>' reference.
	 * @see #getCONTEXT__CORR__e2e2()
	 * @generated
	 */
	void setCONTEXT__CORR__e2e2(End2End value);

} // ConnectionInstanceToConnectionInstance__Marker
