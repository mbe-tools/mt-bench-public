/**
 */
package Copying.util;

import Copying.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see Copying.CopyingPackage
 * @generated
 */
public class CopyingSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CopyingPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyingSwitch() {
		if (modelPackage == null) {
			modelPackage = CopyingPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case CopyingPackage.SYSTEM_TO_SYSTEM: {
				SystemToSystem systemToSystem = (SystemToSystem)theEObject;
				T result = caseSystemToSystem(systemToSystem);
				if (result == null) result = caseCompToComp(systemToSystem);
				if (result == null) result = caseEnd2End(systemToSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.CONNECTION_TO_CONNECTION: {
				ConnectionToConnection connectionToConnection = (ConnectionToConnection)theEObject;
				T result = caseConnectionToConnection(connectionToConnection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.PORT_CONNECTION_TO_SHARED_DATA: {
				PortConnectionToSharedData portConnectionToSharedData = (PortConnectionToSharedData)theEObject;
				T result = casePortConnectionToSharedData(portConnectionToSharedData);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.REFERENCE2_REFERENCE: {
				Reference2Reference reference2Reference = (Reference2Reference)theEObject;
				T result = caseReference2Reference(reference2Reference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.END2_END: {
				End2End end2End = (End2End)theEObject;
				T result = caseEnd2End(end2End);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.COMP_TO_COMP: {
				CompToComp compToComp = (CompToComp)theEObject;
				T result = caseCompToComp(compToComp);
				if (result == null) result = caseEnd2End(compToComp);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.FEAT_TO_FEAT: {
				FeatToFeat featToFeat = (FeatToFeat)theEObject;
				T result = caseFeatToFeat(featToFeat);
				if (result == null) result = caseEnd2End(featToFeat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER: {
				ComponentToComponent__Marker componentToComponent__Marker = (ComponentToComponent__Marker)theEObject;
				T result = caseComponentToComponent__Marker(componentToComponent__Marker);
				if (result == null) result = caseTGGRuleApplication(componentToComponent__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER: {
				ConnectionInstanceToConnectionInstance__Marker connectionInstanceToConnectionInstance__Marker = (ConnectionInstanceToConnectionInstance__Marker)theEObject;
				T result = caseConnectionInstanceToConnectionInstance__Marker(connectionInstanceToConnectionInstance__Marker);
				if (result == null) result = caseTGGRuleApplication(connectionInstanceToConnectionInstance__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.CONNECTION_REF_TO_CONNECTION_REF_MARKER: {
				ConnectionRefToConnectionRef__Marker connectionRefToConnectionRef__Marker = (ConnectionRefToConnectionRef__Marker)theEObject;
				T result = caseConnectionRefToConnectionRef__Marker(connectionRefToConnectionRef__Marker);
				if (result == null) result = caseTGGRuleApplication(connectionRefToConnectionRef__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER: {
				FeatureToFeature__Marker featureToFeature__Marker = (FeatureToFeature__Marker)theEObject;
				T result = caseFeatureToFeature__Marker(featureToFeature__Marker);
				if (result == null) result = caseTGGRuleApplication(featureToFeature__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CopyingPackage.SYSTEM_TO_SYSTEM_MARKER: {
				SystemToSystem__Marker systemToSystem__Marker = (SystemToSystem__Marker)theEObject;
				T result = caseSystemToSystem__Marker(systemToSystem__Marker);
				if (result == null) result = caseTGGRuleApplication(systemToSystem__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System To System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System To System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemToSystem(SystemToSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection To Connection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection To Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionToConnection(ConnectionToConnection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection To Shared Data</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection To Shared Data</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionToSharedData(PortConnectionToSharedData object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference2 Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference2 Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReference2Reference(Reference2Reference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>End2 End</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>End2 End</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnd2End(End2End object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Comp To Comp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Comp To Comp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompToComp(CompToComp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feat To Feat</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feat To Feat</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatToFeat(FeatToFeat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component To Component Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component To Component Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentToComponent__Marker(ComponentToComponent__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Instance To Connection Instance Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Instance To Connection Instance Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionInstanceToConnectionInstance__Marker(ConnectionInstanceToConnectionInstance__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Ref To Connection Ref Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Ref To Connection Ref Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionRefToConnectionRef__Marker(ConnectionRefToConnectionRef__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature To Feature Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature To Feature Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureToFeature__Marker(FeatureToFeature__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System To System Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System To System Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemToSystem__Marker(SystemToSystem__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Rule Application</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Rule Application</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGRuleApplication(TGGRuleApplication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //CopyingSwitch
