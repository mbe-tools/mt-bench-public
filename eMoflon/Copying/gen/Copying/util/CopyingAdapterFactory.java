/**
 */
package Copying.util;

import Copying.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see Copying.CopyingPackage
 * @generated
 */
public class CopyingAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CopyingPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyingAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = CopyingPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CopyingSwitch<Adapter> modelSwitch =
		new CopyingSwitch<Adapter>() {
			@Override
			public Adapter caseSystemToSystem(SystemToSystem object) {
				return createSystemToSystemAdapter();
			}
			@Override
			public Adapter caseConnectionToConnection(ConnectionToConnection object) {
				return createConnectionToConnectionAdapter();
			}
			@Override
			public Adapter casePortConnectionToSharedData(PortConnectionToSharedData object) {
				return createPortConnectionToSharedDataAdapter();
			}
			@Override
			public Adapter caseReference2Reference(Reference2Reference object) {
				return createReference2ReferenceAdapter();
			}
			@Override
			public Adapter caseEnd2End(End2End object) {
				return createEnd2EndAdapter();
			}
			@Override
			public Adapter caseCompToComp(CompToComp object) {
				return createCompToCompAdapter();
			}
			@Override
			public Adapter caseFeatToFeat(FeatToFeat object) {
				return createFeatToFeatAdapter();
			}
			@Override
			public Adapter caseComponentToComponent__Marker(ComponentToComponent__Marker object) {
				return createComponentToComponent__MarkerAdapter();
			}
			@Override
			public Adapter caseConnectionInstanceToConnectionInstance__Marker(ConnectionInstanceToConnectionInstance__Marker object) {
				return createConnectionInstanceToConnectionInstance__MarkerAdapter();
			}
			@Override
			public Adapter caseConnectionRefToConnectionRef__Marker(ConnectionRefToConnectionRef__Marker object) {
				return createConnectionRefToConnectionRef__MarkerAdapter();
			}
			@Override
			public Adapter caseFeatureToFeature__Marker(FeatureToFeature__Marker object) {
				return createFeatureToFeature__MarkerAdapter();
			}
			@Override
			public Adapter caseSystemToSystem__Marker(SystemToSystem__Marker object) {
				return createSystemToSystem__MarkerAdapter();
			}
			@Override
			public Adapter caseTGGRuleApplication(TGGRuleApplication object) {
				return createTGGRuleApplicationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link Copying.SystemToSystem <em>System To System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.SystemToSystem
	 * @generated
	 */
	public Adapter createSystemToSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.ConnectionToConnection <em>Connection To Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.ConnectionToConnection
	 * @generated
	 */
	public Adapter createConnectionToConnectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.PortConnectionToSharedData <em>Port Connection To Shared Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.PortConnectionToSharedData
	 * @generated
	 */
	public Adapter createPortConnectionToSharedDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.Reference2Reference <em>Reference2 Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.Reference2Reference
	 * @generated
	 */
	public Adapter createReference2ReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.End2End <em>End2 End</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.End2End
	 * @generated
	 */
	public Adapter createEnd2EndAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.CompToComp <em>Comp To Comp</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.CompToComp
	 * @generated
	 */
	public Adapter createCompToCompAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.FeatToFeat <em>Feat To Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.FeatToFeat
	 * @generated
	 */
	public Adapter createFeatToFeatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.ComponentToComponent__Marker <em>Component To Component Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.ComponentToComponent__Marker
	 * @generated
	 */
	public Adapter createComponentToComponent__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.ConnectionInstanceToConnectionInstance__Marker <em>Connection Instance To Connection Instance Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker
	 * @generated
	 */
	public Adapter createConnectionInstanceToConnectionInstance__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.ConnectionRefToConnectionRef__Marker <em>Connection Ref To Connection Ref Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.ConnectionRefToConnectionRef__Marker
	 * @generated
	 */
	public Adapter createConnectionRefToConnectionRef__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.FeatureToFeature__Marker <em>Feature To Feature Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.FeatureToFeature__Marker
	 * @generated
	 */
	public Adapter createFeatureToFeature__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Copying.SystemToSystem__Marker <em>System To System Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Copying.SystemToSystem__Marker
	 * @generated
	 */
	public Adapter createSystemToSystem__MarkerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link runtime.TGGRuleApplication <em>TGG Rule Application</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see runtime.TGGRuleApplication
	 * @generated
	 */
	public Adapter createTGGRuleApplicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //CopyingAdapterFactory
