/**
 */
package Copying;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Copying.CopyingFactory
 * @model kind="package"
 * @generated
 */
public interface CopyingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Copying";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/Copying/model/Copying.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Copying";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CopyingPackage eINSTANCE = Copying.impl.CopyingPackageImpl.init();

	/**
	 * The meta object id for the '{@link Copying.impl.End2EndImpl <em>End2 End</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.End2EndImpl
	 * @see Copying.impl.CopyingPackageImpl#getEnd2End()
	 * @generated
	 */
	int END2_END = 4;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END2_END__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END2_END__TARGET = 1;

	/**
	 * The number of structural features of the '<em>End2 End</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END2_END_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>End2 End</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END2_END_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Copying.impl.CompToCompImpl <em>Comp To Comp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.CompToCompImpl
	 * @see Copying.impl.CopyingPackageImpl#getCompToComp()
	 * @generated
	 */
	int COMP_TO_COMP = 5;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMP_TO_COMP__SOURCE = END2_END__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMP_TO_COMP__TARGET = END2_END__TARGET;

	/**
	 * The number of structural features of the '<em>Comp To Comp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMP_TO_COMP_FEATURE_COUNT = END2_END_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Comp To Comp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMP_TO_COMP_OPERATION_COUNT = END2_END_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Copying.impl.SystemToSystemImpl <em>System To System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.SystemToSystemImpl
	 * @see Copying.impl.CopyingPackageImpl#getSystemToSystem()
	 * @generated
	 */
	int SYSTEM_TO_SYSTEM = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM__SOURCE = COMP_TO_COMP__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM__TARGET = COMP_TO_COMP__TARGET;

	/**
	 * The number of structural features of the '<em>System To System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_FEATURE_COUNT = COMP_TO_COMP_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>System To System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_OPERATION_COUNT = COMP_TO_COMP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Copying.impl.ConnectionToConnectionImpl <em>Connection To Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.ConnectionToConnectionImpl
	 * @see Copying.impl.CopyingPackageImpl#getConnectionToConnection()
	 * @generated
	 */
	int CONNECTION_TO_CONNECTION = 1;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TO_CONNECTION__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TO_CONNECTION__TARGET = 1;

	/**
	 * The number of structural features of the '<em>Connection To Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TO_CONNECTION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Connection To Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TO_CONNECTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Copying.impl.PortConnectionToSharedDataImpl <em>Port Connection To Shared Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.PortConnectionToSharedDataImpl
	 * @see Copying.impl.CopyingPackageImpl#getPortConnectionToSharedData()
	 * @generated
	 */
	int PORT_CONNECTION_TO_SHARED_DATA = 2;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_SHARED_DATA__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_SHARED_DATA__TARGET = 1;

	/**
	 * The number of structural features of the '<em>Port Connection To Shared Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_SHARED_DATA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Port Connection To Shared Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_SHARED_DATA_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Copying.impl.Reference2ReferenceImpl <em>Reference2 Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.Reference2ReferenceImpl
	 * @see Copying.impl.CopyingPackageImpl#getReference2Reference()
	 * @generated
	 */
	int REFERENCE2_REFERENCE = 3;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE2_REFERENCE__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE2_REFERENCE__TARGET = 1;

	/**
	 * The number of structural features of the '<em>Reference2 Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE2_REFERENCE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Reference2 Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE2_REFERENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Copying.impl.FeatToFeatImpl <em>Feat To Feat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.FeatToFeatImpl
	 * @see Copying.impl.CopyingPackageImpl#getFeatToFeat()
	 * @generated
	 */
	int FEAT_TO_FEAT = 6;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_TO_FEAT__SOURCE = END2_END__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_TO_FEAT__TARGET = END2_END__TARGET;

	/**
	 * The number of structural features of the '<em>Feat To Feat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_TO_FEAT_FEATURE_COUNT = END2_END_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Feat To Feat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_TO_FEAT_OPERATION_COUNT = END2_END_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Copying.impl.ComponentToComponent__MarkerImpl <em>Component To Component Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.ComponentToComponent__MarkerImpl
	 * @see Copying.impl.CopyingPackageImpl#getComponentToComponent__Marker()
	 * @generated
	 */
	int COMPONENT_TO_COMPONENT_MARKER = 7;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CREATE SRC subcomponent source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CREATE_SRC_SUBCOMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CONTEXT_TRG_COMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE TRG subcomponent target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CREATE_TRG_SUBCOMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE CORR sub2sub</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CREATE_CORR_SUB2SUB = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Component To Component Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Component To Component Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Copying.impl.ConnectionInstanceToConnectionInstance__MarkerImpl <em>Connection Instance To Connection Instance Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.ConnectionInstanceToConnectionInstance__MarkerImpl
	 * @see Copying.impl.CopyingPackageImpl#getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER = 8;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC sdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC ssource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_COMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_TRG_CONNECTION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CREATE CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR comp2comp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_COMP2COMP = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Connection Instance To Connection Instance Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The number of operations of the '<em>Connection Instance To Connection Instance Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Copying.impl.ConnectionRefToConnectionRef__MarkerImpl <em>Connection Ref To Connection Ref Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.ConnectionRefToConnectionRef__MarkerImpl
	 * @see Copying.impl.CopyingPackageImpl#getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER = 9;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection Ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC scontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_SCONTEXT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC sdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_SDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC ssource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_SSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE TRG connection Ref target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CREATE_TRG_CONNECTION_REF_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_CONNECTION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tcontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_TCONTEXT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_TDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_TSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR comp2comp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_COMP2COMP = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_E2E1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_E2E2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER__CREATE_CORR_R2R = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The number of structural features of the '<em>Connection Ref To Connection Ref Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The number of operations of the '<em>Connection Ref To Connection Ref Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Copying.impl.FeatureToFeature__MarkerImpl <em>Feature To Feature Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.FeatureToFeature__MarkerImpl
	 * @see Copying.impl.CopyingPackageImpl#getFeatureToFeature__Marker()
	 * @generated
	 */
	int FEATURE_TO_FEATURE_MARKER = 10;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CREATE_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CONTEXT_TRG_COMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CREATE_TRG_FEATURE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE CORR f2f2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CREATE_CORR_F2F2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Feature To Feature Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Feature To Feature Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Copying.impl.SystemToSystem__MarkerImpl <em>System To System Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Copying.impl.SystemToSystem__MarkerImpl
	 * @see Copying.impl.CopyingPackageImpl#getSystemToSystem__Marker()
	 * @generated
	 */
	int SYSTEM_TO_SYSTEM_MARKER = 11;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER__CREATE_SRC_SYSTEM_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CREATE TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER__CREATE_TRG_SYSTEM_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CREATE CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER__CREATE_CORR_S2S = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>System To System Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>System To System Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link Copying.SystemToSystem <em>System To System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System To System</em>'.
	 * @see Copying.SystemToSystem
	 * @generated
	 */
	EClass getSystemToSystem();

	/**
	 * Returns the meta object for class '{@link Copying.ConnectionToConnection <em>Connection To Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection To Connection</em>'.
	 * @see Copying.ConnectionToConnection
	 * @generated
	 */
	EClass getConnectionToConnection();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionToConnection#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see Copying.ConnectionToConnection#getSource()
	 * @see #getConnectionToConnection()
	 * @generated
	 */
	EReference getConnectionToConnection_Source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionToConnection#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see Copying.ConnectionToConnection#getTarget()
	 * @see #getConnectionToConnection()
	 * @generated
	 */
	EReference getConnectionToConnection_Target();

	/**
	 * Returns the meta object for class '{@link Copying.PortConnectionToSharedData <em>Port Connection To Shared Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection To Shared Data</em>'.
	 * @see Copying.PortConnectionToSharedData
	 * @generated
	 */
	EClass getPortConnectionToSharedData();

	/**
	 * Returns the meta object for the reference '{@link Copying.PortConnectionToSharedData#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see Copying.PortConnectionToSharedData#getSource()
	 * @see #getPortConnectionToSharedData()
	 * @generated
	 */
	EReference getPortConnectionToSharedData_Source();

	/**
	 * Returns the meta object for the reference '{@link Copying.PortConnectionToSharedData#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see Copying.PortConnectionToSharedData#getTarget()
	 * @see #getPortConnectionToSharedData()
	 * @generated
	 */
	EReference getPortConnectionToSharedData_Target();

	/**
	 * Returns the meta object for class '{@link Copying.Reference2Reference <em>Reference2 Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference2 Reference</em>'.
	 * @see Copying.Reference2Reference
	 * @generated
	 */
	EClass getReference2Reference();

	/**
	 * Returns the meta object for the reference '{@link Copying.Reference2Reference#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see Copying.Reference2Reference#getSource()
	 * @see #getReference2Reference()
	 * @generated
	 */
	EReference getReference2Reference_Source();

	/**
	 * Returns the meta object for the reference '{@link Copying.Reference2Reference#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see Copying.Reference2Reference#getTarget()
	 * @see #getReference2Reference()
	 * @generated
	 */
	EReference getReference2Reference_Target();

	/**
	 * Returns the meta object for class '{@link Copying.End2End <em>End2 End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>End2 End</em>'.
	 * @see Copying.End2End
	 * @generated
	 */
	EClass getEnd2End();

	/**
	 * Returns the meta object for the reference '{@link Copying.End2End#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see Copying.End2End#getSource()
	 * @see #getEnd2End()
	 * @generated
	 */
	EReference getEnd2End_Source();

	/**
	 * Returns the meta object for the reference '{@link Copying.End2End#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see Copying.End2End#getTarget()
	 * @see #getEnd2End()
	 * @generated
	 */
	EReference getEnd2End_Target();

	/**
	 * Returns the meta object for class '{@link Copying.CompToComp <em>Comp To Comp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comp To Comp</em>'.
	 * @see Copying.CompToComp
	 * @generated
	 */
	EClass getCompToComp();

	/**
	 * Returns the meta object for class '{@link Copying.FeatToFeat <em>Feat To Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feat To Feat</em>'.
	 * @see Copying.FeatToFeat
	 * @generated
	 */
	EClass getFeatToFeat();

	/**
	 * Returns the meta object for class '{@link Copying.ComponentToComponent__Marker <em>Component To Component Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component To Component Marker</em>'.
	 * @see Copying.ComponentToComponent__Marker
	 * @generated
	 */
	EClass getComponentToComponent__Marker();

	/**
	 * Returns the meta object for the reference '{@link Copying.ComponentToComponent__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Copying.ComponentToComponent__Marker#getCONTEXT__SRC__component_source()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ComponentToComponent__Marker#getCREATE__SRC__subcomponent_source <em>CREATE SRC subcomponent source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC subcomponent source</em>'.
	 * @see Copying.ComponentToComponent__Marker#getCREATE__SRC__subcomponent_source()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CREATE__SRC__subcomponent_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ComponentToComponent__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component target</em>'.
	 * @see Copying.ComponentToComponent__Marker#getCONTEXT__TRG__component_target()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CONTEXT__TRG__component_target();

	/**
	 * Returns the meta object for the reference '{@link Copying.ComponentToComponent__Marker#getCREATE__TRG__subcomponent_target <em>CREATE TRG subcomponent target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG subcomponent target</em>'.
	 * @see Copying.ComponentToComponent__Marker#getCREATE__TRG__subcomponent_target()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CREATE__TRG__subcomponent_target();

	/**
	 * Returns the meta object for the reference '{@link Copying.ComponentToComponent__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Copying.ComponentToComponent__Marker#getCONTEXT__CORR__c2c()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Copying.ComponentToComponent__Marker#getCREATE__CORR__sub2sub <em>CREATE CORR sub2sub</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR sub2sub</em>'.
	 * @see Copying.ComponentToComponent__Marker#getCREATE__CORR__sub2sub()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CREATE__CORR__sub2sub();

	/**
	 * Returns the meta object for class '{@link Copying.ConnectionInstanceToConnectionInstance__Marker <em>Connection Instance To Connection Instance Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Instance To Connection Instance Marker</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker
	 * @generated
	 */
	EClass getConnectionInstanceToConnectionInstance__Marker();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__component_source()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__SRC__connection_source()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_destination <em>CONTEXT SRC sdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC sdestination</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_destination()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_destination();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC ssource</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_source()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component target</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__component_target()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__component_target();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__TRG__connection_target <em>CREATE TRG connection target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG connection target</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__TRG__connection_target()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__TRG__connection_target();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_destination <em>CONTEXT TRG tdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tdestination</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_destination()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_destination();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tsource</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_source()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__CORR__c2c <em>CREATE CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR c2c</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCREATE__CORR__c2c()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR comp2comp</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__comp2comp()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__comp2comp();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e1 <em>CONTEXT CORR e2e1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e1</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e1()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e1();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e2</em>'.
	 * @see Copying.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e2()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e2();

	/**
	 * Returns the meta object for class '{@link Copying.ConnectionRefToConnectionRef__Marker <em>Connection Ref To Connection Ref Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Ref To Connection Ref Marker</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker
	 * @generated
	 */
	EClass getConnectionRefToConnectionRef__Marker();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCREATE__SRC__connectionRef_source <em>CREATE SRC connection Ref source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection Ref source</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCREATE__SRC__connectionRef_source()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CREATE__SRC__connectionRef_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__SRC__connection_source <em>CONTEXT SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC connection source</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__SRC__connection_source()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__SRC__s_context <em>CONTEXT SRC scontext</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC scontext</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__SRC__s_context()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_context();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__SRC__s_destination <em>CONTEXT SRC sdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC sdestination</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__SRC__s_destination()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_destination();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC ssource</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__SRC__s_source()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCREATE__TRG__connectionRef_target <em>CREATE TRG connection Ref target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG connection Ref target</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCREATE__TRG__connectionRef_target()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CREATE__TRG__connectionRef_target();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__TRG__connection_target <em>CONTEXT TRG connection target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG connection target</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__TRG__connection_target()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__connection_target();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__TRG__t_context <em>CONTEXT TRG tcontext</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tcontext</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__TRG__t_context()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_context();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__TRG__t_destination <em>CONTEXT TRG tdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tdestination</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__TRG__t_destination()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_destination();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tsource</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__TRG__t_source()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__CORR__c2c()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR comp2comp</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__CORR__comp2comp()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__comp2comp();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__CORR__e2e1 <em>CONTEXT CORR e2e1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e1</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__CORR__e2e1()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__e2e1();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e2</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCONTEXT__CORR__e2e2()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__e2e2();

	/**
	 * Returns the meta object for the reference '{@link Copying.ConnectionRefToConnectionRef__Marker#getCREATE__CORR__r2r <em>CREATE CORR r2r</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r</em>'.
	 * @see Copying.ConnectionRefToConnectionRef__Marker#getCREATE__CORR__r2r()
	 * @see #getConnectionRefToConnectionRef__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRef__Marker_CREATE__CORR__r2r();

	/**
	 * Returns the meta object for class '{@link Copying.FeatureToFeature__Marker <em>Feature To Feature Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature To Feature Marker</em>'.
	 * @see Copying.FeatureToFeature__Marker
	 * @generated
	 */
	EClass getFeatureToFeature__Marker();

	/**
	 * Returns the meta object for the reference '{@link Copying.FeatureToFeature__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Copying.FeatureToFeature__Marker#getCONTEXT__SRC__component_source()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.FeatureToFeature__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature source</em>'.
	 * @see Copying.FeatureToFeature__Marker#getCREATE__SRC__feature_source()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CREATE__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.FeatureToFeature__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component target</em>'.
	 * @see Copying.FeatureToFeature__Marker#getCONTEXT__TRG__component_target()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CONTEXT__TRG__component_target();

	/**
	 * Returns the meta object for the reference '{@link Copying.FeatureToFeature__Marker#getCREATE__TRG__feature_target <em>CREATE TRG feature target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature target</em>'.
	 * @see Copying.FeatureToFeature__Marker#getCREATE__TRG__feature_target()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CREATE__TRG__feature_target();

	/**
	 * Returns the meta object for the reference '{@link Copying.FeatureToFeature__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Copying.FeatureToFeature__Marker#getCONTEXT__CORR__c2c()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Copying.FeatureToFeature__Marker#getCREATE__CORR__f2f2 <em>CREATE CORR f2f2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR f2f2</em>'.
	 * @see Copying.FeatureToFeature__Marker#getCREATE__CORR__f2f2()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CREATE__CORR__f2f2();

	/**
	 * Returns the meta object for class '{@link Copying.SystemToSystem__Marker <em>System To System Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System To System Marker</em>'.
	 * @see Copying.SystemToSystem__Marker
	 * @generated
	 */
	EClass getSystemToSystem__Marker();

	/**
	 * Returns the meta object for the reference '{@link Copying.SystemToSystem__Marker#getCREATE__SRC__system_source <em>CREATE SRC system source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC system source</em>'.
	 * @see Copying.SystemToSystem__Marker#getCREATE__SRC__system_source()
	 * @see #getSystemToSystem__Marker()
	 * @generated
	 */
	EReference getSystemToSystem__Marker_CREATE__SRC__system_source();

	/**
	 * Returns the meta object for the reference '{@link Copying.SystemToSystem__Marker#getCREATE__TRG__system_target <em>CREATE TRG system target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG system target</em>'.
	 * @see Copying.SystemToSystem__Marker#getCREATE__TRG__system_target()
	 * @see #getSystemToSystem__Marker()
	 * @generated
	 */
	EReference getSystemToSystem__Marker_CREATE__TRG__system_target();

	/**
	 * Returns the meta object for the reference '{@link Copying.SystemToSystem__Marker#getCREATE__CORR__s2s <em>CREATE CORR s2s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR s2s</em>'.
	 * @see Copying.SystemToSystem__Marker#getCREATE__CORR__s2s()
	 * @see #getSystemToSystem__Marker()
	 * @generated
	 */
	EReference getSystemToSystem__Marker_CREATE__CORR__s2s();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CopyingFactory getCopyingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Copying.impl.SystemToSystemImpl <em>System To System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.SystemToSystemImpl
		 * @see Copying.impl.CopyingPackageImpl#getSystemToSystem()
		 * @generated
		 */
		EClass SYSTEM_TO_SYSTEM = eINSTANCE.getSystemToSystem();

		/**
		 * The meta object literal for the '{@link Copying.impl.ConnectionToConnectionImpl <em>Connection To Connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.ConnectionToConnectionImpl
		 * @see Copying.impl.CopyingPackageImpl#getConnectionToConnection()
		 * @generated
		 */
		EClass CONNECTION_TO_CONNECTION = eINSTANCE.getConnectionToConnection();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_TO_CONNECTION__SOURCE = eINSTANCE.getConnectionToConnection_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_TO_CONNECTION__TARGET = eINSTANCE.getConnectionToConnection_Target();

		/**
		 * The meta object literal for the '{@link Copying.impl.PortConnectionToSharedDataImpl <em>Port Connection To Shared Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.PortConnectionToSharedDataImpl
		 * @see Copying.impl.CopyingPackageImpl#getPortConnectionToSharedData()
		 * @generated
		 */
		EClass PORT_CONNECTION_TO_SHARED_DATA = eINSTANCE.getPortConnectionToSharedData();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_CONNECTION_TO_SHARED_DATA__SOURCE = eINSTANCE.getPortConnectionToSharedData_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_CONNECTION_TO_SHARED_DATA__TARGET = eINSTANCE.getPortConnectionToSharedData_Target();

		/**
		 * The meta object literal for the '{@link Copying.impl.Reference2ReferenceImpl <em>Reference2 Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.Reference2ReferenceImpl
		 * @see Copying.impl.CopyingPackageImpl#getReference2Reference()
		 * @generated
		 */
		EClass REFERENCE2_REFERENCE = eINSTANCE.getReference2Reference();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE2_REFERENCE__SOURCE = eINSTANCE.getReference2Reference_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE2_REFERENCE__TARGET = eINSTANCE.getReference2Reference_Target();

		/**
		 * The meta object literal for the '{@link Copying.impl.End2EndImpl <em>End2 End</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.End2EndImpl
		 * @see Copying.impl.CopyingPackageImpl#getEnd2End()
		 * @generated
		 */
		EClass END2_END = eINSTANCE.getEnd2End();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference END2_END__SOURCE = eINSTANCE.getEnd2End_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference END2_END__TARGET = eINSTANCE.getEnd2End_Target();

		/**
		 * The meta object literal for the '{@link Copying.impl.CompToCompImpl <em>Comp To Comp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.CompToCompImpl
		 * @see Copying.impl.CopyingPackageImpl#getCompToComp()
		 * @generated
		 */
		EClass COMP_TO_COMP = eINSTANCE.getCompToComp();

		/**
		 * The meta object literal for the '{@link Copying.impl.FeatToFeatImpl <em>Feat To Feat</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.FeatToFeatImpl
		 * @see Copying.impl.CopyingPackageImpl#getFeatToFeat()
		 * @generated
		 */
		EClass FEAT_TO_FEAT = eINSTANCE.getFeatToFeat();

		/**
		 * The meta object literal for the '{@link Copying.impl.ComponentToComponent__MarkerImpl <em>Component To Component Marker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.ComponentToComponent__MarkerImpl
		 * @see Copying.impl.CopyingPackageImpl#getComponentToComponent__Marker()
		 * @generated
		 */
		EClass COMPONENT_TO_COMPONENT_MARKER = eINSTANCE.getComponentToComponent__Marker();

		/**
		 * The meta object literal for the '<em><b>CONTEXT SRC component source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TO_COMPONENT_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = eINSTANCE.getComponentToComponent__Marker_CONTEXT__SRC__component_source();

		/**
		 * The meta object literal for the '<em><b>CREATE SRC subcomponent source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TO_COMPONENT_MARKER__CREATE_SRC_SUBCOMPONENT_SOURCE = eINSTANCE.getComponentToComponent__Marker_CREATE__SRC__subcomponent_source();

		/**
		 * The meta object literal for the '<em><b>CONTEXT TRG component target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TO_COMPONENT_MARKER__CONTEXT_TRG_COMPONENT_TARGET = eINSTANCE.getComponentToComponent__Marker_CONTEXT__TRG__component_target();

		/**
		 * The meta object literal for the '<em><b>CREATE TRG subcomponent target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TO_COMPONENT_MARKER__CREATE_TRG_SUBCOMPONENT_TARGET = eINSTANCE.getComponentToComponent__Marker_CREATE__TRG__subcomponent_target();

		/**
		 * The meta object literal for the '<em><b>CONTEXT CORR c2c</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TO_COMPONENT_MARKER__CONTEXT_CORR_C2C = eINSTANCE.getComponentToComponent__Marker_CONTEXT__CORR__c2c();

		/**
		 * The meta object literal for the '<em><b>CREATE CORR sub2sub</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TO_COMPONENT_MARKER__CREATE_CORR_SUB2SUB = eINSTANCE.getComponentToComponent__Marker_CREATE__CORR__sub2sub();

		/**
		 * The meta object literal for the '{@link Copying.impl.ConnectionInstanceToConnectionInstance__MarkerImpl <em>Connection Instance To Connection Instance Marker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.ConnectionInstanceToConnectionInstance__MarkerImpl
		 * @see Copying.impl.CopyingPackageImpl#getConnectionInstanceToConnectionInstance__Marker()
		 * @generated
		 */
		EClass CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker();

		/**
		 * The meta object literal for the '<em><b>CONTEXT SRC component source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__component_source();

		/**
		 * The meta object literal for the '<em><b>CREATE SRC connection source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_SRC_CONNECTION_SOURCE = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CREATE__SRC__connection_source();

		/**
		 * The meta object literal for the '<em><b>CONTEXT SRC sdestination</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SDESTINATION = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_destination();

		/**
		 * The meta object literal for the '<em><b>CONTEXT SRC ssource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SSOURCE = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_source();

		/**
		 * The meta object literal for the '<em><b>CONTEXT TRG component target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_COMPONENT_TARGET = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__component_target();

		/**
		 * The meta object literal for the '<em><b>CREATE TRG connection target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_TRG_CONNECTION_TARGET = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CREATE__TRG__connection_target();

		/**
		 * The meta object literal for the '<em><b>CONTEXT TRG tdestination</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TDESTINATION = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_destination();

		/**
		 * The meta object literal for the '<em><b>CONTEXT TRG tsource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TSOURCE = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_source();

		/**
		 * The meta object literal for the '<em><b>CREATE CORR c2c</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_CORR_C2C = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CREATE__CORR__c2c();

		/**
		 * The meta object literal for the '<em><b>CONTEXT CORR comp2comp</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_COMP2COMP = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__comp2comp();

		/**
		 * The meta object literal for the '<em><b>CONTEXT CORR e2e1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E1 = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e1();

		/**
		 * The meta object literal for the '<em><b>CONTEXT CORR e2e2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E2 = eINSTANCE.getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e2();

		/**
		 * The meta object literal for the '{@link Copying.impl.ConnectionRefToConnectionRef__MarkerImpl <em>Connection Ref To Connection Ref Marker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.ConnectionRefToConnectionRef__MarkerImpl
		 * @see Copying.impl.CopyingPackageImpl#getConnectionRefToConnectionRef__Marker()
		 * @generated
		 */
		EClass CONNECTION_REF_TO_CONNECTION_REF_MARKER = eINSTANCE.getConnectionRefToConnectionRef__Marker();

		/**
		 * The meta object literal for the '<em><b>CREATE SRC connection Ref source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE = eINSTANCE.getConnectionRefToConnectionRef__Marker_CREATE__SRC__connectionRef_source();

		/**
		 * The meta object literal for the '<em><b>CONTEXT SRC connection source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_CONNECTION_SOURCE = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__connection_source();

		/**
		 * The meta object literal for the '<em><b>CONTEXT SRC scontext</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_SCONTEXT = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_context();

		/**
		 * The meta object literal for the '<em><b>CONTEXT SRC sdestination</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_SDESTINATION = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_destination();

		/**
		 * The meta object literal for the '<em><b>CONTEXT SRC ssource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_SSOURCE = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_source();

		/**
		 * The meta object literal for the '<em><b>CREATE TRG connection Ref target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CREATE_TRG_CONNECTION_REF_TARGET = eINSTANCE.getConnectionRefToConnectionRef__Marker_CREATE__TRG__connectionRef_target();

		/**
		 * The meta object literal for the '<em><b>CONTEXT TRG connection target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_CONNECTION_TARGET = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__connection_target();

		/**
		 * The meta object literal for the '<em><b>CONTEXT TRG tcontext</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_TCONTEXT = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_context();

		/**
		 * The meta object literal for the '<em><b>CONTEXT TRG tdestination</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_TDESTINATION = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_destination();

		/**
		 * The meta object literal for the '<em><b>CONTEXT TRG tsource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_TSOURCE = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_source();

		/**
		 * The meta object literal for the '<em><b>CONTEXT CORR c2c</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_C2C = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__c2c();

		/**
		 * The meta object literal for the '<em><b>CONTEXT CORR comp2comp</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_COMP2COMP = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__comp2comp();

		/**
		 * The meta object literal for the '<em><b>CONTEXT CORR e2e1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_E2E1 = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__e2e1();

		/**
		 * The meta object literal for the '<em><b>CONTEXT CORR e2e2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_E2E2 = eINSTANCE.getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__e2e2();

		/**
		 * The meta object literal for the '<em><b>CREATE CORR r2r</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION_REF_TO_CONNECTION_REF_MARKER__CREATE_CORR_R2R = eINSTANCE.getConnectionRefToConnectionRef__Marker_CREATE__CORR__r2r();

		/**
		 * The meta object literal for the '{@link Copying.impl.FeatureToFeature__MarkerImpl <em>Feature To Feature Marker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.FeatureToFeature__MarkerImpl
		 * @see Copying.impl.CopyingPackageImpl#getFeatureToFeature__Marker()
		 * @generated
		 */
		EClass FEATURE_TO_FEATURE_MARKER = eINSTANCE.getFeatureToFeature__Marker();

		/**
		 * The meta object literal for the '<em><b>CONTEXT SRC component source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_TO_FEATURE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = eINSTANCE.getFeatureToFeature__Marker_CONTEXT__SRC__component_source();

		/**
		 * The meta object literal for the '<em><b>CREATE SRC feature source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_TO_FEATURE_MARKER__CREATE_SRC_FEATURE_SOURCE = eINSTANCE.getFeatureToFeature__Marker_CREATE__SRC__feature_source();

		/**
		 * The meta object literal for the '<em><b>CONTEXT TRG component target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_TO_FEATURE_MARKER__CONTEXT_TRG_COMPONENT_TARGET = eINSTANCE.getFeatureToFeature__Marker_CONTEXT__TRG__component_target();

		/**
		 * The meta object literal for the '<em><b>CREATE TRG feature target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_TO_FEATURE_MARKER__CREATE_TRG_FEATURE_TARGET = eINSTANCE.getFeatureToFeature__Marker_CREATE__TRG__feature_target();

		/**
		 * The meta object literal for the '<em><b>CONTEXT CORR c2c</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_TO_FEATURE_MARKER__CONTEXT_CORR_C2C = eINSTANCE.getFeatureToFeature__Marker_CONTEXT__CORR__c2c();

		/**
		 * The meta object literal for the '<em><b>CREATE CORR f2f2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_TO_FEATURE_MARKER__CREATE_CORR_F2F2 = eINSTANCE.getFeatureToFeature__Marker_CREATE__CORR__f2f2();

		/**
		 * The meta object literal for the '{@link Copying.impl.SystemToSystem__MarkerImpl <em>System To System Marker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Copying.impl.SystemToSystem__MarkerImpl
		 * @see Copying.impl.CopyingPackageImpl#getSystemToSystem__Marker()
		 * @generated
		 */
		EClass SYSTEM_TO_SYSTEM_MARKER = eINSTANCE.getSystemToSystem__Marker();

		/**
		 * The meta object literal for the '<em><b>CREATE SRC system source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_TO_SYSTEM_MARKER__CREATE_SRC_SYSTEM_SOURCE = eINSTANCE.getSystemToSystem__Marker_CREATE__SRC__system_source();

		/**
		 * The meta object literal for the '<em><b>CREATE TRG system target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_TO_SYSTEM_MARKER__CREATE_TRG_SYSTEM_TARGET = eINSTANCE.getSystemToSystem__Marker_CREATE__TRG__system_target();

		/**
		 * The meta object literal for the '<em><b>CREATE CORR s2s</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_TO_SYSTEM_MARKER__CREATE_CORR_S2S = eINSTANCE.getSystemToSystem__Marker_CREATE__CORR__s2s();

	}

} //CopyingPackage
