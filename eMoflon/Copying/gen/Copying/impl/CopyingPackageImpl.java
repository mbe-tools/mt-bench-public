/**
 */
package Copying.impl;

import Copying.CompToComp;
import Copying.ComponentToComponent__Marker;
import Copying.ConnectionInstanceToConnectionInstance__Marker;
import Copying.ConnectionRefToConnectionRef__Marker;
import Copying.ConnectionToConnection;
import Copying.CopyingFactory;
import Copying.CopyingPackage;
import Copying.End2End;
import Copying.FeatToFeat;
import Copying.FeatureToFeature__Marker;
import Copying.PortConnectionToSharedData;
import Copying.Reference2Reference;
import Copying.SystemToSystem;
import Copying.SystemToSystem__Marker;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.osate.aadl2.Aadl2Package;

import org.osate.aadl2.instance.InstancePackage;

import runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CopyingPackageImpl extends EPackageImpl implements CopyingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemToSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionToConnectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portConnectionToSharedDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reference2ReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass end2EndEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compToCompEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featToFeatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentToComponent__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionInstanceToConnectionInstance__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionRefToConnectionRef__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureToFeature__MarkerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemToSystem__MarkerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see Copying.CopyingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CopyingPackageImpl() {
		super(eNS_URI, CopyingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link CopyingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CopyingPackage init() {
		if (isInited) return (CopyingPackage)EPackage.Registry.INSTANCE.getEPackage(CopyingPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredCopyingPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		CopyingPackageImpl theCopyingPackage = registeredCopyingPackage instanceof CopyingPackageImpl ? (CopyingPackageImpl)registeredCopyingPackage : new CopyingPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		RuntimePackage.eINSTANCE.eClass();
		InstancePackage.eINSTANCE.eClass();
		Aadl2Package.eINSTANCE.eClass();

		// Create package meta-data objects
		theCopyingPackage.createPackageContents();

		// Initialize created meta-data
		theCopyingPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCopyingPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CopyingPackage.eNS_URI, theCopyingPackage);
		return theCopyingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemToSystem() {
		return systemToSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionToConnection() {
		return connectionToConnectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionToConnection_Source() {
		return (EReference)connectionToConnectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionToConnection_Target() {
		return (EReference)connectionToConnectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortConnectionToSharedData() {
		return portConnectionToSharedDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToSharedData_Source() {
		return (EReference)portConnectionToSharedDataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortConnectionToSharedData_Target() {
		return (EReference)portConnectionToSharedDataEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReference2Reference() {
		return reference2ReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReference2Reference_Source() {
		return (EReference)reference2ReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReference2Reference_Target() {
		return (EReference)reference2ReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnd2End() {
		return end2EndEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnd2End_Source() {
		return (EReference)end2EndEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnd2End_Target() {
		return (EReference)end2EndEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompToComp() {
		return compToCompEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatToFeat() {
		return featToFeatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentToComponent__Marker() {
		return componentToComponent__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CONTEXT__SRC__component_source() {
		return (EReference)componentToComponent__MarkerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CREATE__SRC__subcomponent_source() {
		return (EReference)componentToComponent__MarkerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CONTEXT__TRG__component_target() {
		return (EReference)componentToComponent__MarkerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CREATE__TRG__subcomponent_target() {
		return (EReference)componentToComponent__MarkerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CONTEXT__CORR__c2c() {
		return (EReference)componentToComponent__MarkerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentToComponent__Marker_CREATE__CORR__sub2sub() {
		return (EReference)componentToComponent__MarkerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionInstanceToConnectionInstance__Marker() {
		return connectionInstanceToConnectionInstance__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__component_source() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__SRC__connection_source() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_destination() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_source() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__component_target() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__TRG__connection_target() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_destination() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_source() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__CORR__c2c() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__comp2comp() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e1() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e2() {
		return (EReference)connectionInstanceToConnectionInstance__MarkerEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectionRefToConnectionRef__Marker() {
		return connectionRefToConnectionRef__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CREATE__SRC__connectionRef_source() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__connection_source() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_context() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_destination() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_source() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CREATE__TRG__connectionRef_target() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__connection_target() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_context() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_destination() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_source() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__c2c() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__comp2comp() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__e2e1() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__e2e2() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectionRefToConnectionRef__Marker_CREATE__CORR__r2r() {
		return (EReference)connectionRefToConnectionRef__MarkerEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureToFeature__Marker() {
		return featureToFeature__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CONTEXT__SRC__component_source() {
		return (EReference)featureToFeature__MarkerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CREATE__SRC__feature_source() {
		return (EReference)featureToFeature__MarkerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CONTEXT__TRG__component_target() {
		return (EReference)featureToFeature__MarkerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CREATE__TRG__feature_target() {
		return (EReference)featureToFeature__MarkerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CONTEXT__CORR__c2c() {
		return (EReference)featureToFeature__MarkerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureToFeature__Marker_CREATE__CORR__f2f2() {
		return (EReference)featureToFeature__MarkerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystemToSystem__Marker() {
		return systemToSystem__MarkerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemToSystem__Marker_CREATE__SRC__system_source() {
		return (EReference)systemToSystem__MarkerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemToSystem__Marker_CREATE__TRG__system_target() {
		return (EReference)systemToSystem__MarkerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystemToSystem__Marker_CREATE__CORR__s2s() {
		return (EReference)systemToSystem__MarkerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyingFactory getCopyingFactory() {
		return (CopyingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		systemToSystemEClass = createEClass(SYSTEM_TO_SYSTEM);

		connectionToConnectionEClass = createEClass(CONNECTION_TO_CONNECTION);
		createEReference(connectionToConnectionEClass, CONNECTION_TO_CONNECTION__SOURCE);
		createEReference(connectionToConnectionEClass, CONNECTION_TO_CONNECTION__TARGET);

		portConnectionToSharedDataEClass = createEClass(PORT_CONNECTION_TO_SHARED_DATA);
		createEReference(portConnectionToSharedDataEClass, PORT_CONNECTION_TO_SHARED_DATA__SOURCE);
		createEReference(portConnectionToSharedDataEClass, PORT_CONNECTION_TO_SHARED_DATA__TARGET);

		reference2ReferenceEClass = createEClass(REFERENCE2_REFERENCE);
		createEReference(reference2ReferenceEClass, REFERENCE2_REFERENCE__SOURCE);
		createEReference(reference2ReferenceEClass, REFERENCE2_REFERENCE__TARGET);

		end2EndEClass = createEClass(END2_END);
		createEReference(end2EndEClass, END2_END__SOURCE);
		createEReference(end2EndEClass, END2_END__TARGET);

		compToCompEClass = createEClass(COMP_TO_COMP);

		featToFeatEClass = createEClass(FEAT_TO_FEAT);

		componentToComponent__MarkerEClass = createEClass(COMPONENT_TO_COMPONENT_MARKER);
		createEReference(componentToComponent__MarkerEClass, COMPONENT_TO_COMPONENT_MARKER__CONTEXT_SRC_COMPONENT_SOURCE);
		createEReference(componentToComponent__MarkerEClass, COMPONENT_TO_COMPONENT_MARKER__CREATE_SRC_SUBCOMPONENT_SOURCE);
		createEReference(componentToComponent__MarkerEClass, COMPONENT_TO_COMPONENT_MARKER__CONTEXT_TRG_COMPONENT_TARGET);
		createEReference(componentToComponent__MarkerEClass, COMPONENT_TO_COMPONENT_MARKER__CREATE_TRG_SUBCOMPONENT_TARGET);
		createEReference(componentToComponent__MarkerEClass, COMPONENT_TO_COMPONENT_MARKER__CONTEXT_CORR_C2C);
		createEReference(componentToComponent__MarkerEClass, COMPONENT_TO_COMPONENT_MARKER__CREATE_CORR_SUB2SUB);

		connectionInstanceToConnectionInstance__MarkerEClass = createEClass(CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_SRC_CONNECTION_SOURCE);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SDESTINATION);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SSOURCE);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_COMPONENT_TARGET);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_TRG_CONNECTION_TARGET);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TDESTINATION);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TSOURCE);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_CORR_C2C);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_COMP2COMP);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E1);
		createEReference(connectionInstanceToConnectionInstance__MarkerEClass, CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E2);

		connectionRefToConnectionRef__MarkerEClass = createEClass(CONNECTION_REF_TO_CONNECTION_REF_MARKER);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_CONNECTION_SOURCE);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_SCONTEXT);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_SDESTINATION);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_SRC_SSOURCE);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CREATE_TRG_CONNECTION_REF_TARGET);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_CONNECTION_TARGET);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_TCONTEXT);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_TDESTINATION);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_TRG_TSOURCE);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_C2C);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_COMP2COMP);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_E2E1);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CONTEXT_CORR_E2E2);
		createEReference(connectionRefToConnectionRef__MarkerEClass, CONNECTION_REF_TO_CONNECTION_REF_MARKER__CREATE_CORR_R2R);

		featureToFeature__MarkerEClass = createEClass(FEATURE_TO_FEATURE_MARKER);
		createEReference(featureToFeature__MarkerEClass, FEATURE_TO_FEATURE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE);
		createEReference(featureToFeature__MarkerEClass, FEATURE_TO_FEATURE_MARKER__CREATE_SRC_FEATURE_SOURCE);
		createEReference(featureToFeature__MarkerEClass, FEATURE_TO_FEATURE_MARKER__CONTEXT_TRG_COMPONENT_TARGET);
		createEReference(featureToFeature__MarkerEClass, FEATURE_TO_FEATURE_MARKER__CREATE_TRG_FEATURE_TARGET);
		createEReference(featureToFeature__MarkerEClass, FEATURE_TO_FEATURE_MARKER__CONTEXT_CORR_C2C);
		createEReference(featureToFeature__MarkerEClass, FEATURE_TO_FEATURE_MARKER__CREATE_CORR_F2F2);

		systemToSystem__MarkerEClass = createEClass(SYSTEM_TO_SYSTEM_MARKER);
		createEReference(systemToSystem__MarkerEClass, SYSTEM_TO_SYSTEM_MARKER__CREATE_SRC_SYSTEM_SOURCE);
		createEReference(systemToSystem__MarkerEClass, SYSTEM_TO_SYSTEM_MARKER__CREATE_TRG_SYSTEM_TARGET);
		createEReference(systemToSystem__MarkerEClass, SYSTEM_TO_SYSTEM_MARKER__CREATE_CORR_S2S);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		InstancePackage theInstancePackage = (InstancePackage)EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI);
		RuntimePackage theRuntimePackage = (RuntimePackage)EPackage.Registry.INSTANCE.getEPackage(RuntimePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		systemToSystemEClass.getESuperTypes().add(this.getCompToComp());
		compToCompEClass.getESuperTypes().add(this.getEnd2End());
		featToFeatEClass.getESuperTypes().add(this.getEnd2End());
		componentToComponent__MarkerEClass.getESuperTypes().add(theRuntimePackage.getTGGRuleApplication());
		connectionInstanceToConnectionInstance__MarkerEClass.getESuperTypes().add(theRuntimePackage.getTGGRuleApplication());
		connectionRefToConnectionRef__MarkerEClass.getESuperTypes().add(theRuntimePackage.getTGGRuleApplication());
		featureToFeature__MarkerEClass.getESuperTypes().add(theRuntimePackage.getTGGRuleApplication());
		systemToSystem__MarkerEClass.getESuperTypes().add(theRuntimePackage.getTGGRuleApplication());

		// Initialize classes, features, and operations; add parameters
		initEClass(systemToSystemEClass, SystemToSystem.class, "SystemToSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(connectionToConnectionEClass, ConnectionToConnection.class, "ConnectionToConnection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnectionToConnection_Source(), theInstancePackage.getConnectionInstance(), null, "source", null, 0, 1, ConnectionToConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionToConnection_Target(), theInstancePackage.getConnectionInstance(), null, "target", null, 0, 1, ConnectionToConnection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portConnectionToSharedDataEClass, PortConnectionToSharedData.class, "PortConnectionToSharedData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPortConnectionToSharedData_Source(), theInstancePackage.getConnectionInstance(), null, "source", null, 0, 1, PortConnectionToSharedData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPortConnectionToSharedData_Target(), theInstancePackage.getInstanceObject(), null, "target", null, 0, 1, PortConnectionToSharedData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(reference2ReferenceEClass, Reference2Reference.class, "Reference2Reference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReference2Reference_Source(), theInstancePackage.getConnectionReference(), null, "source", null, 0, 1, Reference2Reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReference2Reference_Target(), theInstancePackage.getConnectionReference(), null, "target", null, 0, 1, Reference2Reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(end2EndEClass, End2End.class, "End2End", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnd2End_Source(), theInstancePackage.getConnectionInstanceEnd(), null, "source", null, 0, 1, End2End.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnd2End_Target(), theInstancePackage.getConnectionInstanceEnd(), null, "target", null, 0, 1, End2End.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(compToCompEClass, CompToComp.class, "CompToComp", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(featToFeatEClass, FeatToFeat.class, "FeatToFeat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(componentToComponent__MarkerEClass, ComponentToComponent__Marker.class, "ComponentToComponent__Marker", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentToComponent__Marker_CONTEXT__SRC__component_source(), theInstancePackage.getComponentInstance(), null, "CONTEXT__SRC__component_source", null, 1, 1, ComponentToComponent__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentToComponent__Marker_CREATE__SRC__subcomponent_source(), theInstancePackage.getComponentInstance(), null, "CREATE__SRC__subcomponent_source", null, 1, 1, ComponentToComponent__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentToComponent__Marker_CONTEXT__TRG__component_target(), theInstancePackage.getComponentInstance(), null, "CONTEXT__TRG__component_target", null, 1, 1, ComponentToComponent__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentToComponent__Marker_CREATE__TRG__subcomponent_target(), theInstancePackage.getComponentInstance(), null, "CREATE__TRG__subcomponent_target", null, 1, 1, ComponentToComponent__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentToComponent__Marker_CONTEXT__CORR__c2c(), this.getCompToComp(), null, "CONTEXT__CORR__c2c", null, 1, 1, ComponentToComponent__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentToComponent__Marker_CREATE__CORR__sub2sub(), this.getCompToComp(), null, "CREATE__CORR__sub2sub", null, 1, 1, ComponentToComponent__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connectionInstanceToConnectionInstance__MarkerEClass, ConnectionInstanceToConnectionInstance__Marker.class, "ConnectionInstanceToConnectionInstance__Marker", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__component_source(), theInstancePackage.getComponentInstance(), null, "CONTEXT__SRC__component_source", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CREATE__SRC__connection_source(), theInstancePackage.getConnectionInstance(), null, "CREATE__SRC__connection_source", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_destination(), theInstancePackage.getConnectionInstanceEnd(), null, "CONTEXT__SRC__s_destination", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_source(), theInstancePackage.getConnectionInstanceEnd(), null, "CONTEXT__SRC__s_source", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__component_target(), theInstancePackage.getComponentInstance(), null, "CONTEXT__TRG__component_target", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CREATE__TRG__connection_target(), theInstancePackage.getConnectionInstance(), null, "CREATE__TRG__connection_target", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_destination(), theInstancePackage.getConnectionInstanceEnd(), null, "CONTEXT__TRG__t_destination", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_source(), theInstancePackage.getConnectionInstanceEnd(), null, "CONTEXT__TRG__t_source", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CREATE__CORR__c2c(), this.getConnectionToConnection(), null, "CREATE__CORR__c2c", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__comp2comp(), this.getCompToComp(), null, "CONTEXT__CORR__comp2comp", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e1(), this.getEnd2End(), null, "CONTEXT__CORR__e2e1", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e2(), this.getEnd2End(), null, "CONTEXT__CORR__e2e2", null, 1, 1, ConnectionInstanceToConnectionInstance__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connectionRefToConnectionRef__MarkerEClass, ConnectionRefToConnectionRef__Marker.class, "ConnectionRefToConnectionRef__Marker", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnectionRefToConnectionRef__Marker_CREATE__SRC__connectionRef_source(), theInstancePackage.getConnectionReference(), null, "CREATE__SRC__connectionRef_source", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__connection_source(), theInstancePackage.getConnectionInstance(), null, "CONTEXT__SRC__connection_source", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_context(), theInstancePackage.getComponentInstance(), null, "CONTEXT__SRC__s_context", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_destination(), theInstancePackage.getConnectionInstanceEnd(), null, "CONTEXT__SRC__s_destination", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__SRC__s_source(), theInstancePackage.getConnectionInstanceEnd(), null, "CONTEXT__SRC__s_source", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CREATE__TRG__connectionRef_target(), theInstancePackage.getConnectionReference(), null, "CREATE__TRG__connectionRef_target", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__connection_target(), theInstancePackage.getConnectionInstance(), null, "CONTEXT__TRG__connection_target", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_context(), theInstancePackage.getComponentInstance(), null, "CONTEXT__TRG__t_context", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_destination(), theInstancePackage.getConnectionInstanceEnd(), null, "CONTEXT__TRG__t_destination", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__TRG__t_source(), theInstancePackage.getConnectionInstanceEnd(), null, "CONTEXT__TRG__t_source", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__c2c(), this.getConnectionToConnection(), null, "CONTEXT__CORR__c2c", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__comp2comp(), this.getCompToComp(), null, "CONTEXT__CORR__comp2comp", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__e2e1(), this.getEnd2End(), null, "CONTEXT__CORR__e2e1", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CONTEXT__CORR__e2e2(), this.getEnd2End(), null, "CONTEXT__CORR__e2e2", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConnectionRefToConnectionRef__Marker_CREATE__CORR__r2r(), this.getReference2Reference(), null, "CREATE__CORR__r2r", null, 1, 1, ConnectionRefToConnectionRef__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureToFeature__MarkerEClass, FeatureToFeature__Marker.class, "FeatureToFeature__Marker", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureToFeature__Marker_CONTEXT__SRC__component_source(), theInstancePackage.getComponentInstance(), null, "CONTEXT__SRC__component_source", null, 1, 1, FeatureToFeature__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureToFeature__Marker_CREATE__SRC__feature_source(), theInstancePackage.getFeatureInstance(), null, "CREATE__SRC__feature_source", null, 1, 1, FeatureToFeature__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureToFeature__Marker_CONTEXT__TRG__component_target(), theInstancePackage.getComponentInstance(), null, "CONTEXT__TRG__component_target", null, 1, 1, FeatureToFeature__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureToFeature__Marker_CREATE__TRG__feature_target(), theInstancePackage.getFeatureInstance(), null, "CREATE__TRG__feature_target", null, 1, 1, FeatureToFeature__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureToFeature__Marker_CONTEXT__CORR__c2c(), this.getCompToComp(), null, "CONTEXT__CORR__c2c", null, 1, 1, FeatureToFeature__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureToFeature__Marker_CREATE__CORR__f2f2(), this.getFeatToFeat(), null, "CREATE__CORR__f2f2", null, 1, 1, FeatureToFeature__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(systemToSystem__MarkerEClass, SystemToSystem__Marker.class, "SystemToSystem__Marker", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSystemToSystem__Marker_CREATE__SRC__system_source(), theInstancePackage.getSystemInstance(), null, "CREATE__SRC__system_source", null, 1, 1, SystemToSystem__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystemToSystem__Marker_CREATE__TRG__system_target(), theInstancePackage.getSystemInstance(), null, "CREATE__TRG__system_target", null, 1, 1, SystemToSystem__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystemToSystem__Marker_CREATE__CORR__s2s(), this.getSystemToSystem(), null, "CREATE__CORR__s2s", null, 1, 1, SystemToSystem__Marker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //CopyingPackageImpl
