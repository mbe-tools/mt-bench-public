/**
 */
package Copying.impl;

import Copying.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CopyingFactoryImpl extends EFactoryImpl implements CopyingFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CopyingFactory init() {
		try {
			CopyingFactory theCopyingFactory = (CopyingFactory)EPackage.Registry.INSTANCE.getEFactory(CopyingPackage.eNS_URI);
			if (theCopyingFactory != null) {
				return theCopyingFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CopyingFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyingFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CopyingPackage.SYSTEM_TO_SYSTEM: return createSystemToSystem();
			case CopyingPackage.CONNECTION_TO_CONNECTION: return createConnectionToConnection();
			case CopyingPackage.PORT_CONNECTION_TO_SHARED_DATA: return createPortConnectionToSharedData();
			case CopyingPackage.REFERENCE2_REFERENCE: return createReference2Reference();
			case CopyingPackage.END2_END: return createEnd2End();
			case CopyingPackage.COMP_TO_COMP: return createCompToComp();
			case CopyingPackage.FEAT_TO_FEAT: return createFeatToFeat();
			case CopyingPackage.COMPONENT_TO_COMPONENT_MARKER: return createComponentToComponent__Marker();
			case CopyingPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER: return createConnectionInstanceToConnectionInstance__Marker();
			case CopyingPackage.CONNECTION_REF_TO_CONNECTION_REF_MARKER: return createConnectionRefToConnectionRef__Marker();
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER: return createFeatureToFeature__Marker();
			case CopyingPackage.SYSTEM_TO_SYSTEM_MARKER: return createSystemToSystem__Marker();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemToSystem createSystemToSystem() {
		SystemToSystemImpl systemToSystem = new SystemToSystemImpl();
		return systemToSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionToConnection createConnectionToConnection() {
		ConnectionToConnectionImpl connectionToConnection = new ConnectionToConnectionImpl();
		return connectionToConnection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData createPortConnectionToSharedData() {
		PortConnectionToSharedDataImpl portConnectionToSharedData = new PortConnectionToSharedDataImpl();
		return portConnectionToSharedData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference2Reference createReference2Reference() {
		Reference2ReferenceImpl reference2Reference = new Reference2ReferenceImpl();
		return reference2Reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public End2End createEnd2End() {
		End2EndImpl end2End = new End2EndImpl();
		return end2End;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp createCompToComp() {
		CompToCompImpl compToComp = new CompToCompImpl();
		return compToComp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat createFeatToFeat() {
		FeatToFeatImpl featToFeat = new FeatToFeatImpl();
		return featToFeat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentToComponent__Marker createComponentToComponent__Marker() {
		ComponentToComponent__MarkerImpl componentToComponent__Marker = new ComponentToComponent__MarkerImpl();
		return componentToComponent__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstanceToConnectionInstance__Marker createConnectionInstanceToConnectionInstance__Marker() {
		ConnectionInstanceToConnectionInstance__MarkerImpl connectionInstanceToConnectionInstance__Marker = new ConnectionInstanceToConnectionInstance__MarkerImpl();
		return connectionInstanceToConnectionInstance__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionRefToConnectionRef__Marker createConnectionRefToConnectionRef__Marker() {
		ConnectionRefToConnectionRef__MarkerImpl connectionRefToConnectionRef__Marker = new ConnectionRefToConnectionRef__MarkerImpl();
		return connectionRefToConnectionRef__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureToFeature__Marker createFeatureToFeature__Marker() {
		FeatureToFeature__MarkerImpl featureToFeature__Marker = new FeatureToFeature__MarkerImpl();
		return featureToFeature__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemToSystem__Marker createSystemToSystem__Marker() {
		SystemToSystem__MarkerImpl systemToSystem__Marker = new SystemToSystem__MarkerImpl();
		return systemToSystem__Marker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyingPackage getCopyingPackage() {
		return (CopyingPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CopyingPackage getPackage() {
		return CopyingPackage.eINSTANCE;
	}

} //CopyingFactoryImpl
