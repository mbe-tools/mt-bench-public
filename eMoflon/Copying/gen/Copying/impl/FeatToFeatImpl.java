/**
 */
package Copying.impl;

import Copying.CopyingPackage;
import Copying.FeatToFeat;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feat To Feat</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FeatToFeatImpl extends End2EndImpl implements FeatToFeat {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatToFeatImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyingPackage.Literals.FEAT_TO_FEAT;
	}

} //FeatToFeatImpl
