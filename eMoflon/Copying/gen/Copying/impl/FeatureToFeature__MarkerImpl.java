/**
 */
package Copying.impl;

import Copying.CompToComp;
import Copying.CopyingPackage;
import Copying.FeatToFeat;
import Copying.FeatureToFeature__Marker;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;

import runtime.impl.TGGRuleApplicationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature To Feature Marker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Copying.impl.FeatureToFeature__MarkerImpl#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}</li>
 *   <li>{@link Copying.impl.FeatureToFeature__MarkerImpl#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}</li>
 *   <li>{@link Copying.impl.FeatureToFeature__MarkerImpl#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}</li>
 *   <li>{@link Copying.impl.FeatureToFeature__MarkerImpl#getCREATE__TRG__feature_target <em>CREATE TRG feature target</em>}</li>
 *   <li>{@link Copying.impl.FeatureToFeature__MarkerImpl#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}</li>
 *   <li>{@link Copying.impl.FeatureToFeature__MarkerImpl#getCREATE__CORR__f2f2 <em>CREATE CORR f2f2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureToFeature__MarkerImpl extends TGGRuleApplicationImpl implements FeatureToFeature__Marker {
	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__component_source() <em>CONTEXT SRC component source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__component_source()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__component_source;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__feature_source() <em>CREATE SRC feature source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__feature_source()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__SRC__feature_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__component_target() <em>CONTEXT TRG component target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__component_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__component_target;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__feature_target() <em>CREATE TRG feature target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__feature_target()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__TRG__feature_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__c2c() <em>CONTEXT CORR c2c</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__c2c()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__c2c;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__f2f2() <em>CREATE CORR f2f2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__f2f2()
	 * @generated
	 * @ordered
	 */
	protected FeatToFeat creatE__CORR__f2f2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureToFeature__MarkerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyingPackage.Literals.FEATURE_TO_FEATURE_MARKER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__component_source() {
		if (contexT__SRC__component_source != null && contexT__SRC__component_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__component_source = (InternalEObject)contexT__SRC__component_source;
			contexT__SRC__component_source = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__component_source);
			if (contexT__SRC__component_source != oldCONTEXT__SRC__component_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE, oldCONTEXT__SRC__component_source, contexT__SRC__component_source));
			}
		}
		return contexT__SRC__component_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__component_source() {
		return contexT__SRC__component_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__component_source(ComponentInstance newCONTEXT__SRC__component_source) {
		ComponentInstance oldCONTEXT__SRC__component_source = contexT__SRC__component_source;
		contexT__SRC__component_source = newCONTEXT__SRC__component_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE, oldCONTEXT__SRC__component_source, contexT__SRC__component_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__SRC__feature_source() {
		if (creatE__SRC__feature_source != null && creatE__SRC__feature_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__feature_source = (InternalEObject)creatE__SRC__feature_source;
			creatE__SRC__feature_source = (FeatureInstance)eResolveProxy(oldCREATE__SRC__feature_source);
			if (creatE__SRC__feature_source != oldCREATE__SRC__feature_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_SRC_FEATURE_SOURCE, oldCREATE__SRC__feature_source, creatE__SRC__feature_source));
			}
		}
		return creatE__SRC__feature_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__SRC__feature_source() {
		return creatE__SRC__feature_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__feature_source(FeatureInstance newCREATE__SRC__feature_source) {
		FeatureInstance oldCREATE__SRC__feature_source = creatE__SRC__feature_source;
		creatE__SRC__feature_source = newCREATE__SRC__feature_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_SRC_FEATURE_SOURCE, oldCREATE__SRC__feature_source, creatE__SRC__feature_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__component_target() {
		if (contexT__TRG__component_target != null && contexT__TRG__component_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__component_target = (InternalEObject)contexT__TRG__component_target;
			contexT__TRG__component_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__component_target);
			if (contexT__TRG__component_target != oldCONTEXT__TRG__component_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_TRG_COMPONENT_TARGET, oldCONTEXT__TRG__component_target, contexT__TRG__component_target));
			}
		}
		return contexT__TRG__component_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__component_target() {
		return contexT__TRG__component_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__component_target(ComponentInstance newCONTEXT__TRG__component_target) {
		ComponentInstance oldCONTEXT__TRG__component_target = contexT__TRG__component_target;
		contexT__TRG__component_target = newCONTEXT__TRG__component_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_TRG_COMPONENT_TARGET, oldCONTEXT__TRG__component_target, contexT__TRG__component_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__TRG__feature_target() {
		if (creatE__TRG__feature_target != null && creatE__TRG__feature_target.eIsProxy()) {
			InternalEObject oldCREATE__TRG__feature_target = (InternalEObject)creatE__TRG__feature_target;
			creatE__TRG__feature_target = (FeatureInstance)eResolveProxy(oldCREATE__TRG__feature_target);
			if (creatE__TRG__feature_target != oldCREATE__TRG__feature_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_TRG_FEATURE_TARGET, oldCREATE__TRG__feature_target, creatE__TRG__feature_target));
			}
		}
		return creatE__TRG__feature_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__TRG__feature_target() {
		return creatE__TRG__feature_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__feature_target(FeatureInstance newCREATE__TRG__feature_target) {
		FeatureInstance oldCREATE__TRG__feature_target = creatE__TRG__feature_target;
		creatE__TRG__feature_target = newCREATE__TRG__feature_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_TRG_FEATURE_TARGET, oldCREATE__TRG__feature_target, creatE__TRG__feature_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__c2c() {
		if (contexT__CORR__c2c != null && contexT__CORR__c2c.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__c2c = (InternalEObject)contexT__CORR__c2c;
			contexT__CORR__c2c = (CompToComp)eResolveProxy(oldCONTEXT__CORR__c2c);
			if (contexT__CORR__c2c != oldCONTEXT__CORR__c2c) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_CORR_C2C, oldCONTEXT__CORR__c2c, contexT__CORR__c2c));
			}
		}
		return contexT__CORR__c2c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__c2c() {
		return contexT__CORR__c2c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__c2c(CompToComp newCONTEXT__CORR__c2c) {
		CompToComp oldCONTEXT__CORR__c2c = contexT__CORR__c2c;
		contexT__CORR__c2c = newCONTEXT__CORR__c2c;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_CORR_C2C, oldCONTEXT__CORR__c2c, contexT__CORR__c2c));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat getCREATE__CORR__f2f2() {
		if (creatE__CORR__f2f2 != null && creatE__CORR__f2f2.eIsProxy()) {
			InternalEObject oldCREATE__CORR__f2f2 = (InternalEObject)creatE__CORR__f2f2;
			creatE__CORR__f2f2 = (FeatToFeat)eResolveProxy(oldCREATE__CORR__f2f2);
			if (creatE__CORR__f2f2 != oldCREATE__CORR__f2f2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_CORR_F2F2, oldCREATE__CORR__f2f2, creatE__CORR__f2f2));
			}
		}
		return creatE__CORR__f2f2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat basicGetCREATE__CORR__f2f2() {
		return creatE__CORR__f2f2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__f2f2(FeatToFeat newCREATE__CORR__f2f2) {
		FeatToFeat oldCREATE__CORR__f2f2 = creatE__CORR__f2f2;
		creatE__CORR__f2f2 = newCREATE__CORR__f2f2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_CORR_F2F2, oldCREATE__CORR__f2f2, creatE__CORR__f2f2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				if (resolve) return getCONTEXT__SRC__component_source();
				return basicGetCONTEXT__SRC__component_source();
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_SRC_FEATURE_SOURCE:
				if (resolve) return getCREATE__SRC__feature_source();
				return basicGetCREATE__SRC__feature_source();
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				if (resolve) return getCONTEXT__TRG__component_target();
				return basicGetCONTEXT__TRG__component_target();
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_TRG_FEATURE_TARGET:
				if (resolve) return getCREATE__TRG__feature_target();
				return basicGetCREATE__TRG__feature_target();
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_CORR_C2C:
				if (resolve) return getCONTEXT__CORR__c2c();
				return basicGetCONTEXT__CORR__c2c();
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_CORR_F2F2:
				if (resolve) return getCREATE__CORR__f2f2();
				return basicGetCREATE__CORR__f2f2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				setCONTEXT__SRC__component_source((ComponentInstance)newValue);
				return;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_SRC_FEATURE_SOURCE:
				setCREATE__SRC__feature_source((FeatureInstance)newValue);
				return;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				setCONTEXT__TRG__component_target((ComponentInstance)newValue);
				return;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_TRG_FEATURE_TARGET:
				setCREATE__TRG__feature_target((FeatureInstance)newValue);
				return;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_CORR_C2C:
				setCONTEXT__CORR__c2c((CompToComp)newValue);
				return;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_CORR_F2F2:
				setCREATE__CORR__f2f2((FeatToFeat)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				setCONTEXT__SRC__component_source((ComponentInstance)null);
				return;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_SRC_FEATURE_SOURCE:
				setCREATE__SRC__feature_source((FeatureInstance)null);
				return;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				setCONTEXT__TRG__component_target((ComponentInstance)null);
				return;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_TRG_FEATURE_TARGET:
				setCREATE__TRG__feature_target((FeatureInstance)null);
				return;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_CORR_C2C:
				setCONTEXT__CORR__c2c((CompToComp)null);
				return;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_CORR_F2F2:
				setCREATE__CORR__f2f2((FeatToFeat)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				return contexT__SRC__component_source != null;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_SRC_FEATURE_SOURCE:
				return creatE__SRC__feature_source != null;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				return contexT__TRG__component_target != null;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_TRG_FEATURE_TARGET:
				return creatE__TRG__feature_target != null;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CONTEXT_CORR_C2C:
				return contexT__CORR__c2c != null;
			case CopyingPackage.FEATURE_TO_FEATURE_MARKER__CREATE_CORR_F2F2:
				return creatE__CORR__f2f2 != null;
		}
		return super.eIsSet(featureID);
	}

} //FeatureToFeature__MarkerImpl
