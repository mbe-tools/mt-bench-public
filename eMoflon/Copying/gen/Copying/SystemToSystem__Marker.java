/**
 */
package Copying;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.instance.SystemInstance;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System To System Marker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Copying.SystemToSystem__Marker#getCREATE__SRC__system_source <em>CREATE SRC system source</em>}</li>
 *   <li>{@link Copying.SystemToSystem__Marker#getCREATE__TRG__system_target <em>CREATE TRG system target</em>}</li>
 *   <li>{@link Copying.SystemToSystem__Marker#getCREATE__CORR__s2s <em>CREATE CORR s2s</em>}</li>
 * </ul>
 *
 * @see Copying.CopyingPackage#getSystemToSystem__Marker()
 * @model
 * @generated
 */
public interface SystemToSystem__Marker extends EObject, TGGRuleApplication {
	/**
	 * Returns the value of the '<em><b>CREATE SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC system source</em>' reference.
	 * @see #setCREATE__SRC__system_source(SystemInstance)
	 * @see Copying.CopyingPackage#getSystemToSystem__Marker_CREATE__SRC__system_source()
	 * @model required="true"
	 * @generated
	 */
	SystemInstance getCREATE__SRC__system_source();

	/**
	 * Sets the value of the '{@link Copying.SystemToSystem__Marker#getCREATE__SRC__system_source <em>CREATE SRC system source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC system source</em>' reference.
	 * @see #getCREATE__SRC__system_source()
	 * @generated
	 */
	void setCREATE__SRC__system_source(SystemInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG system target</em>' reference.
	 * @see #setCREATE__TRG__system_target(SystemInstance)
	 * @see Copying.CopyingPackage#getSystemToSystem__Marker_CREATE__TRG__system_target()
	 * @model required="true"
	 * @generated
	 */
	SystemInstance getCREATE__TRG__system_target();

	/**
	 * Sets the value of the '{@link Copying.SystemToSystem__Marker#getCREATE__TRG__system_target <em>CREATE TRG system target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG system target</em>' reference.
	 * @see #getCREATE__TRG__system_target()
	 * @generated
	 */
	void setCREATE__TRG__system_target(SystemInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR s2s</em>' reference.
	 * @see #setCREATE__CORR__s2s(SystemToSystem)
	 * @see Copying.CopyingPackage#getSystemToSystem__Marker_CREATE__CORR__s2s()
	 * @model required="true"
	 * @generated
	 */
	SystemToSystem getCREATE__CORR__s2s();

	/**
	 * Sets the value of the '{@link Copying.SystemToSystem__Marker#getCREATE__CORR__s2s <em>CREATE CORR s2s</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR s2s</em>' reference.
	 * @see #getCREATE__CORR__s2s()
	 * @generated
	 */
	void setCREATE__CORR__s2s(SystemToSystem value);

} // SystemToSystem__Marker
