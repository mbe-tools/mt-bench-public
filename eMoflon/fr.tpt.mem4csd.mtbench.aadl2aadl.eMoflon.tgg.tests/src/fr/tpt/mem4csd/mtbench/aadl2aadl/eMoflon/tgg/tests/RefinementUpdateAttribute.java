package fr.tpt.mem4csd.mtbench.aadl2aadl.eMoflon.tgg.tests;

import java.io.IOException;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class RefinementUpdateAttribute extends AbstracteMoflonScenario {

	public RefinementUpdateAttribute() {
		super();
	}

	@Override
	AbstractEMoflonTransformation getSpecification(String inputModelName) throws IOException {
		return new RefinementEMoflonTransformation(inputModelName, BenchmarkConstants.REFINEMENT_UPDATE_ATT_SUFFIX);

	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.UPDATE_ATT;
	}

}
