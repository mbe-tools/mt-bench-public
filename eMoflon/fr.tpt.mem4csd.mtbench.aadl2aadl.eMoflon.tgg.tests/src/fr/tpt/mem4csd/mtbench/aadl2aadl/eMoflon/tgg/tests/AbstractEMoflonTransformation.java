package fr.tpt.mem4csd.mtbench.aadl2aadl.eMoflon.tgg.tests;

import java.io.IOException;

import org.emoflon.ibex.tgg.compiler.defaults.IRegistrationHelper;
import org.emoflon.ibex.tgg.operational.strategies.modules.TGGResourceHandler;
//import org.emoflon.ibex.tgg.operational.strategies.sync.INITIAL_FWD;
import org.emoflon.ibex.tgg.operational.strategies.sync.SYNC;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.ResourcesUtil;

//use sync.INITIAL_FWD for better performances, but only with Batch scenario
public abstract class AbstractEMoflonTransformation extends SYNC {

	private static final String GEN_DIR = "gen/";

	private static final String DEFAULT_EXT = "xmi";

	public AbstractEMoflonTransformation(	final String inputModelName, 
											final String outputModelSuffix, 
											final IRegistrationHelper registrationHelper )
	throws IOException {
		super(registrationHelper.createIbexOptions().resourceHandler(new TGGResourceHandler() {
			
			@Override
			public void saveModels() throws IOException {
				target.save(null);
				corr.save(null);
				protocol.save(null);
			}

			@Override
			public void loadModels() throws IOException {
				source = loadResource( ResourcesUtil.createAadlInstanceInputURI( inputModelName ).toString() );
				target = createResource( ResourcesUtil.createAadlInstanceOutputURI( inputModelName + outputModelSuffix ).toString() );
				corr = createResource( ResourcesUtil.createOutputURI( GEN_DIR + "corr_" + inputModelName, DEFAULT_EXT ).toString() );
				protocol = createResource( ResourcesUtil.createOutputURI( GEN_DIR + "protocol_" + inputModelName,  DEFAULT_EXT ).toString() );
			}
		} ) );
	}
}
