package fr.tpt.mem4csd.mtbench.aadl2aadl.eMoflon.tgg.tests;

import java.io.IOException;

import org.emoflon.ibex.tgg.run.tgg.config.DemoclesRegistrationHelper;

public class RefinementEMoflonTransformation extends AbstractEMoflonTransformation {

	public RefinementEMoflonTransformation(	final String inputModelName, 
											final String outputModelSuffix )
	throws IOException {
		super( inputModelName, outputModelSuffix, new DemoclesRegistrationHelper() );
	}
}
