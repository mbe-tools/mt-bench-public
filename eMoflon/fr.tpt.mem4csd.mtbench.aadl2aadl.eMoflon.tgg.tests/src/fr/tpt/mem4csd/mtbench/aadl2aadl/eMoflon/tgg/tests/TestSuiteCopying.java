package fr.tpt.mem4csd.mtbench.aadl2aadl.eMoflon.tgg.tests;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.AbstractScenario;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.TestSuiteAbstract;

public class TestSuiteCopying extends TestSuiteAbstract {

	public TestSuiteCopying() {
		super();
	}

	public static void main(String[] args) {
		runAllTests(args, new TestSuiteCopying());
	}

	@Override
	protected String getTestSuiteName() {
		return "eMoflon-Copying";
	}

	@Override
	protected AbstractScenario getScenario(ScenarioKind scenarioKind) {
		switch (scenarioKind) {
		case ADDITION:
			return new CopyingAddition();

		case UPDATE_ATT:
			return new CopyingUpdateAttribute();

		case UPDATE_REF:
			return new CopyingUpdateReference();

		case DELETE:
			return new CopyingDeletion();

		case BATCH:
			return new CopyingBatch();

		default:
			throw new IllegalArgumentException("Unknown scenario kind: '" + scenarioKind + "'.");
		}
	}
}