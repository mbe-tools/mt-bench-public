package fr.tpt.mem4csd.mtbench.aadl2aadl.eMoflon.tgg.tests;

import java.io.IOException;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class RefinementAddition extends AbstracteMoflonScenario {

	public RefinementAddition() {
		super();
	}

	@Override
	AbstractEMoflonTransformation getSpecification(String inputModelName) throws IOException {
		return new RefinementEMoflonTransformation(inputModelName, BenchmarkConstants.REFINEMENT_ADD_SUFFIX);
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.ADDITION;
	}


}
