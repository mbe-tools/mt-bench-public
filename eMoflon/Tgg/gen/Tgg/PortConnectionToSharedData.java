/**
 */
package Tgg;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.InstanceObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Connection To Shared Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Tgg.PortConnectionToSharedData#getSource <em>Source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToSharedData#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see Tgg.TggPackage#getPortConnectionToSharedData()
 * @model
 * @generated
 */
public interface PortConnectionToSharedData extends EObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(ConnectionInstance)
	 * @see Tgg.TggPackage#getPortConnectionToSharedData_Source()
	 * @model
	 * @generated
	 */
	ConnectionInstance getSource();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToSharedData#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(InstanceObject)
	 * @see Tgg.TggPackage#getPortConnectionToSharedData_Target()
	 * @model
	 * @generated
	 */
	InstanceObject getTarget();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToSharedData#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(InstanceObject value);

} // PortConnectionToSharedData
