/**
 */
package Tgg;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.SystemInstance;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Connection For Device Source Marker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__component_destination <em>CONTEXT SRC component destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__component_destination_target <em>CONTEXT TRG component destination target</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__component_source_target <em>CONTEXT TRG component source target</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__connection_target <em>CREATE TRG connection target</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__feature_destination_target <em>CREATE TRG feature destination target</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__feature_source_target <em>CREATE TRG feature source target</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__c2c1 <em>CONTEXT CORR c2c1</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__c2c2 <em>CONTEXT CORR c2c2</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__co2co <em>CREATE CORR co2co</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__feature2feature1 <em>CREATE CORR feature2feature1</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__feature2feature2 <em>CREATE CORR feature2feature2</em>}</li>
 *   <li>{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}</li>
 * </ul>
 *
 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker()
 * @model
 * @generated
 */
public interface PortConnectionForDeviceSource__Marker extends TGGRuleApplication {
	/**
	 * Returns the value of the '<em><b>CONTEXT SRC component destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC component destination</em>' reference.
	 * @see #setCONTEXT__SRC__component_destination(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CONTEXT__SRC__component_destination()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__component_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__component_destination <em>CONTEXT SRC component destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC component destination</em>' reference.
	 * @see #getCONTEXT__SRC__component_destination()
	 * @generated
	 */
	void setCONTEXT__SRC__component_destination(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC component source</em>' reference.
	 * @see #setCONTEXT__SRC__component_source(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CONTEXT__SRC__component_source()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__component_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC component source</em>' reference.
	 * @see #getCONTEXT__SRC__component_source()
	 * @generated
	 */
	void setCONTEXT__SRC__component_source(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC connection source</em>' reference.
	 * @see #setCREATE__SRC__connection_source(ConnectionInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CREATE__SRC__connection_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCREATE__SRC__connection_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC connection source</em>' reference.
	 * @see #getCREATE__SRC__connection_source()
	 * @generated
	 */
	void setCREATE__SRC__connection_source(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC feature destination</em>' reference.
	 * @see #setCREATE__SRC__feature_destination(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CREATE__SRC__feature_destination()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__SRC__feature_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC feature destination</em>' reference.
	 * @see #getCREATE__SRC__feature_destination()
	 * @generated
	 */
	void setCREATE__SRC__feature_destination(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC feature source</em>' reference.
	 * @see #setCREATE__SRC__feature_source(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CREATE__SRC__feature_source()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__SRC__feature_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC feature source</em>' reference.
	 * @see #getCREATE__SRC__feature_source()
	 * @generated
	 */
	void setCREATE__SRC__feature_source(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC system source</em>' reference.
	 * @see #setCONTEXT__SRC__system_source(SystemInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CONTEXT__SRC__system_source()
	 * @model required="true"
	 * @generated
	 */
	SystemInstance getCONTEXT__SRC__system_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC system source</em>' reference.
	 * @see #getCONTEXT__SRC__system_source()
	 * @generated
	 */
	void setCONTEXT__SRC__system_source(SystemInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG component destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG component destination target</em>' reference.
	 * @see #setCONTEXT__TRG__component_destination_target(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CONTEXT__TRG__component_destination_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__component_destination_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__component_destination_target <em>CONTEXT TRG component destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG component destination target</em>' reference.
	 * @see #getCONTEXT__TRG__component_destination_target()
	 * @generated
	 */
	void setCONTEXT__TRG__component_destination_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG component source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG component source target</em>' reference.
	 * @see #setCONTEXT__TRG__component_source_target(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CONTEXT__TRG__component_source_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__component_source_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__component_source_target <em>CONTEXT TRG component source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG component source target</em>' reference.
	 * @see #getCONTEXT__TRG__component_source_target()
	 * @generated
	 */
	void setCONTEXT__TRG__component_source_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG connection target</em>' reference.
	 * @see #setCREATE__TRG__connection_target(ConnectionInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CREATE__TRG__connection_target()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCREATE__TRG__connection_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__connection_target <em>CREATE TRG connection target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG connection target</em>' reference.
	 * @see #getCREATE__TRG__connection_target()
	 * @generated
	 */
	void setCREATE__TRG__connection_target(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG feature destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG feature destination target</em>' reference.
	 * @see #setCREATE__TRG__feature_destination_target(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CREATE__TRG__feature_destination_target()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__TRG__feature_destination_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__feature_destination_target <em>CREATE TRG feature destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG feature destination target</em>' reference.
	 * @see #getCREATE__TRG__feature_destination_target()
	 * @generated
	 */
	void setCREATE__TRG__feature_destination_target(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG feature source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG feature source target</em>' reference.
	 * @see #setCREATE__TRG__feature_source_target(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CREATE__TRG__feature_source_target()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__TRG__feature_source_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__feature_source_target <em>CREATE TRG feature source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG feature source target</em>' reference.
	 * @see #getCREATE__TRG__feature_source_target()
	 * @generated
	 */
	void setCREATE__TRG__feature_source_target(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG system target</em>' reference.
	 * @see #setCONTEXT__TRG__system_target(SystemInstance)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CONTEXT__TRG__system_target()
	 * @model required="true"
	 * @generated
	 */
	SystemInstance getCONTEXT__TRG__system_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG system target</em>' reference.
	 * @see #getCONTEXT__TRG__system_target()
	 * @generated
	 */
	void setCONTEXT__TRG__system_target(SystemInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR c2c1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR c2c1</em>' reference.
	 * @see #setCONTEXT__CORR__c2c1(CompToComp)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CONTEXT__CORR__c2c1()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__c2c1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__c2c1 <em>CONTEXT CORR c2c1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR c2c1</em>' reference.
	 * @see #getCONTEXT__CORR__c2c1()
	 * @generated
	 */
	void setCONTEXT__CORR__c2c1(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR c2c2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR c2c2</em>' reference.
	 * @see #setCONTEXT__CORR__c2c2(CompToComp)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CONTEXT__CORR__c2c2()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__c2c2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__c2c2 <em>CONTEXT CORR c2c2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR c2c2</em>' reference.
	 * @see #getCONTEXT__CORR__c2c2()
	 * @generated
	 */
	void setCONTEXT__CORR__c2c2(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR co2co</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR co2co</em>' reference.
	 * @see #setCREATE__CORR__co2co(ConnectionToConnection)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CREATE__CORR__co2co()
	 * @model required="true"
	 * @generated
	 */
	ConnectionToConnection getCREATE__CORR__co2co();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__co2co <em>CREATE CORR co2co</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR co2co</em>' reference.
	 * @see #getCREATE__CORR__co2co()
	 * @generated
	 */
	void setCREATE__CORR__co2co(ConnectionToConnection value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR feature2feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR feature2feature1</em>' reference.
	 * @see #setCREATE__CORR__feature2feature1(FeatToFeat)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CREATE__CORR__feature2feature1()
	 * @model required="true"
	 * @generated
	 */
	FeatToFeat getCREATE__CORR__feature2feature1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__feature2feature1 <em>CREATE CORR feature2feature1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR feature2feature1</em>' reference.
	 * @see #getCREATE__CORR__feature2feature1()
	 * @generated
	 */
	void setCREATE__CORR__feature2feature1(FeatToFeat value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR feature2feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR feature2feature2</em>' reference.
	 * @see #setCREATE__CORR__feature2feature2(FeatToFeat)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CREATE__CORR__feature2feature2()
	 * @model required="true"
	 * @generated
	 */
	FeatToFeat getCREATE__CORR__feature2feature2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__feature2feature2 <em>CREATE CORR feature2feature2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR feature2feature2</em>' reference.
	 * @see #getCREATE__CORR__feature2feature2()
	 * @generated
	 */
	void setCREATE__CORR__feature2feature2(FeatToFeat value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR s2s</em>' reference.
	 * @see #setCONTEXT__CORR__s2s(SystemToSystem)
	 * @see Tgg.TggPackage#getPortConnectionForDeviceSource__Marker_CONTEXT__CORR__s2s()
	 * @model required="true"
	 * @generated
	 */
	SystemToSystem getCONTEXT__CORR__s2s();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR s2s</em>' reference.
	 * @see #getCONTEXT__CORR__s2s()
	 * @generated
	 */
	void setCONTEXT__CORR__s2s(SystemToSystem value);

} // PortConnectionForDeviceSource__Marker
