/**
 */
package Tgg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System To System</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Tgg.TggPackage#getSystemToSystem()
 * @model
 * @generated
 */
public interface SystemToSystem extends CompToComp {
} // SystemToSystem
