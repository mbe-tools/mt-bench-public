/**
 */
package Tgg;

import org.osate.aadl2.instance.ComponentInstance;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component To Component Marker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Tgg.ComponentToComponent__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}</li>
 *   <li>{@link Tgg.ComponentToComponent__Marker#getCREATE__SRC__subcomponent_source <em>CREATE SRC subcomponent source</em>}</li>
 *   <li>{@link Tgg.ComponentToComponent__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}</li>
 *   <li>{@link Tgg.ComponentToComponent__Marker#getCREATE__TRG__subcomponent_target <em>CREATE TRG subcomponent target</em>}</li>
 *   <li>{@link Tgg.ComponentToComponent__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}</li>
 *   <li>{@link Tgg.ComponentToComponent__Marker#getCREATE__CORR__sub2sub <em>CREATE CORR sub2sub</em>}</li>
 * </ul>
 *
 * @see Tgg.TggPackage#getComponentToComponent__Marker()
 * @model
 * @generated
 */
public interface ComponentToComponent__Marker extends TGGRuleApplication {
	/**
	 * Returns the value of the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC component source</em>' reference.
	 * @see #setCONTEXT__SRC__component_source(ComponentInstance)
	 * @see Tgg.TggPackage#getComponentToComponent__Marker_CONTEXT__SRC__component_source()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__component_source();

	/**
	 * Sets the value of the '{@link Tgg.ComponentToComponent__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC component source</em>' reference.
	 * @see #getCONTEXT__SRC__component_source()
	 * @generated
	 */
	void setCONTEXT__SRC__component_source(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC subcomponent source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC subcomponent source</em>' reference.
	 * @see #setCREATE__SRC__subcomponent_source(ComponentInstance)
	 * @see Tgg.TggPackage#getComponentToComponent__Marker_CREATE__SRC__subcomponent_source()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCREATE__SRC__subcomponent_source();

	/**
	 * Sets the value of the '{@link Tgg.ComponentToComponent__Marker#getCREATE__SRC__subcomponent_source <em>CREATE SRC subcomponent source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC subcomponent source</em>' reference.
	 * @see #getCREATE__SRC__subcomponent_source()
	 * @generated
	 */
	void setCREATE__SRC__subcomponent_source(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG component target</em>' reference.
	 * @see #setCONTEXT__TRG__component_target(ComponentInstance)
	 * @see Tgg.TggPackage#getComponentToComponent__Marker_CONTEXT__TRG__component_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__component_target();

	/**
	 * Sets the value of the '{@link Tgg.ComponentToComponent__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG component target</em>' reference.
	 * @see #getCONTEXT__TRG__component_target()
	 * @generated
	 */
	void setCONTEXT__TRG__component_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG subcomponent target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG subcomponent target</em>' reference.
	 * @see #setCREATE__TRG__subcomponent_target(ComponentInstance)
	 * @see Tgg.TggPackage#getComponentToComponent__Marker_CREATE__TRG__subcomponent_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCREATE__TRG__subcomponent_target();

	/**
	 * Sets the value of the '{@link Tgg.ComponentToComponent__Marker#getCREATE__TRG__subcomponent_target <em>CREATE TRG subcomponent target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG subcomponent target</em>' reference.
	 * @see #getCREATE__TRG__subcomponent_target()
	 * @generated
	 */
	void setCREATE__TRG__subcomponent_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR c2c</em>' reference.
	 * @see #setCONTEXT__CORR__c2c(CompToComp)
	 * @see Tgg.TggPackage#getComponentToComponent__Marker_CONTEXT__CORR__c2c()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__c2c();

	/**
	 * Sets the value of the '{@link Tgg.ComponentToComponent__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR c2c</em>' reference.
	 * @see #getCONTEXT__CORR__c2c()
	 * @generated
	 */
	void setCONTEXT__CORR__c2c(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR sub2sub</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR sub2sub</em>' reference.
	 * @see #setCREATE__CORR__sub2sub(CompToComp)
	 * @see Tgg.TggPackage#getComponentToComponent__Marker_CREATE__CORR__sub2sub()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCREATE__CORR__sub2sub();

	/**
	 * Sets the value of the '{@link Tgg.ComponentToComponent__Marker#getCREATE__CORR__sub2sub <em>CREATE CORR sub2sub</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR sub2sub</em>' reference.
	 * @see #getCREATE__CORR__sub2sub()
	 * @generated
	 */
	void setCREATE__CORR__sub2sub(CompToComp value);

} // ComponentToComponent__Marker
