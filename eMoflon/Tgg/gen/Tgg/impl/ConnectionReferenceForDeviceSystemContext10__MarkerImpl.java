/**
 */
package Tgg.impl;

import Tgg.CompToComp;
import Tgg.ConnectionReferenceForDeviceSystemContext10__Marker;
import Tgg.ConnectionToConnection;
import Tgg.End2End;
import Tgg.Reference2Reference;
import Tgg.TggPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;

import runtime.impl.TGGRuleApplicationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connection Reference For Device System Context10 Marker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCREATE__SRC__connectionRef_source <em>CREATE SRC connection Ref source</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCONTEXT__SRC__connection_source <em>CONTEXT SRC connection source</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCONTEXT__SRC__s_context <em>CONTEXT SRC scontext</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCREATE__SRC__s_destination <em>CREATE SRC sdestination</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCREATE__TRG__connectionRef_target <em>CREATE TRG connection Ref target</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCONTEXT__TRG__connection_target <em>CONTEXT TRG connection target</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCONTEXT__TRG__t_context <em>CONTEXT TRG tcontext</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCREATE__TRG__t_destination <em>CREATE TRG tdestination</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCREATE__CORR__e2e1 <em>CREATE CORR e2e1</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl#getCREATE__CORR__r2r <em>CREATE CORR r2r</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConnectionReferenceForDeviceSystemContext10__MarkerImpl extends TGGRuleApplicationImpl implements ConnectionReferenceForDeviceSystemContext10__Marker {
	/**
	 * The cached value of the '{@link #getCREATE__SRC__connectionRef_source() <em>CREATE SRC connection Ref source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__connectionRef_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionReference creatE__SRC__connectionRef_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__connection_source() <em>CONTEXT SRC connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__connection_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance contexT__SRC__connection_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__s_context() <em>CONTEXT SRC scontext</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__s_context()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__s_context;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__s_destination() <em>CREATE SRC sdestination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__s_destination()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__SRC__s_destination;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__s_source() <em>CONTEXT SRC ssource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__s_source()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance contexT__SRC__s_source;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__connectionRef_target() <em>CREATE TRG connection Ref target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__connectionRef_target()
	 * @generated
	 * @ordered
	 */
	protected ConnectionReference creatE__TRG__connectionRef_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__connection_target() <em>CONTEXT TRG connection target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__connection_target()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance contexT__TRG__connection_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__t_context() <em>CONTEXT TRG tcontext</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__t_context()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__t_context;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__t_destination() <em>CREATE TRG tdestination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__t_destination()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__TRG__t_destination;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__t_source() <em>CONTEXT TRG tsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__t_source()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance contexT__TRG__t_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__c2c() <em>CONTEXT CORR c2c</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__c2c()
	 * @generated
	 * @ordered
	 */
	protected ConnectionToConnection contexT__CORR__c2c;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__comp2comp() <em>CONTEXT CORR comp2comp</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__comp2comp()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__comp2comp;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__e2e1() <em>CREATE CORR e2e1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__e2e1()
	 * @generated
	 * @ordered
	 */
	protected End2End creatE__CORR__e2e1;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__e2e2() <em>CONTEXT CORR e2e2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__e2e2()
	 * @generated
	 * @ordered
	 */
	protected End2End contexT__CORR__e2e2;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__r2r() <em>CREATE CORR r2r</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__r2r()
	 * @generated
	 * @ordered
	 */
	protected Reference2Reference creatE__CORR__r2r;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionReferenceForDeviceSystemContext10__MarkerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.eINSTANCE.getConnectionReferenceForDeviceSystemContext10__Marker();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference getCREATE__SRC__connectionRef_source() {
		if (creatE__SRC__connectionRef_source != null && creatE__SRC__connectionRef_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__connectionRef_source = (InternalEObject)creatE__SRC__connectionRef_source;
			creatE__SRC__connectionRef_source = (ConnectionReference)eResolveProxy(oldCREATE__SRC__connectionRef_source);
			if (creatE__SRC__connectionRef_source != oldCREATE__SRC__connectionRef_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE, oldCREATE__SRC__connectionRef_source, creatE__SRC__connectionRef_source));
			}
		}
		return creatE__SRC__connectionRef_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference basicGetCREATE__SRC__connectionRef_source() {
		return creatE__SRC__connectionRef_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__connectionRef_source(ConnectionReference newCREATE__SRC__connectionRef_source) {
		ConnectionReference oldCREATE__SRC__connectionRef_source = creatE__SRC__connectionRef_source;
		creatE__SRC__connectionRef_source = newCREATE__SRC__connectionRef_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE, oldCREATE__SRC__connectionRef_source, creatE__SRC__connectionRef_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCONTEXT__SRC__connection_source() {
		if (contexT__SRC__connection_source != null && contexT__SRC__connection_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__connection_source = (InternalEObject)contexT__SRC__connection_source;
			contexT__SRC__connection_source = (ConnectionInstance)eResolveProxy(oldCONTEXT__SRC__connection_source);
			if (contexT__SRC__connection_source != oldCONTEXT__SRC__connection_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_CONNECTION_SOURCE, oldCONTEXT__SRC__connection_source, contexT__SRC__connection_source));
			}
		}
		return contexT__SRC__connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCONTEXT__SRC__connection_source() {
		return contexT__SRC__connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__connection_source(ConnectionInstance newCONTEXT__SRC__connection_source) {
		ConnectionInstance oldCONTEXT__SRC__connection_source = contexT__SRC__connection_source;
		contexT__SRC__connection_source = newCONTEXT__SRC__connection_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_CONNECTION_SOURCE, oldCONTEXT__SRC__connection_source, contexT__SRC__connection_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__s_context() {
		if (contexT__SRC__s_context != null && contexT__SRC__s_context.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__s_context = (InternalEObject)contexT__SRC__s_context;
			contexT__SRC__s_context = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__s_context);
			if (contexT__SRC__s_context != oldCONTEXT__SRC__s_context) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SCONTEXT, oldCONTEXT__SRC__s_context, contexT__SRC__s_context));
			}
		}
		return contexT__SRC__s_context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__s_context() {
		return contexT__SRC__s_context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__s_context(ComponentInstance newCONTEXT__SRC__s_context) {
		ComponentInstance oldCONTEXT__SRC__s_context = contexT__SRC__s_context;
		contexT__SRC__s_context = newCONTEXT__SRC__s_context;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SCONTEXT, oldCONTEXT__SRC__s_context, contexT__SRC__s_context));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__SRC__s_destination() {
		if (creatE__SRC__s_destination != null && creatE__SRC__s_destination.eIsProxy()) {
			InternalEObject oldCREATE__SRC__s_destination = (InternalEObject)creatE__SRC__s_destination;
			creatE__SRC__s_destination = (FeatureInstance)eResolveProxy(oldCREATE__SRC__s_destination);
			if (creatE__SRC__s_destination != oldCREATE__SRC__s_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_SDESTINATION, oldCREATE__SRC__s_destination, creatE__SRC__s_destination));
			}
		}
		return creatE__SRC__s_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__SRC__s_destination() {
		return creatE__SRC__s_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__s_destination(FeatureInstance newCREATE__SRC__s_destination) {
		FeatureInstance oldCREATE__SRC__s_destination = creatE__SRC__s_destination;
		creatE__SRC__s_destination = newCREATE__SRC__s_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_SDESTINATION, oldCREATE__SRC__s_destination, creatE__SRC__s_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCONTEXT__SRC__s_source() {
		if (contexT__SRC__s_source != null && contexT__SRC__s_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__s_source = (InternalEObject)contexT__SRC__s_source;
			contexT__SRC__s_source = (FeatureInstance)eResolveProxy(oldCONTEXT__SRC__s_source);
			if (contexT__SRC__s_source != oldCONTEXT__SRC__s_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SSOURCE, oldCONTEXT__SRC__s_source, contexT__SRC__s_source));
			}
		}
		return contexT__SRC__s_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCONTEXT__SRC__s_source() {
		return contexT__SRC__s_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__s_source(FeatureInstance newCONTEXT__SRC__s_source) {
		FeatureInstance oldCONTEXT__SRC__s_source = contexT__SRC__s_source;
		contexT__SRC__s_source = newCONTEXT__SRC__s_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SSOURCE, oldCONTEXT__SRC__s_source, contexT__SRC__s_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference getCREATE__TRG__connectionRef_target() {
		if (creatE__TRG__connectionRef_target != null && creatE__TRG__connectionRef_target.eIsProxy()) {
			InternalEObject oldCREATE__TRG__connectionRef_target = (InternalEObject)creatE__TRG__connectionRef_target;
			creatE__TRG__connectionRef_target = (ConnectionReference)eResolveProxy(oldCREATE__TRG__connectionRef_target);
			if (creatE__TRG__connectionRef_target != oldCREATE__TRG__connectionRef_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_CONNECTION_REF_TARGET, oldCREATE__TRG__connectionRef_target, creatE__TRG__connectionRef_target));
			}
		}
		return creatE__TRG__connectionRef_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference basicGetCREATE__TRG__connectionRef_target() {
		return creatE__TRG__connectionRef_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__connectionRef_target(ConnectionReference newCREATE__TRG__connectionRef_target) {
		ConnectionReference oldCREATE__TRG__connectionRef_target = creatE__TRG__connectionRef_target;
		creatE__TRG__connectionRef_target = newCREATE__TRG__connectionRef_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_CONNECTION_REF_TARGET, oldCREATE__TRG__connectionRef_target, creatE__TRG__connectionRef_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCONTEXT__TRG__connection_target() {
		if (contexT__TRG__connection_target != null && contexT__TRG__connection_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__connection_target = (InternalEObject)contexT__TRG__connection_target;
			contexT__TRG__connection_target = (ConnectionInstance)eResolveProxy(oldCONTEXT__TRG__connection_target);
			if (contexT__TRG__connection_target != oldCONTEXT__TRG__connection_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_CONNECTION_TARGET, oldCONTEXT__TRG__connection_target, contexT__TRG__connection_target));
			}
		}
		return contexT__TRG__connection_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCONTEXT__TRG__connection_target() {
		return contexT__TRG__connection_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__connection_target(ConnectionInstance newCONTEXT__TRG__connection_target) {
		ConnectionInstance oldCONTEXT__TRG__connection_target = contexT__TRG__connection_target;
		contexT__TRG__connection_target = newCONTEXT__TRG__connection_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_CONNECTION_TARGET, oldCONTEXT__TRG__connection_target, contexT__TRG__connection_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__t_context() {
		if (contexT__TRG__t_context != null && contexT__TRG__t_context.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__t_context = (InternalEObject)contexT__TRG__t_context;
			contexT__TRG__t_context = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__t_context);
			if (contexT__TRG__t_context != oldCONTEXT__TRG__t_context) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TCONTEXT, oldCONTEXT__TRG__t_context, contexT__TRG__t_context));
			}
		}
		return contexT__TRG__t_context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__t_context() {
		return contexT__TRG__t_context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__t_context(ComponentInstance newCONTEXT__TRG__t_context) {
		ComponentInstance oldCONTEXT__TRG__t_context = contexT__TRG__t_context;
		contexT__TRG__t_context = newCONTEXT__TRG__t_context;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TCONTEXT, oldCONTEXT__TRG__t_context, contexT__TRG__t_context));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__TRG__t_destination() {
		if (creatE__TRG__t_destination != null && creatE__TRG__t_destination.eIsProxy()) {
			InternalEObject oldCREATE__TRG__t_destination = (InternalEObject)creatE__TRG__t_destination;
			creatE__TRG__t_destination = (FeatureInstance)eResolveProxy(oldCREATE__TRG__t_destination);
			if (creatE__TRG__t_destination != oldCREATE__TRG__t_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_TDESTINATION, oldCREATE__TRG__t_destination, creatE__TRG__t_destination));
			}
		}
		return creatE__TRG__t_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__TRG__t_destination() {
		return creatE__TRG__t_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__t_destination(FeatureInstance newCREATE__TRG__t_destination) {
		FeatureInstance oldCREATE__TRG__t_destination = creatE__TRG__t_destination;
		creatE__TRG__t_destination = newCREATE__TRG__t_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_TDESTINATION, oldCREATE__TRG__t_destination, creatE__TRG__t_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCONTEXT__TRG__t_source() {
		if (contexT__TRG__t_source != null && contexT__TRG__t_source.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__t_source = (InternalEObject)contexT__TRG__t_source;
			contexT__TRG__t_source = (FeatureInstance)eResolveProxy(oldCONTEXT__TRG__t_source);
			if (contexT__TRG__t_source != oldCONTEXT__TRG__t_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TSOURCE, oldCONTEXT__TRG__t_source, contexT__TRG__t_source));
			}
		}
		return contexT__TRG__t_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCONTEXT__TRG__t_source() {
		return contexT__TRG__t_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__t_source(FeatureInstance newCONTEXT__TRG__t_source) {
		FeatureInstance oldCONTEXT__TRG__t_source = contexT__TRG__t_source;
		contexT__TRG__t_source = newCONTEXT__TRG__t_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TSOURCE, oldCONTEXT__TRG__t_source, contexT__TRG__t_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionToConnection getCONTEXT__CORR__c2c() {
		if (contexT__CORR__c2c != null && contexT__CORR__c2c.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__c2c = (InternalEObject)contexT__CORR__c2c;
			contexT__CORR__c2c = (ConnectionToConnection)eResolveProxy(oldCONTEXT__CORR__c2c);
			if (contexT__CORR__c2c != oldCONTEXT__CORR__c2c) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_C2C, oldCONTEXT__CORR__c2c, contexT__CORR__c2c));
			}
		}
		return contexT__CORR__c2c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionToConnection basicGetCONTEXT__CORR__c2c() {
		return contexT__CORR__c2c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__c2c(ConnectionToConnection newCONTEXT__CORR__c2c) {
		ConnectionToConnection oldCONTEXT__CORR__c2c = contexT__CORR__c2c;
		contexT__CORR__c2c = newCONTEXT__CORR__c2c;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_C2C, oldCONTEXT__CORR__c2c, contexT__CORR__c2c));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__comp2comp() {
		if (contexT__CORR__comp2comp != null && contexT__CORR__comp2comp.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__comp2comp = (InternalEObject)contexT__CORR__comp2comp;
			contexT__CORR__comp2comp = (CompToComp)eResolveProxy(oldCONTEXT__CORR__comp2comp);
			if (contexT__CORR__comp2comp != oldCONTEXT__CORR__comp2comp) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_COMP2COMP, oldCONTEXT__CORR__comp2comp, contexT__CORR__comp2comp));
			}
		}
		return contexT__CORR__comp2comp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__comp2comp() {
		return contexT__CORR__comp2comp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__comp2comp(CompToComp newCONTEXT__CORR__comp2comp) {
		CompToComp oldCONTEXT__CORR__comp2comp = contexT__CORR__comp2comp;
		contexT__CORR__comp2comp = newCONTEXT__CORR__comp2comp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_COMP2COMP, oldCONTEXT__CORR__comp2comp, contexT__CORR__comp2comp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public End2End getCREATE__CORR__e2e1() {
		if (creatE__CORR__e2e1 != null && creatE__CORR__e2e1.eIsProxy()) {
			InternalEObject oldCREATE__CORR__e2e1 = (InternalEObject)creatE__CORR__e2e1;
			creatE__CORR__e2e1 = (End2End)eResolveProxy(oldCREATE__CORR__e2e1);
			if (creatE__CORR__e2e1 != oldCREATE__CORR__e2e1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_E2E1, oldCREATE__CORR__e2e1, creatE__CORR__e2e1));
			}
		}
		return creatE__CORR__e2e1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public End2End basicGetCREATE__CORR__e2e1() {
		return creatE__CORR__e2e1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__e2e1(End2End newCREATE__CORR__e2e1) {
		End2End oldCREATE__CORR__e2e1 = creatE__CORR__e2e1;
		creatE__CORR__e2e1 = newCREATE__CORR__e2e1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_E2E1, oldCREATE__CORR__e2e1, creatE__CORR__e2e1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public End2End getCONTEXT__CORR__e2e2() {
		if (contexT__CORR__e2e2 != null && contexT__CORR__e2e2.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__e2e2 = (InternalEObject)contexT__CORR__e2e2;
			contexT__CORR__e2e2 = (End2End)eResolveProxy(oldCONTEXT__CORR__e2e2);
			if (contexT__CORR__e2e2 != oldCONTEXT__CORR__e2e2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_E2E2, oldCONTEXT__CORR__e2e2, contexT__CORR__e2e2));
			}
		}
		return contexT__CORR__e2e2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public End2End basicGetCONTEXT__CORR__e2e2() {
		return contexT__CORR__e2e2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__e2e2(End2End newCONTEXT__CORR__e2e2) {
		End2End oldCONTEXT__CORR__e2e2 = contexT__CORR__e2e2;
		contexT__CORR__e2e2 = newCONTEXT__CORR__e2e2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_E2E2, oldCONTEXT__CORR__e2e2, contexT__CORR__e2e2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference2Reference getCREATE__CORR__r2r() {
		if (creatE__CORR__r2r != null && creatE__CORR__r2r.eIsProxy()) {
			InternalEObject oldCREATE__CORR__r2r = (InternalEObject)creatE__CORR__r2r;
			creatE__CORR__r2r = (Reference2Reference)eResolveProxy(oldCREATE__CORR__r2r);
			if (creatE__CORR__r2r != oldCREATE__CORR__r2r) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_R2R, oldCREATE__CORR__r2r, creatE__CORR__r2r));
			}
		}
		return creatE__CORR__r2r;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference2Reference basicGetCREATE__CORR__r2r() {
		return creatE__CORR__r2r;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__r2r(Reference2Reference newCREATE__CORR__r2r) {
		Reference2Reference oldCREATE__CORR__r2r = creatE__CORR__r2r;
		creatE__CORR__r2r = newCREATE__CORR__r2r;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_R2R, oldCREATE__CORR__r2r, creatE__CORR__r2r));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE:
				if (resolve) return getCREATE__SRC__connectionRef_source();
				return basicGetCREATE__SRC__connectionRef_source();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_CONNECTION_SOURCE:
				if (resolve) return getCONTEXT__SRC__connection_source();
				return basicGetCONTEXT__SRC__connection_source();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SCONTEXT:
				if (resolve) return getCONTEXT__SRC__s_context();
				return basicGetCONTEXT__SRC__s_context();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_SDESTINATION:
				if (resolve) return getCREATE__SRC__s_destination();
				return basicGetCREATE__SRC__s_destination();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SSOURCE:
				if (resolve) return getCONTEXT__SRC__s_source();
				return basicGetCONTEXT__SRC__s_source();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_CONNECTION_REF_TARGET:
				if (resolve) return getCREATE__TRG__connectionRef_target();
				return basicGetCREATE__TRG__connectionRef_target();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_CONNECTION_TARGET:
				if (resolve) return getCONTEXT__TRG__connection_target();
				return basicGetCONTEXT__TRG__connection_target();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TCONTEXT:
				if (resolve) return getCONTEXT__TRG__t_context();
				return basicGetCONTEXT__TRG__t_context();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_TDESTINATION:
				if (resolve) return getCREATE__TRG__t_destination();
				return basicGetCREATE__TRG__t_destination();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TSOURCE:
				if (resolve) return getCONTEXT__TRG__t_source();
				return basicGetCONTEXT__TRG__t_source();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_C2C:
				if (resolve) return getCONTEXT__CORR__c2c();
				return basicGetCONTEXT__CORR__c2c();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_COMP2COMP:
				if (resolve) return getCONTEXT__CORR__comp2comp();
				return basicGetCONTEXT__CORR__comp2comp();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_E2E1:
				if (resolve) return getCREATE__CORR__e2e1();
				return basicGetCREATE__CORR__e2e1();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_E2E2:
				if (resolve) return getCONTEXT__CORR__e2e2();
				return basicGetCONTEXT__CORR__e2e2();
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_R2R:
				if (resolve) return getCREATE__CORR__r2r();
				return basicGetCREATE__CORR__r2r();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE:
				setCREATE__SRC__connectionRef_source((ConnectionReference)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_CONNECTION_SOURCE:
				setCONTEXT__SRC__connection_source((ConnectionInstance)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SCONTEXT:
				setCONTEXT__SRC__s_context((ComponentInstance)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_SDESTINATION:
				setCREATE__SRC__s_destination((FeatureInstance)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SSOURCE:
				setCONTEXT__SRC__s_source((FeatureInstance)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_CONNECTION_REF_TARGET:
				setCREATE__TRG__connectionRef_target((ConnectionReference)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_CONNECTION_TARGET:
				setCONTEXT__TRG__connection_target((ConnectionInstance)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TCONTEXT:
				setCONTEXT__TRG__t_context((ComponentInstance)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_TDESTINATION:
				setCREATE__TRG__t_destination((FeatureInstance)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TSOURCE:
				setCONTEXT__TRG__t_source((FeatureInstance)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_C2C:
				setCONTEXT__CORR__c2c((ConnectionToConnection)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_COMP2COMP:
				setCONTEXT__CORR__comp2comp((CompToComp)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_E2E1:
				setCREATE__CORR__e2e1((End2End)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_E2E2:
				setCONTEXT__CORR__e2e2((End2End)newValue);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_R2R:
				setCREATE__CORR__r2r((Reference2Reference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE:
				setCREATE__SRC__connectionRef_source((ConnectionReference)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_CONNECTION_SOURCE:
				setCONTEXT__SRC__connection_source((ConnectionInstance)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SCONTEXT:
				setCONTEXT__SRC__s_context((ComponentInstance)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_SDESTINATION:
				setCREATE__SRC__s_destination((FeatureInstance)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SSOURCE:
				setCONTEXT__SRC__s_source((FeatureInstance)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_CONNECTION_REF_TARGET:
				setCREATE__TRG__connectionRef_target((ConnectionReference)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_CONNECTION_TARGET:
				setCONTEXT__TRG__connection_target((ConnectionInstance)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TCONTEXT:
				setCONTEXT__TRG__t_context((ComponentInstance)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_TDESTINATION:
				setCREATE__TRG__t_destination((FeatureInstance)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TSOURCE:
				setCONTEXT__TRG__t_source((FeatureInstance)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_C2C:
				setCONTEXT__CORR__c2c((ConnectionToConnection)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_COMP2COMP:
				setCONTEXT__CORR__comp2comp((CompToComp)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_E2E1:
				setCREATE__CORR__e2e1((End2End)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_E2E2:
				setCONTEXT__CORR__e2e2((End2End)null);
				return;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_R2R:
				setCREATE__CORR__r2r((Reference2Reference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE:
				return creatE__SRC__connectionRef_source != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_CONNECTION_SOURCE:
				return contexT__SRC__connection_source != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SCONTEXT:
				return contexT__SRC__s_context != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_SDESTINATION:
				return creatE__SRC__s_destination != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SSOURCE:
				return contexT__SRC__s_source != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_CONNECTION_REF_TARGET:
				return creatE__TRG__connectionRef_target != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_CONNECTION_TARGET:
				return contexT__TRG__connection_target != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TCONTEXT:
				return contexT__TRG__t_context != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_TDESTINATION:
				return creatE__TRG__t_destination != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TSOURCE:
				return contexT__TRG__t_source != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_C2C:
				return contexT__CORR__c2c != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_COMP2COMP:
				return contexT__CORR__comp2comp != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_E2E1:
				return creatE__CORR__e2e1 != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_E2E2:
				return contexT__CORR__e2e2 != null;
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_R2R:
				return creatE__CORR__r2r != null;
		}
		return super.eIsSet(featureID);
	}

} //ConnectionReferenceForDeviceSystemContext10__MarkerImpl
