/**
 */
package Tgg.impl;

import Tgg.CompToComp;
import Tgg.FeatToFeat;
import Tgg.PortConnectionToDataAccessAtSystem11__Marker;
import Tgg.PortConnectionToSharedData;
import Tgg.SystemToSystem;
import Tgg.TggPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.SystemInstance;

import runtime.impl.TGGRuleApplicationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Connection To Data Access At System11 Marker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__SRC__feature_destination <em>CONTEXT SRC feature destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__SRC__feature_source <em>CONTEXT SRC feature source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__SRC__process_destination <em>CONTEXT SRC process destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__SRC__process_source <em>CONTEXT SRC process source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__SRC__ref_connection_destination <em>CREATE SRC ref connection destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__SRC__thread_destination <em>CONTEXT SRC thread destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__SRC__thread_source <em>CONTEXT SRC thread source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__TRG__data_destination <em>CREATE TRG data destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__TRG__data_source <em>CREATE TRG data source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__TRG__dataaccess_destination <em>CREATE TRG dataaccess destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__TRG__dataaccess_source <em>CREATE TRG dataaccess source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__TRG__feature_destination_target <em>CONTEXT TRG feature destination target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__TRG__feature_source_target <em>CONTEXT TRG feature source target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__TRG__process_destination_target <em>CONTEXT TRG process destination target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__TRG__process_source_target <em>CONTEXT TRG process source target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__TRG__ref_destination <em>CREATE TRG ref destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__TRG__ref_source <em>CREATE TRG ref source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__TRG__thread_destination_target <em>CONTEXT TRG thread destination target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__TRG__thread_source_target <em>CONTEXT TRG thread source target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__CORR__feature2feature1 <em>CONTEXT CORR feature2feature1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__CORR__feature2feature2 <em>CONTEXT CORR feature2feature2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__CORR__p2d1 <em>CREATE CORR p2d1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__CORR__p2d2 <em>CREATE CORR p2d2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__CORR__process2process1 <em>CONTEXT CORR process2process1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__CORR__process2process2 <em>CONTEXT CORR process2process2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__CORR__r2r1 <em>CREATE CORR r2r1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCREATE__CORR__r2r2 <em>CREATE CORR r2r2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__CORR__thread2thread1 <em>CONTEXT CORR thread2thread1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl#getCONTEXT__CORR__thread2thread2 <em>CONTEXT CORR thread2thread2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortConnectionToDataAccessAtSystem11__MarkerImpl extends TGGRuleApplicationImpl implements PortConnectionToDataAccessAtSystem11__Marker {
	/**
	 * The cached value of the '{@link #getCREATE__SRC__connection_source() <em>CREATE SRC connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__connection_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance creatE__SRC__connection_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__feature_destination() <em>CONTEXT SRC feature destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__feature_destination()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance contexT__SRC__feature_destination;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__feature_source() <em>CONTEXT SRC feature source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__feature_source()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance contexT__SRC__feature_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__process_destination() <em>CONTEXT SRC process destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__process_destination()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__process_destination;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__process_source() <em>CONTEXT SRC process source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__process_source()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__process_source;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__ref_connection_destination() <em>CREATE SRC ref connection destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__ref_connection_destination()
	 * @generated
	 * @ordered
	 */
	protected ConnectionReference creatE__SRC__ref_connection_destination;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__ref_connection_source() <em>CREATE SRC ref connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__ref_connection_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionReference creatE__SRC__ref_connection_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__system_source() <em>CONTEXT SRC system source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__system_source()
	 * @generated
	 * @ordered
	 */
	protected SystemInstance contexT__SRC__system_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__thread_destination() <em>CONTEXT SRC thread destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__thread_destination()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__thread_destination;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__thread_source() <em>CONTEXT SRC thread source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__thread_source()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__thread_source;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__data_destination() <em>CREATE TRG data destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__data_destination()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance creatE__TRG__data_destination;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__data_source() <em>CREATE TRG data source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__data_source()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance creatE__TRG__data_source;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__dataaccess_destination() <em>CREATE TRG dataaccess destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__dataaccess_destination()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance creatE__TRG__dataaccess_destination;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__dataaccess_source() <em>CREATE TRG dataaccess source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__dataaccess_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance creatE__TRG__dataaccess_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__feature_destination_target() <em>CONTEXT TRG feature destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__feature_destination_target()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance contexT__TRG__feature_destination_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__feature_source_target() <em>CONTEXT TRG feature source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__feature_source_target()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance contexT__TRG__feature_source_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__process_destination_target() <em>CONTEXT TRG process destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__process_destination_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__process_destination_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__process_source_target() <em>CONTEXT TRG process source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__process_source_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__process_source_target;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__ref_destination() <em>CREATE TRG ref destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__ref_destination()
	 * @generated
	 * @ordered
	 */
	protected ConnectionReference creatE__TRG__ref_destination;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__ref_source() <em>CREATE TRG ref source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__ref_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionReference creatE__TRG__ref_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__system_target() <em>CONTEXT TRG system target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__system_target()
	 * @generated
	 * @ordered
	 */
	protected SystemInstance contexT__TRG__system_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__thread_destination_target() <em>CONTEXT TRG thread destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__thread_destination_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__thread_destination_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__thread_source_target() <em>CONTEXT TRG thread source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__thread_source_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__thread_source_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__feature2feature1() <em>CONTEXT CORR feature2feature1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__feature2feature1()
	 * @generated
	 * @ordered
	 */
	protected FeatToFeat contexT__CORR__feature2feature1;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__feature2feature2() <em>CONTEXT CORR feature2feature2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__feature2feature2()
	 * @generated
	 * @ordered
	 */
	protected FeatToFeat contexT__CORR__feature2feature2;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__p2a1() <em>CREATE CORR p2a1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__p2a1()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__p2a1;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__p2a2() <em>CREATE CORR p2a2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__p2a2()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__p2a2;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__p2d1() <em>CREATE CORR p2d1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__p2d1()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__p2d1;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__p2d2() <em>CREATE CORR p2d2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__p2d2()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__p2d2;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__process2process1() <em>CONTEXT CORR process2process1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__process2process1()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__process2process1;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__process2process2() <em>CONTEXT CORR process2process2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__process2process2()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__process2process2;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__r2r1() <em>CREATE CORR r2r1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__r2r1()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__r2r1;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__r2r2() <em>CREATE CORR r2r2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__r2r2()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__r2r2;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__s2s() <em>CONTEXT CORR s2s</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__s2s()
	 * @generated
	 * @ordered
	 */
	protected SystemToSystem contexT__CORR__s2s;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__thread2thread1() <em>CONTEXT CORR thread2thread1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__thread2thread1()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__thread2thread1;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__thread2thread2() <em>CONTEXT CORR thread2thread2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__thread2thread2()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__thread2thread2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortConnectionToDataAccessAtSystem11__MarkerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.eINSTANCE.getPortConnectionToDataAccessAtSystem11__Marker();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCREATE__SRC__connection_source() {
		if (creatE__SRC__connection_source != null && creatE__SRC__connection_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__connection_source = (InternalEObject)creatE__SRC__connection_source;
			creatE__SRC__connection_source = (ConnectionInstance)eResolveProxy(oldCREATE__SRC__connection_source);
			if (creatE__SRC__connection_source != oldCREATE__SRC__connection_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_CONNECTION_SOURCE, oldCREATE__SRC__connection_source, creatE__SRC__connection_source));
			}
		}
		return creatE__SRC__connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCREATE__SRC__connection_source() {
		return creatE__SRC__connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__connection_source(ConnectionInstance newCREATE__SRC__connection_source) {
		ConnectionInstance oldCREATE__SRC__connection_source = creatE__SRC__connection_source;
		creatE__SRC__connection_source = newCREATE__SRC__connection_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_CONNECTION_SOURCE, oldCREATE__SRC__connection_source, creatE__SRC__connection_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCONTEXT__SRC__feature_destination() {
		if (contexT__SRC__feature_destination != null && contexT__SRC__feature_destination.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__feature_destination = (InternalEObject)contexT__SRC__feature_destination;
			contexT__SRC__feature_destination = (FeatureInstance)eResolveProxy(oldCONTEXT__SRC__feature_destination);
			if (contexT__SRC__feature_destination != oldCONTEXT__SRC__feature_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_DESTINATION, oldCONTEXT__SRC__feature_destination, contexT__SRC__feature_destination));
			}
		}
		return contexT__SRC__feature_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCONTEXT__SRC__feature_destination() {
		return contexT__SRC__feature_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__feature_destination(FeatureInstance newCONTEXT__SRC__feature_destination) {
		FeatureInstance oldCONTEXT__SRC__feature_destination = contexT__SRC__feature_destination;
		contexT__SRC__feature_destination = newCONTEXT__SRC__feature_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_DESTINATION, oldCONTEXT__SRC__feature_destination, contexT__SRC__feature_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCONTEXT__SRC__feature_source() {
		if (contexT__SRC__feature_source != null && contexT__SRC__feature_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__feature_source = (InternalEObject)contexT__SRC__feature_source;
			contexT__SRC__feature_source = (FeatureInstance)eResolveProxy(oldCONTEXT__SRC__feature_source);
			if (contexT__SRC__feature_source != oldCONTEXT__SRC__feature_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_SOURCE, oldCONTEXT__SRC__feature_source, contexT__SRC__feature_source));
			}
		}
		return contexT__SRC__feature_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCONTEXT__SRC__feature_source() {
		return contexT__SRC__feature_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__feature_source(FeatureInstance newCONTEXT__SRC__feature_source) {
		FeatureInstance oldCONTEXT__SRC__feature_source = contexT__SRC__feature_source;
		contexT__SRC__feature_source = newCONTEXT__SRC__feature_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_SOURCE, oldCONTEXT__SRC__feature_source, contexT__SRC__feature_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__process_destination() {
		if (contexT__SRC__process_destination != null && contexT__SRC__process_destination.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__process_destination = (InternalEObject)contexT__SRC__process_destination;
			contexT__SRC__process_destination = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__process_destination);
			if (contexT__SRC__process_destination != oldCONTEXT__SRC__process_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_DESTINATION, oldCONTEXT__SRC__process_destination, contexT__SRC__process_destination));
			}
		}
		return contexT__SRC__process_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__process_destination() {
		return contexT__SRC__process_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__process_destination(ComponentInstance newCONTEXT__SRC__process_destination) {
		ComponentInstance oldCONTEXT__SRC__process_destination = contexT__SRC__process_destination;
		contexT__SRC__process_destination = newCONTEXT__SRC__process_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_DESTINATION, oldCONTEXT__SRC__process_destination, contexT__SRC__process_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__process_source() {
		if (contexT__SRC__process_source != null && contexT__SRC__process_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__process_source = (InternalEObject)contexT__SRC__process_source;
			contexT__SRC__process_source = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__process_source);
			if (contexT__SRC__process_source != oldCONTEXT__SRC__process_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_SOURCE, oldCONTEXT__SRC__process_source, contexT__SRC__process_source));
			}
		}
		return contexT__SRC__process_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__process_source() {
		return contexT__SRC__process_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__process_source(ComponentInstance newCONTEXT__SRC__process_source) {
		ComponentInstance oldCONTEXT__SRC__process_source = contexT__SRC__process_source;
		contexT__SRC__process_source = newCONTEXT__SRC__process_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_SOURCE, oldCONTEXT__SRC__process_source, contexT__SRC__process_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference getCREATE__SRC__ref_connection_destination() {
		if (creatE__SRC__ref_connection_destination != null && creatE__SRC__ref_connection_destination.eIsProxy()) {
			InternalEObject oldCREATE__SRC__ref_connection_destination = (InternalEObject)creatE__SRC__ref_connection_destination;
			creatE__SRC__ref_connection_destination = (ConnectionReference)eResolveProxy(oldCREATE__SRC__ref_connection_destination);
			if (creatE__SRC__ref_connection_destination != oldCREATE__SRC__ref_connection_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_DESTINATION, oldCREATE__SRC__ref_connection_destination, creatE__SRC__ref_connection_destination));
			}
		}
		return creatE__SRC__ref_connection_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference basicGetCREATE__SRC__ref_connection_destination() {
		return creatE__SRC__ref_connection_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__ref_connection_destination(ConnectionReference newCREATE__SRC__ref_connection_destination) {
		ConnectionReference oldCREATE__SRC__ref_connection_destination = creatE__SRC__ref_connection_destination;
		creatE__SRC__ref_connection_destination = newCREATE__SRC__ref_connection_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_DESTINATION, oldCREATE__SRC__ref_connection_destination, creatE__SRC__ref_connection_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference getCREATE__SRC__ref_connection_source() {
		if (creatE__SRC__ref_connection_source != null && creatE__SRC__ref_connection_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__ref_connection_source = (InternalEObject)creatE__SRC__ref_connection_source;
			creatE__SRC__ref_connection_source = (ConnectionReference)eResolveProxy(oldCREATE__SRC__ref_connection_source);
			if (creatE__SRC__ref_connection_source != oldCREATE__SRC__ref_connection_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE, oldCREATE__SRC__ref_connection_source, creatE__SRC__ref_connection_source));
			}
		}
		return creatE__SRC__ref_connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference basicGetCREATE__SRC__ref_connection_source() {
		return creatE__SRC__ref_connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__ref_connection_source(ConnectionReference newCREATE__SRC__ref_connection_source) {
		ConnectionReference oldCREATE__SRC__ref_connection_source = creatE__SRC__ref_connection_source;
		creatE__SRC__ref_connection_source = newCREATE__SRC__ref_connection_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE, oldCREATE__SRC__ref_connection_source, creatE__SRC__ref_connection_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance getCONTEXT__SRC__system_source() {
		if (contexT__SRC__system_source != null && contexT__SRC__system_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__system_source = (InternalEObject)contexT__SRC__system_source;
			contexT__SRC__system_source = (SystemInstance)eResolveProxy(oldCONTEXT__SRC__system_source);
			if (contexT__SRC__system_source != oldCONTEXT__SRC__system_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_SYSTEM_SOURCE, oldCONTEXT__SRC__system_source, contexT__SRC__system_source));
			}
		}
		return contexT__SRC__system_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance basicGetCONTEXT__SRC__system_source() {
		return contexT__SRC__system_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__system_source(SystemInstance newCONTEXT__SRC__system_source) {
		SystemInstance oldCONTEXT__SRC__system_source = contexT__SRC__system_source;
		contexT__SRC__system_source = newCONTEXT__SRC__system_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_SYSTEM_SOURCE, oldCONTEXT__SRC__system_source, contexT__SRC__system_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__thread_destination() {
		if (contexT__SRC__thread_destination != null && contexT__SRC__thread_destination.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__thread_destination = (InternalEObject)contexT__SRC__thread_destination;
			contexT__SRC__thread_destination = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__thread_destination);
			if (contexT__SRC__thread_destination != oldCONTEXT__SRC__thread_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_DESTINATION, oldCONTEXT__SRC__thread_destination, contexT__SRC__thread_destination));
			}
		}
		return contexT__SRC__thread_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__thread_destination() {
		return contexT__SRC__thread_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__thread_destination(ComponentInstance newCONTEXT__SRC__thread_destination) {
		ComponentInstance oldCONTEXT__SRC__thread_destination = contexT__SRC__thread_destination;
		contexT__SRC__thread_destination = newCONTEXT__SRC__thread_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_DESTINATION, oldCONTEXT__SRC__thread_destination, contexT__SRC__thread_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__thread_source() {
		if (contexT__SRC__thread_source != null && contexT__SRC__thread_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__thread_source = (InternalEObject)contexT__SRC__thread_source;
			contexT__SRC__thread_source = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__thread_source);
			if (contexT__SRC__thread_source != oldCONTEXT__SRC__thread_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_SOURCE, oldCONTEXT__SRC__thread_source, contexT__SRC__thread_source));
			}
		}
		return contexT__SRC__thread_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__thread_source() {
		return contexT__SRC__thread_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__thread_source(ComponentInstance newCONTEXT__SRC__thread_source) {
		ComponentInstance oldCONTEXT__SRC__thread_source = contexT__SRC__thread_source;
		contexT__SRC__thread_source = newCONTEXT__SRC__thread_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_SOURCE, oldCONTEXT__SRC__thread_source, contexT__SRC__thread_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCREATE__TRG__data_destination() {
		if (creatE__TRG__data_destination != null && creatE__TRG__data_destination.eIsProxy()) {
			InternalEObject oldCREATE__TRG__data_destination = (InternalEObject)creatE__TRG__data_destination;
			creatE__TRG__data_destination = (ComponentInstance)eResolveProxy(oldCREATE__TRG__data_destination);
			if (creatE__TRG__data_destination != oldCREATE__TRG__data_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_DESTINATION, oldCREATE__TRG__data_destination, creatE__TRG__data_destination));
			}
		}
		return creatE__TRG__data_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCREATE__TRG__data_destination() {
		return creatE__TRG__data_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__data_destination(ComponentInstance newCREATE__TRG__data_destination) {
		ComponentInstance oldCREATE__TRG__data_destination = creatE__TRG__data_destination;
		creatE__TRG__data_destination = newCREATE__TRG__data_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_DESTINATION, oldCREATE__TRG__data_destination, creatE__TRG__data_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCREATE__TRG__data_source() {
		if (creatE__TRG__data_source != null && creatE__TRG__data_source.eIsProxy()) {
			InternalEObject oldCREATE__TRG__data_source = (InternalEObject)creatE__TRG__data_source;
			creatE__TRG__data_source = (ComponentInstance)eResolveProxy(oldCREATE__TRG__data_source);
			if (creatE__TRG__data_source != oldCREATE__TRG__data_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_SOURCE, oldCREATE__TRG__data_source, creatE__TRG__data_source));
			}
		}
		return creatE__TRG__data_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCREATE__TRG__data_source() {
		return creatE__TRG__data_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__data_source(ComponentInstance newCREATE__TRG__data_source) {
		ComponentInstance oldCREATE__TRG__data_source = creatE__TRG__data_source;
		creatE__TRG__data_source = newCREATE__TRG__data_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_SOURCE, oldCREATE__TRG__data_source, creatE__TRG__data_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCREATE__TRG__dataaccess_destination() {
		if (creatE__TRG__dataaccess_destination != null && creatE__TRG__dataaccess_destination.eIsProxy()) {
			InternalEObject oldCREATE__TRG__dataaccess_destination = (InternalEObject)creatE__TRG__dataaccess_destination;
			creatE__TRG__dataaccess_destination = (ConnectionInstance)eResolveProxy(oldCREATE__TRG__dataaccess_destination);
			if (creatE__TRG__dataaccess_destination != oldCREATE__TRG__dataaccess_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_DESTINATION, oldCREATE__TRG__dataaccess_destination, creatE__TRG__dataaccess_destination));
			}
		}
		return creatE__TRG__dataaccess_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCREATE__TRG__dataaccess_destination() {
		return creatE__TRG__dataaccess_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__dataaccess_destination(ConnectionInstance newCREATE__TRG__dataaccess_destination) {
		ConnectionInstance oldCREATE__TRG__dataaccess_destination = creatE__TRG__dataaccess_destination;
		creatE__TRG__dataaccess_destination = newCREATE__TRG__dataaccess_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_DESTINATION, oldCREATE__TRG__dataaccess_destination, creatE__TRG__dataaccess_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCREATE__TRG__dataaccess_source() {
		if (creatE__TRG__dataaccess_source != null && creatE__TRG__dataaccess_source.eIsProxy()) {
			InternalEObject oldCREATE__TRG__dataaccess_source = (InternalEObject)creatE__TRG__dataaccess_source;
			creatE__TRG__dataaccess_source = (ConnectionInstance)eResolveProxy(oldCREATE__TRG__dataaccess_source);
			if (creatE__TRG__dataaccess_source != oldCREATE__TRG__dataaccess_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_SOURCE, oldCREATE__TRG__dataaccess_source, creatE__TRG__dataaccess_source));
			}
		}
		return creatE__TRG__dataaccess_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCREATE__TRG__dataaccess_source() {
		return creatE__TRG__dataaccess_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__dataaccess_source(ConnectionInstance newCREATE__TRG__dataaccess_source) {
		ConnectionInstance oldCREATE__TRG__dataaccess_source = creatE__TRG__dataaccess_source;
		creatE__TRG__dataaccess_source = newCREATE__TRG__dataaccess_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_SOURCE, oldCREATE__TRG__dataaccess_source, creatE__TRG__dataaccess_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCONTEXT__TRG__feature_destination_target() {
		if (contexT__TRG__feature_destination_target != null && contexT__TRG__feature_destination_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__feature_destination_target = (InternalEObject)contexT__TRG__feature_destination_target;
			contexT__TRG__feature_destination_target = (FeatureInstance)eResolveProxy(oldCONTEXT__TRG__feature_destination_target);
			if (contexT__TRG__feature_destination_target != oldCONTEXT__TRG__feature_destination_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_DESTINATION_TARGET, oldCONTEXT__TRG__feature_destination_target, contexT__TRG__feature_destination_target));
			}
		}
		return contexT__TRG__feature_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCONTEXT__TRG__feature_destination_target() {
		return contexT__TRG__feature_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__feature_destination_target(FeatureInstance newCONTEXT__TRG__feature_destination_target) {
		FeatureInstance oldCONTEXT__TRG__feature_destination_target = contexT__TRG__feature_destination_target;
		contexT__TRG__feature_destination_target = newCONTEXT__TRG__feature_destination_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_DESTINATION_TARGET, oldCONTEXT__TRG__feature_destination_target, contexT__TRG__feature_destination_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCONTEXT__TRG__feature_source_target() {
		if (contexT__TRG__feature_source_target != null && contexT__TRG__feature_source_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__feature_source_target = (InternalEObject)contexT__TRG__feature_source_target;
			contexT__TRG__feature_source_target = (FeatureInstance)eResolveProxy(oldCONTEXT__TRG__feature_source_target);
			if (contexT__TRG__feature_source_target != oldCONTEXT__TRG__feature_source_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_SOURCE_TARGET, oldCONTEXT__TRG__feature_source_target, contexT__TRG__feature_source_target));
			}
		}
		return contexT__TRG__feature_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCONTEXT__TRG__feature_source_target() {
		return contexT__TRG__feature_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__feature_source_target(FeatureInstance newCONTEXT__TRG__feature_source_target) {
		FeatureInstance oldCONTEXT__TRG__feature_source_target = contexT__TRG__feature_source_target;
		contexT__TRG__feature_source_target = newCONTEXT__TRG__feature_source_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_SOURCE_TARGET, oldCONTEXT__TRG__feature_source_target, contexT__TRG__feature_source_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__process_destination_target() {
		if (contexT__TRG__process_destination_target != null && contexT__TRG__process_destination_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__process_destination_target = (InternalEObject)contexT__TRG__process_destination_target;
			contexT__TRG__process_destination_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__process_destination_target);
			if (contexT__TRG__process_destination_target != oldCONTEXT__TRG__process_destination_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_DESTINATION_TARGET, oldCONTEXT__TRG__process_destination_target, contexT__TRG__process_destination_target));
			}
		}
		return contexT__TRG__process_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__process_destination_target() {
		return contexT__TRG__process_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__process_destination_target(ComponentInstance newCONTEXT__TRG__process_destination_target) {
		ComponentInstance oldCONTEXT__TRG__process_destination_target = contexT__TRG__process_destination_target;
		contexT__TRG__process_destination_target = newCONTEXT__TRG__process_destination_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_DESTINATION_TARGET, oldCONTEXT__TRG__process_destination_target, contexT__TRG__process_destination_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__process_source_target() {
		if (contexT__TRG__process_source_target != null && contexT__TRG__process_source_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__process_source_target = (InternalEObject)contexT__TRG__process_source_target;
			contexT__TRG__process_source_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__process_source_target);
			if (contexT__TRG__process_source_target != oldCONTEXT__TRG__process_source_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_SOURCE_TARGET, oldCONTEXT__TRG__process_source_target, contexT__TRG__process_source_target));
			}
		}
		return contexT__TRG__process_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__process_source_target() {
		return contexT__TRG__process_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__process_source_target(ComponentInstance newCONTEXT__TRG__process_source_target) {
		ComponentInstance oldCONTEXT__TRG__process_source_target = contexT__TRG__process_source_target;
		contexT__TRG__process_source_target = newCONTEXT__TRG__process_source_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_SOURCE_TARGET, oldCONTEXT__TRG__process_source_target, contexT__TRG__process_source_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference getCREATE__TRG__ref_destination() {
		if (creatE__TRG__ref_destination != null && creatE__TRG__ref_destination.eIsProxy()) {
			InternalEObject oldCREATE__TRG__ref_destination = (InternalEObject)creatE__TRG__ref_destination;
			creatE__TRG__ref_destination = (ConnectionReference)eResolveProxy(oldCREATE__TRG__ref_destination);
			if (creatE__TRG__ref_destination != oldCREATE__TRG__ref_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_DESTINATION, oldCREATE__TRG__ref_destination, creatE__TRG__ref_destination));
			}
		}
		return creatE__TRG__ref_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference basicGetCREATE__TRG__ref_destination() {
		return creatE__TRG__ref_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__ref_destination(ConnectionReference newCREATE__TRG__ref_destination) {
		ConnectionReference oldCREATE__TRG__ref_destination = creatE__TRG__ref_destination;
		creatE__TRG__ref_destination = newCREATE__TRG__ref_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_DESTINATION, oldCREATE__TRG__ref_destination, creatE__TRG__ref_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference getCREATE__TRG__ref_source() {
		if (creatE__TRG__ref_source != null && creatE__TRG__ref_source.eIsProxy()) {
			InternalEObject oldCREATE__TRG__ref_source = (InternalEObject)creatE__TRG__ref_source;
			creatE__TRG__ref_source = (ConnectionReference)eResolveProxy(oldCREATE__TRG__ref_source);
			if (creatE__TRG__ref_source != oldCREATE__TRG__ref_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_SOURCE, oldCREATE__TRG__ref_source, creatE__TRG__ref_source));
			}
		}
		return creatE__TRG__ref_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference basicGetCREATE__TRG__ref_source() {
		return creatE__TRG__ref_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__ref_source(ConnectionReference newCREATE__TRG__ref_source) {
		ConnectionReference oldCREATE__TRG__ref_source = creatE__TRG__ref_source;
		creatE__TRG__ref_source = newCREATE__TRG__ref_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_SOURCE, oldCREATE__TRG__ref_source, creatE__TRG__ref_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance getCONTEXT__TRG__system_target() {
		if (contexT__TRG__system_target != null && contexT__TRG__system_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__system_target = (InternalEObject)contexT__TRG__system_target;
			contexT__TRG__system_target = (SystemInstance)eResolveProxy(oldCONTEXT__TRG__system_target);
			if (contexT__TRG__system_target != oldCONTEXT__TRG__system_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_SYSTEM_TARGET, oldCONTEXT__TRG__system_target, contexT__TRG__system_target));
			}
		}
		return contexT__TRG__system_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance basicGetCONTEXT__TRG__system_target() {
		return contexT__TRG__system_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__system_target(SystemInstance newCONTEXT__TRG__system_target) {
		SystemInstance oldCONTEXT__TRG__system_target = contexT__TRG__system_target;
		contexT__TRG__system_target = newCONTEXT__TRG__system_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_SYSTEM_TARGET, oldCONTEXT__TRG__system_target, contexT__TRG__system_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__thread_destination_target() {
		if (contexT__TRG__thread_destination_target != null && contexT__TRG__thread_destination_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__thread_destination_target = (InternalEObject)contexT__TRG__thread_destination_target;
			contexT__TRG__thread_destination_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__thread_destination_target);
			if (contexT__TRG__thread_destination_target != oldCONTEXT__TRG__thread_destination_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_DESTINATION_TARGET, oldCONTEXT__TRG__thread_destination_target, contexT__TRG__thread_destination_target));
			}
		}
		return contexT__TRG__thread_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__thread_destination_target() {
		return contexT__TRG__thread_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__thread_destination_target(ComponentInstance newCONTEXT__TRG__thread_destination_target) {
		ComponentInstance oldCONTEXT__TRG__thread_destination_target = contexT__TRG__thread_destination_target;
		contexT__TRG__thread_destination_target = newCONTEXT__TRG__thread_destination_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_DESTINATION_TARGET, oldCONTEXT__TRG__thread_destination_target, contexT__TRG__thread_destination_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__thread_source_target() {
		if (contexT__TRG__thread_source_target != null && contexT__TRG__thread_source_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__thread_source_target = (InternalEObject)contexT__TRG__thread_source_target;
			contexT__TRG__thread_source_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__thread_source_target);
			if (contexT__TRG__thread_source_target != oldCONTEXT__TRG__thread_source_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_SOURCE_TARGET, oldCONTEXT__TRG__thread_source_target, contexT__TRG__thread_source_target));
			}
		}
		return contexT__TRG__thread_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__thread_source_target() {
		return contexT__TRG__thread_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__thread_source_target(ComponentInstance newCONTEXT__TRG__thread_source_target) {
		ComponentInstance oldCONTEXT__TRG__thread_source_target = contexT__TRG__thread_source_target;
		contexT__TRG__thread_source_target = newCONTEXT__TRG__thread_source_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_SOURCE_TARGET, oldCONTEXT__TRG__thread_source_target, contexT__TRG__thread_source_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat getCONTEXT__CORR__feature2feature1() {
		if (contexT__CORR__feature2feature1 != null && contexT__CORR__feature2feature1.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__feature2feature1 = (InternalEObject)contexT__CORR__feature2feature1;
			contexT__CORR__feature2feature1 = (FeatToFeat)eResolveProxy(oldCONTEXT__CORR__feature2feature1);
			if (contexT__CORR__feature2feature1 != oldCONTEXT__CORR__feature2feature1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE1, oldCONTEXT__CORR__feature2feature1, contexT__CORR__feature2feature1));
			}
		}
		return contexT__CORR__feature2feature1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat basicGetCONTEXT__CORR__feature2feature1() {
		return contexT__CORR__feature2feature1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__feature2feature1(FeatToFeat newCONTEXT__CORR__feature2feature1) {
		FeatToFeat oldCONTEXT__CORR__feature2feature1 = contexT__CORR__feature2feature1;
		contexT__CORR__feature2feature1 = newCONTEXT__CORR__feature2feature1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE1, oldCONTEXT__CORR__feature2feature1, contexT__CORR__feature2feature1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat getCONTEXT__CORR__feature2feature2() {
		if (contexT__CORR__feature2feature2 != null && contexT__CORR__feature2feature2.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__feature2feature2 = (InternalEObject)contexT__CORR__feature2feature2;
			contexT__CORR__feature2feature2 = (FeatToFeat)eResolveProxy(oldCONTEXT__CORR__feature2feature2);
			if (contexT__CORR__feature2feature2 != oldCONTEXT__CORR__feature2feature2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE2, oldCONTEXT__CORR__feature2feature2, contexT__CORR__feature2feature2));
			}
		}
		return contexT__CORR__feature2feature2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat basicGetCONTEXT__CORR__feature2feature2() {
		return contexT__CORR__feature2feature2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__feature2feature2(FeatToFeat newCONTEXT__CORR__feature2feature2) {
		FeatToFeat oldCONTEXT__CORR__feature2feature2 = contexT__CORR__feature2feature2;
		contexT__CORR__feature2feature2 = newCONTEXT__CORR__feature2feature2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE2, oldCONTEXT__CORR__feature2feature2, contexT__CORR__feature2feature2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__p2a1() {
		if (creatE__CORR__p2a1 != null && creatE__CORR__p2a1.eIsProxy()) {
			InternalEObject oldCREATE__CORR__p2a1 = (InternalEObject)creatE__CORR__p2a1;
			creatE__CORR__p2a1 = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__p2a1);
			if (creatE__CORR__p2a1 != oldCREATE__CORR__p2a1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A1, oldCREATE__CORR__p2a1, creatE__CORR__p2a1));
			}
		}
		return creatE__CORR__p2a1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__p2a1() {
		return creatE__CORR__p2a1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__p2a1(PortConnectionToSharedData newCREATE__CORR__p2a1) {
		PortConnectionToSharedData oldCREATE__CORR__p2a1 = creatE__CORR__p2a1;
		creatE__CORR__p2a1 = newCREATE__CORR__p2a1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A1, oldCREATE__CORR__p2a1, creatE__CORR__p2a1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__p2a2() {
		if (creatE__CORR__p2a2 != null && creatE__CORR__p2a2.eIsProxy()) {
			InternalEObject oldCREATE__CORR__p2a2 = (InternalEObject)creatE__CORR__p2a2;
			creatE__CORR__p2a2 = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__p2a2);
			if (creatE__CORR__p2a2 != oldCREATE__CORR__p2a2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A2, oldCREATE__CORR__p2a2, creatE__CORR__p2a2));
			}
		}
		return creatE__CORR__p2a2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__p2a2() {
		return creatE__CORR__p2a2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__p2a2(PortConnectionToSharedData newCREATE__CORR__p2a2) {
		PortConnectionToSharedData oldCREATE__CORR__p2a2 = creatE__CORR__p2a2;
		creatE__CORR__p2a2 = newCREATE__CORR__p2a2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A2, oldCREATE__CORR__p2a2, creatE__CORR__p2a2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__p2d1() {
		if (creatE__CORR__p2d1 != null && creatE__CORR__p2d1.eIsProxy()) {
			InternalEObject oldCREATE__CORR__p2d1 = (InternalEObject)creatE__CORR__p2d1;
			creatE__CORR__p2d1 = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__p2d1);
			if (creatE__CORR__p2d1 != oldCREATE__CORR__p2d1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D1, oldCREATE__CORR__p2d1, creatE__CORR__p2d1));
			}
		}
		return creatE__CORR__p2d1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__p2d1() {
		return creatE__CORR__p2d1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__p2d1(PortConnectionToSharedData newCREATE__CORR__p2d1) {
		PortConnectionToSharedData oldCREATE__CORR__p2d1 = creatE__CORR__p2d1;
		creatE__CORR__p2d1 = newCREATE__CORR__p2d1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D1, oldCREATE__CORR__p2d1, creatE__CORR__p2d1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__p2d2() {
		if (creatE__CORR__p2d2 != null && creatE__CORR__p2d2.eIsProxy()) {
			InternalEObject oldCREATE__CORR__p2d2 = (InternalEObject)creatE__CORR__p2d2;
			creatE__CORR__p2d2 = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__p2d2);
			if (creatE__CORR__p2d2 != oldCREATE__CORR__p2d2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D2, oldCREATE__CORR__p2d2, creatE__CORR__p2d2));
			}
		}
		return creatE__CORR__p2d2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__p2d2() {
		return creatE__CORR__p2d2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__p2d2(PortConnectionToSharedData newCREATE__CORR__p2d2) {
		PortConnectionToSharedData oldCREATE__CORR__p2d2 = creatE__CORR__p2d2;
		creatE__CORR__p2d2 = newCREATE__CORR__p2d2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D2, oldCREATE__CORR__p2d2, creatE__CORR__p2d2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__process2process1() {
		if (contexT__CORR__process2process1 != null && contexT__CORR__process2process1.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__process2process1 = (InternalEObject)contexT__CORR__process2process1;
			contexT__CORR__process2process1 = (CompToComp)eResolveProxy(oldCONTEXT__CORR__process2process1);
			if (contexT__CORR__process2process1 != oldCONTEXT__CORR__process2process1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS1, oldCONTEXT__CORR__process2process1, contexT__CORR__process2process1));
			}
		}
		return contexT__CORR__process2process1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__process2process1() {
		return contexT__CORR__process2process1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__process2process1(CompToComp newCONTEXT__CORR__process2process1) {
		CompToComp oldCONTEXT__CORR__process2process1 = contexT__CORR__process2process1;
		contexT__CORR__process2process1 = newCONTEXT__CORR__process2process1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS1, oldCONTEXT__CORR__process2process1, contexT__CORR__process2process1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__process2process2() {
		if (contexT__CORR__process2process2 != null && contexT__CORR__process2process2.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__process2process2 = (InternalEObject)contexT__CORR__process2process2;
			contexT__CORR__process2process2 = (CompToComp)eResolveProxy(oldCONTEXT__CORR__process2process2);
			if (contexT__CORR__process2process2 != oldCONTEXT__CORR__process2process2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS2, oldCONTEXT__CORR__process2process2, contexT__CORR__process2process2));
			}
		}
		return contexT__CORR__process2process2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__process2process2() {
		return contexT__CORR__process2process2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__process2process2(CompToComp newCONTEXT__CORR__process2process2) {
		CompToComp oldCONTEXT__CORR__process2process2 = contexT__CORR__process2process2;
		contexT__CORR__process2process2 = newCONTEXT__CORR__process2process2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS2, oldCONTEXT__CORR__process2process2, contexT__CORR__process2process2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__r2r1() {
		if (creatE__CORR__r2r1 != null && creatE__CORR__r2r1.eIsProxy()) {
			InternalEObject oldCREATE__CORR__r2r1 = (InternalEObject)creatE__CORR__r2r1;
			creatE__CORR__r2r1 = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__r2r1);
			if (creatE__CORR__r2r1 != oldCREATE__CORR__r2r1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R1, oldCREATE__CORR__r2r1, creatE__CORR__r2r1));
			}
		}
		return creatE__CORR__r2r1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__r2r1() {
		return creatE__CORR__r2r1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__r2r1(PortConnectionToSharedData newCREATE__CORR__r2r1) {
		PortConnectionToSharedData oldCREATE__CORR__r2r1 = creatE__CORR__r2r1;
		creatE__CORR__r2r1 = newCREATE__CORR__r2r1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R1, oldCREATE__CORR__r2r1, creatE__CORR__r2r1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__r2r2() {
		if (creatE__CORR__r2r2 != null && creatE__CORR__r2r2.eIsProxy()) {
			InternalEObject oldCREATE__CORR__r2r2 = (InternalEObject)creatE__CORR__r2r2;
			creatE__CORR__r2r2 = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__r2r2);
			if (creatE__CORR__r2r2 != oldCREATE__CORR__r2r2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R2, oldCREATE__CORR__r2r2, creatE__CORR__r2r2));
			}
		}
		return creatE__CORR__r2r2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__r2r2() {
		return creatE__CORR__r2r2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__r2r2(PortConnectionToSharedData newCREATE__CORR__r2r2) {
		PortConnectionToSharedData oldCREATE__CORR__r2r2 = creatE__CORR__r2r2;
		creatE__CORR__r2r2 = newCREATE__CORR__r2r2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R2, oldCREATE__CORR__r2r2, creatE__CORR__r2r2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemToSystem getCONTEXT__CORR__s2s() {
		if (contexT__CORR__s2s != null && contexT__CORR__s2s.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__s2s = (InternalEObject)contexT__CORR__s2s;
			contexT__CORR__s2s = (SystemToSystem)eResolveProxy(oldCONTEXT__CORR__s2s);
			if (contexT__CORR__s2s != oldCONTEXT__CORR__s2s) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_S2S, oldCONTEXT__CORR__s2s, contexT__CORR__s2s));
			}
		}
		return contexT__CORR__s2s;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemToSystem basicGetCONTEXT__CORR__s2s() {
		return contexT__CORR__s2s;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__s2s(SystemToSystem newCONTEXT__CORR__s2s) {
		SystemToSystem oldCONTEXT__CORR__s2s = contexT__CORR__s2s;
		contexT__CORR__s2s = newCONTEXT__CORR__s2s;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_S2S, oldCONTEXT__CORR__s2s, contexT__CORR__s2s));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__thread2thread1() {
		if (contexT__CORR__thread2thread1 != null && contexT__CORR__thread2thread1.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__thread2thread1 = (InternalEObject)contexT__CORR__thread2thread1;
			contexT__CORR__thread2thread1 = (CompToComp)eResolveProxy(oldCONTEXT__CORR__thread2thread1);
			if (contexT__CORR__thread2thread1 != oldCONTEXT__CORR__thread2thread1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD1, oldCONTEXT__CORR__thread2thread1, contexT__CORR__thread2thread1));
			}
		}
		return contexT__CORR__thread2thread1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__thread2thread1() {
		return contexT__CORR__thread2thread1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__thread2thread1(CompToComp newCONTEXT__CORR__thread2thread1) {
		CompToComp oldCONTEXT__CORR__thread2thread1 = contexT__CORR__thread2thread1;
		contexT__CORR__thread2thread1 = newCONTEXT__CORR__thread2thread1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD1, oldCONTEXT__CORR__thread2thread1, contexT__CORR__thread2thread1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__thread2thread2() {
		if (contexT__CORR__thread2thread2 != null && contexT__CORR__thread2thread2.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__thread2thread2 = (InternalEObject)contexT__CORR__thread2thread2;
			contexT__CORR__thread2thread2 = (CompToComp)eResolveProxy(oldCONTEXT__CORR__thread2thread2);
			if (contexT__CORR__thread2thread2 != oldCONTEXT__CORR__thread2thread2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD2, oldCONTEXT__CORR__thread2thread2, contexT__CORR__thread2thread2));
			}
		}
		return contexT__CORR__thread2thread2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__thread2thread2() {
		return contexT__CORR__thread2thread2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__thread2thread2(CompToComp newCONTEXT__CORR__thread2thread2) {
		CompToComp oldCONTEXT__CORR__thread2thread2 = contexT__CORR__thread2thread2;
		contexT__CORR__thread2thread2 = newCONTEXT__CORR__thread2thread2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD2, oldCONTEXT__CORR__thread2thread2, contexT__CORR__thread2thread2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				if (resolve) return getCREATE__SRC__connection_source();
				return basicGetCREATE__SRC__connection_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_DESTINATION:
				if (resolve) return getCONTEXT__SRC__feature_destination();
				return basicGetCONTEXT__SRC__feature_destination();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_SOURCE:
				if (resolve) return getCONTEXT__SRC__feature_source();
				return basicGetCONTEXT__SRC__feature_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_DESTINATION:
				if (resolve) return getCONTEXT__SRC__process_destination();
				return basicGetCONTEXT__SRC__process_destination();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_SOURCE:
				if (resolve) return getCONTEXT__SRC__process_source();
				return basicGetCONTEXT__SRC__process_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_DESTINATION:
				if (resolve) return getCREATE__SRC__ref_connection_destination();
				return basicGetCREATE__SRC__ref_connection_destination();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE:
				if (resolve) return getCREATE__SRC__ref_connection_source();
				return basicGetCREATE__SRC__ref_connection_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_SYSTEM_SOURCE:
				if (resolve) return getCONTEXT__SRC__system_source();
				return basicGetCONTEXT__SRC__system_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_DESTINATION:
				if (resolve) return getCONTEXT__SRC__thread_destination();
				return basicGetCONTEXT__SRC__thread_destination();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_SOURCE:
				if (resolve) return getCONTEXT__SRC__thread_source();
				return basicGetCONTEXT__SRC__thread_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_DESTINATION:
				if (resolve) return getCREATE__TRG__data_destination();
				return basicGetCREATE__TRG__data_destination();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_SOURCE:
				if (resolve) return getCREATE__TRG__data_source();
				return basicGetCREATE__TRG__data_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_DESTINATION:
				if (resolve) return getCREATE__TRG__dataaccess_destination();
				return basicGetCREATE__TRG__dataaccess_destination();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_SOURCE:
				if (resolve) return getCREATE__TRG__dataaccess_source();
				return basicGetCREATE__TRG__dataaccess_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_DESTINATION_TARGET:
				if (resolve) return getCONTEXT__TRG__feature_destination_target();
				return basicGetCONTEXT__TRG__feature_destination_target();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_SOURCE_TARGET:
				if (resolve) return getCONTEXT__TRG__feature_source_target();
				return basicGetCONTEXT__TRG__feature_source_target();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_DESTINATION_TARGET:
				if (resolve) return getCONTEXT__TRG__process_destination_target();
				return basicGetCONTEXT__TRG__process_destination_target();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_SOURCE_TARGET:
				if (resolve) return getCONTEXT__TRG__process_source_target();
				return basicGetCONTEXT__TRG__process_source_target();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_DESTINATION:
				if (resolve) return getCREATE__TRG__ref_destination();
				return basicGetCREATE__TRG__ref_destination();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_SOURCE:
				if (resolve) return getCREATE__TRG__ref_source();
				return basicGetCREATE__TRG__ref_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_SYSTEM_TARGET:
				if (resolve) return getCONTEXT__TRG__system_target();
				return basicGetCONTEXT__TRG__system_target();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_DESTINATION_TARGET:
				if (resolve) return getCONTEXT__TRG__thread_destination_target();
				return basicGetCONTEXT__TRG__thread_destination_target();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_SOURCE_TARGET:
				if (resolve) return getCONTEXT__TRG__thread_source_target();
				return basicGetCONTEXT__TRG__thread_source_target();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE1:
				if (resolve) return getCONTEXT__CORR__feature2feature1();
				return basicGetCONTEXT__CORR__feature2feature1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE2:
				if (resolve) return getCONTEXT__CORR__feature2feature2();
				return basicGetCONTEXT__CORR__feature2feature2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A1:
				if (resolve) return getCREATE__CORR__p2a1();
				return basicGetCREATE__CORR__p2a1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A2:
				if (resolve) return getCREATE__CORR__p2a2();
				return basicGetCREATE__CORR__p2a2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D1:
				if (resolve) return getCREATE__CORR__p2d1();
				return basicGetCREATE__CORR__p2d1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D2:
				if (resolve) return getCREATE__CORR__p2d2();
				return basicGetCREATE__CORR__p2d2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS1:
				if (resolve) return getCONTEXT__CORR__process2process1();
				return basicGetCONTEXT__CORR__process2process1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS2:
				if (resolve) return getCONTEXT__CORR__process2process2();
				return basicGetCONTEXT__CORR__process2process2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R1:
				if (resolve) return getCREATE__CORR__r2r1();
				return basicGetCREATE__CORR__r2r1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R2:
				if (resolve) return getCREATE__CORR__r2r2();
				return basicGetCREATE__CORR__r2r2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_S2S:
				if (resolve) return getCONTEXT__CORR__s2s();
				return basicGetCONTEXT__CORR__s2s();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD1:
				if (resolve) return getCONTEXT__CORR__thread2thread1();
				return basicGetCONTEXT__CORR__thread2thread1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD2:
				if (resolve) return getCONTEXT__CORR__thread2thread2();
				return basicGetCONTEXT__CORR__thread2thread2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				setCREATE__SRC__connection_source((ConnectionInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_DESTINATION:
				setCONTEXT__SRC__feature_destination((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_SOURCE:
				setCONTEXT__SRC__feature_source((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_DESTINATION:
				setCONTEXT__SRC__process_destination((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_SOURCE:
				setCONTEXT__SRC__process_source((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_DESTINATION:
				setCREATE__SRC__ref_connection_destination((ConnectionReference)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE:
				setCREATE__SRC__ref_connection_source((ConnectionReference)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_SYSTEM_SOURCE:
				setCONTEXT__SRC__system_source((SystemInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_DESTINATION:
				setCONTEXT__SRC__thread_destination((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_SOURCE:
				setCONTEXT__SRC__thread_source((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_DESTINATION:
				setCREATE__TRG__data_destination((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_SOURCE:
				setCREATE__TRG__data_source((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_DESTINATION:
				setCREATE__TRG__dataaccess_destination((ConnectionInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_SOURCE:
				setCREATE__TRG__dataaccess_source((ConnectionInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_DESTINATION_TARGET:
				setCONTEXT__TRG__feature_destination_target((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_SOURCE_TARGET:
				setCONTEXT__TRG__feature_source_target((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_DESTINATION_TARGET:
				setCONTEXT__TRG__process_destination_target((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_SOURCE_TARGET:
				setCONTEXT__TRG__process_source_target((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_DESTINATION:
				setCREATE__TRG__ref_destination((ConnectionReference)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_SOURCE:
				setCREATE__TRG__ref_source((ConnectionReference)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_SYSTEM_TARGET:
				setCONTEXT__TRG__system_target((SystemInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_DESTINATION_TARGET:
				setCONTEXT__TRG__thread_destination_target((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_SOURCE_TARGET:
				setCONTEXT__TRG__thread_source_target((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE1:
				setCONTEXT__CORR__feature2feature1((FeatToFeat)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE2:
				setCONTEXT__CORR__feature2feature2((FeatToFeat)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A1:
				setCREATE__CORR__p2a1((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A2:
				setCREATE__CORR__p2a2((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D1:
				setCREATE__CORR__p2d1((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D2:
				setCREATE__CORR__p2d2((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS1:
				setCONTEXT__CORR__process2process1((CompToComp)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS2:
				setCONTEXT__CORR__process2process2((CompToComp)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R1:
				setCREATE__CORR__r2r1((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R2:
				setCREATE__CORR__r2r2((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_S2S:
				setCONTEXT__CORR__s2s((SystemToSystem)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD1:
				setCONTEXT__CORR__thread2thread1((CompToComp)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD2:
				setCONTEXT__CORR__thread2thread2((CompToComp)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				setCREATE__SRC__connection_source((ConnectionInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_DESTINATION:
				setCONTEXT__SRC__feature_destination((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_SOURCE:
				setCONTEXT__SRC__feature_source((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_DESTINATION:
				setCONTEXT__SRC__process_destination((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_SOURCE:
				setCONTEXT__SRC__process_source((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_DESTINATION:
				setCREATE__SRC__ref_connection_destination((ConnectionReference)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE:
				setCREATE__SRC__ref_connection_source((ConnectionReference)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_SYSTEM_SOURCE:
				setCONTEXT__SRC__system_source((SystemInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_DESTINATION:
				setCONTEXT__SRC__thread_destination((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_SOURCE:
				setCONTEXT__SRC__thread_source((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_DESTINATION:
				setCREATE__TRG__data_destination((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_SOURCE:
				setCREATE__TRG__data_source((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_DESTINATION:
				setCREATE__TRG__dataaccess_destination((ConnectionInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_SOURCE:
				setCREATE__TRG__dataaccess_source((ConnectionInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_DESTINATION_TARGET:
				setCONTEXT__TRG__feature_destination_target((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_SOURCE_TARGET:
				setCONTEXT__TRG__feature_source_target((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_DESTINATION_TARGET:
				setCONTEXT__TRG__process_destination_target((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_SOURCE_TARGET:
				setCONTEXT__TRG__process_source_target((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_DESTINATION:
				setCREATE__TRG__ref_destination((ConnectionReference)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_SOURCE:
				setCREATE__TRG__ref_source((ConnectionReference)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_SYSTEM_TARGET:
				setCONTEXT__TRG__system_target((SystemInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_DESTINATION_TARGET:
				setCONTEXT__TRG__thread_destination_target((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_SOURCE_TARGET:
				setCONTEXT__TRG__thread_source_target((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE1:
				setCONTEXT__CORR__feature2feature1((FeatToFeat)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE2:
				setCONTEXT__CORR__feature2feature2((FeatToFeat)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A1:
				setCREATE__CORR__p2a1((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A2:
				setCREATE__CORR__p2a2((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D1:
				setCREATE__CORR__p2d1((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D2:
				setCREATE__CORR__p2d2((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS1:
				setCONTEXT__CORR__process2process1((CompToComp)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS2:
				setCONTEXT__CORR__process2process2((CompToComp)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R1:
				setCREATE__CORR__r2r1((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R2:
				setCREATE__CORR__r2r2((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_S2S:
				setCONTEXT__CORR__s2s((SystemToSystem)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD1:
				setCONTEXT__CORR__thread2thread1((CompToComp)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD2:
				setCONTEXT__CORR__thread2thread2((CompToComp)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				return creatE__SRC__connection_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_DESTINATION:
				return contexT__SRC__feature_destination != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_SOURCE:
				return contexT__SRC__feature_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_DESTINATION:
				return contexT__SRC__process_destination != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_SOURCE:
				return contexT__SRC__process_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_DESTINATION:
				return creatE__SRC__ref_connection_destination != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE:
				return creatE__SRC__ref_connection_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_SYSTEM_SOURCE:
				return contexT__SRC__system_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_DESTINATION:
				return contexT__SRC__thread_destination != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_SOURCE:
				return contexT__SRC__thread_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_DESTINATION:
				return creatE__TRG__data_destination != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_SOURCE:
				return creatE__TRG__data_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_DESTINATION:
				return creatE__TRG__dataaccess_destination != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_SOURCE:
				return creatE__TRG__dataaccess_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_DESTINATION_TARGET:
				return contexT__TRG__feature_destination_target != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_SOURCE_TARGET:
				return contexT__TRG__feature_source_target != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_DESTINATION_TARGET:
				return contexT__TRG__process_destination_target != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_SOURCE_TARGET:
				return contexT__TRG__process_source_target != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_DESTINATION:
				return creatE__TRG__ref_destination != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_SOURCE:
				return creatE__TRG__ref_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_SYSTEM_TARGET:
				return contexT__TRG__system_target != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_DESTINATION_TARGET:
				return contexT__TRG__thread_destination_target != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_SOURCE_TARGET:
				return contexT__TRG__thread_source_target != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE1:
				return contexT__CORR__feature2feature1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE2:
				return contexT__CORR__feature2feature2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A1:
				return creatE__CORR__p2a1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A2:
				return creatE__CORR__p2a2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D1:
				return creatE__CORR__p2d1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D2:
				return creatE__CORR__p2d2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS1:
				return contexT__CORR__process2process1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS2:
				return contexT__CORR__process2process2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R1:
				return creatE__CORR__r2r1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R2:
				return creatE__CORR__r2r2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_S2S:
				return contexT__CORR__s2s != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD1:
				return contexT__CORR__thread2thread1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD2:
				return contexT__CORR__thread2thread2 != null;
		}
		return super.eIsSet(featureID);
	}

} //PortConnectionToDataAccessAtSystem11__MarkerImpl
