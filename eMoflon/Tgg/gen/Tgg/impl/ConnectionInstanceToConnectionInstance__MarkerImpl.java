/**
 */
package Tgg.impl;

import Tgg.CompToComp;
import Tgg.ConnectionInstanceToConnectionInstance__Marker;
import Tgg.ConnectionToConnection;
import Tgg.End2End;
import Tgg.TggPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionInstanceEnd;

import runtime.impl.TGGRuleApplicationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connection Instance To Connection Instance Marker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCONTEXT__SRC__s_destination <em>CONTEXT SRC sdestination</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCREATE__TRG__connection_target <em>CREATE TRG connection target</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCONTEXT__TRG__t_destination <em>CONTEXT TRG tdestination</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCREATE__CORR__c2c <em>CREATE CORR c2c</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCONTEXT__CORR__e2e1 <em>CONTEXT CORR e2e1</em>}</li>
 *   <li>{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConnectionInstanceToConnectionInstance__MarkerImpl extends TGGRuleApplicationImpl implements ConnectionInstanceToConnectionInstance__Marker {
	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__component_source() <em>CONTEXT SRC component source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__component_source()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__component_source;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__connection_source() <em>CREATE SRC connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__connection_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance creatE__SRC__connection_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__s_destination() <em>CONTEXT SRC sdestination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__s_destination()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstanceEnd contexT__SRC__s_destination;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__s_source() <em>CONTEXT SRC ssource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__s_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstanceEnd contexT__SRC__s_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__component_target() <em>CONTEXT TRG component target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__component_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__component_target;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__connection_target() <em>CREATE TRG connection target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__connection_target()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance creatE__TRG__connection_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__t_destination() <em>CONTEXT TRG tdestination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__t_destination()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstanceEnd contexT__TRG__t_destination;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__t_source() <em>CONTEXT TRG tsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__t_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstanceEnd contexT__TRG__t_source;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__c2c() <em>CREATE CORR c2c</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__c2c()
	 * @generated
	 * @ordered
	 */
	protected ConnectionToConnection creatE__CORR__c2c;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__comp2comp() <em>CONTEXT CORR comp2comp</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__comp2comp()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__comp2comp;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__e2e1() <em>CONTEXT CORR e2e1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__e2e1()
	 * @generated
	 * @ordered
	 */
	protected End2End contexT__CORR__e2e1;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__e2e2() <em>CONTEXT CORR e2e2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__e2e2()
	 * @generated
	 * @ordered
	 */
	protected End2End contexT__CORR__e2e2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionInstanceToConnectionInstance__MarkerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.eINSTANCE.getConnectionInstanceToConnectionInstance__Marker();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__component_source() {
		if (contexT__SRC__component_source != null && contexT__SRC__component_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__component_source = (InternalEObject)contexT__SRC__component_source;
			contexT__SRC__component_source = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__component_source);
			if (contexT__SRC__component_source != oldCONTEXT__SRC__component_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE, oldCONTEXT__SRC__component_source, contexT__SRC__component_source));
			}
		}
		return contexT__SRC__component_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__component_source() {
		return contexT__SRC__component_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__component_source(ComponentInstance newCONTEXT__SRC__component_source) {
		ComponentInstance oldCONTEXT__SRC__component_source = contexT__SRC__component_source;
		contexT__SRC__component_source = newCONTEXT__SRC__component_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE, oldCONTEXT__SRC__component_source, contexT__SRC__component_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCREATE__SRC__connection_source() {
		if (creatE__SRC__connection_source != null && creatE__SRC__connection_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__connection_source = (InternalEObject)creatE__SRC__connection_source;
			creatE__SRC__connection_source = (ConnectionInstance)eResolveProxy(oldCREATE__SRC__connection_source);
			if (creatE__SRC__connection_source != oldCREATE__SRC__connection_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_SRC_CONNECTION_SOURCE, oldCREATE__SRC__connection_source, creatE__SRC__connection_source));
			}
		}
		return creatE__SRC__connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCREATE__SRC__connection_source() {
		return creatE__SRC__connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__connection_source(ConnectionInstance newCREATE__SRC__connection_source) {
		ConnectionInstance oldCREATE__SRC__connection_source = creatE__SRC__connection_source;
		creatE__SRC__connection_source = newCREATE__SRC__connection_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_SRC_CONNECTION_SOURCE, oldCREATE__SRC__connection_source, creatE__SRC__connection_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstanceEnd getCONTEXT__SRC__s_destination() {
		if (contexT__SRC__s_destination != null && contexT__SRC__s_destination.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__s_destination = (InternalEObject)contexT__SRC__s_destination;
			contexT__SRC__s_destination = (ConnectionInstanceEnd)eResolveProxy(oldCONTEXT__SRC__s_destination);
			if (contexT__SRC__s_destination != oldCONTEXT__SRC__s_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SDESTINATION, oldCONTEXT__SRC__s_destination, contexT__SRC__s_destination));
			}
		}
		return contexT__SRC__s_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstanceEnd basicGetCONTEXT__SRC__s_destination() {
		return contexT__SRC__s_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__s_destination(ConnectionInstanceEnd newCONTEXT__SRC__s_destination) {
		ConnectionInstanceEnd oldCONTEXT__SRC__s_destination = contexT__SRC__s_destination;
		contexT__SRC__s_destination = newCONTEXT__SRC__s_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SDESTINATION, oldCONTEXT__SRC__s_destination, contexT__SRC__s_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstanceEnd getCONTEXT__SRC__s_source() {
		if (contexT__SRC__s_source != null && contexT__SRC__s_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__s_source = (InternalEObject)contexT__SRC__s_source;
			contexT__SRC__s_source = (ConnectionInstanceEnd)eResolveProxy(oldCONTEXT__SRC__s_source);
			if (contexT__SRC__s_source != oldCONTEXT__SRC__s_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SSOURCE, oldCONTEXT__SRC__s_source, contexT__SRC__s_source));
			}
		}
		return contexT__SRC__s_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstanceEnd basicGetCONTEXT__SRC__s_source() {
		return contexT__SRC__s_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__s_source(ConnectionInstanceEnd newCONTEXT__SRC__s_source) {
		ConnectionInstanceEnd oldCONTEXT__SRC__s_source = contexT__SRC__s_source;
		contexT__SRC__s_source = newCONTEXT__SRC__s_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SSOURCE, oldCONTEXT__SRC__s_source, contexT__SRC__s_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__component_target() {
		if (contexT__TRG__component_target != null && contexT__TRG__component_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__component_target = (InternalEObject)contexT__TRG__component_target;
			contexT__TRG__component_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__component_target);
			if (contexT__TRG__component_target != oldCONTEXT__TRG__component_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_COMPONENT_TARGET, oldCONTEXT__TRG__component_target, contexT__TRG__component_target));
			}
		}
		return contexT__TRG__component_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__component_target() {
		return contexT__TRG__component_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__component_target(ComponentInstance newCONTEXT__TRG__component_target) {
		ComponentInstance oldCONTEXT__TRG__component_target = contexT__TRG__component_target;
		contexT__TRG__component_target = newCONTEXT__TRG__component_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_COMPONENT_TARGET, oldCONTEXT__TRG__component_target, contexT__TRG__component_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCREATE__TRG__connection_target() {
		if (creatE__TRG__connection_target != null && creatE__TRG__connection_target.eIsProxy()) {
			InternalEObject oldCREATE__TRG__connection_target = (InternalEObject)creatE__TRG__connection_target;
			creatE__TRG__connection_target = (ConnectionInstance)eResolveProxy(oldCREATE__TRG__connection_target);
			if (creatE__TRG__connection_target != oldCREATE__TRG__connection_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_TRG_CONNECTION_TARGET, oldCREATE__TRG__connection_target, creatE__TRG__connection_target));
			}
		}
		return creatE__TRG__connection_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCREATE__TRG__connection_target() {
		return creatE__TRG__connection_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__connection_target(ConnectionInstance newCREATE__TRG__connection_target) {
		ConnectionInstance oldCREATE__TRG__connection_target = creatE__TRG__connection_target;
		creatE__TRG__connection_target = newCREATE__TRG__connection_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_TRG_CONNECTION_TARGET, oldCREATE__TRG__connection_target, creatE__TRG__connection_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstanceEnd getCONTEXT__TRG__t_destination() {
		if (contexT__TRG__t_destination != null && contexT__TRG__t_destination.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__t_destination = (InternalEObject)contexT__TRG__t_destination;
			contexT__TRG__t_destination = (ConnectionInstanceEnd)eResolveProxy(oldCONTEXT__TRG__t_destination);
			if (contexT__TRG__t_destination != oldCONTEXT__TRG__t_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TDESTINATION, oldCONTEXT__TRG__t_destination, contexT__TRG__t_destination));
			}
		}
		return contexT__TRG__t_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstanceEnd basicGetCONTEXT__TRG__t_destination() {
		return contexT__TRG__t_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__t_destination(ConnectionInstanceEnd newCONTEXT__TRG__t_destination) {
		ConnectionInstanceEnd oldCONTEXT__TRG__t_destination = contexT__TRG__t_destination;
		contexT__TRG__t_destination = newCONTEXT__TRG__t_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TDESTINATION, oldCONTEXT__TRG__t_destination, contexT__TRG__t_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstanceEnd getCONTEXT__TRG__t_source() {
		if (contexT__TRG__t_source != null && contexT__TRG__t_source.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__t_source = (InternalEObject)contexT__TRG__t_source;
			contexT__TRG__t_source = (ConnectionInstanceEnd)eResolveProxy(oldCONTEXT__TRG__t_source);
			if (contexT__TRG__t_source != oldCONTEXT__TRG__t_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TSOURCE, oldCONTEXT__TRG__t_source, contexT__TRG__t_source));
			}
		}
		return contexT__TRG__t_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstanceEnd basicGetCONTEXT__TRG__t_source() {
		return contexT__TRG__t_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__t_source(ConnectionInstanceEnd newCONTEXT__TRG__t_source) {
		ConnectionInstanceEnd oldCONTEXT__TRG__t_source = contexT__TRG__t_source;
		contexT__TRG__t_source = newCONTEXT__TRG__t_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TSOURCE, oldCONTEXT__TRG__t_source, contexT__TRG__t_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionToConnection getCREATE__CORR__c2c() {
		if (creatE__CORR__c2c != null && creatE__CORR__c2c.eIsProxy()) {
			InternalEObject oldCREATE__CORR__c2c = (InternalEObject)creatE__CORR__c2c;
			creatE__CORR__c2c = (ConnectionToConnection)eResolveProxy(oldCREATE__CORR__c2c);
			if (creatE__CORR__c2c != oldCREATE__CORR__c2c) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_CORR_C2C, oldCREATE__CORR__c2c, creatE__CORR__c2c));
			}
		}
		return creatE__CORR__c2c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionToConnection basicGetCREATE__CORR__c2c() {
		return creatE__CORR__c2c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__c2c(ConnectionToConnection newCREATE__CORR__c2c) {
		ConnectionToConnection oldCREATE__CORR__c2c = creatE__CORR__c2c;
		creatE__CORR__c2c = newCREATE__CORR__c2c;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_CORR_C2C, oldCREATE__CORR__c2c, creatE__CORR__c2c));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__comp2comp() {
		if (contexT__CORR__comp2comp != null && contexT__CORR__comp2comp.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__comp2comp = (InternalEObject)contexT__CORR__comp2comp;
			contexT__CORR__comp2comp = (CompToComp)eResolveProxy(oldCONTEXT__CORR__comp2comp);
			if (contexT__CORR__comp2comp != oldCONTEXT__CORR__comp2comp) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_COMP2COMP, oldCONTEXT__CORR__comp2comp, contexT__CORR__comp2comp));
			}
		}
		return contexT__CORR__comp2comp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__comp2comp() {
		return contexT__CORR__comp2comp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__comp2comp(CompToComp newCONTEXT__CORR__comp2comp) {
		CompToComp oldCONTEXT__CORR__comp2comp = contexT__CORR__comp2comp;
		contexT__CORR__comp2comp = newCONTEXT__CORR__comp2comp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_COMP2COMP, oldCONTEXT__CORR__comp2comp, contexT__CORR__comp2comp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public End2End getCONTEXT__CORR__e2e1() {
		if (contexT__CORR__e2e1 != null && contexT__CORR__e2e1.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__e2e1 = (InternalEObject)contexT__CORR__e2e1;
			contexT__CORR__e2e1 = (End2End)eResolveProxy(oldCONTEXT__CORR__e2e1);
			if (contexT__CORR__e2e1 != oldCONTEXT__CORR__e2e1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E1, oldCONTEXT__CORR__e2e1, contexT__CORR__e2e1));
			}
		}
		return contexT__CORR__e2e1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public End2End basicGetCONTEXT__CORR__e2e1() {
		return contexT__CORR__e2e1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__e2e1(End2End newCONTEXT__CORR__e2e1) {
		End2End oldCONTEXT__CORR__e2e1 = contexT__CORR__e2e1;
		contexT__CORR__e2e1 = newCONTEXT__CORR__e2e1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E1, oldCONTEXT__CORR__e2e1, contexT__CORR__e2e1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public End2End getCONTEXT__CORR__e2e2() {
		if (contexT__CORR__e2e2 != null && contexT__CORR__e2e2.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__e2e2 = (InternalEObject)contexT__CORR__e2e2;
			contexT__CORR__e2e2 = (End2End)eResolveProxy(oldCONTEXT__CORR__e2e2);
			if (contexT__CORR__e2e2 != oldCONTEXT__CORR__e2e2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E2, oldCONTEXT__CORR__e2e2, contexT__CORR__e2e2));
			}
		}
		return contexT__CORR__e2e2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public End2End basicGetCONTEXT__CORR__e2e2() {
		return contexT__CORR__e2e2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__e2e2(End2End newCONTEXT__CORR__e2e2) {
		End2End oldCONTEXT__CORR__e2e2 = contexT__CORR__e2e2;
		contexT__CORR__e2e2 = newCONTEXT__CORR__e2e2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E2, oldCONTEXT__CORR__e2e2, contexT__CORR__e2e2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				if (resolve) return getCONTEXT__SRC__component_source();
				return basicGetCONTEXT__SRC__component_source();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				if (resolve) return getCREATE__SRC__connection_source();
				return basicGetCREATE__SRC__connection_source();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SDESTINATION:
				if (resolve) return getCONTEXT__SRC__s_destination();
				return basicGetCONTEXT__SRC__s_destination();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SSOURCE:
				if (resolve) return getCONTEXT__SRC__s_source();
				return basicGetCONTEXT__SRC__s_source();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				if (resolve) return getCONTEXT__TRG__component_target();
				return basicGetCONTEXT__TRG__component_target();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_TRG_CONNECTION_TARGET:
				if (resolve) return getCREATE__TRG__connection_target();
				return basicGetCREATE__TRG__connection_target();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TDESTINATION:
				if (resolve) return getCONTEXT__TRG__t_destination();
				return basicGetCONTEXT__TRG__t_destination();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TSOURCE:
				if (resolve) return getCONTEXT__TRG__t_source();
				return basicGetCONTEXT__TRG__t_source();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_CORR_C2C:
				if (resolve) return getCREATE__CORR__c2c();
				return basicGetCREATE__CORR__c2c();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_COMP2COMP:
				if (resolve) return getCONTEXT__CORR__comp2comp();
				return basicGetCONTEXT__CORR__comp2comp();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E1:
				if (resolve) return getCONTEXT__CORR__e2e1();
				return basicGetCONTEXT__CORR__e2e1();
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E2:
				if (resolve) return getCONTEXT__CORR__e2e2();
				return basicGetCONTEXT__CORR__e2e2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				setCONTEXT__SRC__component_source((ComponentInstance)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				setCREATE__SRC__connection_source((ConnectionInstance)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SDESTINATION:
				setCONTEXT__SRC__s_destination((ConnectionInstanceEnd)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SSOURCE:
				setCONTEXT__SRC__s_source((ConnectionInstanceEnd)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				setCONTEXT__TRG__component_target((ComponentInstance)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_TRG_CONNECTION_TARGET:
				setCREATE__TRG__connection_target((ConnectionInstance)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TDESTINATION:
				setCONTEXT__TRG__t_destination((ConnectionInstanceEnd)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TSOURCE:
				setCONTEXT__TRG__t_source((ConnectionInstanceEnd)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_CORR_C2C:
				setCREATE__CORR__c2c((ConnectionToConnection)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_COMP2COMP:
				setCONTEXT__CORR__comp2comp((CompToComp)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E1:
				setCONTEXT__CORR__e2e1((End2End)newValue);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E2:
				setCONTEXT__CORR__e2e2((End2End)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				setCONTEXT__SRC__component_source((ComponentInstance)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				setCREATE__SRC__connection_source((ConnectionInstance)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SDESTINATION:
				setCONTEXT__SRC__s_destination((ConnectionInstanceEnd)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SSOURCE:
				setCONTEXT__SRC__s_source((ConnectionInstanceEnd)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				setCONTEXT__TRG__component_target((ComponentInstance)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_TRG_CONNECTION_TARGET:
				setCREATE__TRG__connection_target((ConnectionInstance)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TDESTINATION:
				setCONTEXT__TRG__t_destination((ConnectionInstanceEnd)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TSOURCE:
				setCONTEXT__TRG__t_source((ConnectionInstanceEnd)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_CORR_C2C:
				setCREATE__CORR__c2c((ConnectionToConnection)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_COMP2COMP:
				setCONTEXT__CORR__comp2comp((CompToComp)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E1:
				setCONTEXT__CORR__e2e1((End2End)null);
				return;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E2:
				setCONTEXT__CORR__e2e2((End2End)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				return contexT__SRC__component_source != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				return creatE__SRC__connection_source != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SDESTINATION:
				return contexT__SRC__s_destination != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SSOURCE:
				return contexT__SRC__s_source != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				return contexT__TRG__component_target != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_TRG_CONNECTION_TARGET:
				return creatE__TRG__connection_target != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TDESTINATION:
				return contexT__TRG__t_destination != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TSOURCE:
				return contexT__TRG__t_source != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_CORR_C2C:
				return creatE__CORR__c2c != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_COMP2COMP:
				return contexT__CORR__comp2comp != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E1:
				return contexT__CORR__e2e1 != null;
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E2:
				return contexT__CORR__e2e2 != null;
		}
		return super.eIsSet(featureID);
	}

} //ConnectionInstanceToConnectionInstance__MarkerImpl
