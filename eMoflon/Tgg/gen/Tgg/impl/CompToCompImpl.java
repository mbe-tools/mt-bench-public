/**
 */
package Tgg.impl;

import Tgg.CompToComp;
import Tgg.TggPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Comp To Comp</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CompToCompImpl extends End2EndImpl implements CompToComp {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompToCompImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.eINSTANCE.getCompToComp();
	}

} //CompToCompImpl
