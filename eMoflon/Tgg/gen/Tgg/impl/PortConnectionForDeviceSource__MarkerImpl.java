/**
 */
package Tgg.impl;

import Tgg.CompToComp;
import Tgg.ConnectionToConnection;
import Tgg.FeatToFeat;
import Tgg.PortConnectionForDeviceSource__Marker;
import Tgg.SystemToSystem;
import Tgg.TggPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.SystemInstance;

import runtime.impl.TGGRuleApplicationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Connection For Device Source Marker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCONTEXT__SRC__component_destination <em>CONTEXT SRC component destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCONTEXT__TRG__component_destination_target <em>CONTEXT TRG component destination target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCONTEXT__TRG__component_source_target <em>CONTEXT TRG component source target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCREATE__TRG__connection_target <em>CREATE TRG connection target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCREATE__TRG__feature_destination_target <em>CREATE TRG feature destination target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCREATE__TRG__feature_source_target <em>CREATE TRG feature source target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCONTEXT__CORR__c2c1 <em>CONTEXT CORR c2c1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCONTEXT__CORR__c2c2 <em>CONTEXT CORR c2c2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCREATE__CORR__co2co <em>CREATE CORR co2co</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCREATE__CORR__feature2feature1 <em>CREATE CORR feature2feature1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCREATE__CORR__feature2feature2 <em>CREATE CORR feature2feature2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortConnectionForDeviceSource__MarkerImpl extends TGGRuleApplicationImpl implements PortConnectionForDeviceSource__Marker {
	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__component_destination() <em>CONTEXT SRC component destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__component_destination()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__component_destination;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__component_source() <em>CONTEXT SRC component source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__component_source()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__component_source;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__connection_source() <em>CREATE SRC connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__connection_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance creatE__SRC__connection_source;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__feature_destination() <em>CREATE SRC feature destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__feature_destination()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__SRC__feature_destination;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__feature_source() <em>CREATE SRC feature source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__feature_source()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__SRC__feature_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__system_source() <em>CONTEXT SRC system source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__system_source()
	 * @generated
	 * @ordered
	 */
	protected SystemInstance contexT__SRC__system_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__component_destination_target() <em>CONTEXT TRG component destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__component_destination_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__component_destination_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__component_source_target() <em>CONTEXT TRG component source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__component_source_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__component_source_target;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__connection_target() <em>CREATE TRG connection target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__connection_target()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance creatE__TRG__connection_target;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__feature_destination_target() <em>CREATE TRG feature destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__feature_destination_target()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__TRG__feature_destination_target;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__feature_source_target() <em>CREATE TRG feature source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__feature_source_target()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__TRG__feature_source_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__system_target() <em>CONTEXT TRG system target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__system_target()
	 * @generated
	 * @ordered
	 */
	protected SystemInstance contexT__TRG__system_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__c2c1() <em>CONTEXT CORR c2c1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__c2c1()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__c2c1;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__c2c2() <em>CONTEXT CORR c2c2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__c2c2()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__c2c2;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__co2co() <em>CREATE CORR co2co</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__co2co()
	 * @generated
	 * @ordered
	 */
	protected ConnectionToConnection creatE__CORR__co2co;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__feature2feature1() <em>CREATE CORR feature2feature1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__feature2feature1()
	 * @generated
	 * @ordered
	 */
	protected FeatToFeat creatE__CORR__feature2feature1;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__feature2feature2() <em>CREATE CORR feature2feature2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__feature2feature2()
	 * @generated
	 * @ordered
	 */
	protected FeatToFeat creatE__CORR__feature2feature2;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__s2s() <em>CONTEXT CORR s2s</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__s2s()
	 * @generated
	 * @ordered
	 */
	protected SystemToSystem contexT__CORR__s2s;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortConnectionForDeviceSource__MarkerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.eINSTANCE.getPortConnectionForDeviceSource__Marker();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__component_destination() {
		if (contexT__SRC__component_destination != null && contexT__SRC__component_destination.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__component_destination = (InternalEObject)contexT__SRC__component_destination;
			contexT__SRC__component_destination = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__component_destination);
			if (contexT__SRC__component_destination != oldCONTEXT__SRC__component_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_DESTINATION, oldCONTEXT__SRC__component_destination, contexT__SRC__component_destination));
			}
		}
		return contexT__SRC__component_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__component_destination() {
		return contexT__SRC__component_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__component_destination(ComponentInstance newCONTEXT__SRC__component_destination) {
		ComponentInstance oldCONTEXT__SRC__component_destination = contexT__SRC__component_destination;
		contexT__SRC__component_destination = newCONTEXT__SRC__component_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_DESTINATION, oldCONTEXT__SRC__component_destination, contexT__SRC__component_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__component_source() {
		if (contexT__SRC__component_source != null && contexT__SRC__component_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__component_source = (InternalEObject)contexT__SRC__component_source;
			contexT__SRC__component_source = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__component_source);
			if (contexT__SRC__component_source != oldCONTEXT__SRC__component_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE, oldCONTEXT__SRC__component_source, contexT__SRC__component_source));
			}
		}
		return contexT__SRC__component_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__component_source() {
		return contexT__SRC__component_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__component_source(ComponentInstance newCONTEXT__SRC__component_source) {
		ComponentInstance oldCONTEXT__SRC__component_source = contexT__SRC__component_source;
		contexT__SRC__component_source = newCONTEXT__SRC__component_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE, oldCONTEXT__SRC__component_source, contexT__SRC__component_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCREATE__SRC__connection_source() {
		if (creatE__SRC__connection_source != null && creatE__SRC__connection_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__connection_source = (InternalEObject)creatE__SRC__connection_source;
			creatE__SRC__connection_source = (ConnectionInstance)eResolveProxy(oldCREATE__SRC__connection_source);
			if (creatE__SRC__connection_source != oldCREATE__SRC__connection_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_CONNECTION_SOURCE, oldCREATE__SRC__connection_source, creatE__SRC__connection_source));
			}
		}
		return creatE__SRC__connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCREATE__SRC__connection_source() {
		return creatE__SRC__connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__connection_source(ConnectionInstance newCREATE__SRC__connection_source) {
		ConnectionInstance oldCREATE__SRC__connection_source = creatE__SRC__connection_source;
		creatE__SRC__connection_source = newCREATE__SRC__connection_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_CONNECTION_SOURCE, oldCREATE__SRC__connection_source, creatE__SRC__connection_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__SRC__feature_destination() {
		if (creatE__SRC__feature_destination != null && creatE__SRC__feature_destination.eIsProxy()) {
			InternalEObject oldCREATE__SRC__feature_destination = (InternalEObject)creatE__SRC__feature_destination;
			creatE__SRC__feature_destination = (FeatureInstance)eResolveProxy(oldCREATE__SRC__feature_destination);
			if (creatE__SRC__feature_destination != oldCREATE__SRC__feature_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_DESTINATION, oldCREATE__SRC__feature_destination, creatE__SRC__feature_destination));
			}
		}
		return creatE__SRC__feature_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__SRC__feature_destination() {
		return creatE__SRC__feature_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__feature_destination(FeatureInstance newCREATE__SRC__feature_destination) {
		FeatureInstance oldCREATE__SRC__feature_destination = creatE__SRC__feature_destination;
		creatE__SRC__feature_destination = newCREATE__SRC__feature_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_DESTINATION, oldCREATE__SRC__feature_destination, creatE__SRC__feature_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__SRC__feature_source() {
		if (creatE__SRC__feature_source != null && creatE__SRC__feature_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__feature_source = (InternalEObject)creatE__SRC__feature_source;
			creatE__SRC__feature_source = (FeatureInstance)eResolveProxy(oldCREATE__SRC__feature_source);
			if (creatE__SRC__feature_source != oldCREATE__SRC__feature_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_SOURCE, oldCREATE__SRC__feature_source, creatE__SRC__feature_source));
			}
		}
		return creatE__SRC__feature_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__SRC__feature_source() {
		return creatE__SRC__feature_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__feature_source(FeatureInstance newCREATE__SRC__feature_source) {
		FeatureInstance oldCREATE__SRC__feature_source = creatE__SRC__feature_source;
		creatE__SRC__feature_source = newCREATE__SRC__feature_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_SOURCE, oldCREATE__SRC__feature_source, creatE__SRC__feature_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance getCONTEXT__SRC__system_source() {
		if (contexT__SRC__system_source != null && contexT__SRC__system_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__system_source = (InternalEObject)contexT__SRC__system_source;
			contexT__SRC__system_source = (SystemInstance)eResolveProxy(oldCONTEXT__SRC__system_source);
			if (contexT__SRC__system_source != oldCONTEXT__SRC__system_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_SYSTEM_SOURCE, oldCONTEXT__SRC__system_source, contexT__SRC__system_source));
			}
		}
		return contexT__SRC__system_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance basicGetCONTEXT__SRC__system_source() {
		return contexT__SRC__system_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__system_source(SystemInstance newCONTEXT__SRC__system_source) {
		SystemInstance oldCONTEXT__SRC__system_source = contexT__SRC__system_source;
		contexT__SRC__system_source = newCONTEXT__SRC__system_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_SYSTEM_SOURCE, oldCONTEXT__SRC__system_source, contexT__SRC__system_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__component_destination_target() {
		if (contexT__TRG__component_destination_target != null && contexT__TRG__component_destination_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__component_destination_target = (InternalEObject)contexT__TRG__component_destination_target;
			contexT__TRG__component_destination_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__component_destination_target);
			if (contexT__TRG__component_destination_target != oldCONTEXT__TRG__component_destination_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_DESTINATION_TARGET, oldCONTEXT__TRG__component_destination_target, contexT__TRG__component_destination_target));
			}
		}
		return contexT__TRG__component_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__component_destination_target() {
		return contexT__TRG__component_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__component_destination_target(ComponentInstance newCONTEXT__TRG__component_destination_target) {
		ComponentInstance oldCONTEXT__TRG__component_destination_target = contexT__TRG__component_destination_target;
		contexT__TRG__component_destination_target = newCONTEXT__TRG__component_destination_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_DESTINATION_TARGET, oldCONTEXT__TRG__component_destination_target, contexT__TRG__component_destination_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__component_source_target() {
		if (contexT__TRG__component_source_target != null && contexT__TRG__component_source_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__component_source_target = (InternalEObject)contexT__TRG__component_source_target;
			contexT__TRG__component_source_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__component_source_target);
			if (contexT__TRG__component_source_target != oldCONTEXT__TRG__component_source_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_SOURCE_TARGET, oldCONTEXT__TRG__component_source_target, contexT__TRG__component_source_target));
			}
		}
		return contexT__TRG__component_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__component_source_target() {
		return contexT__TRG__component_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__component_source_target(ComponentInstance newCONTEXT__TRG__component_source_target) {
		ComponentInstance oldCONTEXT__TRG__component_source_target = contexT__TRG__component_source_target;
		contexT__TRG__component_source_target = newCONTEXT__TRG__component_source_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_SOURCE_TARGET, oldCONTEXT__TRG__component_source_target, contexT__TRG__component_source_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCREATE__TRG__connection_target() {
		if (creatE__TRG__connection_target != null && creatE__TRG__connection_target.eIsProxy()) {
			InternalEObject oldCREATE__TRG__connection_target = (InternalEObject)creatE__TRG__connection_target;
			creatE__TRG__connection_target = (ConnectionInstance)eResolveProxy(oldCREATE__TRG__connection_target);
			if (creatE__TRG__connection_target != oldCREATE__TRG__connection_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_CONNECTION_TARGET, oldCREATE__TRG__connection_target, creatE__TRG__connection_target));
			}
		}
		return creatE__TRG__connection_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCREATE__TRG__connection_target() {
		return creatE__TRG__connection_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__connection_target(ConnectionInstance newCREATE__TRG__connection_target) {
		ConnectionInstance oldCREATE__TRG__connection_target = creatE__TRG__connection_target;
		creatE__TRG__connection_target = newCREATE__TRG__connection_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_CONNECTION_TARGET, oldCREATE__TRG__connection_target, creatE__TRG__connection_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__TRG__feature_destination_target() {
		if (creatE__TRG__feature_destination_target != null && creatE__TRG__feature_destination_target.eIsProxy()) {
			InternalEObject oldCREATE__TRG__feature_destination_target = (InternalEObject)creatE__TRG__feature_destination_target;
			creatE__TRG__feature_destination_target = (FeatureInstance)eResolveProxy(oldCREATE__TRG__feature_destination_target);
			if (creatE__TRG__feature_destination_target != oldCREATE__TRG__feature_destination_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_DESTINATION_TARGET, oldCREATE__TRG__feature_destination_target, creatE__TRG__feature_destination_target));
			}
		}
		return creatE__TRG__feature_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__TRG__feature_destination_target() {
		return creatE__TRG__feature_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__feature_destination_target(FeatureInstance newCREATE__TRG__feature_destination_target) {
		FeatureInstance oldCREATE__TRG__feature_destination_target = creatE__TRG__feature_destination_target;
		creatE__TRG__feature_destination_target = newCREATE__TRG__feature_destination_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_DESTINATION_TARGET, oldCREATE__TRG__feature_destination_target, creatE__TRG__feature_destination_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__TRG__feature_source_target() {
		if (creatE__TRG__feature_source_target != null && creatE__TRG__feature_source_target.eIsProxy()) {
			InternalEObject oldCREATE__TRG__feature_source_target = (InternalEObject)creatE__TRG__feature_source_target;
			creatE__TRG__feature_source_target = (FeatureInstance)eResolveProxy(oldCREATE__TRG__feature_source_target);
			if (creatE__TRG__feature_source_target != oldCREATE__TRG__feature_source_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_SOURCE_TARGET, oldCREATE__TRG__feature_source_target, creatE__TRG__feature_source_target));
			}
		}
		return creatE__TRG__feature_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__TRG__feature_source_target() {
		return creatE__TRG__feature_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__feature_source_target(FeatureInstance newCREATE__TRG__feature_source_target) {
		FeatureInstance oldCREATE__TRG__feature_source_target = creatE__TRG__feature_source_target;
		creatE__TRG__feature_source_target = newCREATE__TRG__feature_source_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_SOURCE_TARGET, oldCREATE__TRG__feature_source_target, creatE__TRG__feature_source_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance getCONTEXT__TRG__system_target() {
		if (contexT__TRG__system_target != null && contexT__TRG__system_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__system_target = (InternalEObject)contexT__TRG__system_target;
			contexT__TRG__system_target = (SystemInstance)eResolveProxy(oldCONTEXT__TRG__system_target);
			if (contexT__TRG__system_target != oldCONTEXT__TRG__system_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_SYSTEM_TARGET, oldCONTEXT__TRG__system_target, contexT__TRG__system_target));
			}
		}
		return contexT__TRG__system_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance basicGetCONTEXT__TRG__system_target() {
		return contexT__TRG__system_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__system_target(SystemInstance newCONTEXT__TRG__system_target) {
		SystemInstance oldCONTEXT__TRG__system_target = contexT__TRG__system_target;
		contexT__TRG__system_target = newCONTEXT__TRG__system_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_SYSTEM_TARGET, oldCONTEXT__TRG__system_target, contexT__TRG__system_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__c2c1() {
		if (contexT__CORR__c2c1 != null && contexT__CORR__c2c1.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__c2c1 = (InternalEObject)contexT__CORR__c2c1;
			contexT__CORR__c2c1 = (CompToComp)eResolveProxy(oldCONTEXT__CORR__c2c1);
			if (contexT__CORR__c2c1 != oldCONTEXT__CORR__c2c1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C1, oldCONTEXT__CORR__c2c1, contexT__CORR__c2c1));
			}
		}
		return contexT__CORR__c2c1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__c2c1() {
		return contexT__CORR__c2c1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__c2c1(CompToComp newCONTEXT__CORR__c2c1) {
		CompToComp oldCONTEXT__CORR__c2c1 = contexT__CORR__c2c1;
		contexT__CORR__c2c1 = newCONTEXT__CORR__c2c1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C1, oldCONTEXT__CORR__c2c1, contexT__CORR__c2c1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__c2c2() {
		if (contexT__CORR__c2c2 != null && contexT__CORR__c2c2.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__c2c2 = (InternalEObject)contexT__CORR__c2c2;
			contexT__CORR__c2c2 = (CompToComp)eResolveProxy(oldCONTEXT__CORR__c2c2);
			if (contexT__CORR__c2c2 != oldCONTEXT__CORR__c2c2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C2, oldCONTEXT__CORR__c2c2, contexT__CORR__c2c2));
			}
		}
		return contexT__CORR__c2c2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__c2c2() {
		return contexT__CORR__c2c2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__c2c2(CompToComp newCONTEXT__CORR__c2c2) {
		CompToComp oldCONTEXT__CORR__c2c2 = contexT__CORR__c2c2;
		contexT__CORR__c2c2 = newCONTEXT__CORR__c2c2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C2, oldCONTEXT__CORR__c2c2, contexT__CORR__c2c2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionToConnection getCREATE__CORR__co2co() {
		if (creatE__CORR__co2co != null && creatE__CORR__co2co.eIsProxy()) {
			InternalEObject oldCREATE__CORR__co2co = (InternalEObject)creatE__CORR__co2co;
			creatE__CORR__co2co = (ConnectionToConnection)eResolveProxy(oldCREATE__CORR__co2co);
			if (creatE__CORR__co2co != oldCREATE__CORR__co2co) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_CO2CO, oldCREATE__CORR__co2co, creatE__CORR__co2co));
			}
		}
		return creatE__CORR__co2co;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionToConnection basicGetCREATE__CORR__co2co() {
		return creatE__CORR__co2co;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__co2co(ConnectionToConnection newCREATE__CORR__co2co) {
		ConnectionToConnection oldCREATE__CORR__co2co = creatE__CORR__co2co;
		creatE__CORR__co2co = newCREATE__CORR__co2co;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_CO2CO, oldCREATE__CORR__co2co, creatE__CORR__co2co));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat getCREATE__CORR__feature2feature1() {
		if (creatE__CORR__feature2feature1 != null && creatE__CORR__feature2feature1.eIsProxy()) {
			InternalEObject oldCREATE__CORR__feature2feature1 = (InternalEObject)creatE__CORR__feature2feature1;
			creatE__CORR__feature2feature1 = (FeatToFeat)eResolveProxy(oldCREATE__CORR__feature2feature1);
			if (creatE__CORR__feature2feature1 != oldCREATE__CORR__feature2feature1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE1, oldCREATE__CORR__feature2feature1, creatE__CORR__feature2feature1));
			}
		}
		return creatE__CORR__feature2feature1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat basicGetCREATE__CORR__feature2feature1() {
		return creatE__CORR__feature2feature1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__feature2feature1(FeatToFeat newCREATE__CORR__feature2feature1) {
		FeatToFeat oldCREATE__CORR__feature2feature1 = creatE__CORR__feature2feature1;
		creatE__CORR__feature2feature1 = newCREATE__CORR__feature2feature1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE1, oldCREATE__CORR__feature2feature1, creatE__CORR__feature2feature1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat getCREATE__CORR__feature2feature2() {
		if (creatE__CORR__feature2feature2 != null && creatE__CORR__feature2feature2.eIsProxy()) {
			InternalEObject oldCREATE__CORR__feature2feature2 = (InternalEObject)creatE__CORR__feature2feature2;
			creatE__CORR__feature2feature2 = (FeatToFeat)eResolveProxy(oldCREATE__CORR__feature2feature2);
			if (creatE__CORR__feature2feature2 != oldCREATE__CORR__feature2feature2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE2, oldCREATE__CORR__feature2feature2, creatE__CORR__feature2feature2));
			}
		}
		return creatE__CORR__feature2feature2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat basicGetCREATE__CORR__feature2feature2() {
		return creatE__CORR__feature2feature2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__feature2feature2(FeatToFeat newCREATE__CORR__feature2feature2) {
		FeatToFeat oldCREATE__CORR__feature2feature2 = creatE__CORR__feature2feature2;
		creatE__CORR__feature2feature2 = newCREATE__CORR__feature2feature2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE2, oldCREATE__CORR__feature2feature2, creatE__CORR__feature2feature2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemToSystem getCONTEXT__CORR__s2s() {
		if (contexT__CORR__s2s != null && contexT__CORR__s2s.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__s2s = (InternalEObject)contexT__CORR__s2s;
			contexT__CORR__s2s = (SystemToSystem)eResolveProxy(oldCONTEXT__CORR__s2s);
			if (contexT__CORR__s2s != oldCONTEXT__CORR__s2s) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_S2S, oldCONTEXT__CORR__s2s, contexT__CORR__s2s));
			}
		}
		return contexT__CORR__s2s;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemToSystem basicGetCONTEXT__CORR__s2s() {
		return contexT__CORR__s2s;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__s2s(SystemToSystem newCONTEXT__CORR__s2s) {
		SystemToSystem oldCONTEXT__CORR__s2s = contexT__CORR__s2s;
		contexT__CORR__s2s = newCONTEXT__CORR__s2s;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_S2S, oldCONTEXT__CORR__s2s, contexT__CORR__s2s));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_DESTINATION:
				if (resolve) return getCONTEXT__SRC__component_destination();
				return basicGetCONTEXT__SRC__component_destination();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				if (resolve) return getCONTEXT__SRC__component_source();
				return basicGetCONTEXT__SRC__component_source();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				if (resolve) return getCREATE__SRC__connection_source();
				return basicGetCREATE__SRC__connection_source();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_DESTINATION:
				if (resolve) return getCREATE__SRC__feature_destination();
				return basicGetCREATE__SRC__feature_destination();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_SOURCE:
				if (resolve) return getCREATE__SRC__feature_source();
				return basicGetCREATE__SRC__feature_source();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_SYSTEM_SOURCE:
				if (resolve) return getCONTEXT__SRC__system_source();
				return basicGetCONTEXT__SRC__system_source();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_DESTINATION_TARGET:
				if (resolve) return getCONTEXT__TRG__component_destination_target();
				return basicGetCONTEXT__TRG__component_destination_target();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_SOURCE_TARGET:
				if (resolve) return getCONTEXT__TRG__component_source_target();
				return basicGetCONTEXT__TRG__component_source_target();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_CONNECTION_TARGET:
				if (resolve) return getCREATE__TRG__connection_target();
				return basicGetCREATE__TRG__connection_target();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_DESTINATION_TARGET:
				if (resolve) return getCREATE__TRG__feature_destination_target();
				return basicGetCREATE__TRG__feature_destination_target();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_SOURCE_TARGET:
				if (resolve) return getCREATE__TRG__feature_source_target();
				return basicGetCREATE__TRG__feature_source_target();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_SYSTEM_TARGET:
				if (resolve) return getCONTEXT__TRG__system_target();
				return basicGetCONTEXT__TRG__system_target();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C1:
				if (resolve) return getCONTEXT__CORR__c2c1();
				return basicGetCONTEXT__CORR__c2c1();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C2:
				if (resolve) return getCONTEXT__CORR__c2c2();
				return basicGetCONTEXT__CORR__c2c2();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_CO2CO:
				if (resolve) return getCREATE__CORR__co2co();
				return basicGetCREATE__CORR__co2co();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE1:
				if (resolve) return getCREATE__CORR__feature2feature1();
				return basicGetCREATE__CORR__feature2feature1();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE2:
				if (resolve) return getCREATE__CORR__feature2feature2();
				return basicGetCREATE__CORR__feature2feature2();
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_S2S:
				if (resolve) return getCONTEXT__CORR__s2s();
				return basicGetCONTEXT__CORR__s2s();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_DESTINATION:
				setCONTEXT__SRC__component_destination((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				setCONTEXT__SRC__component_source((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				setCREATE__SRC__connection_source((ConnectionInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_DESTINATION:
				setCREATE__SRC__feature_destination((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_SOURCE:
				setCREATE__SRC__feature_source((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_SYSTEM_SOURCE:
				setCONTEXT__SRC__system_source((SystemInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_DESTINATION_TARGET:
				setCONTEXT__TRG__component_destination_target((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_SOURCE_TARGET:
				setCONTEXT__TRG__component_source_target((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_CONNECTION_TARGET:
				setCREATE__TRG__connection_target((ConnectionInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_DESTINATION_TARGET:
				setCREATE__TRG__feature_destination_target((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_SOURCE_TARGET:
				setCREATE__TRG__feature_source_target((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_SYSTEM_TARGET:
				setCONTEXT__TRG__system_target((SystemInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C1:
				setCONTEXT__CORR__c2c1((CompToComp)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C2:
				setCONTEXT__CORR__c2c2((CompToComp)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_CO2CO:
				setCREATE__CORR__co2co((ConnectionToConnection)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE1:
				setCREATE__CORR__feature2feature1((FeatToFeat)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE2:
				setCREATE__CORR__feature2feature2((FeatToFeat)newValue);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_S2S:
				setCONTEXT__CORR__s2s((SystemToSystem)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_DESTINATION:
				setCONTEXT__SRC__component_destination((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				setCONTEXT__SRC__component_source((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				setCREATE__SRC__connection_source((ConnectionInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_DESTINATION:
				setCREATE__SRC__feature_destination((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_SOURCE:
				setCREATE__SRC__feature_source((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_SYSTEM_SOURCE:
				setCONTEXT__SRC__system_source((SystemInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_DESTINATION_TARGET:
				setCONTEXT__TRG__component_destination_target((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_SOURCE_TARGET:
				setCONTEXT__TRG__component_source_target((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_CONNECTION_TARGET:
				setCREATE__TRG__connection_target((ConnectionInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_DESTINATION_TARGET:
				setCREATE__TRG__feature_destination_target((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_SOURCE_TARGET:
				setCREATE__TRG__feature_source_target((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_SYSTEM_TARGET:
				setCONTEXT__TRG__system_target((SystemInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C1:
				setCONTEXT__CORR__c2c1((CompToComp)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C2:
				setCONTEXT__CORR__c2c2((CompToComp)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_CO2CO:
				setCREATE__CORR__co2co((ConnectionToConnection)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE1:
				setCREATE__CORR__feature2feature1((FeatToFeat)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE2:
				setCREATE__CORR__feature2feature2((FeatToFeat)null);
				return;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_S2S:
				setCONTEXT__CORR__s2s((SystemToSystem)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_DESTINATION:
				return contexT__SRC__component_destination != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				return contexT__SRC__component_source != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				return creatE__SRC__connection_source != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_DESTINATION:
				return creatE__SRC__feature_destination != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_SOURCE:
				return creatE__SRC__feature_source != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_SYSTEM_SOURCE:
				return contexT__SRC__system_source != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_DESTINATION_TARGET:
				return contexT__TRG__component_destination_target != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_SOURCE_TARGET:
				return contexT__TRG__component_source_target != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_CONNECTION_TARGET:
				return creatE__TRG__connection_target != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_DESTINATION_TARGET:
				return creatE__TRG__feature_destination_target != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_SOURCE_TARGET:
				return creatE__TRG__feature_source_target != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_SYSTEM_TARGET:
				return contexT__TRG__system_target != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C1:
				return contexT__CORR__c2c1 != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C2:
				return contexT__CORR__c2c2 != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_CO2CO:
				return creatE__CORR__co2co != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE1:
				return creatE__CORR__feature2feature1 != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE2:
				return creatE__CORR__feature2feature2 != null;
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_S2S:
				return contexT__CORR__s2s != null;
		}
		return super.eIsSet(featureID);
	}

} //PortConnectionForDeviceSource__MarkerImpl
