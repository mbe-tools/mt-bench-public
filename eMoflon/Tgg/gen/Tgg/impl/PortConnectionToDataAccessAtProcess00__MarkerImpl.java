/**
 */
package Tgg.impl;

import Tgg.CompToComp;
import Tgg.FeatToFeat;
import Tgg.PortConnectionToDataAccessAtProcess00__Marker;
import Tgg.PortConnectionToSharedData;
import Tgg.TggPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;

import runtime.impl.TGGRuleApplicationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Connection To Data Access At Process00 Marker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCONTEXT__SRC__component_conn_destination <em>CONTEXT SRC component conn destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCONTEXT__SRC__component_conn_source <em>CONTEXT SRC component conn source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCONTEXT__TRG__component_conn_destination_target <em>CONTEXT TRG component conn destination target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCONTEXT__TRG__component_conn_source_target <em>CONTEXT TRG component conn source target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__TRG__data <em>CREATE TRG data</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__TRG__dataaccess1 <em>CREATE TRG dataaccess1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__TRG__dataaccess2 <em>CREATE TRG dataaccess2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__TRG__feature1 <em>CREATE TRG feature1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__TRG__feature2 <em>CREATE TRG feature2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__TRG__refconnection1 <em>CREATE TRG refconnection1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__TRG__refconnection2 <em>CREATE TRG refconnection2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__CORR__f2f1 <em>CREATE CORR f2f1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__CORR__f2f2 <em>CREATE CORR f2f2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__CORR__p2d <em>CREATE CORR p2d</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__CORR__p2r1 <em>CREATE CORR p2r1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCREATE__CORR__p2r2 <em>CREATE CORR p2r2</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCONTEXT__CORR__sub2sub1 <em>CONTEXT CORR sub2sub1</em>}</li>
 *   <li>{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl#getCONTEXT__CORR__sub2sub2 <em>CONTEXT CORR sub2sub2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PortConnectionToDataAccessAtProcess00__MarkerImpl extends TGGRuleApplicationImpl implements PortConnectionToDataAccessAtProcess00__Marker {
	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__component_conn_destination() <em>CONTEXT SRC component conn destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__component_conn_destination()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__component_conn_destination;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__component_conn_source() <em>CONTEXT SRC component conn source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__component_conn_source()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__component_conn_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__SRC__component_source() <em>CONTEXT SRC component source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__SRC__component_source()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__SRC__component_source;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__connection_source() <em>CREATE SRC connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__connection_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance creatE__SRC__connection_source;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__feature_destination() <em>CREATE SRC feature destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__feature_destination()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__SRC__feature_destination;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__feature_source() <em>CREATE SRC feature source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__feature_source()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__SRC__feature_source;

	/**
	 * The cached value of the '{@link #getCREATE__SRC__ref_connection_source() <em>CREATE SRC ref connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__ref_connection_source()
	 * @generated
	 * @ordered
	 */
	protected ConnectionReference creatE__SRC__ref_connection_source;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__component_conn_destination_target() <em>CONTEXT TRG component conn destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__component_conn_destination_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__component_conn_destination_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__component_conn_source_target() <em>CONTEXT TRG component conn source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__component_conn_source_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__component_conn_source_target;

	/**
	 * The cached value of the '{@link #getCONTEXT__TRG__component_target() <em>CONTEXT TRG component target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__TRG__component_target()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance contexT__TRG__component_target;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__data() <em>CREATE TRG data</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__data()
	 * @generated
	 * @ordered
	 */
	protected ComponentInstance creatE__TRG__data;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__dataaccess1() <em>CREATE TRG dataaccess1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__dataaccess1()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance creatE__TRG__dataaccess1;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__dataaccess2() <em>CREATE TRG dataaccess2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__dataaccess2()
	 * @generated
	 * @ordered
	 */
	protected ConnectionInstance creatE__TRG__dataaccess2;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__feature1() <em>CREATE TRG feature1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__feature1()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__TRG__feature1;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__feature2() <em>CREATE TRG feature2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__feature2()
	 * @generated
	 * @ordered
	 */
	protected FeatureInstance creatE__TRG__feature2;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__refconnection1() <em>CREATE TRG refconnection1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__refconnection1()
	 * @generated
	 * @ordered
	 */
	protected ConnectionReference creatE__TRG__refconnection1;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__refconnection2() <em>CREATE TRG refconnection2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__refconnection2()
	 * @generated
	 * @ordered
	 */
	protected ConnectionReference creatE__TRG__refconnection2;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__c2c() <em>CONTEXT CORR c2c</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__c2c()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__c2c;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__f2f1() <em>CREATE CORR f2f1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__f2f1()
	 * @generated
	 * @ordered
	 */
	protected FeatToFeat creatE__CORR__f2f1;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__f2f2() <em>CREATE CORR f2f2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__f2f2()
	 * @generated
	 * @ordered
	 */
	protected FeatToFeat creatE__CORR__f2f2;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__p2a1() <em>CREATE CORR p2a1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__p2a1()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__p2a1;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__p2a2() <em>CREATE CORR p2a2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__p2a2()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__p2a2;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__p2d() <em>CREATE CORR p2d</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__p2d()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__p2d;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__p2r1() <em>CREATE CORR p2r1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__p2r1()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__p2r1;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__p2r2() <em>CREATE CORR p2r2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__p2r2()
	 * @generated
	 * @ordered
	 */
	protected PortConnectionToSharedData creatE__CORR__p2r2;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__sub2sub1() <em>CONTEXT CORR sub2sub1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__sub2sub1()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__sub2sub1;

	/**
	 * The cached value of the '{@link #getCONTEXT__CORR__sub2sub2() <em>CONTEXT CORR sub2sub2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCONTEXT__CORR__sub2sub2()
	 * @generated
	 * @ordered
	 */
	protected CompToComp contexT__CORR__sub2sub2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortConnectionToDataAccessAtProcess00__MarkerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.eINSTANCE.getPortConnectionToDataAccessAtProcess00__Marker();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__component_conn_destination() {
		if (contexT__SRC__component_conn_destination != null && contexT__SRC__component_conn_destination.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__component_conn_destination = (InternalEObject)contexT__SRC__component_conn_destination;
			contexT__SRC__component_conn_destination = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__component_conn_destination);
			if (contexT__SRC__component_conn_destination != oldCONTEXT__SRC__component_conn_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_DESTINATION, oldCONTEXT__SRC__component_conn_destination, contexT__SRC__component_conn_destination));
			}
		}
		return contexT__SRC__component_conn_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__component_conn_destination() {
		return contexT__SRC__component_conn_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__component_conn_destination(ComponentInstance newCONTEXT__SRC__component_conn_destination) {
		ComponentInstance oldCONTEXT__SRC__component_conn_destination = contexT__SRC__component_conn_destination;
		contexT__SRC__component_conn_destination = newCONTEXT__SRC__component_conn_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_DESTINATION, oldCONTEXT__SRC__component_conn_destination, contexT__SRC__component_conn_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__component_conn_source() {
		if (contexT__SRC__component_conn_source != null && contexT__SRC__component_conn_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__component_conn_source = (InternalEObject)contexT__SRC__component_conn_source;
			contexT__SRC__component_conn_source = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__component_conn_source);
			if (contexT__SRC__component_conn_source != oldCONTEXT__SRC__component_conn_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_SOURCE, oldCONTEXT__SRC__component_conn_source, contexT__SRC__component_conn_source));
			}
		}
		return contexT__SRC__component_conn_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__component_conn_source() {
		return contexT__SRC__component_conn_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__component_conn_source(ComponentInstance newCONTEXT__SRC__component_conn_source) {
		ComponentInstance oldCONTEXT__SRC__component_conn_source = contexT__SRC__component_conn_source;
		contexT__SRC__component_conn_source = newCONTEXT__SRC__component_conn_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_SOURCE, oldCONTEXT__SRC__component_conn_source, contexT__SRC__component_conn_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__SRC__component_source() {
		if (contexT__SRC__component_source != null && contexT__SRC__component_source.eIsProxy()) {
			InternalEObject oldCONTEXT__SRC__component_source = (InternalEObject)contexT__SRC__component_source;
			contexT__SRC__component_source = (ComponentInstance)eResolveProxy(oldCONTEXT__SRC__component_source);
			if (contexT__SRC__component_source != oldCONTEXT__SRC__component_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_SOURCE, oldCONTEXT__SRC__component_source, contexT__SRC__component_source));
			}
		}
		return contexT__SRC__component_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__SRC__component_source() {
		return contexT__SRC__component_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__SRC__component_source(ComponentInstance newCONTEXT__SRC__component_source) {
		ComponentInstance oldCONTEXT__SRC__component_source = contexT__SRC__component_source;
		contexT__SRC__component_source = newCONTEXT__SRC__component_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_SOURCE, oldCONTEXT__SRC__component_source, contexT__SRC__component_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCREATE__SRC__connection_source() {
		if (creatE__SRC__connection_source != null && creatE__SRC__connection_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__connection_source = (InternalEObject)creatE__SRC__connection_source;
			creatE__SRC__connection_source = (ConnectionInstance)eResolveProxy(oldCREATE__SRC__connection_source);
			if (creatE__SRC__connection_source != oldCREATE__SRC__connection_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_CONNECTION_SOURCE, oldCREATE__SRC__connection_source, creatE__SRC__connection_source));
			}
		}
		return creatE__SRC__connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCREATE__SRC__connection_source() {
		return creatE__SRC__connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__connection_source(ConnectionInstance newCREATE__SRC__connection_source) {
		ConnectionInstance oldCREATE__SRC__connection_source = creatE__SRC__connection_source;
		creatE__SRC__connection_source = newCREATE__SRC__connection_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_CONNECTION_SOURCE, oldCREATE__SRC__connection_source, creatE__SRC__connection_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__SRC__feature_destination() {
		if (creatE__SRC__feature_destination != null && creatE__SRC__feature_destination.eIsProxy()) {
			InternalEObject oldCREATE__SRC__feature_destination = (InternalEObject)creatE__SRC__feature_destination;
			creatE__SRC__feature_destination = (FeatureInstance)eResolveProxy(oldCREATE__SRC__feature_destination);
			if (creatE__SRC__feature_destination != oldCREATE__SRC__feature_destination) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_DESTINATION, oldCREATE__SRC__feature_destination, creatE__SRC__feature_destination));
			}
		}
		return creatE__SRC__feature_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__SRC__feature_destination() {
		return creatE__SRC__feature_destination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__feature_destination(FeatureInstance newCREATE__SRC__feature_destination) {
		FeatureInstance oldCREATE__SRC__feature_destination = creatE__SRC__feature_destination;
		creatE__SRC__feature_destination = newCREATE__SRC__feature_destination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_DESTINATION, oldCREATE__SRC__feature_destination, creatE__SRC__feature_destination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__SRC__feature_source() {
		if (creatE__SRC__feature_source != null && creatE__SRC__feature_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__feature_source = (InternalEObject)creatE__SRC__feature_source;
			creatE__SRC__feature_source = (FeatureInstance)eResolveProxy(oldCREATE__SRC__feature_source);
			if (creatE__SRC__feature_source != oldCREATE__SRC__feature_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_SOURCE, oldCREATE__SRC__feature_source, creatE__SRC__feature_source));
			}
		}
		return creatE__SRC__feature_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__SRC__feature_source() {
		return creatE__SRC__feature_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__feature_source(FeatureInstance newCREATE__SRC__feature_source) {
		FeatureInstance oldCREATE__SRC__feature_source = creatE__SRC__feature_source;
		creatE__SRC__feature_source = newCREATE__SRC__feature_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_SOURCE, oldCREATE__SRC__feature_source, creatE__SRC__feature_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference getCREATE__SRC__ref_connection_source() {
		if (creatE__SRC__ref_connection_source != null && creatE__SRC__ref_connection_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__ref_connection_source = (InternalEObject)creatE__SRC__ref_connection_source;
			creatE__SRC__ref_connection_source = (ConnectionReference)eResolveProxy(oldCREATE__SRC__ref_connection_source);
			if (creatE__SRC__ref_connection_source != oldCREATE__SRC__ref_connection_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE, oldCREATE__SRC__ref_connection_source, creatE__SRC__ref_connection_source));
			}
		}
		return creatE__SRC__ref_connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference basicGetCREATE__SRC__ref_connection_source() {
		return creatE__SRC__ref_connection_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__ref_connection_source(ConnectionReference newCREATE__SRC__ref_connection_source) {
		ConnectionReference oldCREATE__SRC__ref_connection_source = creatE__SRC__ref_connection_source;
		creatE__SRC__ref_connection_source = newCREATE__SRC__ref_connection_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE, oldCREATE__SRC__ref_connection_source, creatE__SRC__ref_connection_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__component_conn_destination_target() {
		if (contexT__TRG__component_conn_destination_target != null && contexT__TRG__component_conn_destination_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__component_conn_destination_target = (InternalEObject)contexT__TRG__component_conn_destination_target;
			contexT__TRG__component_conn_destination_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__component_conn_destination_target);
			if (contexT__TRG__component_conn_destination_target != oldCONTEXT__TRG__component_conn_destination_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_DESTINATION_TARGET, oldCONTEXT__TRG__component_conn_destination_target, contexT__TRG__component_conn_destination_target));
			}
		}
		return contexT__TRG__component_conn_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__component_conn_destination_target() {
		return contexT__TRG__component_conn_destination_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__component_conn_destination_target(ComponentInstance newCONTEXT__TRG__component_conn_destination_target) {
		ComponentInstance oldCONTEXT__TRG__component_conn_destination_target = contexT__TRG__component_conn_destination_target;
		contexT__TRG__component_conn_destination_target = newCONTEXT__TRG__component_conn_destination_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_DESTINATION_TARGET, oldCONTEXT__TRG__component_conn_destination_target, contexT__TRG__component_conn_destination_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__component_conn_source_target() {
		if (contexT__TRG__component_conn_source_target != null && contexT__TRG__component_conn_source_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__component_conn_source_target = (InternalEObject)contexT__TRG__component_conn_source_target;
			contexT__TRG__component_conn_source_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__component_conn_source_target);
			if (contexT__TRG__component_conn_source_target != oldCONTEXT__TRG__component_conn_source_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_SOURCE_TARGET, oldCONTEXT__TRG__component_conn_source_target, contexT__TRG__component_conn_source_target));
			}
		}
		return contexT__TRG__component_conn_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__component_conn_source_target() {
		return contexT__TRG__component_conn_source_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__component_conn_source_target(ComponentInstance newCONTEXT__TRG__component_conn_source_target) {
		ComponentInstance oldCONTEXT__TRG__component_conn_source_target = contexT__TRG__component_conn_source_target;
		contexT__TRG__component_conn_source_target = newCONTEXT__TRG__component_conn_source_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_SOURCE_TARGET, oldCONTEXT__TRG__component_conn_source_target, contexT__TRG__component_conn_source_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCONTEXT__TRG__component_target() {
		if (contexT__TRG__component_target != null && contexT__TRG__component_target.eIsProxy()) {
			InternalEObject oldCONTEXT__TRG__component_target = (InternalEObject)contexT__TRG__component_target;
			contexT__TRG__component_target = (ComponentInstance)eResolveProxy(oldCONTEXT__TRG__component_target);
			if (contexT__TRG__component_target != oldCONTEXT__TRG__component_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_TARGET, oldCONTEXT__TRG__component_target, contexT__TRG__component_target));
			}
		}
		return contexT__TRG__component_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCONTEXT__TRG__component_target() {
		return contexT__TRG__component_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__TRG__component_target(ComponentInstance newCONTEXT__TRG__component_target) {
		ComponentInstance oldCONTEXT__TRG__component_target = contexT__TRG__component_target;
		contexT__TRG__component_target = newCONTEXT__TRG__component_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_TARGET, oldCONTEXT__TRG__component_target, contexT__TRG__component_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getCREATE__TRG__data() {
		if (creatE__TRG__data != null && creatE__TRG__data.eIsProxy()) {
			InternalEObject oldCREATE__TRG__data = (InternalEObject)creatE__TRG__data;
			creatE__TRG__data = (ComponentInstance)eResolveProxy(oldCREATE__TRG__data);
			if (creatE__TRG__data != oldCREATE__TRG__data) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATA, oldCREATE__TRG__data, creatE__TRG__data));
			}
		}
		return creatE__TRG__data;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance basicGetCREATE__TRG__data() {
		return creatE__TRG__data;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__data(ComponentInstance newCREATE__TRG__data) {
		ComponentInstance oldCREATE__TRG__data = creatE__TRG__data;
		creatE__TRG__data = newCREATE__TRG__data;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATA, oldCREATE__TRG__data, creatE__TRG__data));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCREATE__TRG__dataaccess1() {
		if (creatE__TRG__dataaccess1 != null && creatE__TRG__dataaccess1.eIsProxy()) {
			InternalEObject oldCREATE__TRG__dataaccess1 = (InternalEObject)creatE__TRG__dataaccess1;
			creatE__TRG__dataaccess1 = (ConnectionInstance)eResolveProxy(oldCREATE__TRG__dataaccess1);
			if (creatE__TRG__dataaccess1 != oldCREATE__TRG__dataaccess1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS1, oldCREATE__TRG__dataaccess1, creatE__TRG__dataaccess1));
			}
		}
		return creatE__TRG__dataaccess1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCREATE__TRG__dataaccess1() {
		return creatE__TRG__dataaccess1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__dataaccess1(ConnectionInstance newCREATE__TRG__dataaccess1) {
		ConnectionInstance oldCREATE__TRG__dataaccess1 = creatE__TRG__dataaccess1;
		creatE__TRG__dataaccess1 = newCREATE__TRG__dataaccess1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS1, oldCREATE__TRG__dataaccess1, creatE__TRG__dataaccess1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance getCREATE__TRG__dataaccess2() {
		if (creatE__TRG__dataaccess2 != null && creatE__TRG__dataaccess2.eIsProxy()) {
			InternalEObject oldCREATE__TRG__dataaccess2 = (InternalEObject)creatE__TRG__dataaccess2;
			creatE__TRG__dataaccess2 = (ConnectionInstance)eResolveProxy(oldCREATE__TRG__dataaccess2);
			if (creatE__TRG__dataaccess2 != oldCREATE__TRG__dataaccess2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS2, oldCREATE__TRG__dataaccess2, creatE__TRG__dataaccess2));
			}
		}
		return creatE__TRG__dataaccess2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionInstance basicGetCREATE__TRG__dataaccess2() {
		return creatE__TRG__dataaccess2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__dataaccess2(ConnectionInstance newCREATE__TRG__dataaccess2) {
		ConnectionInstance oldCREATE__TRG__dataaccess2 = creatE__TRG__dataaccess2;
		creatE__TRG__dataaccess2 = newCREATE__TRG__dataaccess2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS2, oldCREATE__TRG__dataaccess2, creatE__TRG__dataaccess2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__TRG__feature1() {
		if (creatE__TRG__feature1 != null && creatE__TRG__feature1.eIsProxy()) {
			InternalEObject oldCREATE__TRG__feature1 = (InternalEObject)creatE__TRG__feature1;
			creatE__TRG__feature1 = (FeatureInstance)eResolveProxy(oldCREATE__TRG__feature1);
			if (creatE__TRG__feature1 != oldCREATE__TRG__feature1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE1, oldCREATE__TRG__feature1, creatE__TRG__feature1));
			}
		}
		return creatE__TRG__feature1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__TRG__feature1() {
		return creatE__TRG__feature1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__feature1(FeatureInstance newCREATE__TRG__feature1) {
		FeatureInstance oldCREATE__TRG__feature1 = creatE__TRG__feature1;
		creatE__TRG__feature1 = newCREATE__TRG__feature1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE1, oldCREATE__TRG__feature1, creatE__TRG__feature1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance getCREATE__TRG__feature2() {
		if (creatE__TRG__feature2 != null && creatE__TRG__feature2.eIsProxy()) {
			InternalEObject oldCREATE__TRG__feature2 = (InternalEObject)creatE__TRG__feature2;
			creatE__TRG__feature2 = (FeatureInstance)eResolveProxy(oldCREATE__TRG__feature2);
			if (creatE__TRG__feature2 != oldCREATE__TRG__feature2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE2, oldCREATE__TRG__feature2, creatE__TRG__feature2));
			}
		}
		return creatE__TRG__feature2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureInstance basicGetCREATE__TRG__feature2() {
		return creatE__TRG__feature2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__feature2(FeatureInstance newCREATE__TRG__feature2) {
		FeatureInstance oldCREATE__TRG__feature2 = creatE__TRG__feature2;
		creatE__TRG__feature2 = newCREATE__TRG__feature2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE2, oldCREATE__TRG__feature2, creatE__TRG__feature2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference getCREATE__TRG__refconnection1() {
		if (creatE__TRG__refconnection1 != null && creatE__TRG__refconnection1.eIsProxy()) {
			InternalEObject oldCREATE__TRG__refconnection1 = (InternalEObject)creatE__TRG__refconnection1;
			creatE__TRG__refconnection1 = (ConnectionReference)eResolveProxy(oldCREATE__TRG__refconnection1);
			if (creatE__TRG__refconnection1 != oldCREATE__TRG__refconnection1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION1, oldCREATE__TRG__refconnection1, creatE__TRG__refconnection1));
			}
		}
		return creatE__TRG__refconnection1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference basicGetCREATE__TRG__refconnection1() {
		return creatE__TRG__refconnection1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__refconnection1(ConnectionReference newCREATE__TRG__refconnection1) {
		ConnectionReference oldCREATE__TRG__refconnection1 = creatE__TRG__refconnection1;
		creatE__TRG__refconnection1 = newCREATE__TRG__refconnection1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION1, oldCREATE__TRG__refconnection1, creatE__TRG__refconnection1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference getCREATE__TRG__refconnection2() {
		if (creatE__TRG__refconnection2 != null && creatE__TRG__refconnection2.eIsProxy()) {
			InternalEObject oldCREATE__TRG__refconnection2 = (InternalEObject)creatE__TRG__refconnection2;
			creatE__TRG__refconnection2 = (ConnectionReference)eResolveProxy(oldCREATE__TRG__refconnection2);
			if (creatE__TRG__refconnection2 != oldCREATE__TRG__refconnection2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION2, oldCREATE__TRG__refconnection2, creatE__TRG__refconnection2));
			}
		}
		return creatE__TRG__refconnection2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionReference basicGetCREATE__TRG__refconnection2() {
		return creatE__TRG__refconnection2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__refconnection2(ConnectionReference newCREATE__TRG__refconnection2) {
		ConnectionReference oldCREATE__TRG__refconnection2 = creatE__TRG__refconnection2;
		creatE__TRG__refconnection2 = newCREATE__TRG__refconnection2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION2, oldCREATE__TRG__refconnection2, creatE__TRG__refconnection2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__c2c() {
		if (contexT__CORR__c2c != null && contexT__CORR__c2c.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__c2c = (InternalEObject)contexT__CORR__c2c;
			contexT__CORR__c2c = (CompToComp)eResolveProxy(oldCONTEXT__CORR__c2c);
			if (contexT__CORR__c2c != oldCONTEXT__CORR__c2c) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_C2C, oldCONTEXT__CORR__c2c, contexT__CORR__c2c));
			}
		}
		return contexT__CORR__c2c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__c2c() {
		return contexT__CORR__c2c;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__c2c(CompToComp newCONTEXT__CORR__c2c) {
		CompToComp oldCONTEXT__CORR__c2c = contexT__CORR__c2c;
		contexT__CORR__c2c = newCONTEXT__CORR__c2c;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_C2C, oldCONTEXT__CORR__c2c, contexT__CORR__c2c));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat getCREATE__CORR__f2f1() {
		if (creatE__CORR__f2f1 != null && creatE__CORR__f2f1.eIsProxy()) {
			InternalEObject oldCREATE__CORR__f2f1 = (InternalEObject)creatE__CORR__f2f1;
			creatE__CORR__f2f1 = (FeatToFeat)eResolveProxy(oldCREATE__CORR__f2f1);
			if (creatE__CORR__f2f1 != oldCREATE__CORR__f2f1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F1, oldCREATE__CORR__f2f1, creatE__CORR__f2f1));
			}
		}
		return creatE__CORR__f2f1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat basicGetCREATE__CORR__f2f1() {
		return creatE__CORR__f2f1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__f2f1(FeatToFeat newCREATE__CORR__f2f1) {
		FeatToFeat oldCREATE__CORR__f2f1 = creatE__CORR__f2f1;
		creatE__CORR__f2f1 = newCREATE__CORR__f2f1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F1, oldCREATE__CORR__f2f1, creatE__CORR__f2f1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat getCREATE__CORR__f2f2() {
		if (creatE__CORR__f2f2 != null && creatE__CORR__f2f2.eIsProxy()) {
			InternalEObject oldCREATE__CORR__f2f2 = (InternalEObject)creatE__CORR__f2f2;
			creatE__CORR__f2f2 = (FeatToFeat)eResolveProxy(oldCREATE__CORR__f2f2);
			if (creatE__CORR__f2f2 != oldCREATE__CORR__f2f2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F2, oldCREATE__CORR__f2f2, creatE__CORR__f2f2));
			}
		}
		return creatE__CORR__f2f2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatToFeat basicGetCREATE__CORR__f2f2() {
		return creatE__CORR__f2f2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__f2f2(FeatToFeat newCREATE__CORR__f2f2) {
		FeatToFeat oldCREATE__CORR__f2f2 = creatE__CORR__f2f2;
		creatE__CORR__f2f2 = newCREATE__CORR__f2f2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F2, oldCREATE__CORR__f2f2, creatE__CORR__f2f2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__p2a1() {
		if (creatE__CORR__p2a1 != null && creatE__CORR__p2a1.eIsProxy()) {
			InternalEObject oldCREATE__CORR__p2a1 = (InternalEObject)creatE__CORR__p2a1;
			creatE__CORR__p2a1 = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__p2a1);
			if (creatE__CORR__p2a1 != oldCREATE__CORR__p2a1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A1, oldCREATE__CORR__p2a1, creatE__CORR__p2a1));
			}
		}
		return creatE__CORR__p2a1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__p2a1() {
		return creatE__CORR__p2a1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__p2a1(PortConnectionToSharedData newCREATE__CORR__p2a1) {
		PortConnectionToSharedData oldCREATE__CORR__p2a1 = creatE__CORR__p2a1;
		creatE__CORR__p2a1 = newCREATE__CORR__p2a1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A1, oldCREATE__CORR__p2a1, creatE__CORR__p2a1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__p2a2() {
		if (creatE__CORR__p2a2 != null && creatE__CORR__p2a2.eIsProxy()) {
			InternalEObject oldCREATE__CORR__p2a2 = (InternalEObject)creatE__CORR__p2a2;
			creatE__CORR__p2a2 = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__p2a2);
			if (creatE__CORR__p2a2 != oldCREATE__CORR__p2a2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A2, oldCREATE__CORR__p2a2, creatE__CORR__p2a2));
			}
		}
		return creatE__CORR__p2a2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__p2a2() {
		return creatE__CORR__p2a2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__p2a2(PortConnectionToSharedData newCREATE__CORR__p2a2) {
		PortConnectionToSharedData oldCREATE__CORR__p2a2 = creatE__CORR__p2a2;
		creatE__CORR__p2a2 = newCREATE__CORR__p2a2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A2, oldCREATE__CORR__p2a2, creatE__CORR__p2a2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__p2d() {
		if (creatE__CORR__p2d != null && creatE__CORR__p2d.eIsProxy()) {
			InternalEObject oldCREATE__CORR__p2d = (InternalEObject)creatE__CORR__p2d;
			creatE__CORR__p2d = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__p2d);
			if (creatE__CORR__p2d != oldCREATE__CORR__p2d) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2D, oldCREATE__CORR__p2d, creatE__CORR__p2d));
			}
		}
		return creatE__CORR__p2d;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__p2d() {
		return creatE__CORR__p2d;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__p2d(PortConnectionToSharedData newCREATE__CORR__p2d) {
		PortConnectionToSharedData oldCREATE__CORR__p2d = creatE__CORR__p2d;
		creatE__CORR__p2d = newCREATE__CORR__p2d;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2D, oldCREATE__CORR__p2d, creatE__CORR__p2d));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__p2r1() {
		if (creatE__CORR__p2r1 != null && creatE__CORR__p2r1.eIsProxy()) {
			InternalEObject oldCREATE__CORR__p2r1 = (InternalEObject)creatE__CORR__p2r1;
			creatE__CORR__p2r1 = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__p2r1);
			if (creatE__CORR__p2r1 != oldCREATE__CORR__p2r1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R1, oldCREATE__CORR__p2r1, creatE__CORR__p2r1));
			}
		}
		return creatE__CORR__p2r1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__p2r1() {
		return creatE__CORR__p2r1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__p2r1(PortConnectionToSharedData newCREATE__CORR__p2r1) {
		PortConnectionToSharedData oldCREATE__CORR__p2r1 = creatE__CORR__p2r1;
		creatE__CORR__p2r1 = newCREATE__CORR__p2r1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R1, oldCREATE__CORR__p2r1, creatE__CORR__p2r1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData getCREATE__CORR__p2r2() {
		if (creatE__CORR__p2r2 != null && creatE__CORR__p2r2.eIsProxy()) {
			InternalEObject oldCREATE__CORR__p2r2 = (InternalEObject)creatE__CORR__p2r2;
			creatE__CORR__p2r2 = (PortConnectionToSharedData)eResolveProxy(oldCREATE__CORR__p2r2);
			if (creatE__CORR__p2r2 != oldCREATE__CORR__p2r2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R2, oldCREATE__CORR__p2r2, creatE__CORR__p2r2));
			}
		}
		return creatE__CORR__p2r2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortConnectionToSharedData basicGetCREATE__CORR__p2r2() {
		return creatE__CORR__p2r2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__p2r2(PortConnectionToSharedData newCREATE__CORR__p2r2) {
		PortConnectionToSharedData oldCREATE__CORR__p2r2 = creatE__CORR__p2r2;
		creatE__CORR__p2r2 = newCREATE__CORR__p2r2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R2, oldCREATE__CORR__p2r2, creatE__CORR__p2r2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__sub2sub1() {
		if (contexT__CORR__sub2sub1 != null && contexT__CORR__sub2sub1.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__sub2sub1 = (InternalEObject)contexT__CORR__sub2sub1;
			contexT__CORR__sub2sub1 = (CompToComp)eResolveProxy(oldCONTEXT__CORR__sub2sub1);
			if (contexT__CORR__sub2sub1 != oldCONTEXT__CORR__sub2sub1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB1, oldCONTEXT__CORR__sub2sub1, contexT__CORR__sub2sub1));
			}
		}
		return contexT__CORR__sub2sub1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__sub2sub1() {
		return contexT__CORR__sub2sub1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__sub2sub1(CompToComp newCONTEXT__CORR__sub2sub1) {
		CompToComp oldCONTEXT__CORR__sub2sub1 = contexT__CORR__sub2sub1;
		contexT__CORR__sub2sub1 = newCONTEXT__CORR__sub2sub1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB1, oldCONTEXT__CORR__sub2sub1, contexT__CORR__sub2sub1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp getCONTEXT__CORR__sub2sub2() {
		if (contexT__CORR__sub2sub2 != null && contexT__CORR__sub2sub2.eIsProxy()) {
			InternalEObject oldCONTEXT__CORR__sub2sub2 = (InternalEObject)contexT__CORR__sub2sub2;
			contexT__CORR__sub2sub2 = (CompToComp)eResolveProxy(oldCONTEXT__CORR__sub2sub2);
			if (contexT__CORR__sub2sub2 != oldCONTEXT__CORR__sub2sub2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB2, oldCONTEXT__CORR__sub2sub2, contexT__CORR__sub2sub2));
			}
		}
		return contexT__CORR__sub2sub2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompToComp basicGetCONTEXT__CORR__sub2sub2() {
		return contexT__CORR__sub2sub2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCONTEXT__CORR__sub2sub2(CompToComp newCONTEXT__CORR__sub2sub2) {
		CompToComp oldCONTEXT__CORR__sub2sub2 = contexT__CORR__sub2sub2;
		contexT__CORR__sub2sub2 = newCONTEXT__CORR__sub2sub2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB2, oldCONTEXT__CORR__sub2sub2, contexT__CORR__sub2sub2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_DESTINATION:
				if (resolve) return getCONTEXT__SRC__component_conn_destination();
				return basicGetCONTEXT__SRC__component_conn_destination();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_SOURCE:
				if (resolve) return getCONTEXT__SRC__component_conn_source();
				return basicGetCONTEXT__SRC__component_conn_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				if (resolve) return getCONTEXT__SRC__component_source();
				return basicGetCONTEXT__SRC__component_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				if (resolve) return getCREATE__SRC__connection_source();
				return basicGetCREATE__SRC__connection_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_DESTINATION:
				if (resolve) return getCREATE__SRC__feature_destination();
				return basicGetCREATE__SRC__feature_destination();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_SOURCE:
				if (resolve) return getCREATE__SRC__feature_source();
				return basicGetCREATE__SRC__feature_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE:
				if (resolve) return getCREATE__SRC__ref_connection_source();
				return basicGetCREATE__SRC__ref_connection_source();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_DESTINATION_TARGET:
				if (resolve) return getCONTEXT__TRG__component_conn_destination_target();
				return basicGetCONTEXT__TRG__component_conn_destination_target();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_SOURCE_TARGET:
				if (resolve) return getCONTEXT__TRG__component_conn_source_target();
				return basicGetCONTEXT__TRG__component_conn_source_target();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				if (resolve) return getCONTEXT__TRG__component_target();
				return basicGetCONTEXT__TRG__component_target();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATA:
				if (resolve) return getCREATE__TRG__data();
				return basicGetCREATE__TRG__data();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS1:
				if (resolve) return getCREATE__TRG__dataaccess1();
				return basicGetCREATE__TRG__dataaccess1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS2:
				if (resolve) return getCREATE__TRG__dataaccess2();
				return basicGetCREATE__TRG__dataaccess2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE1:
				if (resolve) return getCREATE__TRG__feature1();
				return basicGetCREATE__TRG__feature1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE2:
				if (resolve) return getCREATE__TRG__feature2();
				return basicGetCREATE__TRG__feature2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION1:
				if (resolve) return getCREATE__TRG__refconnection1();
				return basicGetCREATE__TRG__refconnection1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION2:
				if (resolve) return getCREATE__TRG__refconnection2();
				return basicGetCREATE__TRG__refconnection2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_C2C:
				if (resolve) return getCONTEXT__CORR__c2c();
				return basicGetCONTEXT__CORR__c2c();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F1:
				if (resolve) return getCREATE__CORR__f2f1();
				return basicGetCREATE__CORR__f2f1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F2:
				if (resolve) return getCREATE__CORR__f2f2();
				return basicGetCREATE__CORR__f2f2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A1:
				if (resolve) return getCREATE__CORR__p2a1();
				return basicGetCREATE__CORR__p2a1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A2:
				if (resolve) return getCREATE__CORR__p2a2();
				return basicGetCREATE__CORR__p2a2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2D:
				if (resolve) return getCREATE__CORR__p2d();
				return basicGetCREATE__CORR__p2d();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R1:
				if (resolve) return getCREATE__CORR__p2r1();
				return basicGetCREATE__CORR__p2r1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R2:
				if (resolve) return getCREATE__CORR__p2r2();
				return basicGetCREATE__CORR__p2r2();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB1:
				if (resolve) return getCONTEXT__CORR__sub2sub1();
				return basicGetCONTEXT__CORR__sub2sub1();
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB2:
				if (resolve) return getCONTEXT__CORR__sub2sub2();
				return basicGetCONTEXT__CORR__sub2sub2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_DESTINATION:
				setCONTEXT__SRC__component_conn_destination((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_SOURCE:
				setCONTEXT__SRC__component_conn_source((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				setCONTEXT__SRC__component_source((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				setCREATE__SRC__connection_source((ConnectionInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_DESTINATION:
				setCREATE__SRC__feature_destination((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_SOURCE:
				setCREATE__SRC__feature_source((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE:
				setCREATE__SRC__ref_connection_source((ConnectionReference)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_DESTINATION_TARGET:
				setCONTEXT__TRG__component_conn_destination_target((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_SOURCE_TARGET:
				setCONTEXT__TRG__component_conn_source_target((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				setCONTEXT__TRG__component_target((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATA:
				setCREATE__TRG__data((ComponentInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS1:
				setCREATE__TRG__dataaccess1((ConnectionInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS2:
				setCREATE__TRG__dataaccess2((ConnectionInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE1:
				setCREATE__TRG__feature1((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE2:
				setCREATE__TRG__feature2((FeatureInstance)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION1:
				setCREATE__TRG__refconnection1((ConnectionReference)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION2:
				setCREATE__TRG__refconnection2((ConnectionReference)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_C2C:
				setCONTEXT__CORR__c2c((CompToComp)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F1:
				setCREATE__CORR__f2f1((FeatToFeat)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F2:
				setCREATE__CORR__f2f2((FeatToFeat)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A1:
				setCREATE__CORR__p2a1((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A2:
				setCREATE__CORR__p2a2((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2D:
				setCREATE__CORR__p2d((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R1:
				setCREATE__CORR__p2r1((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R2:
				setCREATE__CORR__p2r2((PortConnectionToSharedData)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB1:
				setCONTEXT__CORR__sub2sub1((CompToComp)newValue);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB2:
				setCONTEXT__CORR__sub2sub2((CompToComp)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_DESTINATION:
				setCONTEXT__SRC__component_conn_destination((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_SOURCE:
				setCONTEXT__SRC__component_conn_source((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				setCONTEXT__SRC__component_source((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				setCREATE__SRC__connection_source((ConnectionInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_DESTINATION:
				setCREATE__SRC__feature_destination((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_SOURCE:
				setCREATE__SRC__feature_source((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE:
				setCREATE__SRC__ref_connection_source((ConnectionReference)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_DESTINATION_TARGET:
				setCONTEXT__TRG__component_conn_destination_target((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_SOURCE_TARGET:
				setCONTEXT__TRG__component_conn_source_target((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				setCONTEXT__TRG__component_target((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATA:
				setCREATE__TRG__data((ComponentInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS1:
				setCREATE__TRG__dataaccess1((ConnectionInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS2:
				setCREATE__TRG__dataaccess2((ConnectionInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE1:
				setCREATE__TRG__feature1((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE2:
				setCREATE__TRG__feature2((FeatureInstance)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION1:
				setCREATE__TRG__refconnection1((ConnectionReference)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION2:
				setCREATE__TRG__refconnection2((ConnectionReference)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_C2C:
				setCONTEXT__CORR__c2c((CompToComp)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F1:
				setCREATE__CORR__f2f1((FeatToFeat)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F2:
				setCREATE__CORR__f2f2((FeatToFeat)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A1:
				setCREATE__CORR__p2a1((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A2:
				setCREATE__CORR__p2a2((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2D:
				setCREATE__CORR__p2d((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R1:
				setCREATE__CORR__p2r1((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R2:
				setCREATE__CORR__p2r2((PortConnectionToSharedData)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB1:
				setCONTEXT__CORR__sub2sub1((CompToComp)null);
				return;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB2:
				setCONTEXT__CORR__sub2sub2((CompToComp)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_DESTINATION:
				return contexT__SRC__component_conn_destination != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_SOURCE:
				return contexT__SRC__component_conn_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_SOURCE:
				return contexT__SRC__component_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_CONNECTION_SOURCE:
				return creatE__SRC__connection_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_DESTINATION:
				return creatE__SRC__feature_destination != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_SOURCE:
				return creatE__SRC__feature_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE:
				return creatE__SRC__ref_connection_source != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_DESTINATION_TARGET:
				return contexT__TRG__component_conn_destination_target != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_SOURCE_TARGET:
				return contexT__TRG__component_conn_source_target != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_TARGET:
				return contexT__TRG__component_target != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATA:
				return creatE__TRG__data != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS1:
				return creatE__TRG__dataaccess1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS2:
				return creatE__TRG__dataaccess2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE1:
				return creatE__TRG__feature1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE2:
				return creatE__TRG__feature2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION1:
				return creatE__TRG__refconnection1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION2:
				return creatE__TRG__refconnection2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_C2C:
				return contexT__CORR__c2c != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F1:
				return creatE__CORR__f2f1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F2:
				return creatE__CORR__f2f2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A1:
				return creatE__CORR__p2a1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A2:
				return creatE__CORR__p2a2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2D:
				return creatE__CORR__p2d != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R1:
				return creatE__CORR__p2r1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R2:
				return creatE__CORR__p2r2 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB1:
				return contexT__CORR__sub2sub1 != null;
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB2:
				return contexT__CORR__sub2sub2 != null;
		}
		return super.eIsSet(featureID);
	}

} //PortConnectionToDataAccessAtProcess00__MarkerImpl
