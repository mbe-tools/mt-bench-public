/**
 */
package Tgg.impl;

import Tgg.SystemToSystem;
import Tgg.SystemToSystem__Marker;
import Tgg.TggPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.osate.aadl2.instance.SystemInstance;

import runtime.impl.TGGRuleApplicationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System To System Marker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Tgg.impl.SystemToSystem__MarkerImpl#getCREATE__SRC__system_source <em>CREATE SRC system source</em>}</li>
 *   <li>{@link Tgg.impl.SystemToSystem__MarkerImpl#getCREATE__TRG__system_target <em>CREATE TRG system target</em>}</li>
 *   <li>{@link Tgg.impl.SystemToSystem__MarkerImpl#getCREATE__CORR__s2s <em>CREATE CORR s2s</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SystemToSystem__MarkerImpl extends TGGRuleApplicationImpl implements SystemToSystem__Marker {
	/**
	 * The cached value of the '{@link #getCREATE__SRC__system_source() <em>CREATE SRC system source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__SRC__system_source()
	 * @generated
	 * @ordered
	 */
	protected SystemInstance creatE__SRC__system_source;

	/**
	 * The cached value of the '{@link #getCREATE__TRG__system_target() <em>CREATE TRG system target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__TRG__system_target()
	 * @generated
	 * @ordered
	 */
	protected SystemInstance creatE__TRG__system_target;

	/**
	 * The cached value of the '{@link #getCREATE__CORR__s2s() <em>CREATE CORR s2s</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATE__CORR__s2s()
	 * @generated
	 * @ordered
	 */
	protected SystemToSystem creatE__CORR__s2s;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemToSystem__MarkerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TggPackage.eINSTANCE.getSystemToSystem__Marker();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance getCREATE__SRC__system_source() {
		if (creatE__SRC__system_source != null && creatE__SRC__system_source.eIsProxy()) {
			InternalEObject oldCREATE__SRC__system_source = (InternalEObject)creatE__SRC__system_source;
			creatE__SRC__system_source = (SystemInstance)eResolveProxy(oldCREATE__SRC__system_source);
			if (creatE__SRC__system_source != oldCREATE__SRC__system_source) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_SRC_SYSTEM_SOURCE, oldCREATE__SRC__system_source, creatE__SRC__system_source));
			}
		}
		return creatE__SRC__system_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance basicGetCREATE__SRC__system_source() {
		return creatE__SRC__system_source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__SRC__system_source(SystemInstance newCREATE__SRC__system_source) {
		SystemInstance oldCREATE__SRC__system_source = creatE__SRC__system_source;
		creatE__SRC__system_source = newCREATE__SRC__system_source;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_SRC_SYSTEM_SOURCE, oldCREATE__SRC__system_source, creatE__SRC__system_source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance getCREATE__TRG__system_target() {
		if (creatE__TRG__system_target != null && creatE__TRG__system_target.eIsProxy()) {
			InternalEObject oldCREATE__TRG__system_target = (InternalEObject)creatE__TRG__system_target;
			creatE__TRG__system_target = (SystemInstance)eResolveProxy(oldCREATE__TRG__system_target);
			if (creatE__TRG__system_target != oldCREATE__TRG__system_target) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_TRG_SYSTEM_TARGET, oldCREATE__TRG__system_target, creatE__TRG__system_target));
			}
		}
		return creatE__TRG__system_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance basicGetCREATE__TRG__system_target() {
		return creatE__TRG__system_target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__TRG__system_target(SystemInstance newCREATE__TRG__system_target) {
		SystemInstance oldCREATE__TRG__system_target = creatE__TRG__system_target;
		creatE__TRG__system_target = newCREATE__TRG__system_target;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_TRG_SYSTEM_TARGET, oldCREATE__TRG__system_target, creatE__TRG__system_target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemToSystem getCREATE__CORR__s2s() {
		if (creatE__CORR__s2s != null && creatE__CORR__s2s.eIsProxy()) {
			InternalEObject oldCREATE__CORR__s2s = (InternalEObject)creatE__CORR__s2s;
			creatE__CORR__s2s = (SystemToSystem)eResolveProxy(oldCREATE__CORR__s2s);
			if (creatE__CORR__s2s != oldCREATE__CORR__s2s) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_CORR_S2S, oldCREATE__CORR__s2s, creatE__CORR__s2s));
			}
		}
		return creatE__CORR__s2s;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemToSystem basicGetCREATE__CORR__s2s() {
		return creatE__CORR__s2s;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCREATE__CORR__s2s(SystemToSystem newCREATE__CORR__s2s) {
		SystemToSystem oldCREATE__CORR__s2s = creatE__CORR__s2s;
		creatE__CORR__s2s = newCREATE__CORR__s2s;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_CORR_S2S, oldCREATE__CORR__s2s, creatE__CORR__s2s));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_SRC_SYSTEM_SOURCE:
				if (resolve) return getCREATE__SRC__system_source();
				return basicGetCREATE__SRC__system_source();
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_TRG_SYSTEM_TARGET:
				if (resolve) return getCREATE__TRG__system_target();
				return basicGetCREATE__TRG__system_target();
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_CORR_S2S:
				if (resolve) return getCREATE__CORR__s2s();
				return basicGetCREATE__CORR__s2s();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_SRC_SYSTEM_SOURCE:
				setCREATE__SRC__system_source((SystemInstance)newValue);
				return;
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_TRG_SYSTEM_TARGET:
				setCREATE__TRG__system_target((SystemInstance)newValue);
				return;
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_CORR_S2S:
				setCREATE__CORR__s2s((SystemToSystem)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_SRC_SYSTEM_SOURCE:
				setCREATE__SRC__system_source((SystemInstance)null);
				return;
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_TRG_SYSTEM_TARGET:
				setCREATE__TRG__system_target((SystemInstance)null);
				return;
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_CORR_S2S:
				setCREATE__CORR__s2s((SystemToSystem)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_SRC_SYSTEM_SOURCE:
				return creatE__SRC__system_source != null;
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_TRG_SYSTEM_TARGET:
				return creatE__TRG__system_target != null;
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER__CREATE_CORR_S2S:
				return creatE__CORR__s2s != null;
		}
		return super.eIsSet(featureID);
	}

} //SystemToSystem__MarkerImpl
