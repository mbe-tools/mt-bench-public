/**
 */
package Tgg.util;

import Tgg.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see Tgg.TggPackage
 * @generated
 */
public class TggSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TggPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TggSwitch() {
		if (modelPackage == null) {
			modelPackage = TggPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TggPackage.SYSTEM_TO_SYSTEM: {
				SystemToSystem systemToSystem = (SystemToSystem)theEObject;
				T result = caseSystemToSystem(systemToSystem);
				if (result == null) result = caseCompToComp(systemToSystem);
				if (result == null) result = caseEnd2End(systemToSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.CONNECTION_TO_CONNECTION: {
				ConnectionToConnection connectionToConnection = (ConnectionToConnection)theEObject;
				T result = caseConnectionToConnection(connectionToConnection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_TO_SHARED_DATA: {
				PortConnectionToSharedData portConnectionToSharedData = (PortConnectionToSharedData)theEObject;
				T result = casePortConnectionToSharedData(portConnectionToSharedData);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.REFERENCE2_REFERENCE: {
				Reference2Reference reference2Reference = (Reference2Reference)theEObject;
				T result = caseReference2Reference(reference2Reference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.END2_END: {
				End2End end2End = (End2End)theEObject;
				T result = caseEnd2End(end2End);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.COMP_TO_COMP: {
				CompToComp compToComp = (CompToComp)theEObject;
				T result = caseCompToComp(compToComp);
				if (result == null) result = caseEnd2End(compToComp);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.FEAT_TO_FEAT: {
				FeatToFeat featToFeat = (FeatToFeat)theEObject;
				T result = caseFeatToFeat(featToFeat);
				if (result == null) result = caseEnd2End(featToFeat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.COMPONENT_TO_COMPONENT_MARKER: {
				ComponentToComponent__Marker componentToComponent__Marker = (ComponentToComponent__Marker)theEObject;
				T result = caseComponentToComponent__Marker(componentToComponent__Marker);
				if (result == null) result = caseTGGRuleApplication(componentToComponent__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER: {
				ConnectionInstanceToConnectionInstance__Marker connectionInstanceToConnectionInstance__Marker = (ConnectionInstanceToConnectionInstance__Marker)theEObject;
				T result = caseConnectionInstanceToConnectionInstance__Marker(connectionInstanceToConnectionInstance__Marker);
				if (result == null) result = caseTGGRuleApplication(connectionInstanceToConnectionInstance__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER: {
				ConnectionRefToConnectionRefForComponentContext__Marker connectionRefToConnectionRefForComponentContext__Marker = (ConnectionRefToConnectionRefForComponentContext__Marker)theEObject;
				T result = caseConnectionRefToConnectionRefForComponentContext__Marker(connectionRefToConnectionRefForComponentContext__Marker);
				if (result == null) result = caseTGGRuleApplication(connectionRefToConnectionRefForComponentContext__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER: {
				ConnectionReferenceForDeviceSystemContext01__Marker connectionReferenceForDeviceSystemContext01__Marker = (ConnectionReferenceForDeviceSystemContext01__Marker)theEObject;
				T result = caseConnectionReferenceForDeviceSystemContext01__Marker(connectionReferenceForDeviceSystemContext01__Marker);
				if (result == null) result = caseTGGRuleApplication(connectionReferenceForDeviceSystemContext01__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER: {
				ConnectionReferenceForDeviceSystemContext10__Marker connectionReferenceForDeviceSystemContext10__Marker = (ConnectionReferenceForDeviceSystemContext10__Marker)theEObject;
				T result = caseConnectionReferenceForDeviceSystemContext10__Marker(connectionReferenceForDeviceSystemContext10__Marker);
				if (result == null) result = caseTGGRuleApplication(connectionReferenceForDeviceSystemContext10__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER: {
				ConnectionReferenceForDeviceSystemContext11__Marker connectionReferenceForDeviceSystemContext11__Marker = (ConnectionReferenceForDeviceSystemContext11__Marker)theEObject;
				T result = caseConnectionReferenceForDeviceSystemContext11__Marker(connectionReferenceForDeviceSystemContext11__Marker);
				if (result == null) result = caseTGGRuleApplication(connectionReferenceForDeviceSystemContext11__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.FEATURE_TO_FEATURE_MARKER: {
				FeatureToFeature__Marker featureToFeature__Marker = (FeatureToFeature__Marker)theEObject;
				T result = caseFeatureToFeature__Marker(featureToFeature__Marker);
				if (result == null) result = caseTGGRuleApplication(featureToFeature__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER: {
				PortConnectionForDeviceDestination__Marker portConnectionForDeviceDestination__Marker = (PortConnectionForDeviceDestination__Marker)theEObject;
				T result = casePortConnectionForDeviceDestination__Marker(portConnectionForDeviceDestination__Marker);
				if (result == null) result = caseTGGRuleApplication(portConnectionForDeviceDestination__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER: {
				PortConnectionForDeviceSource__Marker portConnectionForDeviceSource__Marker = (PortConnectionForDeviceSource__Marker)theEObject;
				T result = casePortConnectionForDeviceSource__Marker(portConnectionForDeviceSource__Marker);
				if (result == null) result = caseTGGRuleApplication(portConnectionForDeviceSource__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER: {
				PortConnectionToDataAccessAtProcess00__Marker portConnectionToDataAccessAtProcess00__Marker = (PortConnectionToDataAccessAtProcess00__Marker)theEObject;
				T result = casePortConnectionToDataAccessAtProcess00__Marker(portConnectionToDataAccessAtProcess00__Marker);
				if (result == null) result = caseTGGRuleApplication(portConnectionToDataAccessAtProcess00__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER: {
				PortConnectionToDataAccessAtProcess01__Marker portConnectionToDataAccessAtProcess01__Marker = (PortConnectionToDataAccessAtProcess01__Marker)theEObject;
				T result = casePortConnectionToDataAccessAtProcess01__Marker(portConnectionToDataAccessAtProcess01__Marker);
				if (result == null) result = caseTGGRuleApplication(portConnectionToDataAccessAtProcess01__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER: {
				PortConnectionToDataAccessAtProcess10__Marker portConnectionToDataAccessAtProcess10__Marker = (PortConnectionToDataAccessAtProcess10__Marker)theEObject;
				T result = casePortConnectionToDataAccessAtProcess10__Marker(portConnectionToDataAccessAtProcess10__Marker);
				if (result == null) result = caseTGGRuleApplication(portConnectionToDataAccessAtProcess10__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER: {
				PortConnectionToDataAccessAtProcess11__Marker portConnectionToDataAccessAtProcess11__Marker = (PortConnectionToDataAccessAtProcess11__Marker)theEObject;
				T result = casePortConnectionToDataAccessAtProcess11__Marker(portConnectionToDataAccessAtProcess11__Marker);
				if (result == null) result = caseTGGRuleApplication(portConnectionToDataAccessAtProcess11__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER: {
				PortConnectionToDataAccessAtSystem00__Marker portConnectionToDataAccessAtSystem00__Marker = (PortConnectionToDataAccessAtSystem00__Marker)theEObject;
				T result = casePortConnectionToDataAccessAtSystem00__Marker(portConnectionToDataAccessAtSystem00__Marker);
				if (result == null) result = caseTGGRuleApplication(portConnectionToDataAccessAtSystem00__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER: {
				PortConnectionToDataAccessAtSystem01__Marker portConnectionToDataAccessAtSystem01__Marker = (PortConnectionToDataAccessAtSystem01__Marker)theEObject;
				T result = casePortConnectionToDataAccessAtSystem01__Marker(portConnectionToDataAccessAtSystem01__Marker);
				if (result == null) result = caseTGGRuleApplication(portConnectionToDataAccessAtSystem01__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER: {
				PortConnectionToDataAccessAtSystem10__Marker portConnectionToDataAccessAtSystem10__Marker = (PortConnectionToDataAccessAtSystem10__Marker)theEObject;
				T result = casePortConnectionToDataAccessAtSystem10__Marker(portConnectionToDataAccessAtSystem10__Marker);
				if (result == null) result = caseTGGRuleApplication(portConnectionToDataAccessAtSystem10__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER: {
				PortConnectionToDataAccessAtSystem11__Marker portConnectionToDataAccessAtSystem11__Marker = (PortConnectionToDataAccessAtSystem11__Marker)theEObject;
				T result = casePortConnectionToDataAccessAtSystem11__Marker(portConnectionToDataAccessAtSystem11__Marker);
				if (result == null) result = caseTGGRuleApplication(portConnectionToDataAccessAtSystem11__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TggPackage.SYSTEM_TO_SYSTEM_MARKER: {
				SystemToSystem__Marker systemToSystem__Marker = (SystemToSystem__Marker)theEObject;
				T result = caseSystemToSystem__Marker(systemToSystem__Marker);
				if (result == null) result = caseTGGRuleApplication(systemToSystem__Marker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System To System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System To System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemToSystem(SystemToSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection To Connection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection To Connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionToConnection(ConnectionToConnection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection To Shared Data</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection To Shared Data</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionToSharedData(PortConnectionToSharedData object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference2 Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference2 Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReference2Reference(Reference2Reference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>End2 End</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>End2 End</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnd2End(End2End object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Comp To Comp</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Comp To Comp</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompToComp(CompToComp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feat To Feat</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feat To Feat</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatToFeat(FeatToFeat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component To Component Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component To Component Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentToComponent__Marker(ComponentToComponent__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Instance To Connection Instance Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Instance To Connection Instance Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionInstanceToConnectionInstance__Marker(ConnectionInstanceToConnectionInstance__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Ref To Connection Ref For Component Context Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Ref To Connection Ref For Component Context Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionRefToConnectionRefForComponentContext__Marker(ConnectionRefToConnectionRefForComponentContext__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Reference For Device System Context01 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Reference For Device System Context01 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionReferenceForDeviceSystemContext01__Marker(ConnectionReferenceForDeviceSystemContext01__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Reference For Device System Context10 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Reference For Device System Context10 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionReferenceForDeviceSystemContext10__Marker(ConnectionReferenceForDeviceSystemContext10__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection Reference For Device System Context11 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection Reference For Device System Context11 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionReferenceForDeviceSystemContext11__Marker(ConnectionReferenceForDeviceSystemContext11__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature To Feature Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature To Feature Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureToFeature__Marker(FeatureToFeature__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection For Device Destination Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection For Device Destination Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionForDeviceDestination__Marker(PortConnectionForDeviceDestination__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection For Device Source Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection For Device Source Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionForDeviceSource__Marker(PortConnectionForDeviceSource__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection To Data Access At Process00 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection To Data Access At Process00 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionToDataAccessAtProcess00__Marker(PortConnectionToDataAccessAtProcess00__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection To Data Access At Process01 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection To Data Access At Process01 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionToDataAccessAtProcess01__Marker(PortConnectionToDataAccessAtProcess01__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection To Data Access At Process10 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection To Data Access At Process10 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionToDataAccessAtProcess10__Marker(PortConnectionToDataAccessAtProcess10__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection To Data Access At Process11 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection To Data Access At Process11 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionToDataAccessAtProcess11__Marker(PortConnectionToDataAccessAtProcess11__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection To Data Access At System00 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection To Data Access At System00 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionToDataAccessAtSystem00__Marker(PortConnectionToDataAccessAtSystem00__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection To Data Access At System01 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection To Data Access At System01 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionToDataAccessAtSystem01__Marker(PortConnectionToDataAccessAtSystem01__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection To Data Access At System10 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection To Data Access At System10 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionToDataAccessAtSystem10__Marker(PortConnectionToDataAccessAtSystem10__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port Connection To Data Access At System11 Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port Connection To Data Access At System11 Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortConnectionToDataAccessAtSystem11__Marker(PortConnectionToDataAccessAtSystem11__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System To System Marker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System To System Marker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystemToSystem__Marker(SystemToSystem__Marker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TGG Rule Application</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TGG Rule Application</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTGGRuleApplication(TGGRuleApplication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TggSwitch
