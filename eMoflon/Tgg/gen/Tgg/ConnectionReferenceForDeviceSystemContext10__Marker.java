/**
 */
package Tgg;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection Reference For Device System Context10 Marker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__SRC__connectionRef_source <em>CREATE SRC connection Ref source</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__connection_source <em>CONTEXT SRC connection source</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__s_context <em>CONTEXT SRC scontext</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__SRC__s_destination <em>CREATE SRC sdestination</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__TRG__connectionRef_target <em>CREATE TRG connection Ref target</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__connection_target <em>CONTEXT TRG connection target</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__t_context <em>CONTEXT TRG tcontext</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__TRG__t_destination <em>CREATE TRG tdestination</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__CORR__e2e1 <em>CREATE CORR e2e1</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}</li>
 *   <li>{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__CORR__r2r <em>CREATE CORR r2r</em>}</li>
 * </ul>
 *
 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker()
 * @model
 * @generated
 */
public interface ConnectionReferenceForDeviceSystemContext10__Marker extends TGGRuleApplication {
	/**
	 * Returns the value of the '<em><b>CREATE SRC connection Ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC connection Ref source</em>' reference.
	 * @see #setCREATE__SRC__connectionRef_source(ConnectionReference)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__SRC__connectionRef_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionReference getCREATE__SRC__connectionRef_source();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__SRC__connectionRef_source <em>CREATE SRC connection Ref source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC connection Ref source</em>' reference.
	 * @see #getCREATE__SRC__connectionRef_source()
	 * @generated
	 */
	void setCREATE__SRC__connectionRef_source(ConnectionReference value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC connection source</em>' reference.
	 * @see #setCONTEXT__SRC__connection_source(ConnectionInstance)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__SRC__connection_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCONTEXT__SRC__connection_source();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__connection_source <em>CONTEXT SRC connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC connection source</em>' reference.
	 * @see #getCONTEXT__SRC__connection_source()
	 * @generated
	 */
	void setCONTEXT__SRC__connection_source(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC scontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC scontext</em>' reference.
	 * @see #setCONTEXT__SRC__s_context(ComponentInstance)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__SRC__s_context()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__s_context();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__s_context <em>CONTEXT SRC scontext</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC scontext</em>' reference.
	 * @see #getCONTEXT__SRC__s_context()
	 * @generated
	 */
	void setCONTEXT__SRC__s_context(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC sdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC sdestination</em>' reference.
	 * @see #setCREATE__SRC__s_destination(FeatureInstance)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__SRC__s_destination()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__SRC__s_destination();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__SRC__s_destination <em>CREATE SRC sdestination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC sdestination</em>' reference.
	 * @see #getCREATE__SRC__s_destination()
	 * @generated
	 */
	void setCREATE__SRC__s_destination(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC ssource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC ssource</em>' reference.
	 * @see #setCONTEXT__SRC__s_source(FeatureInstance)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__SRC__s_source()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCONTEXT__SRC__s_source();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC ssource</em>' reference.
	 * @see #getCONTEXT__SRC__s_source()
	 * @generated
	 */
	void setCONTEXT__SRC__s_source(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG connection Ref target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG connection Ref target</em>' reference.
	 * @see #setCREATE__TRG__connectionRef_target(ConnectionReference)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__TRG__connectionRef_target()
	 * @model required="true"
	 * @generated
	 */
	ConnectionReference getCREATE__TRG__connectionRef_target();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__TRG__connectionRef_target <em>CREATE TRG connection Ref target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG connection Ref target</em>' reference.
	 * @see #getCREATE__TRG__connectionRef_target()
	 * @generated
	 */
	void setCREATE__TRG__connectionRef_target(ConnectionReference value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG connection target</em>' reference.
	 * @see #setCONTEXT__TRG__connection_target(ConnectionInstance)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__TRG__connection_target()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCONTEXT__TRG__connection_target();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__connection_target <em>CONTEXT TRG connection target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG connection target</em>' reference.
	 * @see #getCONTEXT__TRG__connection_target()
	 * @generated
	 */
	void setCONTEXT__TRG__connection_target(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG tcontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG tcontext</em>' reference.
	 * @see #setCONTEXT__TRG__t_context(ComponentInstance)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__TRG__t_context()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__t_context();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__t_context <em>CONTEXT TRG tcontext</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG tcontext</em>' reference.
	 * @see #getCONTEXT__TRG__t_context()
	 * @generated
	 */
	void setCONTEXT__TRG__t_context(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG tdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG tdestination</em>' reference.
	 * @see #setCREATE__TRG__t_destination(FeatureInstance)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__TRG__t_destination()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__TRG__t_destination();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__TRG__t_destination <em>CREATE TRG tdestination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG tdestination</em>' reference.
	 * @see #getCREATE__TRG__t_destination()
	 * @generated
	 */
	void setCREATE__TRG__t_destination(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG tsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG tsource</em>' reference.
	 * @see #setCONTEXT__TRG__t_source(FeatureInstance)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__TRG__t_source()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCONTEXT__TRG__t_source();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG tsource</em>' reference.
	 * @see #getCONTEXT__TRG__t_source()
	 * @generated
	 */
	void setCONTEXT__TRG__t_source(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR c2c</em>' reference.
	 * @see #setCONTEXT__CORR__c2c(ConnectionToConnection)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__CORR__c2c()
	 * @model required="true"
	 * @generated
	 */
	ConnectionToConnection getCONTEXT__CORR__c2c();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR c2c</em>' reference.
	 * @see #getCONTEXT__CORR__c2c()
	 * @generated
	 */
	void setCONTEXT__CORR__c2c(ConnectionToConnection value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR comp2comp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR comp2comp</em>' reference.
	 * @see #setCONTEXT__CORR__comp2comp(CompToComp)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__CORR__comp2comp()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__comp2comp();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR comp2comp</em>' reference.
	 * @see #getCONTEXT__CORR__comp2comp()
	 * @generated
	 */
	void setCONTEXT__CORR__comp2comp(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR e2e1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR e2e1</em>' reference.
	 * @see #setCREATE__CORR__e2e1(End2End)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__CORR__e2e1()
	 * @model required="true"
	 * @generated
	 */
	End2End getCREATE__CORR__e2e1();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__CORR__e2e1 <em>CREATE CORR e2e1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR e2e1</em>' reference.
	 * @see #getCREATE__CORR__e2e1()
	 * @generated
	 */
	void setCREATE__CORR__e2e1(End2End value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR e2e2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR e2e2</em>' reference.
	 * @see #setCONTEXT__CORR__e2e2(End2End)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__CORR__e2e2()
	 * @model required="true"
	 * @generated
	 */
	End2End getCONTEXT__CORR__e2e2();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR e2e2</em>' reference.
	 * @see #getCONTEXT__CORR__e2e2()
	 * @generated
	 */
	void setCONTEXT__CORR__e2e2(End2End value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR r2r</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR r2r</em>' reference.
	 * @see #setCREATE__CORR__r2r(Reference2Reference)
	 * @see Tgg.TggPackage#getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__CORR__r2r()
	 * @model required="true"
	 * @generated
	 */
	Reference2Reference getCREATE__CORR__r2r();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__CORR__r2r <em>CREATE CORR r2r</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR r2r</em>' reference.
	 * @see #getCREATE__CORR__r2r()
	 * @generated
	 */
	void setCREATE__CORR__r2r(Reference2Reference value);

} // ConnectionReferenceForDeviceSystemContext10__Marker
