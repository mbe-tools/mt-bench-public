/**
 */
package Tgg;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature To Feature Marker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Tgg.FeatureToFeature__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}</li>
 *   <li>{@link Tgg.FeatureToFeature__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}</li>
 *   <li>{@link Tgg.FeatureToFeature__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}</li>
 *   <li>{@link Tgg.FeatureToFeature__Marker#getCREATE__TRG__feature_target <em>CREATE TRG feature target</em>}</li>
 *   <li>{@link Tgg.FeatureToFeature__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}</li>
 *   <li>{@link Tgg.FeatureToFeature__Marker#getCREATE__CORR__f2f2 <em>CREATE CORR f2f2</em>}</li>
 * </ul>
 *
 * @see Tgg.TggPackage#getFeatureToFeature__Marker()
 * @model
 * @generated
 */
public interface FeatureToFeature__Marker extends TGGRuleApplication {
	/**
	 * Returns the value of the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC component source</em>' reference.
	 * @see #setCONTEXT__SRC__component_source(ComponentInstance)
	 * @see Tgg.TggPackage#getFeatureToFeature__Marker_CONTEXT__SRC__component_source()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__component_source();

	/**
	 * Sets the value of the '{@link Tgg.FeatureToFeature__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC component source</em>' reference.
	 * @see #getCONTEXT__SRC__component_source()
	 * @generated
	 */
	void setCONTEXT__SRC__component_source(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC feature source</em>' reference.
	 * @see #setCREATE__SRC__feature_source(FeatureInstance)
	 * @see Tgg.TggPackage#getFeatureToFeature__Marker_CREATE__SRC__feature_source()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__SRC__feature_source();

	/**
	 * Sets the value of the '{@link Tgg.FeatureToFeature__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC feature source</em>' reference.
	 * @see #getCREATE__SRC__feature_source()
	 * @generated
	 */
	void setCREATE__SRC__feature_source(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG component target</em>' reference.
	 * @see #setCONTEXT__TRG__component_target(ComponentInstance)
	 * @see Tgg.TggPackage#getFeatureToFeature__Marker_CONTEXT__TRG__component_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__component_target();

	/**
	 * Sets the value of the '{@link Tgg.FeatureToFeature__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG component target</em>' reference.
	 * @see #getCONTEXT__TRG__component_target()
	 * @generated
	 */
	void setCONTEXT__TRG__component_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG feature target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG feature target</em>' reference.
	 * @see #setCREATE__TRG__feature_target(FeatureInstance)
	 * @see Tgg.TggPackage#getFeatureToFeature__Marker_CREATE__TRG__feature_target()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__TRG__feature_target();

	/**
	 * Sets the value of the '{@link Tgg.FeatureToFeature__Marker#getCREATE__TRG__feature_target <em>CREATE TRG feature target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG feature target</em>' reference.
	 * @see #getCREATE__TRG__feature_target()
	 * @generated
	 */
	void setCREATE__TRG__feature_target(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR c2c</em>' reference.
	 * @see #setCONTEXT__CORR__c2c(CompToComp)
	 * @see Tgg.TggPackage#getFeatureToFeature__Marker_CONTEXT__CORR__c2c()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__c2c();

	/**
	 * Sets the value of the '{@link Tgg.FeatureToFeature__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR c2c</em>' reference.
	 * @see #getCONTEXT__CORR__c2c()
	 * @generated
	 */
	void setCONTEXT__CORR__c2c(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR f2f2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR f2f2</em>' reference.
	 * @see #setCREATE__CORR__f2f2(FeatToFeat)
	 * @see Tgg.TggPackage#getFeatureToFeature__Marker_CREATE__CORR__f2f2()
	 * @model required="true"
	 * @generated
	 */
	FeatToFeat getCREATE__CORR__f2f2();

	/**
	 * Sets the value of the '{@link Tgg.FeatureToFeature__Marker#getCREATE__CORR__f2f2 <em>CREATE CORR f2f2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR f2f2</em>' reference.
	 * @see #getCREATE__CORR__f2f2()
	 * @generated
	 */
	void setCREATE__CORR__f2f2(FeatToFeat value);

} // FeatureToFeature__Marker
