/**
 */
package Tgg;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.instance.ConnectionInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection To Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Tgg.ConnectionToConnection#getSource <em>Source</em>}</li>
 *   <li>{@link Tgg.ConnectionToConnection#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see Tgg.TggPackage#getConnectionToConnection()
 * @model
 * @generated
 */
public interface ConnectionToConnection extends EObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(ConnectionInstance)
	 * @see Tgg.TggPackage#getConnectionToConnection_Source()
	 * @model
	 * @generated
	 */
	ConnectionInstance getSource();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionToConnection#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ConnectionInstance)
	 * @see Tgg.TggPackage#getConnectionToConnection_Target()
	 * @model
	 * @generated
	 */
	ConnectionInstance getTarget();

	/**
	 * Sets the value of the '{@link Tgg.ConnectionToConnection#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ConnectionInstance value);

} // ConnectionToConnection
