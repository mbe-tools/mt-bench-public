/**
 */
package Tgg;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Connection To Data Access At Process00 Marker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_conn_destination <em>CONTEXT SRC component conn destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_conn_source <em>CONTEXT SRC component conn source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_conn_destination_target <em>CONTEXT TRG component conn destination target</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_conn_source_target <em>CONTEXT TRG component conn source target</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__data <em>CREATE TRG data</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__dataaccess1 <em>CREATE TRG dataaccess1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__dataaccess2 <em>CREATE TRG dataaccess2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__feature1 <em>CREATE TRG feature1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__feature2 <em>CREATE TRG feature2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__refconnection1 <em>CREATE TRG refconnection1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__refconnection2 <em>CREATE TRG refconnection2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__f2f1 <em>CREATE CORR f2f1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__f2f2 <em>CREATE CORR f2f2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2d <em>CREATE CORR p2d</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2r1 <em>CREATE CORR p2r1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2r2 <em>CREATE CORR p2r2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__sub2sub1 <em>CONTEXT CORR sub2sub1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__sub2sub2 <em>CONTEXT CORR sub2sub2</em>}</li>
 * </ul>
 *
 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker()
 * @model
 * @generated
 */
public interface PortConnectionToDataAccessAtProcess00__Marker extends TGGRuleApplication {
	/**
	 * Returns the value of the '<em><b>CONTEXT SRC component conn destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC component conn destination</em>' reference.
	 * @see #setCONTEXT__SRC__component_conn_destination(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__SRC__component_conn_destination()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__component_conn_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_conn_destination <em>CONTEXT SRC component conn destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC component conn destination</em>' reference.
	 * @see #getCONTEXT__SRC__component_conn_destination()
	 * @generated
	 */
	void setCONTEXT__SRC__component_conn_destination(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC component conn source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC component conn source</em>' reference.
	 * @see #setCONTEXT__SRC__component_conn_source(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__SRC__component_conn_source()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__component_conn_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_conn_source <em>CONTEXT SRC component conn source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC component conn source</em>' reference.
	 * @see #getCONTEXT__SRC__component_conn_source()
	 * @generated
	 */
	void setCONTEXT__SRC__component_conn_source(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC component source</em>' reference.
	 * @see #setCONTEXT__SRC__component_source(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__SRC__component_source()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__component_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC component source</em>' reference.
	 * @see #getCONTEXT__SRC__component_source()
	 * @generated
	 */
	void setCONTEXT__SRC__component_source(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC connection source</em>' reference.
	 * @see #setCREATE__SRC__connection_source(ConnectionInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__connection_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCREATE__SRC__connection_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC connection source</em>' reference.
	 * @see #getCREATE__SRC__connection_source()
	 * @generated
	 */
	void setCREATE__SRC__connection_source(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC feature destination</em>' reference.
	 * @see #setCREATE__SRC__feature_destination(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__feature_destination()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__SRC__feature_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC feature destination</em>' reference.
	 * @see #getCREATE__SRC__feature_destination()
	 * @generated
	 */
	void setCREATE__SRC__feature_destination(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC feature source</em>' reference.
	 * @see #setCREATE__SRC__feature_source(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__feature_source()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__SRC__feature_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC feature source</em>' reference.
	 * @see #getCREATE__SRC__feature_source()
	 * @generated
	 */
	void setCREATE__SRC__feature_source(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC ref connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC ref connection source</em>' reference.
	 * @see #setCREATE__SRC__ref_connection_source(ConnectionReference)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__ref_connection_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionReference getCREATE__SRC__ref_connection_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC ref connection source</em>' reference.
	 * @see #getCREATE__SRC__ref_connection_source()
	 * @generated
	 */
	void setCREATE__SRC__ref_connection_source(ConnectionReference value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG component conn destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG component conn destination target</em>' reference.
	 * @see #setCONTEXT__TRG__component_conn_destination_target(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__TRG__component_conn_destination_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__component_conn_destination_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_conn_destination_target <em>CONTEXT TRG component conn destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG component conn destination target</em>' reference.
	 * @see #getCONTEXT__TRG__component_conn_destination_target()
	 * @generated
	 */
	void setCONTEXT__TRG__component_conn_destination_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG component conn source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG component conn source target</em>' reference.
	 * @see #setCONTEXT__TRG__component_conn_source_target(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__TRG__component_conn_source_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__component_conn_source_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_conn_source_target <em>CONTEXT TRG component conn source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG component conn source target</em>' reference.
	 * @see #getCONTEXT__TRG__component_conn_source_target()
	 * @generated
	 */
	void setCONTEXT__TRG__component_conn_source_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG component target</em>' reference.
	 * @see #setCONTEXT__TRG__component_target(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__TRG__component_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__component_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG component target</em>' reference.
	 * @see #getCONTEXT__TRG__component_target()
	 * @generated
	 */
	void setCONTEXT__TRG__component_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG data</em>' reference.
	 * @see #setCREATE__TRG__data(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__data()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCREATE__TRG__data();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__data <em>CREATE TRG data</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG data</em>' reference.
	 * @see #getCREATE__TRG__data()
	 * @generated
	 */
	void setCREATE__TRG__data(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG dataaccess1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG dataaccess1</em>' reference.
	 * @see #setCREATE__TRG__dataaccess1(ConnectionInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__dataaccess1()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCREATE__TRG__dataaccess1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__dataaccess1 <em>CREATE TRG dataaccess1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG dataaccess1</em>' reference.
	 * @see #getCREATE__TRG__dataaccess1()
	 * @generated
	 */
	void setCREATE__TRG__dataaccess1(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG dataaccess2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG dataaccess2</em>' reference.
	 * @see #setCREATE__TRG__dataaccess2(ConnectionInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__dataaccess2()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCREATE__TRG__dataaccess2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__dataaccess2 <em>CREATE TRG dataaccess2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG dataaccess2</em>' reference.
	 * @see #getCREATE__TRG__dataaccess2()
	 * @generated
	 */
	void setCREATE__TRG__dataaccess2(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG feature1</em>' reference.
	 * @see #setCREATE__TRG__feature1(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__feature1()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__TRG__feature1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__feature1 <em>CREATE TRG feature1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG feature1</em>' reference.
	 * @see #getCREATE__TRG__feature1()
	 * @generated
	 */
	void setCREATE__TRG__feature1(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG feature2</em>' reference.
	 * @see #setCREATE__TRG__feature2(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__feature2()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCREATE__TRG__feature2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__feature2 <em>CREATE TRG feature2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG feature2</em>' reference.
	 * @see #getCREATE__TRG__feature2()
	 * @generated
	 */
	void setCREATE__TRG__feature2(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG refconnection1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG refconnection1</em>' reference.
	 * @see #setCREATE__TRG__refconnection1(ConnectionReference)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__refconnection1()
	 * @model required="true"
	 * @generated
	 */
	ConnectionReference getCREATE__TRG__refconnection1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__refconnection1 <em>CREATE TRG refconnection1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG refconnection1</em>' reference.
	 * @see #getCREATE__TRG__refconnection1()
	 * @generated
	 */
	void setCREATE__TRG__refconnection1(ConnectionReference value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG refconnection2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG refconnection2</em>' reference.
	 * @see #setCREATE__TRG__refconnection2(ConnectionReference)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__refconnection2()
	 * @model required="true"
	 * @generated
	 */
	ConnectionReference getCREATE__TRG__refconnection2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__refconnection2 <em>CREATE TRG refconnection2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG refconnection2</em>' reference.
	 * @see #getCREATE__TRG__refconnection2()
	 * @generated
	 */
	void setCREATE__TRG__refconnection2(ConnectionReference value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR c2c</em>' reference.
	 * @see #setCONTEXT__CORR__c2c(CompToComp)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__CORR__c2c()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__c2c();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR c2c</em>' reference.
	 * @see #getCONTEXT__CORR__c2c()
	 * @generated
	 */
	void setCONTEXT__CORR__c2c(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR f2f1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR f2f1</em>' reference.
	 * @see #setCREATE__CORR__f2f1(FeatToFeat)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__f2f1()
	 * @model required="true"
	 * @generated
	 */
	FeatToFeat getCREATE__CORR__f2f1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__f2f1 <em>CREATE CORR f2f1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR f2f1</em>' reference.
	 * @see #getCREATE__CORR__f2f1()
	 * @generated
	 */
	void setCREATE__CORR__f2f1(FeatToFeat value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR f2f2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR f2f2</em>' reference.
	 * @see #setCREATE__CORR__f2f2(FeatToFeat)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__f2f2()
	 * @model required="true"
	 * @generated
	 */
	FeatToFeat getCREATE__CORR__f2f2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__f2f2 <em>CREATE CORR f2f2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR f2f2</em>' reference.
	 * @see #getCREATE__CORR__f2f2()
	 * @generated
	 */
	void setCREATE__CORR__f2f2(FeatToFeat value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR p2a1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR p2a1</em>' reference.
	 * @see #setCREATE__CORR__p2a1(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2a1()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__p2a1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR p2a1</em>' reference.
	 * @see #getCREATE__CORR__p2a1()
	 * @generated
	 */
	void setCREATE__CORR__p2a1(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR p2a2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR p2a2</em>' reference.
	 * @see #setCREATE__CORR__p2a2(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2a2()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__p2a2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR p2a2</em>' reference.
	 * @see #getCREATE__CORR__p2a2()
	 * @generated
	 */
	void setCREATE__CORR__p2a2(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR p2d</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR p2d</em>' reference.
	 * @see #setCREATE__CORR__p2d(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2d()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__p2d();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2d <em>CREATE CORR p2d</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR p2d</em>' reference.
	 * @see #getCREATE__CORR__p2d()
	 * @generated
	 */
	void setCREATE__CORR__p2d(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR p2r1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR p2r1</em>' reference.
	 * @see #setCREATE__CORR__p2r1(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2r1()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__p2r1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2r1 <em>CREATE CORR p2r1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR p2r1</em>' reference.
	 * @see #getCREATE__CORR__p2r1()
	 * @generated
	 */
	void setCREATE__CORR__p2r1(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR p2r2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR p2r2</em>' reference.
	 * @see #setCREATE__CORR__p2r2(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2r2()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__p2r2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2r2 <em>CREATE CORR p2r2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR p2r2</em>' reference.
	 * @see #getCREATE__CORR__p2r2()
	 * @generated
	 */
	void setCREATE__CORR__p2r2(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR sub2sub1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR sub2sub1</em>' reference.
	 * @see #setCONTEXT__CORR__sub2sub1(CompToComp)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__CORR__sub2sub1()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__sub2sub1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__sub2sub1 <em>CONTEXT CORR sub2sub1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR sub2sub1</em>' reference.
	 * @see #getCONTEXT__CORR__sub2sub1()
	 * @generated
	 */
	void setCONTEXT__CORR__sub2sub1(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR sub2sub2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR sub2sub2</em>' reference.
	 * @see #setCONTEXT__CORR__sub2sub2(CompToComp)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__CORR__sub2sub2()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__sub2sub2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__sub2sub2 <em>CONTEXT CORR sub2sub2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR sub2sub2</em>' reference.
	 * @see #getCONTEXT__CORR__sub2sub2()
	 * @generated
	 */
	void setCONTEXT__CORR__sub2sub2(CompToComp value);

} // PortConnectionToDataAccessAtProcess00__Marker
