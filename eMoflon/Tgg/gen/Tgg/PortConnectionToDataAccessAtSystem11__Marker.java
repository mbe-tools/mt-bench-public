/**
 */
package Tgg;

import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.SystemInstance;

import runtime.TGGRuleApplication;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Connection To Data Access At System11 Marker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__feature_destination <em>CONTEXT SRC feature destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__feature_source <em>CONTEXT SRC feature source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__process_destination <em>CONTEXT SRC process destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__process_source <em>CONTEXT SRC process source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__ref_connection_destination <em>CREATE SRC ref connection destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__thread_destination <em>CONTEXT SRC thread destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__thread_source <em>CONTEXT SRC thread source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__data_destination <em>CREATE TRG data destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__data_source <em>CREATE TRG data source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__dataaccess_destination <em>CREATE TRG dataaccess destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__dataaccess_source <em>CREATE TRG dataaccess source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__feature_destination_target <em>CONTEXT TRG feature destination target</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__feature_source_target <em>CONTEXT TRG feature source target</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__process_destination_target <em>CONTEXT TRG process destination target</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__process_source_target <em>CONTEXT TRG process source target</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__ref_destination <em>CREATE TRG ref destination</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__ref_source <em>CREATE TRG ref source</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__thread_destination_target <em>CONTEXT TRG thread destination target</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__thread_source_target <em>CONTEXT TRG thread source target</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__feature2feature1 <em>CONTEXT CORR feature2feature1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__feature2feature2 <em>CONTEXT CORR feature2feature2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2d1 <em>CREATE CORR p2d1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2d2 <em>CREATE CORR p2d2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__process2process1 <em>CONTEXT CORR process2process1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__process2process2 <em>CONTEXT CORR process2process2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__r2r1 <em>CREATE CORR r2r1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__r2r2 <em>CREATE CORR r2r2</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__thread2thread1 <em>CONTEXT CORR thread2thread1</em>}</li>
 *   <li>{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__thread2thread2 <em>CONTEXT CORR thread2thread2</em>}</li>
 * </ul>
 *
 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker()
 * @model
 * @generated
 */
public interface PortConnectionToDataAccessAtSystem11__Marker extends TGGRuleApplication {
	/**
	 * Returns the value of the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC connection source</em>' reference.
	 * @see #setCREATE__SRC__connection_source(ConnectionInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__SRC__connection_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCREATE__SRC__connection_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC connection source</em>' reference.
	 * @see #getCREATE__SRC__connection_source()
	 * @generated
	 */
	void setCREATE__SRC__connection_source(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC feature destination</em>' reference.
	 * @see #setCONTEXT__SRC__feature_destination(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__feature_destination()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCONTEXT__SRC__feature_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__feature_destination <em>CONTEXT SRC feature destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC feature destination</em>' reference.
	 * @see #getCONTEXT__SRC__feature_destination()
	 * @generated
	 */
	void setCONTEXT__SRC__feature_destination(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC feature source</em>' reference.
	 * @see #setCONTEXT__SRC__feature_source(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__feature_source()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCONTEXT__SRC__feature_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__feature_source <em>CONTEXT SRC feature source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC feature source</em>' reference.
	 * @see #getCONTEXT__SRC__feature_source()
	 * @generated
	 */
	void setCONTEXT__SRC__feature_source(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC process destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC process destination</em>' reference.
	 * @see #setCONTEXT__SRC__process_destination(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__process_destination()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__process_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__process_destination <em>CONTEXT SRC process destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC process destination</em>' reference.
	 * @see #getCONTEXT__SRC__process_destination()
	 * @generated
	 */
	void setCONTEXT__SRC__process_destination(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC process source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC process source</em>' reference.
	 * @see #setCONTEXT__SRC__process_source(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__process_source()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__process_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__process_source <em>CONTEXT SRC process source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC process source</em>' reference.
	 * @see #getCONTEXT__SRC__process_source()
	 * @generated
	 */
	void setCONTEXT__SRC__process_source(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC ref connection destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC ref connection destination</em>' reference.
	 * @see #setCREATE__SRC__ref_connection_destination(ConnectionReference)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__SRC__ref_connection_destination()
	 * @model required="true"
	 * @generated
	 */
	ConnectionReference getCREATE__SRC__ref_connection_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__ref_connection_destination <em>CREATE SRC ref connection destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC ref connection destination</em>' reference.
	 * @see #getCREATE__SRC__ref_connection_destination()
	 * @generated
	 */
	void setCREATE__SRC__ref_connection_destination(ConnectionReference value);

	/**
	 * Returns the value of the '<em><b>CREATE SRC ref connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE SRC ref connection source</em>' reference.
	 * @see #setCREATE__SRC__ref_connection_source(ConnectionReference)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__SRC__ref_connection_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionReference getCREATE__SRC__ref_connection_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE SRC ref connection source</em>' reference.
	 * @see #getCREATE__SRC__ref_connection_source()
	 * @generated
	 */
	void setCREATE__SRC__ref_connection_source(ConnectionReference value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC system source</em>' reference.
	 * @see #setCONTEXT__SRC__system_source(SystemInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__system_source()
	 * @model required="true"
	 * @generated
	 */
	SystemInstance getCONTEXT__SRC__system_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC system source</em>' reference.
	 * @see #getCONTEXT__SRC__system_source()
	 * @generated
	 */
	void setCONTEXT__SRC__system_source(SystemInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC thread destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC thread destination</em>' reference.
	 * @see #setCONTEXT__SRC__thread_destination(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__thread_destination()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__thread_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__thread_destination <em>CONTEXT SRC thread destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC thread destination</em>' reference.
	 * @see #getCONTEXT__SRC__thread_destination()
	 * @generated
	 */
	void setCONTEXT__SRC__thread_destination(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT SRC thread source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT SRC thread source</em>' reference.
	 * @see #setCONTEXT__SRC__thread_source(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__thread_source()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__SRC__thread_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__thread_source <em>CONTEXT SRC thread source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT SRC thread source</em>' reference.
	 * @see #getCONTEXT__SRC__thread_source()
	 * @generated
	 */
	void setCONTEXT__SRC__thread_source(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG data destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG data destination</em>' reference.
	 * @see #setCREATE__TRG__data_destination(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__data_destination()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCREATE__TRG__data_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__data_destination <em>CREATE TRG data destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG data destination</em>' reference.
	 * @see #getCREATE__TRG__data_destination()
	 * @generated
	 */
	void setCREATE__TRG__data_destination(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG data source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG data source</em>' reference.
	 * @see #setCREATE__TRG__data_source(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__data_source()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCREATE__TRG__data_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__data_source <em>CREATE TRG data source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG data source</em>' reference.
	 * @see #getCREATE__TRG__data_source()
	 * @generated
	 */
	void setCREATE__TRG__data_source(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG dataaccess destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG dataaccess destination</em>' reference.
	 * @see #setCREATE__TRG__dataaccess_destination(ConnectionInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__dataaccess_destination()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCREATE__TRG__dataaccess_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__dataaccess_destination <em>CREATE TRG dataaccess destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG dataaccess destination</em>' reference.
	 * @see #getCREATE__TRG__dataaccess_destination()
	 * @generated
	 */
	void setCREATE__TRG__dataaccess_destination(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG dataaccess source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG dataaccess source</em>' reference.
	 * @see #setCREATE__TRG__dataaccess_source(ConnectionInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__dataaccess_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionInstance getCREATE__TRG__dataaccess_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__dataaccess_source <em>CREATE TRG dataaccess source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG dataaccess source</em>' reference.
	 * @see #getCREATE__TRG__dataaccess_source()
	 * @generated
	 */
	void setCREATE__TRG__dataaccess_source(ConnectionInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG feature destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG feature destination target</em>' reference.
	 * @see #setCONTEXT__TRG__feature_destination_target(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__feature_destination_target()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCONTEXT__TRG__feature_destination_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__feature_destination_target <em>CONTEXT TRG feature destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG feature destination target</em>' reference.
	 * @see #getCONTEXT__TRG__feature_destination_target()
	 * @generated
	 */
	void setCONTEXT__TRG__feature_destination_target(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG feature source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG feature source target</em>' reference.
	 * @see #setCONTEXT__TRG__feature_source_target(FeatureInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__feature_source_target()
	 * @model required="true"
	 * @generated
	 */
	FeatureInstance getCONTEXT__TRG__feature_source_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__feature_source_target <em>CONTEXT TRG feature source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG feature source target</em>' reference.
	 * @see #getCONTEXT__TRG__feature_source_target()
	 * @generated
	 */
	void setCONTEXT__TRG__feature_source_target(FeatureInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG process destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG process destination target</em>' reference.
	 * @see #setCONTEXT__TRG__process_destination_target(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__process_destination_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__process_destination_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__process_destination_target <em>CONTEXT TRG process destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG process destination target</em>' reference.
	 * @see #getCONTEXT__TRG__process_destination_target()
	 * @generated
	 */
	void setCONTEXT__TRG__process_destination_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG process source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG process source target</em>' reference.
	 * @see #setCONTEXT__TRG__process_source_target(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__process_source_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__process_source_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__process_source_target <em>CONTEXT TRG process source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG process source target</em>' reference.
	 * @see #getCONTEXT__TRG__process_source_target()
	 * @generated
	 */
	void setCONTEXT__TRG__process_source_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG ref destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG ref destination</em>' reference.
	 * @see #setCREATE__TRG__ref_destination(ConnectionReference)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__ref_destination()
	 * @model required="true"
	 * @generated
	 */
	ConnectionReference getCREATE__TRG__ref_destination();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__ref_destination <em>CREATE TRG ref destination</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG ref destination</em>' reference.
	 * @see #getCREATE__TRG__ref_destination()
	 * @generated
	 */
	void setCREATE__TRG__ref_destination(ConnectionReference value);

	/**
	 * Returns the value of the '<em><b>CREATE TRG ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE TRG ref source</em>' reference.
	 * @see #setCREATE__TRG__ref_source(ConnectionReference)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__ref_source()
	 * @model required="true"
	 * @generated
	 */
	ConnectionReference getCREATE__TRG__ref_source();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__ref_source <em>CREATE TRG ref source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE TRG ref source</em>' reference.
	 * @see #getCREATE__TRG__ref_source()
	 * @generated
	 */
	void setCREATE__TRG__ref_source(ConnectionReference value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG system target</em>' reference.
	 * @see #setCONTEXT__TRG__system_target(SystemInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__system_target()
	 * @model required="true"
	 * @generated
	 */
	SystemInstance getCONTEXT__TRG__system_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG system target</em>' reference.
	 * @see #getCONTEXT__TRG__system_target()
	 * @generated
	 */
	void setCONTEXT__TRG__system_target(SystemInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG thread destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG thread destination target</em>' reference.
	 * @see #setCONTEXT__TRG__thread_destination_target(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__thread_destination_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__thread_destination_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__thread_destination_target <em>CONTEXT TRG thread destination target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG thread destination target</em>' reference.
	 * @see #getCONTEXT__TRG__thread_destination_target()
	 * @generated
	 */
	void setCONTEXT__TRG__thread_destination_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT TRG thread source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT TRG thread source target</em>' reference.
	 * @see #setCONTEXT__TRG__thread_source_target(ComponentInstance)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__thread_source_target()
	 * @model required="true"
	 * @generated
	 */
	ComponentInstance getCONTEXT__TRG__thread_source_target();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__thread_source_target <em>CONTEXT TRG thread source target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT TRG thread source target</em>' reference.
	 * @see #getCONTEXT__TRG__thread_source_target()
	 * @generated
	 */
	void setCONTEXT__TRG__thread_source_target(ComponentInstance value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR feature2feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR feature2feature1</em>' reference.
	 * @see #setCONTEXT__CORR__feature2feature1(FeatToFeat)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__feature2feature1()
	 * @model required="true"
	 * @generated
	 */
	FeatToFeat getCONTEXT__CORR__feature2feature1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__feature2feature1 <em>CONTEXT CORR feature2feature1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR feature2feature1</em>' reference.
	 * @see #getCONTEXT__CORR__feature2feature1()
	 * @generated
	 */
	void setCONTEXT__CORR__feature2feature1(FeatToFeat value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR feature2feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR feature2feature2</em>' reference.
	 * @see #setCONTEXT__CORR__feature2feature2(FeatToFeat)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__feature2feature2()
	 * @model required="true"
	 * @generated
	 */
	FeatToFeat getCONTEXT__CORR__feature2feature2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__feature2feature2 <em>CONTEXT CORR feature2feature2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR feature2feature2</em>' reference.
	 * @see #getCONTEXT__CORR__feature2feature2()
	 * @generated
	 */
	void setCONTEXT__CORR__feature2feature2(FeatToFeat value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR p2a1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR p2a1</em>' reference.
	 * @see #setCREATE__CORR__p2a1(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2a1()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__p2a1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR p2a1</em>' reference.
	 * @see #getCREATE__CORR__p2a1()
	 * @generated
	 */
	void setCREATE__CORR__p2a1(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR p2a2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR p2a2</em>' reference.
	 * @see #setCREATE__CORR__p2a2(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2a2()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__p2a2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR p2a2</em>' reference.
	 * @see #getCREATE__CORR__p2a2()
	 * @generated
	 */
	void setCREATE__CORR__p2a2(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR p2d1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR p2d1</em>' reference.
	 * @see #setCREATE__CORR__p2d1(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2d1()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__p2d1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2d1 <em>CREATE CORR p2d1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR p2d1</em>' reference.
	 * @see #getCREATE__CORR__p2d1()
	 * @generated
	 */
	void setCREATE__CORR__p2d1(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR p2d2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR p2d2</em>' reference.
	 * @see #setCREATE__CORR__p2d2(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2d2()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__p2d2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2d2 <em>CREATE CORR p2d2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR p2d2</em>' reference.
	 * @see #getCREATE__CORR__p2d2()
	 * @generated
	 */
	void setCREATE__CORR__p2d2(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR process2process1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR process2process1</em>' reference.
	 * @see #setCONTEXT__CORR__process2process1(CompToComp)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__process2process1()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__process2process1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__process2process1 <em>CONTEXT CORR process2process1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR process2process1</em>' reference.
	 * @see #getCONTEXT__CORR__process2process1()
	 * @generated
	 */
	void setCONTEXT__CORR__process2process1(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR process2process2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR process2process2</em>' reference.
	 * @see #setCONTEXT__CORR__process2process2(CompToComp)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__process2process2()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__process2process2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__process2process2 <em>CONTEXT CORR process2process2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR process2process2</em>' reference.
	 * @see #getCONTEXT__CORR__process2process2()
	 * @generated
	 */
	void setCONTEXT__CORR__process2process2(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR r2r1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR r2r1</em>' reference.
	 * @see #setCREATE__CORR__r2r1(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__r2r1()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__r2r1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__r2r1 <em>CREATE CORR r2r1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR r2r1</em>' reference.
	 * @see #getCREATE__CORR__r2r1()
	 * @generated
	 */
	void setCREATE__CORR__r2r1(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CREATE CORR r2r2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATE CORR r2r2</em>' reference.
	 * @see #setCREATE__CORR__r2r2(PortConnectionToSharedData)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__r2r2()
	 * @model required="true"
	 * @generated
	 */
	PortConnectionToSharedData getCREATE__CORR__r2r2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__r2r2 <em>CREATE CORR r2r2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATE CORR r2r2</em>' reference.
	 * @see #getCREATE__CORR__r2r2()
	 * @generated
	 */
	void setCREATE__CORR__r2r2(PortConnectionToSharedData value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR s2s</em>' reference.
	 * @see #setCONTEXT__CORR__s2s(SystemToSystem)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__s2s()
	 * @model required="true"
	 * @generated
	 */
	SystemToSystem getCONTEXT__CORR__s2s();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR s2s</em>' reference.
	 * @see #getCONTEXT__CORR__s2s()
	 * @generated
	 */
	void setCONTEXT__CORR__s2s(SystemToSystem value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR thread2thread1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR thread2thread1</em>' reference.
	 * @see #setCONTEXT__CORR__thread2thread1(CompToComp)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__thread2thread1()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__thread2thread1();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__thread2thread1 <em>CONTEXT CORR thread2thread1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR thread2thread1</em>' reference.
	 * @see #getCONTEXT__CORR__thread2thread1()
	 * @generated
	 */
	void setCONTEXT__CORR__thread2thread1(CompToComp value);

	/**
	 * Returns the value of the '<em><b>CONTEXT CORR thread2thread2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CONTEXT CORR thread2thread2</em>' reference.
	 * @see #setCONTEXT__CORR__thread2thread2(CompToComp)
	 * @see Tgg.TggPackage#getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__thread2thread2()
	 * @model required="true"
	 * @generated
	 */
	CompToComp getCONTEXT__CORR__thread2thread2();

	/**
	 * Sets the value of the '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__thread2thread2 <em>CONTEXT CORR thread2thread2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CONTEXT CORR thread2thread2</em>' reference.
	 * @see #getCONTEXT__CORR__thread2thread2()
	 * @generated
	 */
	void setCONTEXT__CORR__thread2thread2(CompToComp value);

} // PortConnectionToDataAccessAtSystem11__Marker
