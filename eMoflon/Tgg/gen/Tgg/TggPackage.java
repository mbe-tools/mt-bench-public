/**
 */
package Tgg;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import runtime.RuntimePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Tgg.TggFactory
 * @model kind="package"
 * @generated
 */
public interface TggPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Tgg";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/Tgg/model/Tgg.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Tgg";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TggPackage eINSTANCE = Tgg.impl.TggPackageImpl.init();

	/**
	 * The meta object id for the '{@link Tgg.impl.End2EndImpl <em>End2 End</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.End2EndImpl
	 * @see Tgg.impl.TggPackageImpl#getEnd2End()
	 * @generated
	 */
	int END2_END = 4;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END2_END__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END2_END__TARGET = 1;

	/**
	 * The number of structural features of the '<em>End2 End</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END2_END_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>End2 End</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END2_END_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.CompToCompImpl <em>Comp To Comp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.CompToCompImpl
	 * @see Tgg.impl.TggPackageImpl#getCompToComp()
	 * @generated
	 */
	int COMP_TO_COMP = 5;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMP_TO_COMP__SOURCE = END2_END__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMP_TO_COMP__TARGET = END2_END__TARGET;

	/**
	 * The number of structural features of the '<em>Comp To Comp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMP_TO_COMP_FEATURE_COUNT = END2_END_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Comp To Comp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMP_TO_COMP_OPERATION_COUNT = END2_END_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.SystemToSystemImpl <em>System To System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.SystemToSystemImpl
	 * @see Tgg.impl.TggPackageImpl#getSystemToSystem()
	 * @generated
	 */
	int SYSTEM_TO_SYSTEM = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM__SOURCE = COMP_TO_COMP__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM__TARGET = COMP_TO_COMP__TARGET;

	/**
	 * The number of structural features of the '<em>System To System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_FEATURE_COUNT = COMP_TO_COMP_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>System To System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_OPERATION_COUNT = COMP_TO_COMP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.ConnectionToConnectionImpl <em>Connection To Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.ConnectionToConnectionImpl
	 * @see Tgg.impl.TggPackageImpl#getConnectionToConnection()
	 * @generated
	 */
	int CONNECTION_TO_CONNECTION = 1;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TO_CONNECTION__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TO_CONNECTION__TARGET = 1;

	/**
	 * The number of structural features of the '<em>Connection To Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TO_CONNECTION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Connection To Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_TO_CONNECTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionToSharedDataImpl <em>Port Connection To Shared Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionToSharedDataImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionToSharedData()
	 * @generated
	 */
	int PORT_CONNECTION_TO_SHARED_DATA = 2;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_SHARED_DATA__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_SHARED_DATA__TARGET = 1;

	/**
	 * The number of structural features of the '<em>Port Connection To Shared Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_SHARED_DATA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Port Connection To Shared Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_SHARED_DATA_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.Reference2ReferenceImpl <em>Reference2 Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.Reference2ReferenceImpl
	 * @see Tgg.impl.TggPackageImpl#getReference2Reference()
	 * @generated
	 */
	int REFERENCE2_REFERENCE = 3;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE2_REFERENCE__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE2_REFERENCE__TARGET = 1;

	/**
	 * The number of structural features of the '<em>Reference2 Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE2_REFERENCE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Reference2 Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE2_REFERENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.FeatToFeatImpl <em>Feat To Feat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.FeatToFeatImpl
	 * @see Tgg.impl.TggPackageImpl#getFeatToFeat()
	 * @generated
	 */
	int FEAT_TO_FEAT = 6;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_TO_FEAT__SOURCE = END2_END__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_TO_FEAT__TARGET = END2_END__TARGET;

	/**
	 * The number of structural features of the '<em>Feat To Feat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_TO_FEAT_FEATURE_COUNT = END2_END_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Feat To Feat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEAT_TO_FEAT_OPERATION_COUNT = END2_END_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.ComponentToComponent__MarkerImpl <em>Component To Component Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.ComponentToComponent__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getComponentToComponent__Marker()
	 * @generated
	 */
	int COMPONENT_TO_COMPONENT_MARKER = 7;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CREATE SRC subcomponent source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CREATE_SRC_SUBCOMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CONTEXT_TRG_COMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE TRG subcomponent target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CREATE_TRG_SUBCOMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE CORR sub2sub</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER__CREATE_CORR_SUB2SUB = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Component To Component Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Component To Component Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TO_COMPONENT_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl <em>Connection Instance To Connection Instance Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.ConnectionInstanceToConnectionInstance__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER = 8;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC sdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC ssource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_SRC_SSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_COMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_TRG_CONNECTION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_TRG_TSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CREATE CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CREATE_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR comp2comp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_COMP2COMP = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER__CONTEXT_CORR_E2E2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Connection Instance To Connection Instance Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The number of operations of the '<em>Connection Instance To Connection Instance Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_INSTANCE_TO_CONNECTION_INSTANCE_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.ConnectionRefToConnectionRefForComponentContext__MarkerImpl <em>Connection Ref To Connection Ref For Component Context Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.ConnectionRefToConnectionRefForComponentContext__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER = 9;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection Ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC scontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_SRC_SCONTEXT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC sdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_SRC_SDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC ssource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_SRC_SSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE TRG connection Ref target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CREATE_TRG_CONNECTION_REF_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_TRG_CONNECTION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tcontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_TRG_TCONTEXT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_TRG_TDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_TRG_TSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR comp2comp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_CORR_COMP2COMP = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_CORR_E2E1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CONTEXT_CORR_E2E2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER__CREATE_CORR_R2R = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The number of structural features of the '<em>Connection Ref To Connection Ref For Component Context Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The number of operations of the '<em>Connection Ref To Connection Ref For Component Context Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REF_TO_CONNECTION_REF_FOR_COMPONENT_CONTEXT_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext01__MarkerImpl <em>Connection Reference For Device System Context01 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.ConnectionReferenceForDeviceSystemContext01__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER = 10;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection Ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CONTEXT_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC scontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CONTEXT_SRC_SCONTEXT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC sdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CONTEXT_SRC_SDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CREATE SRC ssource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CREATE_SRC_SSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE TRG connection Ref target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CREATE_TRG_CONNECTION_REF_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CONTEXT_TRG_CONNECTION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tcontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CONTEXT_TRG_TCONTEXT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CONTEXT_TRG_TDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CREATE TRG tsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CREATE_TRG_TSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR comp2comp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CONTEXT_CORR_COMP2COMP = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CONTEXT_CORR_E2E1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CREATE CORR e2e2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CREATE_CORR_E2E2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER__CREATE_CORR_R2R = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The number of structural features of the '<em>Connection Reference For Device System Context01 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The number of operations of the '<em>Connection Reference For Device System Context01 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT01_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl <em>Connection Reference For Device System Context10 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.ConnectionReferenceForDeviceSystemContext10__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER = 11;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection Ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC scontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SCONTEXT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE SRC sdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_SRC_SDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC ssource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_SRC_SSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE TRG connection Ref target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_CONNECTION_REF_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_CONNECTION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tcontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TCONTEXT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CREATE TRG tdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_TRG_TDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_TRG_TSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR comp2comp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_COMP2COMP = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CREATE CORR e2e1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_E2E1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CONTEXT_CORR_E2E2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER__CREATE_CORR_R2R = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The number of structural features of the '<em>Connection Reference For Device System Context10 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The number of operations of the '<em>Connection Reference For Device System Context10 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT10_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.ConnectionReferenceForDeviceSystemContext11__MarkerImpl <em>Connection Reference For Device System Context11 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.ConnectionReferenceForDeviceSystemContext11__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER = 12;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection Ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CREATE_SRC_CONNECTION_REF_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC scontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_SRC_SCONTEXT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC sdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_SRC_SDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC ssource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_SRC_SSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE TRG connection Ref target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CREATE_TRG_CONNECTION_REF_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_TRG_CONNECTION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tcontext</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_TRG_TCONTEXT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tdestination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_TRG_TDESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG tsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_TRG_TSOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR comp2comp</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_CORR_COMP2COMP = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_CORR_E2E1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR e2e2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CONTEXT_CORR_E2E2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER__CREATE_CORR_R2R = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The number of structural features of the '<em>Connection Reference For Device System Context11 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The number of operations of the '<em>Connection Reference For Device System Context11 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_REFERENCE_FOR_DEVICE_SYSTEM_CONTEXT11_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.FeatureToFeature__MarkerImpl <em>Feature To Feature Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.FeatureToFeature__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getFeatureToFeature__Marker()
	 * @generated
	 */
	int FEATURE_TO_FEATURE_MARKER = 13;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CREATE_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CONTEXT_TRG_COMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CREATE_TRG_FEATURE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE CORR f2f2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER__CREATE_CORR_F2F2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Feature To Feature Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Feature To Feature Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TO_FEATURE_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionForDeviceDestination__MarkerImpl <em>Port Connection For Device Destination Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionForDeviceDestination__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER = 14;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CONTEXT_SRC_COMPONENT_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CREATE_SRC_FEATURE_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CREATE_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CONTEXT_SRC_SYSTEM_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CONTEXT_TRG_COMPONENT_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CONTEXT_TRG_COMPONENT_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CREATE TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CREATE_TRG_CONNECTION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CREATE_TRG_FEATURE_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CREATE_TRG_FEATURE_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CONTEXT_TRG_SYSTEM_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CONTEXT_CORR_C2C1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CONTEXT_CORR_C2C2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE CORR co2co</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CREATE_CORR_CO2CO = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>CREATE CORR feature2feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CREATE_CORR_FEATURE2FEATURE1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>CREATE CORR feature2feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CREATE_CORR_FEATURE2FEATURE2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER__CONTEXT_CORR_S2S = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 17;

	/**
	 * The number of structural features of the '<em>Port Connection For Device Destination Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 18;

	/**
	 * The number of operations of the '<em>Port Connection For Device Destination Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_DESTINATION_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionForDeviceSource__MarkerImpl <em>Port Connection For Device Source Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionForDeviceSource__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER = 15;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_SRC_SYSTEM_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_COMPONENT_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CREATE TRG connection target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_CONNECTION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_TRG_FEATURE_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_TRG_SYSTEM_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_C2C2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE CORR co2co</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_CO2CO = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>CREATE CORR feature2feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>CREATE CORR feature2feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CREATE_CORR_FEATURE2FEATURE2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER__CONTEXT_CORR_S2S = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 17;

	/**
	 * The number of structural features of the '<em>Port Connection For Device Source Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 18;

	/**
	 * The number of operations of the '<em>Port Connection For Device Source Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_FOR_DEVICE_SOURCE_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl <em>Port Connection To Data Access At Process00 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionToDataAccessAtProcess00__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER = 16;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component conn destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component conn source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_CONN_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component conn destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component conn source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_CONN_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_TRG_COMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CREATE TRG data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATA = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_DATAACCESS2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_FEATURE2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>CREATE TRG refconnection1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>CREATE TRG refconnection2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_TRG_REFCONNECTION2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>CREATE CORR f2f1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>CREATE CORR f2f2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_F2F2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2A2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2D = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2r1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2r2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CREATE_CORR_P2R2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR sub2sub1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR sub2sub2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER__CONTEXT_CORR_SUB2SUB2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 26;

	/**
	 * The number of structural features of the '<em>Port Connection To Data Access At Process00 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 27;

	/**
	 * The number of operations of the '<em>Port Connection To Data Access At Process00 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS00_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionToDataAccessAtProcess01__MarkerImpl <em>Port Connection To Data Access At Process01 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionToDataAccessAtProcess01__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER = 17;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component conn destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_SRC_COMPONENT_CONN_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component conn source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_SRC_COMPONENT_CONN_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_SRC_FEATURE_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component conn destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_TRG_COMPONENT_CONN_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component conn source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_TRG_COMPONENT_CONN_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_TRG_COMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CREATE TRG data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_TRG_DATA = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_TRG_DATAACCESS1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_TRG_DATAACCESS2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_TRG_FEATURE1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_TRG_FEATURE2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>CREATE TRG refconnection1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_TRG_REFCONNECTION1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>CREATE TRG refconnection2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_TRG_REFCONNECTION2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>CREATE CORR f2f1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_CORR_F2F1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR f2f2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_CORR_F2F2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_CORR_P2A1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_CORR_P2A2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_CORR_P2D = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2r1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_CORR_P2R1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2r2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CREATE_CORR_P2R2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR sub2sub1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_CORR_SUB2SUB1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR sub2sub2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER__CONTEXT_CORR_SUB2SUB2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 26;

	/**
	 * The number of structural features of the '<em>Port Connection To Data Access At Process01 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 27;

	/**
	 * The number of operations of the '<em>Port Connection To Data Access At Process01 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS01_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionToDataAccessAtProcess10__MarkerImpl <em>Port Connection To Data Access At Process10 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionToDataAccessAtProcess10__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER = 18;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component conn destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_SRC_COMPONENT_CONN_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component conn source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_SRC_COMPONENT_CONN_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_SRC_FEATURE_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component conn destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_TRG_COMPONENT_CONN_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component conn source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_TRG_COMPONENT_CONN_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_TRG_COMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CREATE TRG data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_TRG_DATA = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_TRG_DATAACCESS1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_TRG_DATAACCESS2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_TRG_FEATURE1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_TRG_FEATURE2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>CREATE TRG refconnection1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_TRG_REFCONNECTION1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>CREATE TRG refconnection2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_TRG_REFCONNECTION2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR f2f1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_CORR_F2F1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>CREATE CORR f2f2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_CORR_F2F2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_CORR_P2A1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_CORR_P2A2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_CORR_P2D = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2r1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_CORR_P2R1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2r2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CREATE_CORR_P2R2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR sub2sub1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_CORR_SUB2SUB1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR sub2sub2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER__CONTEXT_CORR_SUB2SUB2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 26;

	/**
	 * The number of structural features of the '<em>Port Connection To Data Access At Process10 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 27;

	/**
	 * The number of operations of the '<em>Port Connection To Data Access At Process10 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS10_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionToDataAccessAtProcess11__MarkerImpl <em>Port Connection To Data Access At Process11 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionToDataAccessAtProcess11__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER = 19;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component conn destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_SRC_COMPONENT_CONN_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component conn source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_SRC_COMPONENT_CONN_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC component source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_SRC_COMPONENT_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_SRC_FEATURE_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component conn destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_TRG_COMPONENT_CONN_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component conn source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_TRG_COMPONENT_CONN_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG component target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_TRG_COMPONENT_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CREATE TRG data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_TRG_DATA = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_TRG_DATAACCESS1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_TRG_DATAACCESS2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_TRG_FEATURE1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_TRG_FEATURE2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>CREATE TRG refconnection1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_TRG_REFCONNECTION1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>CREATE TRG refconnection2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_TRG_REFCONNECTION2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR c2c</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_CORR_C2C = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR f2f1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_CORR_F2F1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR f2f2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_CORR_F2F2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_CORR_P2A1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_CORR_P2A2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_CORR_P2D = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2r1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_CORR_P2R1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2r2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CREATE_CORR_P2R2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR sub2sub1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_CORR_SUB2SUB1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR sub2sub2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER__CONTEXT_CORR_SUB2SUB2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 26;

	/**
	 * The number of structural features of the '<em>Port Connection To Data Access At Process11 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 27;

	/**
	 * The number of operations of the '<em>Port Connection To Data Access At Process11 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_PROCESS11_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionToDataAccessAtSystem00__MarkerImpl <em>Port Connection To Data Access At System00 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionToDataAccessAtSystem00__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER = 20;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_SRC_FEATURE_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC process destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_SRC_PROCESS_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC process source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_SRC_PROCESS_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_SRC_REF_CONNECTION_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_SRC_SYSTEM_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC thread destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_SRC_THREAD_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC thread source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_SRC_THREAD_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CREATE TRG data destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_TRG_DATA_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CREATE TRG data source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_TRG_DATA_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_TRG_DATAACCESS_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_TRG_DATAACCESS_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_TRG_FEATURE_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_TRG_FEATURE_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG process destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_TRG_PROCESS_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG process source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_TRG_PROCESS_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>CREATE TRG ref destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_TRG_REF_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>CREATE TRG ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_TRG_REF_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_TRG_SYSTEM_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG thread destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_TRG_THREAD_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG thread source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_TRG_THREAD_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>CREATE CORR feature2feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_CORR_FEATURE2FEATURE1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>CREATE CORR feature2feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_CORR_FEATURE2FEATURE2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_CORR_P2A1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_CORR_P2A2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 26;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_CORR_P2D1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 27;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_CORR_P2D2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 28;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR process2process1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_CORR_PROCESS2PROCESS1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 29;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR process2process2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_CORR_PROCESS2PROCESS2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 30;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_CORR_R2R1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 31;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CREATE_CORR_R2R2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 32;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_CORR_S2S = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 33;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR thread2thread1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_CORR_THREAD2THREAD1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 34;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR thread2thread2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER__CONTEXT_CORR_THREAD2THREAD2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 35;

	/**
	 * The number of structural features of the '<em>Port Connection To Data Access At System00 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 36;

	/**
	 * The number of operations of the '<em>Port Connection To Data Access At System00 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM00_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionToDataAccessAtSystem01__MarkerImpl <em>Port Connection To Data Access At System01 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionToDataAccessAtSystem01__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER = 21;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_SRC_FEATURE_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC process destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_SRC_PROCESS_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC process source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_SRC_PROCESS_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_SRC_REF_CONNECTION_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_SRC_SYSTEM_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC thread destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_SRC_THREAD_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC thread source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_SRC_THREAD_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CREATE TRG data destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_TRG_DATA_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CREATE TRG data source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_TRG_DATA_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_TRG_DATAACCESS_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_TRG_DATAACCESS_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_TRG_FEATURE_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG feature source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_TRG_FEATURE_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG process destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_TRG_PROCESS_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG process source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_TRG_PROCESS_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>CREATE TRG ref destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_TRG_REF_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>CREATE TRG ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_TRG_REF_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_TRG_SYSTEM_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG thread destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_TRG_THREAD_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG thread source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_TRG_THREAD_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR feature2feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_CORR_FEATURE2FEATURE1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>CREATE CORR feature2feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_CORR_FEATURE2FEATURE2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_CORR_P2A1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_CORR_P2A2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 26;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_CORR_P2D1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 27;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_CORR_P2D2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 28;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR process2process1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_CORR_PROCESS2PROCESS1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 29;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR process2process2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_CORR_PROCESS2PROCESS2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 30;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_CORR_R2R1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 31;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CREATE_CORR_R2R2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 32;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_CORR_S2S = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 33;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR thread2thread1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_CORR_THREAD2THREAD1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 34;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR thread2thread2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER__CONTEXT_CORR_THREAD2THREAD2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 35;

	/**
	 * The number of structural features of the '<em>Port Connection To Data Access At System01 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 36;

	/**
	 * The number of operations of the '<em>Port Connection To Data Access At System01 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM01_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionToDataAccessAtSystem10__MarkerImpl <em>Port Connection To Data Access At System10 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionToDataAccessAtSystem10__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER = 22;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_SRC_FEATURE_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CREATE SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC process destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_SRC_PROCESS_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC process source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_SRC_PROCESS_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_SRC_REF_CONNECTION_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_SRC_SYSTEM_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC thread destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_SRC_THREAD_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC thread source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_SRC_THREAD_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CREATE TRG data destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_TRG_DATA_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CREATE TRG data source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_TRG_DATA_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_TRG_DATAACCESS_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_TRG_DATAACCESS_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG feature destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_TRG_FEATURE_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>CREATE TRG feature source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_TRG_FEATURE_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG process destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_TRG_PROCESS_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG process source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_TRG_PROCESS_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>CREATE TRG ref destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_TRG_REF_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>CREATE TRG ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_TRG_REF_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_TRG_SYSTEM_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG thread destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_TRG_THREAD_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG thread source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_TRG_THREAD_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>CREATE CORR feature2feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_CORR_FEATURE2FEATURE1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR feature2feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_CORR_FEATURE2FEATURE2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_CORR_P2A1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_CORR_P2A2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 26;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_CORR_P2D1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 27;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_CORR_P2D2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 28;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR process2process1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_CORR_PROCESS2PROCESS1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 29;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR process2process2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_CORR_PROCESS2PROCESS2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 30;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_CORR_R2R1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 31;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CREATE_CORR_R2R2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 32;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_CORR_S2S = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 33;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR thread2thread1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_CORR_THREAD2THREAD1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 34;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR thread2thread2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER__CONTEXT_CORR_THREAD2THREAD2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 35;

	/**
	 * The number of structural features of the '<em>Port Connection To Data Access At System10 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 36;

	/**
	 * The number of operations of the '<em>Port Connection To Data Access At System10 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM10_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl <em>Port Connection To Data Access At System11 Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.PortConnectionToDataAccessAtSystem11__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER = 23;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC feature destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC feature source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_FEATURE_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC process destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC process source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_PROCESS_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>CREATE SRC ref connection source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_SRC_REF_CONNECTION_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_SYSTEM_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC thread destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CONTEXT SRC thread source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_SRC_THREAD_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>CREATE TRG data destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>CREATE TRG data source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATA_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>CREATE TRG dataaccess source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_DATAACCESS_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG feature destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG feature source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_FEATURE_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG process destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG process source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_PROCESS_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>CREATE TRG ref destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_DESTINATION = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>CREATE TRG ref source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_TRG_REF_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_SYSTEM_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG thread destination target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_DESTINATION_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>CONTEXT TRG thread source target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_TRG_THREAD_SOURCE_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR feature2feature1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR feature2feature2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_FEATURE2FEATURE2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2a2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2A2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 26;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 27;

	/**
	 * The feature id for the '<em><b>CREATE CORR p2d2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_P2D2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 28;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR process2process1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 29;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR process2process2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_PROCESS2PROCESS2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 30;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 31;

	/**
	 * The feature id for the '<em><b>CREATE CORR r2r2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CREATE_CORR_R2R2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 32;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_S2S = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 33;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR thread2thread1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD1 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 34;

	/**
	 * The feature id for the '<em><b>CONTEXT CORR thread2thread2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER__CONTEXT_CORR_THREAD2THREAD2 = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 35;

	/**
	 * The number of structural features of the '<em>Port Connection To Data Access At System11 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 36;

	/**
	 * The number of operations of the '<em>Port Connection To Data Access At System11 Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION_TO_DATA_ACCESS_AT_SYSTEM11_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link Tgg.impl.SystemToSystem__MarkerImpl <em>System To System Marker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Tgg.impl.SystemToSystem__MarkerImpl
	 * @see Tgg.impl.TggPackageImpl#getSystemToSystem__Marker()
	 * @generated
	 */
	int SYSTEM_TO_SYSTEM_MARKER = 24;

	/**
	 * The feature id for the '<em><b>Protocol</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER__PROTOCOL = RuntimePackage.TGG_RULE_APPLICATION__PROTOCOL;

	/**
	 * The feature id for the '<em><b>CREATE SRC system source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER__CREATE_SRC_SYSTEM_SOURCE = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>CREATE TRG system target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER__CREATE_TRG_SYSTEM_TARGET = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>CREATE CORR s2s</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER__CREATE_CORR_S2S = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>System To System Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER_FEATURE_COUNT = RuntimePackage.TGG_RULE_APPLICATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>System To System Marker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TO_SYSTEM_MARKER_OPERATION_COUNT = RuntimePackage.TGG_RULE_APPLICATION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link Tgg.SystemToSystem <em>System To System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System To System</em>'.
	 * @see Tgg.SystemToSystem
	 * @generated
	 */
	EClass getSystemToSystem();

	/**
	 * Returns the meta object for class '{@link Tgg.ConnectionToConnection <em>Connection To Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection To Connection</em>'.
	 * @see Tgg.ConnectionToConnection
	 * @generated
	 */
	EClass getConnectionToConnection();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionToConnection#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see Tgg.ConnectionToConnection#getSource()
	 * @see #getConnectionToConnection()
	 * @generated
	 */
	EReference getConnectionToConnection_Source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionToConnection#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see Tgg.ConnectionToConnection#getTarget()
	 * @see #getConnectionToConnection()
	 * @generated
	 */
	EReference getConnectionToConnection_Target();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionToSharedData <em>Port Connection To Shared Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection To Shared Data</em>'.
	 * @see Tgg.PortConnectionToSharedData
	 * @generated
	 */
	EClass getPortConnectionToSharedData();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToSharedData#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see Tgg.PortConnectionToSharedData#getSource()
	 * @see #getPortConnectionToSharedData()
	 * @generated
	 */
	EReference getPortConnectionToSharedData_Source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToSharedData#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see Tgg.PortConnectionToSharedData#getTarget()
	 * @see #getPortConnectionToSharedData()
	 * @generated
	 */
	EReference getPortConnectionToSharedData_Target();

	/**
	 * Returns the meta object for class '{@link Tgg.Reference2Reference <em>Reference2 Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference2 Reference</em>'.
	 * @see Tgg.Reference2Reference
	 * @generated
	 */
	EClass getReference2Reference();

	/**
	 * Returns the meta object for the reference '{@link Tgg.Reference2Reference#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see Tgg.Reference2Reference#getSource()
	 * @see #getReference2Reference()
	 * @generated
	 */
	EReference getReference2Reference_Source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.Reference2Reference#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see Tgg.Reference2Reference#getTarget()
	 * @see #getReference2Reference()
	 * @generated
	 */
	EReference getReference2Reference_Target();

	/**
	 * Returns the meta object for class '{@link Tgg.End2End <em>End2 End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>End2 End</em>'.
	 * @see Tgg.End2End
	 * @generated
	 */
	EClass getEnd2End();

	/**
	 * Returns the meta object for the reference '{@link Tgg.End2End#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see Tgg.End2End#getSource()
	 * @see #getEnd2End()
	 * @generated
	 */
	EReference getEnd2End_Source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.End2End#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see Tgg.End2End#getTarget()
	 * @see #getEnd2End()
	 * @generated
	 */
	EReference getEnd2End_Target();

	/**
	 * Returns the meta object for class '{@link Tgg.CompToComp <em>Comp To Comp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comp To Comp</em>'.
	 * @see Tgg.CompToComp
	 * @generated
	 */
	EClass getCompToComp();

	/**
	 * Returns the meta object for class '{@link Tgg.FeatToFeat <em>Feat To Feat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feat To Feat</em>'.
	 * @see Tgg.FeatToFeat
	 * @generated
	 */
	EClass getFeatToFeat();

	/**
	 * Returns the meta object for class '{@link Tgg.ComponentToComponent__Marker <em>Component To Component Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component To Component Marker</em>'.
	 * @see Tgg.ComponentToComponent__Marker
	 * @generated
	 */
	EClass getComponentToComponent__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ComponentToComponent__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Tgg.ComponentToComponent__Marker#getCONTEXT__SRC__component_source()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ComponentToComponent__Marker#getCREATE__SRC__subcomponent_source <em>CREATE SRC subcomponent source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC subcomponent source</em>'.
	 * @see Tgg.ComponentToComponent__Marker#getCREATE__SRC__subcomponent_source()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CREATE__SRC__subcomponent_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ComponentToComponent__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component target</em>'.
	 * @see Tgg.ComponentToComponent__Marker#getCONTEXT__TRG__component_target()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CONTEXT__TRG__component_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ComponentToComponent__Marker#getCREATE__TRG__subcomponent_target <em>CREATE TRG subcomponent target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG subcomponent target</em>'.
	 * @see Tgg.ComponentToComponent__Marker#getCREATE__TRG__subcomponent_target()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CREATE__TRG__subcomponent_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ComponentToComponent__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Tgg.ComponentToComponent__Marker#getCONTEXT__CORR__c2c()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ComponentToComponent__Marker#getCREATE__CORR__sub2sub <em>CREATE CORR sub2sub</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR sub2sub</em>'.
	 * @see Tgg.ComponentToComponent__Marker#getCREATE__CORR__sub2sub()
	 * @see #getComponentToComponent__Marker()
	 * @generated
	 */
	EReference getComponentToComponent__Marker_CREATE__CORR__sub2sub();

	/**
	 * Returns the meta object for class '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker <em>Connection Instance To Connection Instance Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Instance To Connection Instance Marker</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker
	 * @generated
	 */
	EClass getConnectionInstanceToConnectionInstance__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__component_source()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCREATE__SRC__connection_source()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_destination <em>CONTEXT SRC sdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC sdestination</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_destination()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC ssource</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__SRC__s_source()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__SRC__s_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component target</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__component_target()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__component_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCREATE__TRG__connection_target <em>CREATE TRG connection target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG connection target</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCREATE__TRG__connection_target()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__TRG__connection_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_destination <em>CONTEXT TRG tdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tdestination</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_destination()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tsource</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__TRG__t_source()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__TRG__t_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCREATE__CORR__c2c <em>CREATE CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR c2c</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCREATE__CORR__c2c()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CREATE__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR comp2comp</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__comp2comp()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__comp2comp();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e1 <em>CONTEXT CORR e2e1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e1</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e1()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e2</em>'.
	 * @see Tgg.ConnectionInstanceToConnectionInstance__Marker#getCONTEXT__CORR__e2e2()
	 * @see #getConnectionInstanceToConnectionInstance__Marker()
	 * @generated
	 */
	EReference getConnectionInstanceToConnectionInstance__Marker_CONTEXT__CORR__e2e2();

	/**
	 * Returns the meta object for class '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker <em>Connection Ref To Connection Ref For Component Context Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Ref To Connection Ref For Component Context Marker</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker
	 * @generated
	 */
	EClass getConnectionRefToConnectionRefForComponentContext__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCREATE__SRC__connectionRef_source <em>CREATE SRC connection Ref source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection Ref source</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCREATE__SRC__connectionRef_source()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CREATE__SRC__connectionRef_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__SRC__connection_source <em>CONTEXT SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC connection source</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__SRC__connection_source()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__SRC__s_context <em>CONTEXT SRC scontext</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC scontext</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__SRC__s_context()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__SRC__s_context();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__SRC__s_destination <em>CONTEXT SRC sdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC sdestination</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__SRC__s_destination()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__SRC__s_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC ssource</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__SRC__s_source()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__SRC__s_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCREATE__TRG__connectionRef_target <em>CREATE TRG connection Ref target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG connection Ref target</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCREATE__TRG__connectionRef_target()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CREATE__TRG__connectionRef_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__TRG__connection_target <em>CONTEXT TRG connection target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG connection target</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__TRG__connection_target()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__TRG__connection_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__TRG__t_context <em>CONTEXT TRG tcontext</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tcontext</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__TRG__t_context()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__TRG__t_context();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__TRG__t_destination <em>CONTEXT TRG tdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tdestination</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__TRG__t_destination()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__TRG__t_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tsource</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__TRG__t_source()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__TRG__t_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__CORR__c2c()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR comp2comp</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__CORR__comp2comp()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__CORR__comp2comp();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__CORR__e2e1 <em>CONTEXT CORR e2e1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e1</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__CORR__e2e1()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__CORR__e2e1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e2</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCONTEXT__CORR__e2e2()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CONTEXT__CORR__e2e2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCREATE__CORR__r2r <em>CREATE CORR r2r</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r</em>'.
	 * @see Tgg.ConnectionRefToConnectionRefForComponentContext__Marker#getCREATE__CORR__r2r()
	 * @see #getConnectionRefToConnectionRefForComponentContext__Marker()
	 * @generated
	 */
	EReference getConnectionRefToConnectionRefForComponentContext__Marker_CREATE__CORR__r2r();

	/**
	 * Returns the meta object for class '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker <em>Connection Reference For Device System Context01 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Reference For Device System Context01 Marker</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker
	 * @generated
	 */
	EClass getConnectionReferenceForDeviceSystemContext01__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__SRC__connectionRef_source <em>CREATE SRC connection Ref source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection Ref source</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__SRC__connectionRef_source()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__SRC__connectionRef_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__SRC__connection_source <em>CONTEXT SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC connection source</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__SRC__connection_source()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__SRC__s_context <em>CONTEXT SRC scontext</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC scontext</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__SRC__s_context()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__SRC__s_context();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__SRC__s_destination <em>CONTEXT SRC sdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC sdestination</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__SRC__s_destination()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__SRC__s_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__SRC__s_source <em>CREATE SRC ssource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ssource</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__SRC__s_source()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__SRC__s_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__TRG__connectionRef_target <em>CREATE TRG connection Ref target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG connection Ref target</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__TRG__connectionRef_target()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__TRG__connectionRef_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__TRG__connection_target <em>CONTEXT TRG connection target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG connection target</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__TRG__connection_target()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__TRG__connection_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__TRG__t_context <em>CONTEXT TRG tcontext</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tcontext</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__TRG__t_context()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__TRG__t_context();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__TRG__t_destination <em>CONTEXT TRG tdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tdestination</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__TRG__t_destination()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__TRG__t_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__TRG__t_source <em>CREATE TRG tsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG tsource</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__TRG__t_source()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__TRG__t_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__CORR__c2c()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR comp2comp</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__CORR__comp2comp()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__CORR__comp2comp();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__CORR__e2e1 <em>CONTEXT CORR e2e1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e1</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCONTEXT__CORR__e2e1()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CONTEXT__CORR__e2e1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__CORR__e2e2 <em>CREATE CORR e2e2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR e2e2</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__CORR__e2e2()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__CORR__e2e2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__CORR__r2r <em>CREATE CORR r2r</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext01__Marker#getCREATE__CORR__r2r()
	 * @see #getConnectionReferenceForDeviceSystemContext01__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext01__Marker_CREATE__CORR__r2r();

	/**
	 * Returns the meta object for class '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker <em>Connection Reference For Device System Context10 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Reference For Device System Context10 Marker</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker
	 * @generated
	 */
	EClass getConnectionReferenceForDeviceSystemContext10__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__SRC__connectionRef_source <em>CREATE SRC connection Ref source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection Ref source</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__SRC__connectionRef_source()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__SRC__connectionRef_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__connection_source <em>CONTEXT SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC connection source</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__connection_source()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__s_context <em>CONTEXT SRC scontext</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC scontext</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__s_context()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__SRC__s_context();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__SRC__s_destination <em>CREATE SRC sdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC sdestination</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__SRC__s_destination()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__SRC__s_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC ssource</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__SRC__s_source()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__SRC__s_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__TRG__connectionRef_target <em>CREATE TRG connection Ref target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG connection Ref target</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__TRG__connectionRef_target()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__TRG__connectionRef_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__connection_target <em>CONTEXT TRG connection target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG connection target</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__connection_target()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__TRG__connection_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__t_context <em>CONTEXT TRG tcontext</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tcontext</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__t_context()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__TRG__t_context();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__TRG__t_destination <em>CREATE TRG tdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG tdestination</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__TRG__t_destination()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__TRG__t_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tsource</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__TRG__t_source()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__TRG__t_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__c2c()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR comp2comp</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__comp2comp()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__CORR__comp2comp();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__CORR__e2e1 <em>CREATE CORR e2e1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR e2e1</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__CORR__e2e1()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__CORR__e2e1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e2</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCONTEXT__CORR__e2e2()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CONTEXT__CORR__e2e2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__CORR__r2r <em>CREATE CORR r2r</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext10__Marker#getCREATE__CORR__r2r()
	 * @see #getConnectionReferenceForDeviceSystemContext10__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext10__Marker_CREATE__CORR__r2r();

	/**
	 * Returns the meta object for class '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker <em>Connection Reference For Device System Context11 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection Reference For Device System Context11 Marker</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker
	 * @generated
	 */
	EClass getConnectionReferenceForDeviceSystemContext11__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCREATE__SRC__connectionRef_source <em>CREATE SRC connection Ref source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection Ref source</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCREATE__SRC__connectionRef_source()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CREATE__SRC__connectionRef_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__SRC__connection_source <em>CONTEXT SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC connection source</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__SRC__connection_source()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__SRC__s_context <em>CONTEXT SRC scontext</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC scontext</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__SRC__s_context()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__SRC__s_context();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__SRC__s_destination <em>CONTEXT SRC sdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC sdestination</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__SRC__s_destination()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__SRC__s_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__SRC__s_source <em>CONTEXT SRC ssource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC ssource</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__SRC__s_source()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__SRC__s_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCREATE__TRG__connectionRef_target <em>CREATE TRG connection Ref target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG connection Ref target</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCREATE__TRG__connectionRef_target()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CREATE__TRG__connectionRef_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__TRG__connection_target <em>CONTEXT TRG connection target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG connection target</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__TRG__connection_target()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__TRG__connection_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__TRG__t_context <em>CONTEXT TRG tcontext</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tcontext</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__TRG__t_context()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__TRG__t_context();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__TRG__t_destination <em>CONTEXT TRG tdestination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tdestination</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__TRG__t_destination()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__TRG__t_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__TRG__t_source <em>CONTEXT TRG tsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG tsource</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__TRG__t_source()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__TRG__t_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__CORR__c2c()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__CORR__comp2comp <em>CONTEXT CORR comp2comp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR comp2comp</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__CORR__comp2comp()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__CORR__comp2comp();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__CORR__e2e1 <em>CONTEXT CORR e2e1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e1</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__CORR__e2e1()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__CORR__e2e1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__CORR__e2e2 <em>CONTEXT CORR e2e2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR e2e2</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCONTEXT__CORR__e2e2()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CONTEXT__CORR__e2e2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCREATE__CORR__r2r <em>CREATE CORR r2r</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r</em>'.
	 * @see Tgg.ConnectionReferenceForDeviceSystemContext11__Marker#getCREATE__CORR__r2r()
	 * @see #getConnectionReferenceForDeviceSystemContext11__Marker()
	 * @generated
	 */
	EReference getConnectionReferenceForDeviceSystemContext11__Marker_CREATE__CORR__r2r();

	/**
	 * Returns the meta object for class '{@link Tgg.FeatureToFeature__Marker <em>Feature To Feature Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature To Feature Marker</em>'.
	 * @see Tgg.FeatureToFeature__Marker
	 * @generated
	 */
	EClass getFeatureToFeature__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.FeatureToFeature__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Tgg.FeatureToFeature__Marker#getCONTEXT__SRC__component_source()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.FeatureToFeature__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature source</em>'.
	 * @see Tgg.FeatureToFeature__Marker#getCREATE__SRC__feature_source()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CREATE__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.FeatureToFeature__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component target</em>'.
	 * @see Tgg.FeatureToFeature__Marker#getCONTEXT__TRG__component_target()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CONTEXT__TRG__component_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.FeatureToFeature__Marker#getCREATE__TRG__feature_target <em>CREATE TRG feature target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature target</em>'.
	 * @see Tgg.FeatureToFeature__Marker#getCREATE__TRG__feature_target()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CREATE__TRG__feature_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.FeatureToFeature__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Tgg.FeatureToFeature__Marker#getCONTEXT__CORR__c2c()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.FeatureToFeature__Marker#getCREATE__CORR__f2f2 <em>CREATE CORR f2f2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR f2f2</em>'.
	 * @see Tgg.FeatureToFeature__Marker#getCREATE__CORR__f2f2()
	 * @see #getFeatureToFeature__Marker()
	 * @generated
	 */
	EReference getFeatureToFeature__Marker_CREATE__CORR__f2f2();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionForDeviceDestination__Marker <em>Port Connection For Device Destination Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection For Device Destination Marker</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker
	 * @generated
	 */
	EClass getPortConnectionForDeviceDestination__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__SRC__component_destination <em>CONTEXT SRC component destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component destination</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__SRC__component_destination()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__SRC__component_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__SRC__component_source()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__SRC__connection_source()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature destination</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__SRC__feature_destination()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CREATE__SRC__feature_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature source</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__SRC__feature_source()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CREATE__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC system source</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__SRC__system_source()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__SRC__system_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__TRG__component_destination_target <em>CONTEXT TRG component destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component destination target</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__TRG__component_destination_target()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__TRG__component_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__TRG__component_source_target <em>CONTEXT TRG component source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component source target</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__TRG__component_source_target()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__TRG__component_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__TRG__connection_target <em>CREATE TRG connection target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG connection target</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__TRG__connection_target()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CREATE__TRG__connection_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__TRG__feature_destination_target <em>CREATE TRG feature destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature destination target</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__TRG__feature_destination_target()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CREATE__TRG__feature_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__TRG__feature_source_target <em>CREATE TRG feature source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature source target</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__TRG__feature_source_target()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CREATE__TRG__feature_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG system target</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__TRG__system_target()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__TRG__system_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__CORR__c2c1 <em>CONTEXT CORR c2c1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c1</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__CORR__c2c1()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__CORR__c2c1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__CORR__c2c2 <em>CONTEXT CORR c2c2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c2</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__CORR__c2c2()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__CORR__c2c2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__CORR__co2co <em>CREATE CORR co2co</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR co2co</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__CORR__co2co()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CREATE__CORR__co2co();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__CORR__feature2feature1 <em>CREATE CORR feature2feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR feature2feature1</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__CORR__feature2feature1()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CREATE__CORR__feature2feature1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__CORR__feature2feature2 <em>CREATE CORR feature2feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR feature2feature2</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCREATE__CORR__feature2feature2()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CREATE__CORR__feature2feature2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR s2s</em>'.
	 * @see Tgg.PortConnectionForDeviceDestination__Marker#getCONTEXT__CORR__s2s()
	 * @see #getPortConnectionForDeviceDestination__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceDestination__Marker_CONTEXT__CORR__s2s();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionForDeviceSource__Marker <em>Port Connection For Device Source Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection For Device Source Marker</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker
	 * @generated
	 */
	EClass getPortConnectionForDeviceSource__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__component_destination <em>CONTEXT SRC component destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component destination</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__component_destination()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CONTEXT__SRC__component_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__component_source()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__connection_source()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature destination</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__feature_destination()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CREATE__SRC__feature_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature source</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCREATE__SRC__feature_source()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CREATE__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC system source</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__SRC__system_source()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CONTEXT__SRC__system_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__component_destination_target <em>CONTEXT TRG component destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component destination target</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__component_destination_target()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CONTEXT__TRG__component_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__component_source_target <em>CONTEXT TRG component source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component source target</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__component_source_target()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CONTEXT__TRG__component_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__connection_target <em>CREATE TRG connection target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG connection target</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__connection_target()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CREATE__TRG__connection_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__feature_destination_target <em>CREATE TRG feature destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature destination target</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__feature_destination_target()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CREATE__TRG__feature_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__feature_source_target <em>CREATE TRG feature source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature source target</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCREATE__TRG__feature_source_target()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CREATE__TRG__feature_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG system target</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__TRG__system_target()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CONTEXT__TRG__system_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__c2c1 <em>CONTEXT CORR c2c1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c1</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__c2c1()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CONTEXT__CORR__c2c1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__c2c2 <em>CONTEXT CORR c2c2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c2</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__c2c2()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CONTEXT__CORR__c2c2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__co2co <em>CREATE CORR co2co</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR co2co</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__co2co()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CREATE__CORR__co2co();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__feature2feature1 <em>CREATE CORR feature2feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR feature2feature1</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__feature2feature1()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CREATE__CORR__feature2feature1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__feature2feature2 <em>CREATE CORR feature2feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR feature2feature2</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCREATE__CORR__feature2feature2()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CREATE__CORR__feature2feature2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR s2s</em>'.
	 * @see Tgg.PortConnectionForDeviceSource__Marker#getCONTEXT__CORR__s2s()
	 * @see #getPortConnectionForDeviceSource__Marker()
	 * @generated
	 */
	EReference getPortConnectionForDeviceSource__Marker_CONTEXT__CORR__s2s();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker <em>Port Connection To Data Access At Process00 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection To Data Access At Process00 Marker</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker
	 * @generated
	 */
	EClass getPortConnectionToDataAccessAtProcess00__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_conn_destination <em>CONTEXT SRC component conn destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component conn destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_conn_destination()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__SRC__component_conn_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_conn_source <em>CONTEXT SRC component conn source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component conn source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_conn_source()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__SRC__component_conn_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__SRC__component_source()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__connection_source()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__feature_destination()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__feature_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__feature_source()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__SRC__ref_connection_source()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__SRC__ref_connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_conn_destination_target <em>CONTEXT TRG component conn destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component conn destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_conn_destination_target()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__TRG__component_conn_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_conn_source_target <em>CONTEXT TRG component conn source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component conn source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_conn_source_target()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__TRG__component_conn_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__TRG__component_target()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__TRG__component_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__data <em>CREATE TRG data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__data()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__data();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__dataaccess1 <em>CREATE TRG dataaccess1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__dataaccess1()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__dataaccess1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__dataaccess2 <em>CREATE TRG dataaccess2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__dataaccess2()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__dataaccess2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__feature1 <em>CREATE TRG feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__feature1()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__feature1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__feature2 <em>CREATE TRG feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__feature2()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__feature2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__refconnection1 <em>CREATE TRG refconnection1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG refconnection1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__refconnection1()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__refconnection1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__refconnection2 <em>CREATE TRG refconnection2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG refconnection2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__TRG__refconnection2()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__TRG__refconnection2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__c2c()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__f2f1 <em>CREATE CORR f2f1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR f2f1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__f2f1()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__f2f1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__f2f2 <em>CREATE CORR f2f2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR f2f2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__f2f2()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__f2f2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2a1()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2a1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2a2()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2a2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2d <em>CREATE CORR p2d</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2d()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2d();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2r1 <em>CREATE CORR p2r1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2r1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2r1()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2r1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2r2 <em>CREATE CORR p2r2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2r2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCREATE__CORR__p2r2()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CREATE__CORR__p2r2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__sub2sub1 <em>CONTEXT CORR sub2sub1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR sub2sub1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__sub2sub1()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__CORR__sub2sub1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__sub2sub2 <em>CONTEXT CORR sub2sub2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR sub2sub2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess00__Marker#getCONTEXT__CORR__sub2sub2()
	 * @see #getPortConnectionToDataAccessAtProcess00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess00__Marker_CONTEXT__CORR__sub2sub2();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker <em>Port Connection To Data Access At Process01 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection To Data Access At Process01 Marker</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker
	 * @generated
	 */
	EClass getPortConnectionToDataAccessAtProcess01__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__SRC__component_conn_destination <em>CONTEXT SRC component conn destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component conn destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__SRC__component_conn_destination()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__SRC__component_conn_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__SRC__component_conn_source <em>CONTEXT SRC component conn source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component conn source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__SRC__component_conn_source()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__SRC__component_conn_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__SRC__component_source()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__SRC__connection_source()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__SRC__feature_destination <em>CONTEXT SRC feature destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC feature destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__SRC__feature_destination()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__SRC__feature_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__SRC__feature_source()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__SRC__ref_connection_source()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__SRC__ref_connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__TRG__component_conn_destination_target <em>CONTEXT TRG component conn destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component conn destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__TRG__component_conn_destination_target()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__TRG__component_conn_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__TRG__component_conn_source_target <em>CONTEXT TRG component conn source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component conn source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__TRG__component_conn_source_target()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__TRG__component_conn_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__TRG__component_target()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__TRG__component_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__data <em>CREATE TRG data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__data()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__data();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__dataaccess1 <em>CREATE TRG dataaccess1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__dataaccess1()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__dataaccess1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__dataaccess2 <em>CREATE TRG dataaccess2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__dataaccess2()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__dataaccess2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__feature1 <em>CREATE TRG feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__feature1()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__feature1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__TRG__feature2 <em>CONTEXT TRG feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG feature2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__TRG__feature2()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__TRG__feature2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__refconnection1 <em>CREATE TRG refconnection1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG refconnection1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__refconnection1()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__refconnection1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__refconnection2 <em>CREATE TRG refconnection2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG refconnection2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__TRG__refconnection2()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__TRG__refconnection2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__CORR__c2c()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__f2f1 <em>CREATE CORR f2f1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR f2f1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__f2f1()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__f2f1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__CORR__f2f2 <em>CONTEXT CORR f2f2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR f2f2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__CORR__f2f2()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__CORR__f2f2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__p2a1()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__p2a1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__p2a2()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__p2a2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__p2d <em>CREATE CORR p2d</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__p2d()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__p2d();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__p2r1 <em>CREATE CORR p2r1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2r1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__p2r1()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__p2r1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__p2r2 <em>CREATE CORR p2r2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2r2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCREATE__CORR__p2r2()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CREATE__CORR__p2r2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__CORR__sub2sub1 <em>CONTEXT CORR sub2sub1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR sub2sub1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__CORR__sub2sub1()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__CORR__sub2sub1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__CORR__sub2sub2 <em>CONTEXT CORR sub2sub2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR sub2sub2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess01__Marker#getCONTEXT__CORR__sub2sub2()
	 * @see #getPortConnectionToDataAccessAtProcess01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess01__Marker_CONTEXT__CORR__sub2sub2();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker <em>Port Connection To Data Access At Process10 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection To Data Access At Process10 Marker</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker
	 * @generated
	 */
	EClass getPortConnectionToDataAccessAtProcess10__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__SRC__component_conn_destination <em>CONTEXT SRC component conn destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component conn destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__SRC__component_conn_destination()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__SRC__component_conn_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__SRC__component_conn_source <em>CONTEXT SRC component conn source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component conn source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__SRC__component_conn_source()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__SRC__component_conn_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__SRC__component_source()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__SRC__connection_source()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__SRC__feature_destination()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__SRC__feature_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__SRC__feature_source <em>CONTEXT SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC feature source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__SRC__feature_source()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__SRC__ref_connection_source()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__SRC__ref_connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__TRG__component_conn_destination_target <em>CONTEXT TRG component conn destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component conn destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__TRG__component_conn_destination_target()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__TRG__component_conn_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__TRG__component_conn_source_target <em>CONTEXT TRG component conn source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component conn source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__TRG__component_conn_source_target()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__TRG__component_conn_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__TRG__component_target()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__TRG__component_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__data <em>CREATE TRG data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__data()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__data();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__dataaccess1 <em>CREATE TRG dataaccess1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__dataaccess1()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__dataaccess1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__dataaccess2 <em>CREATE TRG dataaccess2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__dataaccess2()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__dataaccess2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__TRG__feature1 <em>CONTEXT TRG feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG feature1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__TRG__feature1()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__TRG__feature1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__feature2 <em>CREATE TRG feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__feature2()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__feature2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__refconnection1 <em>CREATE TRG refconnection1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG refconnection1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__refconnection1()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__refconnection1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__refconnection2 <em>CREATE TRG refconnection2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG refconnection2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__TRG__refconnection2()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__TRG__refconnection2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__CORR__c2c()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__CORR__f2f1 <em>CONTEXT CORR f2f1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR f2f1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__CORR__f2f1()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__CORR__f2f1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__f2f2 <em>CREATE CORR f2f2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR f2f2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__f2f2()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__f2f2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__p2a1()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__p2a1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__p2a2()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__p2a2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__p2d <em>CREATE CORR p2d</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__p2d()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__p2d();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__p2r1 <em>CREATE CORR p2r1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2r1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__p2r1()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__p2r1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__p2r2 <em>CREATE CORR p2r2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2r2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCREATE__CORR__p2r2()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CREATE__CORR__p2r2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__CORR__sub2sub1 <em>CONTEXT CORR sub2sub1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR sub2sub1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__CORR__sub2sub1()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__CORR__sub2sub1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__CORR__sub2sub2 <em>CONTEXT CORR sub2sub2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR sub2sub2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess10__Marker#getCONTEXT__CORR__sub2sub2()
	 * @see #getPortConnectionToDataAccessAtProcess10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess10__Marker_CONTEXT__CORR__sub2sub2();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker <em>Port Connection To Data Access At Process11 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection To Data Access At Process11 Marker</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker
	 * @generated
	 */
	EClass getPortConnectionToDataAccessAtProcess11__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__SRC__component_conn_destination <em>CONTEXT SRC component conn destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component conn destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__SRC__component_conn_destination()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__SRC__component_conn_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__SRC__component_conn_source <em>CONTEXT SRC component conn source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component conn source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__SRC__component_conn_source()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__SRC__component_conn_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__SRC__component_source <em>CONTEXT SRC component source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC component source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__SRC__component_source()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__SRC__component_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__SRC__connection_source()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__SRC__feature_destination <em>CONTEXT SRC feature destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC feature destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__SRC__feature_destination()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__SRC__feature_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__SRC__feature_source <em>CONTEXT SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC feature source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__SRC__feature_source()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__SRC__ref_connection_source()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__SRC__ref_connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__TRG__component_conn_destination_target <em>CONTEXT TRG component conn destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component conn destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__TRG__component_conn_destination_target()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__TRG__component_conn_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__TRG__component_conn_source_target <em>CONTEXT TRG component conn source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component conn source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__TRG__component_conn_source_target()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__TRG__component_conn_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__TRG__component_target <em>CONTEXT TRG component target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG component target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__TRG__component_target()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__TRG__component_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__TRG__data <em>CREATE TRG data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__TRG__data()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__TRG__data();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__TRG__dataaccess1 <em>CREATE TRG dataaccess1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__TRG__dataaccess1()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__TRG__dataaccess1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__TRG__dataaccess2 <em>CREATE TRG dataaccess2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__TRG__dataaccess2()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__TRG__dataaccess2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__TRG__feature1 <em>CONTEXT TRG feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG feature1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__TRG__feature1()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__TRG__feature1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__TRG__feature2 <em>CONTEXT TRG feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG feature2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__TRG__feature2()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__TRG__feature2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__TRG__refconnection1 <em>CREATE TRG refconnection1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG refconnection1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__TRG__refconnection1()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__TRG__refconnection1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__TRG__refconnection2 <em>CREATE TRG refconnection2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG refconnection2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__TRG__refconnection2()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__TRG__refconnection2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__CORR__c2c <em>CONTEXT CORR c2c</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR c2c</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__CORR__c2c()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__CORR__c2c();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__CORR__f2f1 <em>CONTEXT CORR f2f1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR f2f1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__CORR__f2f1()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__CORR__f2f1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__CORR__f2f2 <em>CONTEXT CORR f2f2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR f2f2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__CORR__f2f2()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__CORR__f2f2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__CORR__p2a1()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__CORR__p2a1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__CORR__p2a2()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__CORR__p2a2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__CORR__p2d <em>CREATE CORR p2d</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__CORR__p2d()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__CORR__p2d();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__CORR__p2r1 <em>CREATE CORR p2r1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2r1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__CORR__p2r1()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__CORR__p2r1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__CORR__p2r2 <em>CREATE CORR p2r2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2r2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCREATE__CORR__p2r2()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CREATE__CORR__p2r2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__CORR__sub2sub1 <em>CONTEXT CORR sub2sub1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR sub2sub1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__CORR__sub2sub1()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__CORR__sub2sub1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__CORR__sub2sub2 <em>CONTEXT CORR sub2sub2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR sub2sub2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtProcess11__Marker#getCONTEXT__CORR__sub2sub2()
	 * @see #getPortConnectionToDataAccessAtProcess11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtProcess11__Marker_CONTEXT__CORR__sub2sub2();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker <em>Port Connection To Data Access At System00 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection To Data Access At System00 Marker</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker
	 * @generated
	 */
	EClass getPortConnectionToDataAccessAtSystem00__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__SRC__connection_source()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__SRC__feature_destination()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__SRC__feature_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__SRC__feature_source()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__SRC__process_destination <em>CONTEXT SRC process destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC process destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__SRC__process_destination()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__SRC__process_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__SRC__process_source <em>CONTEXT SRC process source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC process source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__SRC__process_source()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__SRC__process_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__SRC__ref_connection_destination <em>CREATE SRC ref connection destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__SRC__ref_connection_destination()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__SRC__ref_connection_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__SRC__ref_connection_source()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__SRC__ref_connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC system source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__SRC__system_source()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__SRC__system_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__SRC__thread_destination <em>CONTEXT SRC thread destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC thread destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__SRC__thread_destination()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__SRC__thread_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__SRC__thread_source <em>CONTEXT SRC thread source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC thread source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__SRC__thread_source()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__SRC__thread_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__data_destination <em>CREATE TRG data destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__data_destination()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__data_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__data_source <em>CREATE TRG data source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__data_source()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__data_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__dataaccess_destination <em>CREATE TRG dataaccess destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__dataaccess_destination()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__dataaccess_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__dataaccess_source <em>CREATE TRG dataaccess source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__dataaccess_source()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__dataaccess_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__feature_destination_target <em>CREATE TRG feature destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__feature_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__feature_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__feature_source_target <em>CREATE TRG feature source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__feature_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__feature_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__TRG__process_destination_target <em>CONTEXT TRG process destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG process destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__TRG__process_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__TRG__process_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__TRG__process_source_target <em>CONTEXT TRG process source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG process source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__TRG__process_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__TRG__process_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__ref_destination <em>CREATE TRG ref destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG ref destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__ref_destination()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__ref_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__ref_source <em>CREATE TRG ref source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG ref source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__TRG__ref_source()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__TRG__ref_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG system target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__TRG__system_target()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__TRG__system_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__TRG__thread_destination_target <em>CONTEXT TRG thread destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG thread destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__TRG__thread_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__TRG__thread_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__TRG__thread_source_target <em>CONTEXT TRG thread source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG thread source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__TRG__thread_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__TRG__thread_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__feature2feature1 <em>CREATE CORR feature2feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR feature2feature1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__feature2feature1()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__feature2feature1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__feature2feature2 <em>CREATE CORR feature2feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR feature2feature2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__feature2feature2()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__feature2feature2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__p2a1()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__p2a1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__p2a2()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__p2a2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__p2d1 <em>CREATE CORR p2d1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__p2d1()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__p2d1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__p2d2 <em>CREATE CORR p2d2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__p2d2()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__p2d2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__CORR__process2process1 <em>CONTEXT CORR process2process1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR process2process1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__CORR__process2process1()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__CORR__process2process1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__CORR__process2process2 <em>CONTEXT CORR process2process2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR process2process2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__CORR__process2process2()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__CORR__process2process2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__r2r1 <em>CREATE CORR r2r1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__r2r1()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__r2r1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__r2r2 <em>CREATE CORR r2r2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCREATE__CORR__r2r2()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CREATE__CORR__r2r2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR s2s</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__CORR__s2s()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__CORR__s2s();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__CORR__thread2thread1 <em>CONTEXT CORR thread2thread1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR thread2thread1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__CORR__thread2thread1()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__CORR__thread2thread1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__CORR__thread2thread2 <em>CONTEXT CORR thread2thread2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR thread2thread2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem00__Marker#getCONTEXT__CORR__thread2thread2()
	 * @see #getPortConnectionToDataAccessAtSystem00__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem00__Marker_CONTEXT__CORR__thread2thread2();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker <em>Port Connection To Data Access At System01 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection To Data Access At System01 Marker</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker
	 * @generated
	 */
	EClass getPortConnectionToDataAccessAtSystem01__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__SRC__connection_source()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__SRC__feature_destination <em>CREATE SRC feature destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__SRC__feature_destination()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__SRC__feature_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__feature_source <em>CONTEXT SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC feature source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__feature_source()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__process_destination <em>CONTEXT SRC process destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC process destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__process_destination()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__process_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__process_source <em>CONTEXT SRC process source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC process source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__process_source()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__process_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__SRC__ref_connection_destination <em>CREATE SRC ref connection destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__SRC__ref_connection_destination()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__SRC__ref_connection_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__SRC__ref_connection_source()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__SRC__ref_connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC system source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__system_source()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__system_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__thread_destination <em>CONTEXT SRC thread destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC thread destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__thread_destination()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__thread_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__thread_source <em>CONTEXT SRC thread source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC thread source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__SRC__thread_source()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__SRC__thread_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__data_destination <em>CREATE TRG data destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__data_destination()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__data_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__data_source <em>CREATE TRG data source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__data_source()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__data_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__dataaccess_destination <em>CREATE TRG dataaccess destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__dataaccess_destination()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__dataaccess_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__dataaccess_source <em>CREATE TRG dataaccess source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__dataaccess_source()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__dataaccess_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__feature_destination_target <em>CREATE TRG feature destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__feature_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__feature_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__feature_source_target <em>CONTEXT TRG feature source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG feature source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__feature_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__feature_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__process_destination_target <em>CONTEXT TRG process destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG process destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__process_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__process_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__process_source_target <em>CONTEXT TRG process source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG process source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__process_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__process_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__ref_destination <em>CREATE TRG ref destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG ref destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__ref_destination()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__ref_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__ref_source <em>CREATE TRG ref source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG ref source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__TRG__ref_source()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__TRG__ref_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG system target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__system_target()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__system_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__thread_destination_target <em>CONTEXT TRG thread destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG thread destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__thread_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__thread_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__thread_source_target <em>CONTEXT TRG thread source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG thread source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__TRG__thread_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__TRG__thread_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__feature2feature1 <em>CONTEXT CORR feature2feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR feature2feature1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__feature2feature1()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__feature2feature1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__feature2feature2 <em>CREATE CORR feature2feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR feature2feature2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__feature2feature2()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__feature2feature2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__p2a1()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__p2a1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__p2a2()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__p2a2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__p2d1 <em>CREATE CORR p2d1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__p2d1()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__p2d1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__p2d2 <em>CREATE CORR p2d2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__p2d2()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__p2d2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__process2process1 <em>CONTEXT CORR process2process1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR process2process1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__process2process1()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__process2process1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__process2process2 <em>CONTEXT CORR process2process2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR process2process2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__process2process2()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__process2process2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__r2r1 <em>CREATE CORR r2r1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__r2r1()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__r2r1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__r2r2 <em>CREATE CORR r2r2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCREATE__CORR__r2r2()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CREATE__CORR__r2r2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR s2s</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__s2s()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__s2s();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__thread2thread1 <em>CONTEXT CORR thread2thread1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR thread2thread1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__thread2thread1()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__thread2thread1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__thread2thread2 <em>CONTEXT CORR thread2thread2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR thread2thread2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem01__Marker#getCONTEXT__CORR__thread2thread2()
	 * @see #getPortConnectionToDataAccessAtSystem01__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem01__Marker_CONTEXT__CORR__thread2thread2();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker <em>Port Connection To Data Access At System10 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection To Data Access At System10 Marker</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker
	 * @generated
	 */
	EClass getPortConnectionToDataAccessAtSystem10__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__SRC__connection_source()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__feature_destination <em>CONTEXT SRC feature destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC feature destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__feature_destination()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__feature_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__SRC__feature_source <em>CREATE SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC feature source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__SRC__feature_source()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__process_destination <em>CONTEXT SRC process destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC process destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__process_destination()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__process_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__process_source <em>CONTEXT SRC process source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC process source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__process_source()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__process_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__SRC__ref_connection_destination <em>CREATE SRC ref connection destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__SRC__ref_connection_destination()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__SRC__ref_connection_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__SRC__ref_connection_source()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__SRC__ref_connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC system source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__system_source()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__system_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__thread_destination <em>CONTEXT SRC thread destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC thread destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__thread_destination()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__thread_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__thread_source <em>CONTEXT SRC thread source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC thread source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__SRC__thread_source()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__SRC__thread_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__data_destination <em>CREATE TRG data destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__data_destination()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__data_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__data_source <em>CREATE TRG data source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__data_source()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__data_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__dataaccess_destination <em>CREATE TRG dataaccess destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__dataaccess_destination()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__dataaccess_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__dataaccess_source <em>CREATE TRG dataaccess source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__dataaccess_source()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__dataaccess_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__feature_destination_target <em>CONTEXT TRG feature destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG feature destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__feature_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__feature_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__feature_source_target <em>CREATE TRG feature source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG feature source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__feature_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__feature_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__process_destination_target <em>CONTEXT TRG process destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG process destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__process_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__process_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__process_source_target <em>CONTEXT TRG process source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG process source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__process_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__process_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__ref_destination <em>CREATE TRG ref destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG ref destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__ref_destination()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__ref_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__ref_source <em>CREATE TRG ref source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG ref source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__TRG__ref_source()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__TRG__ref_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG system target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__system_target()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__system_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__thread_destination_target <em>CONTEXT TRG thread destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG thread destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__thread_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__thread_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__thread_source_target <em>CONTEXT TRG thread source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG thread source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__TRG__thread_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__TRG__thread_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__feature2feature1 <em>CREATE CORR feature2feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR feature2feature1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__feature2feature1()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__feature2feature1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__feature2feature2 <em>CONTEXT CORR feature2feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR feature2feature2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__feature2feature2()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__feature2feature2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__p2a1()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__p2a1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__p2a2()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__p2a2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__p2d1 <em>CREATE CORR p2d1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__p2d1()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__p2d1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__p2d2 <em>CREATE CORR p2d2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__p2d2()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__p2d2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__process2process1 <em>CONTEXT CORR process2process1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR process2process1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__process2process1()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__process2process1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__process2process2 <em>CONTEXT CORR process2process2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR process2process2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__process2process2()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__process2process2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__r2r1 <em>CREATE CORR r2r1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__r2r1()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__r2r1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__r2r2 <em>CREATE CORR r2r2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCREATE__CORR__r2r2()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CREATE__CORR__r2r2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR s2s</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__s2s()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__s2s();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__thread2thread1 <em>CONTEXT CORR thread2thread1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR thread2thread1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__thread2thread1()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__thread2thread1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__thread2thread2 <em>CONTEXT CORR thread2thread2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR thread2thread2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem10__Marker#getCONTEXT__CORR__thread2thread2()
	 * @see #getPortConnectionToDataAccessAtSystem10__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem10__Marker_CONTEXT__CORR__thread2thread2();

	/**
	 * Returns the meta object for class '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker <em>Port Connection To Data Access At System11 Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection To Data Access At System11 Marker</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker
	 * @generated
	 */
	EClass getPortConnectionToDataAccessAtSystem11__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__connection_source <em>CREATE SRC connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__connection_source()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__SRC__connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__feature_destination <em>CONTEXT SRC feature destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC feature destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__feature_destination()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__feature_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__feature_source <em>CONTEXT SRC feature source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC feature source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__feature_source()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__feature_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__process_destination <em>CONTEXT SRC process destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC process destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__process_destination()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__process_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__process_source <em>CONTEXT SRC process source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC process source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__process_source()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__process_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__ref_connection_destination <em>CREATE SRC ref connection destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__ref_connection_destination()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__SRC__ref_connection_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__ref_connection_source <em>CREATE SRC ref connection source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC ref connection source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__SRC__ref_connection_source()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__SRC__ref_connection_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__system_source <em>CONTEXT SRC system source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC system source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__system_source()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__system_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__thread_destination <em>CONTEXT SRC thread destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC thread destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__thread_destination()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__thread_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__thread_source <em>CONTEXT SRC thread source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT SRC thread source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__SRC__thread_source()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__SRC__thread_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__data_destination <em>CREATE TRG data destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__data_destination()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__data_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__data_source <em>CREATE TRG data source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG data source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__data_source()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__data_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__dataaccess_destination <em>CREATE TRG dataaccess destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__dataaccess_destination()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__dataaccess_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__dataaccess_source <em>CREATE TRG dataaccess source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG dataaccess source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__dataaccess_source()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__dataaccess_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__feature_destination_target <em>CONTEXT TRG feature destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG feature destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__feature_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__feature_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__feature_source_target <em>CONTEXT TRG feature source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG feature source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__feature_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__feature_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__process_destination_target <em>CONTEXT TRG process destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG process destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__process_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__process_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__process_source_target <em>CONTEXT TRG process source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG process source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__process_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__process_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__ref_destination <em>CREATE TRG ref destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG ref destination</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__ref_destination()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__ref_destination();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__ref_source <em>CREATE TRG ref source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG ref source</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__TRG__ref_source()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__TRG__ref_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__system_target <em>CONTEXT TRG system target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG system target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__system_target()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__system_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__thread_destination_target <em>CONTEXT TRG thread destination target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG thread destination target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__thread_destination_target()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__thread_destination_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__thread_source_target <em>CONTEXT TRG thread source target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT TRG thread source target</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__TRG__thread_source_target()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__TRG__thread_source_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__feature2feature1 <em>CONTEXT CORR feature2feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR feature2feature1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__feature2feature1()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__feature2feature1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__feature2feature2 <em>CONTEXT CORR feature2feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR feature2feature2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__feature2feature2()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__feature2feature2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2a1 <em>CREATE CORR p2a1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2a1()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2a1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2a2 <em>CREATE CORR p2a2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2a2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2a2()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2a2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2d1 <em>CREATE CORR p2d1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2d1()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2d1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2d2 <em>CREATE CORR p2d2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR p2d2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__p2d2()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__p2d2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__process2process1 <em>CONTEXT CORR process2process1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR process2process1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__process2process1()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__process2process1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__process2process2 <em>CONTEXT CORR process2process2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR process2process2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__process2process2()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__process2process2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__r2r1 <em>CREATE CORR r2r1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__r2r1()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__r2r1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__r2r2 <em>CREATE CORR r2r2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR r2r2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCREATE__CORR__r2r2()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CREATE__CORR__r2r2();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__s2s <em>CONTEXT CORR s2s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR s2s</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__s2s()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__s2s();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__thread2thread1 <em>CONTEXT CORR thread2thread1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR thread2thread1</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__thread2thread1()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__thread2thread1();

	/**
	 * Returns the meta object for the reference '{@link Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__thread2thread2 <em>CONTEXT CORR thread2thread2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CONTEXT CORR thread2thread2</em>'.
	 * @see Tgg.PortConnectionToDataAccessAtSystem11__Marker#getCONTEXT__CORR__thread2thread2()
	 * @see #getPortConnectionToDataAccessAtSystem11__Marker()
	 * @generated
	 */
	EReference getPortConnectionToDataAccessAtSystem11__Marker_CONTEXT__CORR__thread2thread2();

	/**
	 * Returns the meta object for class '{@link Tgg.SystemToSystem__Marker <em>System To System Marker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System To System Marker</em>'.
	 * @see Tgg.SystemToSystem__Marker
	 * @generated
	 */
	EClass getSystemToSystem__Marker();

	/**
	 * Returns the meta object for the reference '{@link Tgg.SystemToSystem__Marker#getCREATE__SRC__system_source <em>CREATE SRC system source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE SRC system source</em>'.
	 * @see Tgg.SystemToSystem__Marker#getCREATE__SRC__system_source()
	 * @see #getSystemToSystem__Marker()
	 * @generated
	 */
	EReference getSystemToSystem__Marker_CREATE__SRC__system_source();

	/**
	 * Returns the meta object for the reference '{@link Tgg.SystemToSystem__Marker#getCREATE__TRG__system_target <em>CREATE TRG system target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE TRG system target</em>'.
	 * @see Tgg.SystemToSystem__Marker#getCREATE__TRG__system_target()
	 * @see #getSystemToSystem__Marker()
	 * @generated
	 */
	EReference getSystemToSystem__Marker_CREATE__TRG__system_target();

	/**
	 * Returns the meta object for the reference '{@link Tgg.SystemToSystem__Marker#getCREATE__CORR__s2s <em>CREATE CORR s2s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>CREATE CORR s2s</em>'.
	 * @see Tgg.SystemToSystem__Marker#getCREATE__CORR__s2s()
	 * @see #getSystemToSystem__Marker()
	 * @generated
	 */
	EReference getSystemToSystem__Marker_CREATE__CORR__s2s();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TggFactory getTggFactory();

} //TggPackage
