# mt-bench-public

This project contains artifacts for the incremental model transformation tools benchmark, applied to MoTE, eMoflon, VIATRA and YAMTL.

This Benchmark contains three main projects for MoTE, eMoflon, VIATRA and YAMTL:
- _fr.tpt.mem4csd.mtbench.aadl2aadl.emoflon.tgg[.copying][.tests]_
- _fr.tpt.mem4csd.mtbench.aadl2aadl.mote[.copying][.tests]_
- _fr.tpt.mem4csd.mtbench.aadl2aadl.viatra[.tests]_
- _fr.tpt.mem4csd.mtbench.aadl2aadl.YAMTL_

With the following dependencies:
- _fr.tpt.mem4csd.mtbench.aadl2aadl.tests_  (with _fr.tpt.mem4csd.utils.compare_ and _fr.tpt.mem4csd.utils.compare.emf_).  
- _fr.tpt.mem4csd.mtbench.aadl2aadl.trace[.edit][.editor]_ (required for VIATRA project)
- _org.osate.aadl2_  (patch of OSATE that fixes a null pointer exception that is thrown when transforming OSATE instance models with the IMT tools)
- _fr.tpt.mem4csd.utils.osate.standalone_ (required to run OSATE in as a plain Java application (without the Eclipse platform and OSGI))

- _org.eclipse.xtext_ (required to avoid verbose logs)

The installation/execution instructions are included in [this file](https://gitlab.telecom-paris.fr/mbe-tools/mt-bench-public/-/blob/master/docs/install.md).

A brief background about AADL, OSATE, RAMSES, MoTE, eMoflon, VIATRA and YAMTL may be found [here](https://gitlab.telecom-paris.fr/mbe-tools/mt-bench-public/-/blob/master/docs/background.pdf) and the model transformation is briefly explained [here](https://gitlab.telecom-paris.fr/mbe-tools/mt-bench-public/-/blob/master/docs/specification.pdf).

The proposed test suite is presented [here](https://gitlab.telecom-paris.fr/mbe-tools/mt-bench-public/-/blob/master/docs/test-suite.pdf).


This benchmark is performed with the following tool versions:

- Eclipse: 2020-12 (4.18.0)
- eMoflon::IBeX (Democles): 1.0.0.202003161959 (updatesite: https://emoflon.org/emoflon-ibex-updatesite/snapshot/updatesite/)
- MoTE2 Development Tools	1.2.0.202102051638 (updatesite: https://www.hpi.uni-potsdam.de/giese/update-site/)
- VIATRA Query and Transformation SDK	2.5.0.202012091732 (updatesite: http://download.eclipse.org/viatra/updates/release/latest)
- YAMTL 0.2.1
- Open Source AADL Tool Environment	6.0.0.v20201013-1222 (updatesite: https://osate-build.sei.cmu.edu/download/osate/stable/2.9.0/updates/)


