/**
 * Generated from platform:/resource/fr.tpt.mem4csd.mtbench.aadl2aadl.viatra/src/fr/tpt/mem4csd/mtbench/aadl2aadl/viatra/copying/Copying_Required_queries.vql
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying;

import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTrace;
import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTraceSpec;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.InstanceObject;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         pattern
 *         is_in_trace_for_copy(aadl2aadlref : Aadl2AadlTraceSpec, trace : Aadl2AadlTrace, aadlElement : InstanceObject, aadlrefElement : InstanceObject) {
 *         	Aadl2AadlTraceSpec.traces(aadl2aadlref, trace);
 *         	Aadl2AadlTrace.leftInstance(trace, aadlElement);
 *         	Aadl2AadlTrace.rightInstance(trace, aadlrefElement);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Is_in_trace_for_copy extends BaseGeneratedEMFQuerySpecification<Is_in_trace_for_copy.Matcher> {
  /**
   * Pattern-specific match representation of the fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.is_in_trace_for_copy pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private Aadl2AadlTraceSpec fAadl2aadlref;
    
    private Aadl2AadlTrace fTrace;
    
    private InstanceObject fAadlElement;
    
    private InstanceObject fAadlrefElement;
    
    private static List<String> parameterNames = makeImmutableList("aadl2aadlref", "trace", "aadlElement", "aadlrefElement");
    
    private Match(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      this.fAadl2aadlref = pAadl2aadlref;
      this.fTrace = pTrace;
      this.fAadlElement = pAadlElement;
      this.fAadlrefElement = pAadlrefElement;
    }
    
    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "aadl2aadlref": return this.fAadl2aadlref;
          case "trace": return this.fTrace;
          case "aadlElement": return this.fAadlElement;
          case "aadlrefElement": return this.fAadlrefElement;
          default: return null;
      }
    }
    
    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fAadl2aadlref;
          case 1: return this.fTrace;
          case 2: return this.fAadlElement;
          case 3: return this.fAadlrefElement;
          default: return null;
      }
    }
    
    public Aadl2AadlTraceSpec getAadl2aadlref() {
      return this.fAadl2aadlref;
    }
    
    public Aadl2AadlTrace getTrace() {
      return this.fTrace;
    }
    
    public InstanceObject getAadlElement() {
      return this.fAadlElement;
    }
    
    public InstanceObject getAadlrefElement() {
      return this.fAadlrefElement;
    }
    
    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("aadl2aadlref".equals(parameterName) ) {
          this.fAadl2aadlref = (Aadl2AadlTraceSpec) newValue;
          return true;
      }
      if ("trace".equals(parameterName) ) {
          this.fTrace = (Aadl2AadlTrace) newValue;
          return true;
      }
      if ("aadlElement".equals(parameterName) ) {
          this.fAadlElement = (InstanceObject) newValue;
          return true;
      }
      if ("aadlrefElement".equals(parameterName) ) {
          this.fAadlrefElement = (InstanceObject) newValue;
          return true;
      }
      return false;
    }
    
    public void setAadl2aadlref(final Aadl2AadlTraceSpec pAadl2aadlref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fAadl2aadlref = pAadl2aadlref;
    }
    
    public void setTrace(final Aadl2AadlTrace pTrace) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fTrace = pTrace;
    }
    
    public void setAadlElement(final InstanceObject pAadlElement) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fAadlElement = pAadlElement;
    }
    
    public void setAadlrefElement(final InstanceObject pAadlrefElement) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fAadlrefElement = pAadlrefElement;
    }
    
    @Override
    public String patternName() {
      return "fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.is_in_trace_for_copy";
    }
    
    @Override
    public List<String> parameterNames() {
      return Is_in_trace_for_copy.Match.parameterNames;
    }
    
    @Override
    public Object[] toArray() {
      return new Object[]{fAadl2aadlref, fTrace, fAadlElement, fAadlrefElement};
    }
    
    @Override
    public Is_in_trace_for_copy.Match toImmutable() {
      return isMutable() ? newMatch(fAadl2aadlref, fTrace, fAadlElement, fAadlrefElement) : this;
    }
    
    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"aadl2aadlref\"=" + prettyPrintValue(fAadl2aadlref) + ", ");
      result.append("\"trace\"=" + prettyPrintValue(fTrace) + ", ");
      result.append("\"aadlElement\"=" + prettyPrintValue(fAadlElement) + ", ");
      result.append("\"aadlrefElement\"=" + prettyPrintValue(fAadlrefElement));
      return result.toString();
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(fAadl2aadlref, fTrace, fAadlElement, fAadlrefElement);
    }
    
    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Is_in_trace_for_copy.Match)) {
          Is_in_trace_for_copy.Match other = (Is_in_trace_for_copy.Match) obj;
          return Objects.equals(fAadl2aadlref, other.fAadl2aadlref) && Objects.equals(fTrace, other.fTrace) && Objects.equals(fAadlElement, other.fAadlElement) && Objects.equals(fAadlrefElement, other.fAadlrefElement);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }
    
    @Override
    public Is_in_trace_for_copy specification() {
      return Is_in_trace_for_copy.instance();
    }
    
    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Is_in_trace_for_copy.Match newEmptyMatch() {
      return new Mutable(null, null, null, null);
    }
    
    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pAadlElement the fixed value of pattern parameter aadlElement, or null if not bound.
     * @param pAadlrefElement the fixed value of pattern parameter aadlrefElement, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Is_in_trace_for_copy.Match newMutableMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return new Mutable(pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement);
    }
    
    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pAadlElement the fixed value of pattern parameter aadlElement, or null if not bound.
     * @param pAadlrefElement the fixed value of pattern parameter aadlrefElement, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Is_in_trace_for_copy.Match newMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return new Immutable(pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement);
    }
    
    private static final class Mutable extends Is_in_trace_for_copy.Match {
      Mutable(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
        super(pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement);
      }
      
      @Override
      public boolean isMutable() {
        return true;
      }
    }
    
    private static final class Immutable extends Is_in_trace_for_copy.Match {
      Immutable(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
        super(pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement);
      }
      
      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }
  
  /**
   * Generated pattern matcher API of the fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.is_in_trace_for_copy pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * pattern
   * is_in_trace_for_copy(aadl2aadlref : Aadl2AadlTraceSpec, trace : Aadl2AadlTrace, aadlElement : InstanceObject, aadlrefElement : InstanceObject) {
   * 	Aadl2AadlTraceSpec.traces(aadl2aadlref, trace);
   * 	Aadl2AadlTrace.leftInstance(trace, aadlElement);
   * 	Aadl2AadlTrace.rightInstance(trace, aadlrefElement);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Is_in_trace_for_copy
   * 
   */
  public static class Matcher extends BaseMatcher<Is_in_trace_for_copy.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Is_in_trace_for_copy.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }
    
    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Is_in_trace_for_copy.Matcher create() {
      return new Matcher();
    }
    
    private static final int POSITION_AADL2AADLREF = 0;
    
    private static final int POSITION_TRACE = 1;
    
    private static final int POSITION_AADLELEMENT = 2;
    
    private static final int POSITION_AADLREFELEMENT = 3;
    
    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Is_in_trace_for_copy.Matcher.class);
    
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }
    
    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pAadlElement the fixed value of pattern parameter aadlElement, or null if not bound.
     * @param pAadlrefElement the fixed value of pattern parameter aadlrefElement, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Is_in_trace_for_copy.Match> getAllMatches(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return rawStreamAllMatches(new Object[]{pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement}).collect(Collectors.toSet());
    }
    
    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pAadlElement the fixed value of pattern parameter aadlElement, or null if not bound.
     * @param pAadlrefElement the fixed value of pattern parameter aadlrefElement, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Is_in_trace_for_copy.Match> streamAllMatches(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return rawStreamAllMatches(new Object[]{pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement});
    }
    
    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pAadlElement the fixed value of pattern parameter aadlElement, or null if not bound.
     * @param pAadlrefElement the fixed value of pattern parameter aadlrefElement, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Is_in_trace_for_copy.Match> getOneArbitraryMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return rawGetOneArbitraryMatch(new Object[]{pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement});
    }
    
    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pAadlElement the fixed value of pattern parameter aadlElement, or null if not bound.
     * @param pAadlrefElement the fixed value of pattern parameter aadlrefElement, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return rawHasMatch(new Object[]{pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement});
    }
    
    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pAadlElement the fixed value of pattern parameter aadlElement, or null if not bound.
     * @param pAadlrefElement the fixed value of pattern parameter aadlrefElement, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return rawCountMatches(new Object[]{pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement});
    }
    
    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pAadlElement the fixed value of pattern parameter aadlElement, or null if not bound.
     * @param pAadlrefElement the fixed value of pattern parameter aadlrefElement, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement, final Consumer<? super Is_in_trace_for_copy.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement}, processor);
    }
    
    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pAadlElement the fixed value of pattern parameter aadlElement, or null if not bound.
     * @param pAadlrefElement the fixed value of pattern parameter aadlrefElement, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Is_in_trace_for_copy.Match newMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return Is_in_trace_for_copy.Match.newMatch(pAadl2aadlref, pTrace, pAadlElement, pAadlrefElement);
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Aadl2AadlTraceSpec> rawStreamAllValuesOfaadl2aadlref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_AADL2AADLREF, parameters).map(Aadl2AadlTraceSpec.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aadl2AadlTraceSpec> getAllValuesOfaadl2aadlref() {
      return rawStreamAllValuesOfaadl2aadlref(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Aadl2AadlTraceSpec> streamAllValuesOfaadl2aadlref() {
      return rawStreamAllValuesOfaadl2aadlref(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aadl2AadlTraceSpec> streamAllValuesOfaadl2aadlref(final Is_in_trace_for_copy.Match partialMatch) {
      return rawStreamAllValuesOfaadl2aadlref(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aadl2AadlTraceSpec> streamAllValuesOfaadl2aadlref(final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return rawStreamAllValuesOfaadl2aadlref(new Object[]{null, pTrace, pAadlElement, pAadlrefElement});
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aadl2AadlTraceSpec> getAllValuesOfaadl2aadlref(final Is_in_trace_for_copy.Match partialMatch) {
      return rawStreamAllValuesOfaadl2aadlref(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aadl2AadlTraceSpec> getAllValuesOfaadl2aadlref(final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return rawStreamAllValuesOfaadl2aadlref(new Object[]{null, pTrace, pAadlElement, pAadlrefElement}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Aadl2AadlTrace> rawStreamAllValuesOftrace(final Object[] parameters) {
      return rawStreamAllValues(POSITION_TRACE, parameters).map(Aadl2AadlTrace.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aadl2AadlTrace> getAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Aadl2AadlTrace> streamAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aadl2AadlTrace> streamAllValuesOftrace(final Is_in_trace_for_copy.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aadl2AadlTrace> streamAllValuesOftrace(final Aadl2AadlTraceSpec pAadl2aadlref, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return rawStreamAllValuesOftrace(new Object[]{pAadl2aadlref, null, pAadlElement, pAadlrefElement});
    }
    
    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aadl2AadlTrace> getAllValuesOftrace(final Is_in_trace_for_copy.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aadl2AadlTrace> getAllValuesOftrace(final Aadl2AadlTraceSpec pAadl2aadlref, final InstanceObject pAadlElement, final InstanceObject pAadlrefElement) {
      return rawStreamAllValuesOftrace(new Object[]{pAadl2aadlref, null, pAadlElement, pAadlrefElement}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlElement.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<InstanceObject> rawStreamAllValuesOfaadlElement(final Object[] parameters) {
      return rawStreamAllValues(POSITION_AADLELEMENT, parameters).map(InstanceObject.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlElement.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceObject> getAllValuesOfaadlElement() {
      return rawStreamAllValuesOfaadlElement(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlElement.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceObject> streamAllValuesOfaadlElement() {
      return rawStreamAllValuesOfaadlElement(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlElement.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceObject> streamAllValuesOfaadlElement(final Is_in_trace_for_copy.Match partialMatch) {
      return rawStreamAllValuesOfaadlElement(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlElement.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceObject> streamAllValuesOfaadlElement(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlrefElement) {
      return rawStreamAllValuesOfaadlElement(new Object[]{pAadl2aadlref, pTrace, null, pAadlrefElement});
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlElement.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceObject> getAllValuesOfaadlElement(final Is_in_trace_for_copy.Match partialMatch) {
      return rawStreamAllValuesOfaadlElement(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlElement.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceObject> getAllValuesOfaadlElement(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlrefElement) {
      return rawStreamAllValuesOfaadlElement(new Object[]{pAadl2aadlref, pTrace, null, pAadlrefElement}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlrefElement.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<InstanceObject> rawStreamAllValuesOfaadlrefElement(final Object[] parameters) {
      return rawStreamAllValues(POSITION_AADLREFELEMENT, parameters).map(InstanceObject.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlrefElement.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceObject> getAllValuesOfaadlrefElement() {
      return rawStreamAllValuesOfaadlrefElement(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlrefElement.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceObject> streamAllValuesOfaadlrefElement() {
      return rawStreamAllValuesOfaadlrefElement(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlrefElement.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceObject> streamAllValuesOfaadlrefElement(final Is_in_trace_for_copy.Match partialMatch) {
      return rawStreamAllValuesOfaadlrefElement(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlrefElement.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceObject> streamAllValuesOfaadlrefElement(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement) {
      return rawStreamAllValuesOfaadlrefElement(new Object[]{pAadl2aadlref, pTrace, pAadlElement, null});
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlrefElement.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceObject> getAllValuesOfaadlrefElement(final Is_in_trace_for_copy.Match partialMatch) {
      return rawStreamAllValuesOfaadlrefElement(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadlrefElement.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceObject> getAllValuesOfaadlrefElement(final Aadl2AadlTraceSpec pAadl2aadlref, final Aadl2AadlTrace pTrace, final InstanceObject pAadlElement) {
      return rawStreamAllValuesOfaadlrefElement(new Object[]{pAadl2aadlref, pTrace, pAadlElement, null}).collect(Collectors.toSet());
    }
    
    @Override
    protected Is_in_trace_for_copy.Match tupleToMatch(final Tuple t) {
      try {
          return Is_in_trace_for_copy.Match.newMatch((Aadl2AadlTraceSpec) t.get(POSITION_AADL2AADLREF), (Aadl2AadlTrace) t.get(POSITION_TRACE), (InstanceObject) t.get(POSITION_AADLELEMENT), (InstanceObject) t.get(POSITION_AADLREFELEMENT));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }
    
    @Override
    protected Is_in_trace_for_copy.Match arrayToMatch(final Object[] match) {
      try {
          return Is_in_trace_for_copy.Match.newMatch((Aadl2AadlTraceSpec) match[POSITION_AADL2AADLREF], (Aadl2AadlTrace) match[POSITION_TRACE], (InstanceObject) match[POSITION_AADLELEMENT], (InstanceObject) match[POSITION_AADLREFELEMENT]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }
    
    @Override
    protected Is_in_trace_for_copy.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Is_in_trace_for_copy.Match.newMutableMatch((Aadl2AadlTraceSpec) match[POSITION_AADL2AADLREF], (Aadl2AadlTrace) match[POSITION_TRACE], (InstanceObject) match[POSITION_AADLELEMENT], (InstanceObject) match[POSITION_AADLREFELEMENT]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }
    
    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Is_in_trace_for_copy.Matcher> querySpecification() {
      return Is_in_trace_for_copy.instance();
    }
  }
  
  private Is_in_trace_for_copy() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Is_in_trace_for_copy instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }
  
  @Override
  protected Is_in_trace_for_copy.Matcher instantiate(final ViatraQueryEngine engine) {
    return Is_in_trace_for_copy.Matcher.on(engine);
  }
  
  @Override
  public Is_in_trace_for_copy.Matcher instantiate() {
    return Is_in_trace_for_copy.Matcher.create();
  }
  
  @Override
  public Is_in_trace_for_copy.Match newEmptyMatch() {
    return Is_in_trace_for_copy.Match.newEmptyMatch();
  }
  
  @Override
  public Is_in_trace_for_copy.Match newMatch(final Object... parameters) {
    return Is_in_trace_for_copy.Match.newMatch((fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTraceSpec) parameters[0], (fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTrace) parameters[1], (org.osate.aadl2.instance.InstanceObject) parameters[2], (org.osate.aadl2.instance.InstanceObject) parameters[3]);
  }
  
  /**
   * Inner class allowing the singleton instance of {@link Is_in_trace_for_copy} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Is_in_trace_for_copy#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Is_in_trace_for_copy INSTANCE = new Is_in_trace_for_copy();
    
    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();
    
    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Is_in_trace_for_copy.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    private final PParameter parameter_aadl2aadlref = new PParameter("aadl2aadlref", "fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTraceSpec", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTraceSpec")), PParameterDirection.INOUT);
    
    private final PParameter parameter_trace = new PParameter("trace", "fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTrace", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTrace")), PParameterDirection.INOUT);
    
    private final PParameter parameter_aadlElement = new PParameter("aadlElement", "org.osate.aadl2.instance.InstanceObject", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "InstanceObject")), PParameterDirection.INOUT);
    
    private final PParameter parameter_aadlrefElement = new PParameter("aadlrefElement", "org.osate.aadl2.instance.InstanceObject", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "InstanceObject")), PParameterDirection.INOUT);
    
    private final List<PParameter> parameters = Arrays.asList(parameter_aadl2aadlref, parameter_trace, parameter_aadlElement, parameter_aadlrefElement);
    
    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }
    
    @Override
    public String getFullyQualifiedName() {
      return "fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.is_in_trace_for_copy";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("aadl2aadlref","trace","aadlElement","aadlrefElement");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_aadl2aadlref = body.getOrCreateVariableByName("aadl2aadlref");
          PVariable var_trace = body.getOrCreateVariableByName("trace");
          PVariable var_aadlElement = body.getOrCreateVariableByName("aadlElement");
          PVariable var_aadlrefElement = body.getOrCreateVariableByName("aadlrefElement");
          new TypeConstraint(body, Tuples.flatTupleOf(var_aadl2aadlref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTraceSpec")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTrace")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_aadlElement), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "InstanceObject")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_aadlrefElement), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "InstanceObject")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_aadl2aadlref, parameter_aadl2aadlref),
             new ExportedParameter(body, var_trace, parameter_trace),
             new ExportedParameter(body, var_aadlElement, parameter_aadlElement),
             new ExportedParameter(body, var_aadlrefElement, parameter_aadlrefElement)
          ));
          // 	Aadl2AadlTraceSpec.traces(aadl2aadlref, trace)
          new TypeConstraint(body, Tuples.flatTupleOf(var_aadl2aadlref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTraceSpec")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_aadl2aadlref, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTraceSpec", "traces")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTrace")));
          new Equality(body, var__virtual_0_, var_trace);
          // 	Aadl2AadlTrace.leftInstance(trace, aadlElement)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTrace")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTrace", "leftInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "InstanceObject")));
          new Equality(body, var__virtual_1_, var_aadlElement);
          // 	Aadl2AadlTrace.rightInstance(trace, aadlrefElement)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTrace")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTrace", "rightInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "InstanceObject")));
          new Equality(body, var__virtual_2_, var_aadlrefElement);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
