/**
 * Generated from platform:/resource/fr.tpt.mem4csd.mtbench.aadl2aadl.viatra/src/fr/tpt/mem4csd/mtbench/aadl2aadl/viatra/copying/Copying_Required_queries.vql
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying;

import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Is_in_trace_for_copy;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionInstanceEnd;
import org.osate.aadl2.instance.ConnectionReference;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         pattern
 *         copy_connectionref(connection : ConnectionInstance, connectionref : ConnectionInstance, contextR : ComponentInstance, contextRref : ComponentInstance, source : ConnectionInstanceEnd, destination : ConnectionInstanceEnd, sourceref : ConnectionInstanceEnd, destinationref : ConnectionInstanceEnd, connectionR : ConnectionReference) {
 *         	find is_in_trace_for_copy(_, _, contextR, contextRref);
 *         	find is_in_trace_for_copy(_, _, source, sourceref);
 *         	find is_in_trace_for_copy(_, _, destination, destinationref);
 *         	find is_in_trace_for_copy(_, _, connection, connectionref);
 *         
 *         	ConnectionInstance.connectionReference(connection, connectionR);
 *         	ConnectionReference.source(connectionR, source);
 *         	ConnectionReference.destination(connectionR, destination);
 *         	ConnectionReference.context(connectionR, contextR);
 *         
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Copy_connectionref extends BaseGeneratedEMFQuerySpecification<Copy_connectionref.Matcher> {
  /**
   * Pattern-specific match representation of the fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_connectionref pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ConnectionInstance fConnection;
    
    private ConnectionInstance fConnectionref;
    
    private ComponentInstance fContextR;
    
    private ComponentInstance fContextRref;
    
    private ConnectionInstanceEnd fSource;
    
    private ConnectionInstanceEnd fDestination;
    
    private ConnectionInstanceEnd fSourceref;
    
    private ConnectionInstanceEnd fDestinationref;
    
    private ConnectionReference fConnectionR;
    
    private static List<String> parameterNames = makeImmutableList("connection", "connectionref", "contextR", "contextRref", "source", "destination", "sourceref", "destinationref", "connectionR");
    
    private Match(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      this.fConnection = pConnection;
      this.fConnectionref = pConnectionref;
      this.fContextR = pContextR;
      this.fContextRref = pContextRref;
      this.fSource = pSource;
      this.fDestination = pDestination;
      this.fSourceref = pSourceref;
      this.fDestinationref = pDestinationref;
      this.fConnectionR = pConnectionR;
    }
    
    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "connection": return this.fConnection;
          case "connectionref": return this.fConnectionref;
          case "contextR": return this.fContextR;
          case "contextRref": return this.fContextRref;
          case "source": return this.fSource;
          case "destination": return this.fDestination;
          case "sourceref": return this.fSourceref;
          case "destinationref": return this.fDestinationref;
          case "connectionR": return this.fConnectionR;
          default: return null;
      }
    }
    
    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fConnection;
          case 1: return this.fConnectionref;
          case 2: return this.fContextR;
          case 3: return this.fContextRref;
          case 4: return this.fSource;
          case 5: return this.fDestination;
          case 6: return this.fSourceref;
          case 7: return this.fDestinationref;
          case 8: return this.fConnectionR;
          default: return null;
      }
    }
    
    public ConnectionInstance getConnection() {
      return this.fConnection;
    }
    
    public ConnectionInstance getConnectionref() {
      return this.fConnectionref;
    }
    
    public ComponentInstance getContextR() {
      return this.fContextR;
    }
    
    public ComponentInstance getContextRref() {
      return this.fContextRref;
    }
    
    public ConnectionInstanceEnd getSource() {
      return this.fSource;
    }
    
    public ConnectionInstanceEnd getDestination() {
      return this.fDestination;
    }
    
    public ConnectionInstanceEnd getSourceref() {
      return this.fSourceref;
    }
    
    public ConnectionInstanceEnd getDestinationref() {
      return this.fDestinationref;
    }
    
    public ConnectionReference getConnectionR() {
      return this.fConnectionR;
    }
    
    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("connection".equals(parameterName) ) {
          this.fConnection = (ConnectionInstance) newValue;
          return true;
      }
      if ("connectionref".equals(parameterName) ) {
          this.fConnectionref = (ConnectionInstance) newValue;
          return true;
      }
      if ("contextR".equals(parameterName) ) {
          this.fContextR = (ComponentInstance) newValue;
          return true;
      }
      if ("contextRref".equals(parameterName) ) {
          this.fContextRref = (ComponentInstance) newValue;
          return true;
      }
      if ("source".equals(parameterName) ) {
          this.fSource = (ConnectionInstanceEnd) newValue;
          return true;
      }
      if ("destination".equals(parameterName) ) {
          this.fDestination = (ConnectionInstanceEnd) newValue;
          return true;
      }
      if ("sourceref".equals(parameterName) ) {
          this.fSourceref = (ConnectionInstanceEnd) newValue;
          return true;
      }
      if ("destinationref".equals(parameterName) ) {
          this.fDestinationref = (ConnectionInstanceEnd) newValue;
          return true;
      }
      if ("connectionR".equals(parameterName) ) {
          this.fConnectionR = (ConnectionReference) newValue;
          return true;
      }
      return false;
    }
    
    public void setConnection(final ConnectionInstance pConnection) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fConnection = pConnection;
    }
    
    public void setConnectionref(final ConnectionInstance pConnectionref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fConnectionref = pConnectionref;
    }
    
    public void setContextR(final ComponentInstance pContextR) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fContextR = pContextR;
    }
    
    public void setContextRref(final ComponentInstance pContextRref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fContextRref = pContextRref;
    }
    
    public void setSource(final ConnectionInstanceEnd pSource) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSource = pSource;
    }
    
    public void setDestination(final ConnectionInstanceEnd pDestination) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fDestination = pDestination;
    }
    
    public void setSourceref(final ConnectionInstanceEnd pSourceref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSourceref = pSourceref;
    }
    
    public void setDestinationref(final ConnectionInstanceEnd pDestinationref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fDestinationref = pDestinationref;
    }
    
    public void setConnectionR(final ConnectionReference pConnectionR) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fConnectionR = pConnectionR;
    }
    
    @Override
    public String patternName() {
      return "fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_connectionref";
    }
    
    @Override
    public List<String> parameterNames() {
      return Copy_connectionref.Match.parameterNames;
    }
    
    @Override
    public Object[] toArray() {
      return new Object[]{fConnection, fConnectionref, fContextR, fContextRref, fSource, fDestination, fSourceref, fDestinationref, fConnectionR};
    }
    
    @Override
    public Copy_connectionref.Match toImmutable() {
      return isMutable() ? newMatch(fConnection, fConnectionref, fContextR, fContextRref, fSource, fDestination, fSourceref, fDestinationref, fConnectionR) : this;
    }
    
    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"connection\"=" + prettyPrintValue(fConnection) + ", ");
      result.append("\"connectionref\"=" + prettyPrintValue(fConnectionref) + ", ");
      result.append("\"contextR\"=" + prettyPrintValue(fContextR) + ", ");
      result.append("\"contextRref\"=" + prettyPrintValue(fContextRref) + ", ");
      result.append("\"source\"=" + prettyPrintValue(fSource) + ", ");
      result.append("\"destination\"=" + prettyPrintValue(fDestination) + ", ");
      result.append("\"sourceref\"=" + prettyPrintValue(fSourceref) + ", ");
      result.append("\"destinationref\"=" + prettyPrintValue(fDestinationref) + ", ");
      result.append("\"connectionR\"=" + prettyPrintValue(fConnectionR));
      return result.toString();
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(fConnection, fConnectionref, fContextR, fContextRref, fSource, fDestination, fSourceref, fDestinationref, fConnectionR);
    }
    
    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Copy_connectionref.Match)) {
          Copy_connectionref.Match other = (Copy_connectionref.Match) obj;
          return Objects.equals(fConnection, other.fConnection) && Objects.equals(fConnectionref, other.fConnectionref) && Objects.equals(fContextR, other.fContextR) && Objects.equals(fContextRref, other.fContextRref) && Objects.equals(fSource, other.fSource) && Objects.equals(fDestination, other.fDestination) && Objects.equals(fSourceref, other.fSourceref) && Objects.equals(fDestinationref, other.fDestinationref) && Objects.equals(fConnectionR, other.fConnectionR);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }
    
    @Override
    public Copy_connectionref specification() {
      return Copy_connectionref.instance();
    }
    
    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Copy_connectionref.Match newEmptyMatch() {
      return new Mutable(null, null, null, null, null, null, null, null, null);
    }
    
    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @param pConnectionref the fixed value of pattern parameter connectionref, or null if not bound.
     * @param pContextR the fixed value of pattern parameter contextR, or null if not bound.
     * @param pContextRref the fixed value of pattern parameter contextRref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnectionR the fixed value of pattern parameter connectionR, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Copy_connectionref.Match newMutableMatch(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return new Mutable(pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR);
    }
    
    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @param pConnectionref the fixed value of pattern parameter connectionref, or null if not bound.
     * @param pContextR the fixed value of pattern parameter contextR, or null if not bound.
     * @param pContextRref the fixed value of pattern parameter contextRref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnectionR the fixed value of pattern parameter connectionR, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Copy_connectionref.Match newMatch(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return new Immutable(pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR);
    }
    
    private static final class Mutable extends Copy_connectionref.Match {
      Mutable(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
        super(pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR);
      }
      
      @Override
      public boolean isMutable() {
        return true;
      }
    }
    
    private static final class Immutable extends Copy_connectionref.Match {
      Immutable(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
        super(pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR);
      }
      
      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }
  
  /**
   * Generated pattern matcher API of the fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_connectionref pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * pattern
   * copy_connectionref(connection : ConnectionInstance, connectionref : ConnectionInstance, contextR : ComponentInstance, contextRref : ComponentInstance, source : ConnectionInstanceEnd, destination : ConnectionInstanceEnd, sourceref : ConnectionInstanceEnd, destinationref : ConnectionInstanceEnd, connectionR : ConnectionReference) {
   * 	find is_in_trace_for_copy(_, _, contextR, contextRref);
   * 	find is_in_trace_for_copy(_, _, source, sourceref);
   * 	find is_in_trace_for_copy(_, _, destination, destinationref);
   * 	find is_in_trace_for_copy(_, _, connection, connectionref);
   * 
   * 	ConnectionInstance.connectionReference(connection, connectionR);
   * 	ConnectionReference.source(connectionR, source);
   * 	ConnectionReference.destination(connectionR, destination);
   * 	ConnectionReference.context(connectionR, contextR);
   * 
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Copy_connectionref
   * 
   */
  public static class Matcher extends BaseMatcher<Copy_connectionref.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Copy_connectionref.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }
    
    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Copy_connectionref.Matcher create() {
      return new Matcher();
    }
    
    private static final int POSITION_CONNECTION = 0;
    
    private static final int POSITION_CONNECTIONREF = 1;
    
    private static final int POSITION_CONTEXTR = 2;
    
    private static final int POSITION_CONTEXTRREF = 3;
    
    private static final int POSITION_SOURCE = 4;
    
    private static final int POSITION_DESTINATION = 5;
    
    private static final int POSITION_SOURCEREF = 6;
    
    private static final int POSITION_DESTINATIONREF = 7;
    
    private static final int POSITION_CONNECTIONR = 8;
    
    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Copy_connectionref.Matcher.class);
    
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }
    
    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @param pConnectionref the fixed value of pattern parameter connectionref, or null if not bound.
     * @param pContextR the fixed value of pattern parameter contextR, or null if not bound.
     * @param pContextRref the fixed value of pattern parameter contextRref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnectionR the fixed value of pattern parameter connectionR, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Copy_connectionref.Match> getAllMatches(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllMatches(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR}).collect(Collectors.toSet());
    }
    
    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @param pConnectionref the fixed value of pattern parameter connectionref, or null if not bound.
     * @param pContextR the fixed value of pattern parameter contextR, or null if not bound.
     * @param pContextRref the fixed value of pattern parameter contextRref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnectionR the fixed value of pattern parameter connectionR, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Copy_connectionref.Match> streamAllMatches(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllMatches(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR});
    }
    
    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @param pConnectionref the fixed value of pattern parameter connectionref, or null if not bound.
     * @param pContextR the fixed value of pattern parameter contextR, or null if not bound.
     * @param pContextRref the fixed value of pattern parameter contextRref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnectionR the fixed value of pattern parameter connectionR, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Copy_connectionref.Match> getOneArbitraryMatch(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawGetOneArbitraryMatch(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR});
    }
    
    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @param pConnectionref the fixed value of pattern parameter connectionref, or null if not bound.
     * @param pContextR the fixed value of pattern parameter contextR, or null if not bound.
     * @param pContextRref the fixed value of pattern parameter contextRref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnectionR the fixed value of pattern parameter connectionR, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawHasMatch(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR});
    }
    
    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @param pConnectionref the fixed value of pattern parameter connectionref, or null if not bound.
     * @param pContextR the fixed value of pattern parameter contextR, or null if not bound.
     * @param pContextRref the fixed value of pattern parameter contextRref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnectionR the fixed value of pattern parameter connectionR, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawCountMatches(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR});
    }
    
    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @param pConnectionref the fixed value of pattern parameter connectionref, or null if not bound.
     * @param pContextR the fixed value of pattern parameter contextR, or null if not bound.
     * @param pContextRref the fixed value of pattern parameter contextRref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnectionR the fixed value of pattern parameter connectionR, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR, final Consumer<? super Copy_connectionref.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR}, processor);
    }
    
    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @param pConnectionref the fixed value of pattern parameter connectionref, or null if not bound.
     * @param pContextR the fixed value of pattern parameter contextR, or null if not bound.
     * @param pContextRref the fixed value of pattern parameter contextRref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnectionR the fixed value of pattern parameter connectionR, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Copy_connectionref.Match newMatch(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return Copy_connectionref.Match.newMatch(pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR);
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstance> rawStreamAllValuesOfconnection(final Object[] parameters) {
      return rawStreamAllValues(POSITION_CONNECTION, parameters).map(ConnectionInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconnection() {
      return rawStreamAllValuesOfconnection(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconnection() {
      return rawStreamAllValuesOfconnection(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconnection(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfconnection(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconnection(final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfconnection(new Object[]{null, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR});
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconnection(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfconnection(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconnection(final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfconnection(new Object[]{null, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstance> rawStreamAllValuesOfconnectionref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_CONNECTIONREF, parameters).map(ConnectionInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconnectionref() {
      return rawStreamAllValuesOfconnectionref(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconnectionref() {
      return rawStreamAllValuesOfconnectionref(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconnectionref(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfconnectionref(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconnectionref(final ConnectionInstance pConnection, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfconnectionref(new Object[]{pConnection, null, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR});
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconnectionref(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfconnectionref(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconnectionref(final ConnectionInstance pConnection, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfconnectionref(new Object[]{pConnection, null, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextR.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcontextR(final Object[] parameters) {
      return rawStreamAllValues(POSITION_CONTEXTR, parameters).map(ComponentInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextR.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcontextR() {
      return rawStreamAllValuesOfcontextR(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextR.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcontextR() {
      return rawStreamAllValuesOfcontextR(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextR.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcontextR(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfcontextR(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextR.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcontextR(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfcontextR(new Object[]{pConnection, pConnectionref, null, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR});
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextR.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcontextR(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfcontextR(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextR.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcontextR(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfcontextR(new Object[]{pConnection, pConnectionref, null, pContextRref, pSource, pDestination, pSourceref, pDestinationref, pConnectionR}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextRref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcontextRref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_CONTEXTRREF, parameters).map(ComponentInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextRref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcontextRref() {
      return rawStreamAllValuesOfcontextRref(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextRref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcontextRref() {
      return rawStreamAllValuesOfcontextRref(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextRref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcontextRref(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfcontextRref(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextRref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcontextRref(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfcontextRref(new Object[]{pConnection, pConnectionref, pContextR, null, pSource, pDestination, pSourceref, pDestinationref, pConnectionR});
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextRref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcontextRref(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfcontextRref(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for contextRref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcontextRref(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfcontextRref(new Object[]{pConnection, pConnectionref, pContextR, null, pSource, pDestination, pSourceref, pDestinationref, pConnectionR}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstanceEnd> rawStreamAllValuesOfsource(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SOURCE, parameters).map(ConnectionInstanceEnd.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsource() {
      return rawStreamAllValuesOfsource(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsource() {
      return rawStreamAllValuesOfsource(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsource(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfsource(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsource(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfsource(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, null, pDestination, pSourceref, pDestinationref, pConnectionR});
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsource(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfsource(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsource(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfsource(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, null, pDestination, pSourceref, pDestinationref, pConnectionR}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstanceEnd> rawStreamAllValuesOfdestination(final Object[] parameters) {
      return rawStreamAllValues(POSITION_DESTINATION, parameters).map(ConnectionInstanceEnd.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestination() {
      return rawStreamAllValuesOfdestination(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestination() {
      return rawStreamAllValuesOfdestination(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestination(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfdestination(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestination(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfdestination(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, null, pSourceref, pDestinationref, pConnectionR});
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestination(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfdestination(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestination(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfdestination(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, null, pSourceref, pDestinationref, pConnectionR}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstanceEnd> rawStreamAllValuesOfsourceref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SOURCEREF, parameters).map(ConnectionInstanceEnd.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsourceref() {
      return rawStreamAllValuesOfsourceref(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsourceref() {
      return rawStreamAllValuesOfsourceref(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsourceref(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfsourceref(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsourceref(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfsourceref(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, null, pDestinationref, pConnectionR});
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsourceref(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfsourceref(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsourceref(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pDestinationref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfsourceref(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, null, pDestinationref, pConnectionR}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstanceEnd> rawStreamAllValuesOfdestinationref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_DESTINATIONREF, parameters).map(ConnectionInstanceEnd.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestinationref() {
      return rawStreamAllValuesOfdestinationref(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestinationref() {
      return rawStreamAllValuesOfdestinationref(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestinationref(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfdestinationref(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestinationref(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfdestinationref(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, null, pConnectionR});
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestinationref(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfdestinationref(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestinationref(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionReference pConnectionR) {
      return rawStreamAllValuesOfdestinationref(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, null, pConnectionR}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionR.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionReference> rawStreamAllValuesOfconnectionR(final Object[] parameters) {
      return rawStreamAllValues(POSITION_CONNECTIONR, parameters).map(ConnectionReference.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionR.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionReference> getAllValuesOfconnectionR() {
      return rawStreamAllValuesOfconnectionR(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionR.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionReference> streamAllValuesOfconnectionR() {
      return rawStreamAllValuesOfconnectionR(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionR.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionReference> streamAllValuesOfconnectionR(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfconnectionR(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionR.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionReference> streamAllValuesOfconnectionR(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref) {
      return rawStreamAllValuesOfconnectionR(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, null});
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionR.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionReference> getAllValuesOfconnectionR(final Copy_connectionref.Match partialMatch) {
      return rawStreamAllValuesOfconnectionR(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connectionR.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionReference> getAllValuesOfconnectionR(final ConnectionInstance pConnection, final ConnectionInstance pConnectionref, final ComponentInstance pContextR, final ComponentInstance pContextRref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref) {
      return rawStreamAllValuesOfconnectionR(new Object[]{pConnection, pConnectionref, pContextR, pContextRref, pSource, pDestination, pSourceref, pDestinationref, null}).collect(Collectors.toSet());
    }
    
    @Override
    protected Copy_connectionref.Match tupleToMatch(final Tuple t) {
      try {
          return Copy_connectionref.Match.newMatch((ConnectionInstance) t.get(POSITION_CONNECTION), (ConnectionInstance) t.get(POSITION_CONNECTIONREF), (ComponentInstance) t.get(POSITION_CONTEXTR), (ComponentInstance) t.get(POSITION_CONTEXTRREF), (ConnectionInstanceEnd) t.get(POSITION_SOURCE), (ConnectionInstanceEnd) t.get(POSITION_DESTINATION), (ConnectionInstanceEnd) t.get(POSITION_SOURCEREF), (ConnectionInstanceEnd) t.get(POSITION_DESTINATIONREF), (ConnectionReference) t.get(POSITION_CONNECTIONR));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }
    
    @Override
    protected Copy_connectionref.Match arrayToMatch(final Object[] match) {
      try {
          return Copy_connectionref.Match.newMatch((ConnectionInstance) match[POSITION_CONNECTION], (ConnectionInstance) match[POSITION_CONNECTIONREF], (ComponentInstance) match[POSITION_CONTEXTR], (ComponentInstance) match[POSITION_CONTEXTRREF], (ConnectionInstanceEnd) match[POSITION_SOURCE], (ConnectionInstanceEnd) match[POSITION_DESTINATION], (ConnectionInstanceEnd) match[POSITION_SOURCEREF], (ConnectionInstanceEnd) match[POSITION_DESTINATIONREF], (ConnectionReference) match[POSITION_CONNECTIONR]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }
    
    @Override
    protected Copy_connectionref.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Copy_connectionref.Match.newMutableMatch((ConnectionInstance) match[POSITION_CONNECTION], (ConnectionInstance) match[POSITION_CONNECTIONREF], (ComponentInstance) match[POSITION_CONTEXTR], (ComponentInstance) match[POSITION_CONTEXTRREF], (ConnectionInstanceEnd) match[POSITION_SOURCE], (ConnectionInstanceEnd) match[POSITION_DESTINATION], (ConnectionInstanceEnd) match[POSITION_SOURCEREF], (ConnectionInstanceEnd) match[POSITION_DESTINATIONREF], (ConnectionReference) match[POSITION_CONNECTIONR]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }
    
    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Copy_connectionref.Matcher> querySpecification() {
      return Copy_connectionref.instance();
    }
  }
  
  private Copy_connectionref() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Copy_connectionref instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }
  
  @Override
  protected Copy_connectionref.Matcher instantiate(final ViatraQueryEngine engine) {
    return Copy_connectionref.Matcher.on(engine);
  }
  
  @Override
  public Copy_connectionref.Matcher instantiate() {
    return Copy_connectionref.Matcher.create();
  }
  
  @Override
  public Copy_connectionref.Match newEmptyMatch() {
    return Copy_connectionref.Match.newEmptyMatch();
  }
  
  @Override
  public Copy_connectionref.Match newMatch(final Object... parameters) {
    return Copy_connectionref.Match.newMatch((org.osate.aadl2.instance.ConnectionInstance) parameters[0], (org.osate.aadl2.instance.ConnectionInstance) parameters[1], (org.osate.aadl2.instance.ComponentInstance) parameters[2], (org.osate.aadl2.instance.ComponentInstance) parameters[3], (org.osate.aadl2.instance.ConnectionInstanceEnd) parameters[4], (org.osate.aadl2.instance.ConnectionInstanceEnd) parameters[5], (org.osate.aadl2.instance.ConnectionInstanceEnd) parameters[6], (org.osate.aadl2.instance.ConnectionInstanceEnd) parameters[7], (org.osate.aadl2.instance.ConnectionReference) parameters[8]);
  }
  
  /**
   * Inner class allowing the singleton instance of {@link Copy_connectionref} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Copy_connectionref#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Copy_connectionref INSTANCE = new Copy_connectionref();
    
    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();
    
    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Copy_connectionref.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    private final PParameter parameter_connection = new PParameter("connection", "org.osate.aadl2.instance.ConnectionInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")), PParameterDirection.INOUT);
    
    private final PParameter parameter_connectionref = new PParameter("connectionref", "org.osate.aadl2.instance.ConnectionInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")), PParameterDirection.INOUT);
    
    private final PParameter parameter_contextR = new PParameter("contextR", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);
    
    private final PParameter parameter_contextRref = new PParameter("contextRref", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);
    
    private final PParameter parameter_source = new PParameter("source", "org.osate.aadl2.instance.ConnectionInstanceEnd", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")), PParameterDirection.INOUT);
    
    private final PParameter parameter_destination = new PParameter("destination", "org.osate.aadl2.instance.ConnectionInstanceEnd", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")), PParameterDirection.INOUT);
    
    private final PParameter parameter_sourceref = new PParameter("sourceref", "org.osate.aadl2.instance.ConnectionInstanceEnd", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")), PParameterDirection.INOUT);
    
    private final PParameter parameter_destinationref = new PParameter("destinationref", "org.osate.aadl2.instance.ConnectionInstanceEnd", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")), PParameterDirection.INOUT);
    
    private final PParameter parameter_connectionR = new PParameter("connectionR", "org.osate.aadl2.instance.ConnectionReference", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionReference")), PParameterDirection.INOUT);
    
    private final List<PParameter> parameters = Arrays.asList(parameter_connection, parameter_connectionref, parameter_contextR, parameter_contextRref, parameter_source, parameter_destination, parameter_sourceref, parameter_destinationref, parameter_connectionR);
    
    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }
    
    @Override
    public String getFullyQualifiedName() {
      return "fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_connectionref";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("connection","connectionref","contextR","contextRref","source","destination","sourceref","destinationref","connectionR");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_connection = body.getOrCreateVariableByName("connection");
          PVariable var_connectionref = body.getOrCreateVariableByName("connectionref");
          PVariable var_contextR = body.getOrCreateVariableByName("contextR");
          PVariable var_contextRref = body.getOrCreateVariableByName("contextRref");
          PVariable var_source = body.getOrCreateVariableByName("source");
          PVariable var_destination = body.getOrCreateVariableByName("destination");
          PVariable var_sourceref = body.getOrCreateVariableByName("sourceref");
          PVariable var_destinationref = body.getOrCreateVariableByName("destinationref");
          PVariable var_connectionR = body.getOrCreateVariableByName("connectionR");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          PVariable var___1_ = body.getOrCreateVariableByName("_<1>");
          PVariable var___2_ = body.getOrCreateVariableByName("_<2>");
          PVariable var___3_ = body.getOrCreateVariableByName("_<3>");
          PVariable var___4_ = body.getOrCreateVariableByName("_<4>");
          PVariable var___5_ = body.getOrCreateVariableByName("_<5>");
          PVariable var___6_ = body.getOrCreateVariableByName("_<6>");
          PVariable var___7_ = body.getOrCreateVariableByName("_<7>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_connection), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_connectionref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_contextR), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_contextRref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_source), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_destination), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_sourceref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_destinationref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_connectionR), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_connection, parameter_connection),
             new ExportedParameter(body, var_connectionref, parameter_connectionref),
             new ExportedParameter(body, var_contextR, parameter_contextR),
             new ExportedParameter(body, var_contextRref, parameter_contextRref),
             new ExportedParameter(body, var_source, parameter_source),
             new ExportedParameter(body, var_destination, parameter_destination),
             new ExportedParameter(body, var_sourceref, parameter_sourceref),
             new ExportedParameter(body, var_destinationref, parameter_destinationref),
             new ExportedParameter(body, var_connectionR, parameter_connectionR)
          ));
          // 	find is_in_trace_for_copy(_, _, contextR, contextRref)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___0_, var___1_, var_contextR, var_contextRref), Is_in_trace_for_copy.instance().getInternalQueryRepresentation());
          // 	find is_in_trace_for_copy(_, _, source, sourceref)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___2_, var___3_, var_source, var_sourceref), Is_in_trace_for_copy.instance().getInternalQueryRepresentation());
          // 	find is_in_trace_for_copy(_, _, destination, destinationref)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___4_, var___5_, var_destination, var_destinationref), Is_in_trace_for_copy.instance().getInternalQueryRepresentation());
          // 	find is_in_trace_for_copy(_, _, connection, connectionref)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___6_, var___7_, var_connection, var_connectionref), Is_in_trace_for_copy.instance().getInternalQueryRepresentation());
          // 	ConnectionInstance.connectionReference(connection, connectionR)
          new TypeConstraint(body, Tuples.flatTupleOf(var_connection), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_connection, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance", "connectionReference")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference")));
          new Equality(body, var__virtual_0_, var_connectionR);
          // 	ConnectionReference.source(connectionR, source)
          new TypeConstraint(body, Tuples.flatTupleOf(var_connectionR), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_connectionR, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new Equality(body, var__virtual_1_, var_source);
          // 	ConnectionReference.destination(connectionR, destination)
          new TypeConstraint(body, Tuples.flatTupleOf(var_connectionR), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_connectionR, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new Equality(body, var__virtual_2_, var_destination);
          // 	ConnectionReference.context(connectionR, contextR)
          new TypeConstraint(body, Tuples.flatTupleOf(var_connectionR), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference")));
          PVariable var__virtual_3_ = body.getOrCreateVariableByName(".virtual{3}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_connectionR, var__virtual_3_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference", "context")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_3_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new Equality(body, var__virtual_3_, var_contextR);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
