/**
 * Generated from platform:/resource/fr.tpt.mem4csd.mtbench.aadl2aadl.viatra/src/fr/tpt/mem4csd/mtbench/aadl2aadl/viatra/copying/Copying_Required_queries.vql
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying;

import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Is_in_trace_for_copy;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionInstanceEnd;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         pattern
 *         copy_connection(component : ComponentInstance, componentref : ComponentInstance, source : ConnectionInstanceEnd, destination : ConnectionInstanceEnd, sourceref : ConnectionInstanceEnd, destinationref : ConnectionInstanceEnd, connection : ConnectionInstance) {
 *         	find is_in_trace_for_copy(_, _, component, componentref);
 *         	find is_in_trace_for_copy(_, _, source, sourceref);
 *         	find is_in_trace_for_copy(_, _, destination, destinationref);
 *         	ComponentInstance.connectionInstance(component, connection);
 *         	ConnectionInstance.source(connection, source);
 *         	ConnectionInstance.destination(connection, destination);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Copy_connection extends BaseGeneratedEMFQuerySpecification<Copy_connection.Matcher> {
  /**
   * Pattern-specific match representation of the fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_connection pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fComponent;
    
    private ComponentInstance fComponentref;
    
    private ConnectionInstanceEnd fSource;
    
    private ConnectionInstanceEnd fDestination;
    
    private ConnectionInstanceEnd fSourceref;
    
    private ConnectionInstanceEnd fDestinationref;
    
    private ConnectionInstance fConnection;
    
    private static List<String> parameterNames = makeImmutableList("component", "componentref", "source", "destination", "sourceref", "destinationref", "connection");
    
    private Match(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      this.fComponent = pComponent;
      this.fComponentref = pComponentref;
      this.fSource = pSource;
      this.fDestination = pDestination;
      this.fSourceref = pSourceref;
      this.fDestinationref = pDestinationref;
      this.fConnection = pConnection;
    }
    
    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "component": return this.fComponent;
          case "componentref": return this.fComponentref;
          case "source": return this.fSource;
          case "destination": return this.fDestination;
          case "sourceref": return this.fSourceref;
          case "destinationref": return this.fDestinationref;
          case "connection": return this.fConnection;
          default: return null;
      }
    }
    
    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fComponent;
          case 1: return this.fComponentref;
          case 2: return this.fSource;
          case 3: return this.fDestination;
          case 4: return this.fSourceref;
          case 5: return this.fDestinationref;
          case 6: return this.fConnection;
          default: return null;
      }
    }
    
    public ComponentInstance getComponent() {
      return this.fComponent;
    }
    
    public ComponentInstance getComponentref() {
      return this.fComponentref;
    }
    
    public ConnectionInstanceEnd getSource() {
      return this.fSource;
    }
    
    public ConnectionInstanceEnd getDestination() {
      return this.fDestination;
    }
    
    public ConnectionInstanceEnd getSourceref() {
      return this.fSourceref;
    }
    
    public ConnectionInstanceEnd getDestinationref() {
      return this.fDestinationref;
    }
    
    public ConnectionInstance getConnection() {
      return this.fConnection;
    }
    
    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("component".equals(parameterName) ) {
          this.fComponent = (ComponentInstance) newValue;
          return true;
      }
      if ("componentref".equals(parameterName) ) {
          this.fComponentref = (ComponentInstance) newValue;
          return true;
      }
      if ("source".equals(parameterName) ) {
          this.fSource = (ConnectionInstanceEnd) newValue;
          return true;
      }
      if ("destination".equals(parameterName) ) {
          this.fDestination = (ConnectionInstanceEnd) newValue;
          return true;
      }
      if ("sourceref".equals(parameterName) ) {
          this.fSourceref = (ConnectionInstanceEnd) newValue;
          return true;
      }
      if ("destinationref".equals(parameterName) ) {
          this.fDestinationref = (ConnectionInstanceEnd) newValue;
          return true;
      }
      if ("connection".equals(parameterName) ) {
          this.fConnection = (ConnectionInstance) newValue;
          return true;
      }
      return false;
    }
    
    public void setComponent(final ComponentInstance pComponent) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fComponent = pComponent;
    }
    
    public void setComponentref(final ComponentInstance pComponentref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fComponentref = pComponentref;
    }
    
    public void setSource(final ConnectionInstanceEnd pSource) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSource = pSource;
    }
    
    public void setDestination(final ConnectionInstanceEnd pDestination) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fDestination = pDestination;
    }
    
    public void setSourceref(final ConnectionInstanceEnd pSourceref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSourceref = pSourceref;
    }
    
    public void setDestinationref(final ConnectionInstanceEnd pDestinationref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fDestinationref = pDestinationref;
    }
    
    public void setConnection(final ConnectionInstance pConnection) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fConnection = pConnection;
    }
    
    @Override
    public String patternName() {
      return "fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_connection";
    }
    
    @Override
    public List<String> parameterNames() {
      return Copy_connection.Match.parameterNames;
    }
    
    @Override
    public Object[] toArray() {
      return new Object[]{fComponent, fComponentref, fSource, fDestination, fSourceref, fDestinationref, fConnection};
    }
    
    @Override
    public Copy_connection.Match toImmutable() {
      return isMutable() ? newMatch(fComponent, fComponentref, fSource, fDestination, fSourceref, fDestinationref, fConnection) : this;
    }
    
    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"component\"=" + prettyPrintValue(fComponent) + ", ");
      result.append("\"componentref\"=" + prettyPrintValue(fComponentref) + ", ");
      result.append("\"source\"=" + prettyPrintValue(fSource) + ", ");
      result.append("\"destination\"=" + prettyPrintValue(fDestination) + ", ");
      result.append("\"sourceref\"=" + prettyPrintValue(fSourceref) + ", ");
      result.append("\"destinationref\"=" + prettyPrintValue(fDestinationref) + ", ");
      result.append("\"connection\"=" + prettyPrintValue(fConnection));
      return result.toString();
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(fComponent, fComponentref, fSource, fDestination, fSourceref, fDestinationref, fConnection);
    }
    
    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Copy_connection.Match)) {
          Copy_connection.Match other = (Copy_connection.Match) obj;
          return Objects.equals(fComponent, other.fComponent) && Objects.equals(fComponentref, other.fComponentref) && Objects.equals(fSource, other.fSource) && Objects.equals(fDestination, other.fDestination) && Objects.equals(fSourceref, other.fSourceref) && Objects.equals(fDestinationref, other.fDestinationref) && Objects.equals(fConnection, other.fConnection);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }
    
    @Override
    public Copy_connection specification() {
      return Copy_connection.instance();
    }
    
    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Copy_connection.Match newEmptyMatch() {
      return new Mutable(null, null, null, null, null, null, null);
    }
    
    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Copy_connection.Match newMutableMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return new Mutable(pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection);
    }
    
    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Copy_connection.Match newMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return new Immutable(pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection);
    }
    
    private static final class Mutable extends Copy_connection.Match {
      Mutable(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
        super(pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection);
      }
      
      @Override
      public boolean isMutable() {
        return true;
      }
    }
    
    private static final class Immutable extends Copy_connection.Match {
      Immutable(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
        super(pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection);
      }
      
      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }
  
  /**
   * Generated pattern matcher API of the fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_connection pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * pattern
   * copy_connection(component : ComponentInstance, componentref : ComponentInstance, source : ConnectionInstanceEnd, destination : ConnectionInstanceEnd, sourceref : ConnectionInstanceEnd, destinationref : ConnectionInstanceEnd, connection : ConnectionInstance) {
   * 	find is_in_trace_for_copy(_, _, component, componentref);
   * 	find is_in_trace_for_copy(_, _, source, sourceref);
   * 	find is_in_trace_for_copy(_, _, destination, destinationref);
   * 	ComponentInstance.connectionInstance(component, connection);
   * 	ConnectionInstance.source(connection, source);
   * 	ConnectionInstance.destination(connection, destination);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Copy_connection
   * 
   */
  public static class Matcher extends BaseMatcher<Copy_connection.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Copy_connection.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }
    
    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Copy_connection.Matcher create() {
      return new Matcher();
    }
    
    private static final int POSITION_COMPONENT = 0;
    
    private static final int POSITION_COMPONENTREF = 1;
    
    private static final int POSITION_SOURCE = 2;
    
    private static final int POSITION_DESTINATION = 3;
    
    private static final int POSITION_SOURCEREF = 4;
    
    private static final int POSITION_DESTINATIONREF = 5;
    
    private static final int POSITION_CONNECTION = 6;
    
    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Copy_connection.Matcher.class);
    
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }
    
    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Copy_connection.Match> getAllMatches(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllMatches(new Object[]{pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection}).collect(Collectors.toSet());
    }
    
    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Copy_connection.Match> streamAllMatches(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllMatches(new Object[]{pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection});
    }
    
    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Copy_connection.Match> getOneArbitraryMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawGetOneArbitraryMatch(new Object[]{pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection});
    }
    
    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawHasMatch(new Object[]{pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection});
    }
    
    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawCountMatches(new Object[]{pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection});
    }
    
    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection, final Consumer<? super Copy_connection.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection}, processor);
    }
    
    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSource the fixed value of pattern parameter source, or null if not bound.
     * @param pDestination the fixed value of pattern parameter destination, or null if not bound.
     * @param pSourceref the fixed value of pattern parameter sourceref, or null if not bound.
     * @param pDestinationref the fixed value of pattern parameter destinationref, or null if not bound.
     * @param pConnection the fixed value of pattern parameter connection, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Copy_connection.Match newMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return Copy_connection.Match.newMatch(pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection);
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcomponent(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPONENT, parameters).map(ComponentInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponent() {
      return rawStreamAllValuesOfcomponent(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponent() {
      return rawStreamAllValuesOfcomponent(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponent(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfcomponent(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponent(final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfcomponent(new Object[]{null, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection});
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponent(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfcomponent(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponent(final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfcomponent(new Object[]{null, pComponentref, pSource, pDestination, pSourceref, pDestinationref, pConnection}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcomponentref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPONENTREF, parameters).map(ComponentInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponentref() {
      return rawStreamAllValuesOfcomponentref(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponentref() {
      return rawStreamAllValuesOfcomponentref(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponentref(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfcomponentref(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponentref(final ComponentInstance pComponent, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfcomponentref(new Object[]{pComponent, null, pSource, pDestination, pSourceref, pDestinationref, pConnection});
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponentref(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfcomponentref(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponentref(final ComponentInstance pComponent, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfcomponentref(new Object[]{pComponent, null, pSource, pDestination, pSourceref, pDestinationref, pConnection}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstanceEnd> rawStreamAllValuesOfsource(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SOURCE, parameters).map(ConnectionInstanceEnd.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsource() {
      return rawStreamAllValuesOfsource(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsource() {
      return rawStreamAllValuesOfsource(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsource(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfsource(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsource(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfsource(new Object[]{pComponent, pComponentref, null, pDestination, pSourceref, pDestinationref, pConnection});
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsource(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfsource(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for source.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsource(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfsource(new Object[]{pComponent, pComponentref, null, pDestination, pSourceref, pDestinationref, pConnection}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstanceEnd> rawStreamAllValuesOfdestination(final Object[] parameters) {
      return rawStreamAllValues(POSITION_DESTINATION, parameters).map(ConnectionInstanceEnd.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestination() {
      return rawStreamAllValuesOfdestination(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestination() {
      return rawStreamAllValuesOfdestination(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestination(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfdestination(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestination(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfdestination(new Object[]{pComponent, pComponentref, pSource, null, pSourceref, pDestinationref, pConnection});
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestination(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfdestination(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destination.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestination(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfdestination(new Object[]{pComponent, pComponentref, pSource, null, pSourceref, pDestinationref, pConnection}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstanceEnd> rawStreamAllValuesOfsourceref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SOURCEREF, parameters).map(ConnectionInstanceEnd.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsourceref() {
      return rawStreamAllValuesOfsourceref(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsourceref() {
      return rawStreamAllValuesOfsourceref(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsourceref(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfsourceref(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfsourceref(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfsourceref(new Object[]{pComponent, pComponentref, pSource, pDestination, null, pDestinationref, pConnection});
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsourceref(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfsourceref(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for sourceref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfsourceref(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pDestinationref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfsourceref(new Object[]{pComponent, pComponentref, pSource, pDestination, null, pDestinationref, pConnection}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstanceEnd> rawStreamAllValuesOfdestinationref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_DESTINATIONREF, parameters).map(ConnectionInstanceEnd.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestinationref() {
      return rawStreamAllValuesOfdestinationref(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestinationref() {
      return rawStreamAllValuesOfdestinationref(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestinationref(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfdestinationref(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstanceEnd> streamAllValuesOfdestinationref(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfdestinationref(new Object[]{pComponent, pComponentref, pSource, pDestination, pSourceref, null, pConnection});
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestinationref(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfdestinationref(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for destinationref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstanceEnd> getAllValuesOfdestinationref(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstance pConnection) {
      return rawStreamAllValuesOfdestinationref(new Object[]{pComponent, pComponentref, pSource, pDestination, pSourceref, null, pConnection}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstance> rawStreamAllValuesOfconnection(final Object[] parameters) {
      return rawStreamAllValues(POSITION_CONNECTION, parameters).map(ConnectionInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconnection() {
      return rawStreamAllValuesOfconnection(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconnection() {
      return rawStreamAllValuesOfconnection(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconnection(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfconnection(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconnection(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref) {
      return rawStreamAllValuesOfconnection(new Object[]{pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, null});
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconnection(final Copy_connection.Match partialMatch) {
      return rawStreamAllValuesOfconnection(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for connection.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconnection(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ConnectionInstanceEnd pSource, final ConnectionInstanceEnd pDestination, final ConnectionInstanceEnd pSourceref, final ConnectionInstanceEnd pDestinationref) {
      return rawStreamAllValuesOfconnection(new Object[]{pComponent, pComponentref, pSource, pDestination, pSourceref, pDestinationref, null}).collect(Collectors.toSet());
    }
    
    @Override
    protected Copy_connection.Match tupleToMatch(final Tuple t) {
      try {
          return Copy_connection.Match.newMatch((ComponentInstance) t.get(POSITION_COMPONENT), (ComponentInstance) t.get(POSITION_COMPONENTREF), (ConnectionInstanceEnd) t.get(POSITION_SOURCE), (ConnectionInstanceEnd) t.get(POSITION_DESTINATION), (ConnectionInstanceEnd) t.get(POSITION_SOURCEREF), (ConnectionInstanceEnd) t.get(POSITION_DESTINATIONREF), (ConnectionInstance) t.get(POSITION_CONNECTION));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }
    
    @Override
    protected Copy_connection.Match arrayToMatch(final Object[] match) {
      try {
          return Copy_connection.Match.newMatch((ComponentInstance) match[POSITION_COMPONENT], (ComponentInstance) match[POSITION_COMPONENTREF], (ConnectionInstanceEnd) match[POSITION_SOURCE], (ConnectionInstanceEnd) match[POSITION_DESTINATION], (ConnectionInstanceEnd) match[POSITION_SOURCEREF], (ConnectionInstanceEnd) match[POSITION_DESTINATIONREF], (ConnectionInstance) match[POSITION_CONNECTION]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }
    
    @Override
    protected Copy_connection.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Copy_connection.Match.newMutableMatch((ComponentInstance) match[POSITION_COMPONENT], (ComponentInstance) match[POSITION_COMPONENTREF], (ConnectionInstanceEnd) match[POSITION_SOURCE], (ConnectionInstanceEnd) match[POSITION_DESTINATION], (ConnectionInstanceEnd) match[POSITION_SOURCEREF], (ConnectionInstanceEnd) match[POSITION_DESTINATIONREF], (ConnectionInstance) match[POSITION_CONNECTION]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }
    
    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Copy_connection.Matcher> querySpecification() {
      return Copy_connection.instance();
    }
  }
  
  private Copy_connection() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Copy_connection instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }
  
  @Override
  protected Copy_connection.Matcher instantiate(final ViatraQueryEngine engine) {
    return Copy_connection.Matcher.on(engine);
  }
  
  @Override
  public Copy_connection.Matcher instantiate() {
    return Copy_connection.Matcher.create();
  }
  
  @Override
  public Copy_connection.Match newEmptyMatch() {
    return Copy_connection.Match.newEmptyMatch();
  }
  
  @Override
  public Copy_connection.Match newMatch(final Object... parameters) {
    return Copy_connection.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0], (org.osate.aadl2.instance.ComponentInstance) parameters[1], (org.osate.aadl2.instance.ConnectionInstanceEnd) parameters[2], (org.osate.aadl2.instance.ConnectionInstanceEnd) parameters[3], (org.osate.aadl2.instance.ConnectionInstanceEnd) parameters[4], (org.osate.aadl2.instance.ConnectionInstanceEnd) parameters[5], (org.osate.aadl2.instance.ConnectionInstance) parameters[6]);
  }
  
  /**
   * Inner class allowing the singleton instance of {@link Copy_connection} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Copy_connection#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Copy_connection INSTANCE = new Copy_connection();
    
    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();
    
    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Copy_connection.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    private final PParameter parameter_component = new PParameter("component", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);
    
    private final PParameter parameter_componentref = new PParameter("componentref", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);
    
    private final PParameter parameter_source = new PParameter("source", "org.osate.aadl2.instance.ConnectionInstanceEnd", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")), PParameterDirection.INOUT);
    
    private final PParameter parameter_destination = new PParameter("destination", "org.osate.aadl2.instance.ConnectionInstanceEnd", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")), PParameterDirection.INOUT);
    
    private final PParameter parameter_sourceref = new PParameter("sourceref", "org.osate.aadl2.instance.ConnectionInstanceEnd", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")), PParameterDirection.INOUT);
    
    private final PParameter parameter_destinationref = new PParameter("destinationref", "org.osate.aadl2.instance.ConnectionInstanceEnd", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")), PParameterDirection.INOUT);
    
    private final PParameter parameter_connection = new PParameter("connection", "org.osate.aadl2.instance.ConnectionInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")), PParameterDirection.INOUT);
    
    private final List<PParameter> parameters = Arrays.asList(parameter_component, parameter_componentref, parameter_source, parameter_destination, parameter_sourceref, parameter_destinationref, parameter_connection);
    
    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }
    
    @Override
    public String getFullyQualifiedName() {
      return "fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_connection";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("component","componentref","source","destination","sourceref","destinationref","connection");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_component = body.getOrCreateVariableByName("component");
          PVariable var_componentref = body.getOrCreateVariableByName("componentref");
          PVariable var_source = body.getOrCreateVariableByName("source");
          PVariable var_destination = body.getOrCreateVariableByName("destination");
          PVariable var_sourceref = body.getOrCreateVariableByName("sourceref");
          PVariable var_destinationref = body.getOrCreateVariableByName("destinationref");
          PVariable var_connection = body.getOrCreateVariableByName("connection");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          PVariable var___1_ = body.getOrCreateVariableByName("_<1>");
          PVariable var___2_ = body.getOrCreateVariableByName("_<2>");
          PVariable var___3_ = body.getOrCreateVariableByName("_<3>");
          PVariable var___4_ = body.getOrCreateVariableByName("_<4>");
          PVariable var___5_ = body.getOrCreateVariableByName("_<5>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_component), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_componentref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_source), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_destination), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_sourceref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_destinationref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_connection), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_component, parameter_component),
             new ExportedParameter(body, var_componentref, parameter_componentref),
             new ExportedParameter(body, var_source, parameter_source),
             new ExportedParameter(body, var_destination, parameter_destination),
             new ExportedParameter(body, var_sourceref, parameter_sourceref),
             new ExportedParameter(body, var_destinationref, parameter_destinationref),
             new ExportedParameter(body, var_connection, parameter_connection)
          ));
          // 	find is_in_trace_for_copy(_, _, component, componentref)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___0_, var___1_, var_component, var_componentref), Is_in_trace_for_copy.instance().getInternalQueryRepresentation());
          // 	find is_in_trace_for_copy(_, _, source, sourceref)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___2_, var___3_, var_source, var_sourceref), Is_in_trace_for_copy.instance().getInternalQueryRepresentation());
          // 	find is_in_trace_for_copy(_, _, destination, destinationref)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___4_, var___5_, var_destination, var_destinationref), Is_in_trace_for_copy.instance().getInternalQueryRepresentation());
          // 	ComponentInstance.connectionInstance(component, connection)
          new TypeConstraint(body, Tuples.flatTupleOf(var_component), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_component, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "connectionInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          new Equality(body, var__virtual_0_, var_connection);
          // 	ConnectionInstance.source(connection, source)
          new TypeConstraint(body, Tuples.flatTupleOf(var_connection), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_connection, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new Equality(body, var__virtual_1_, var_source);
          // 	ConnectionInstance.destination(connection, destination)
          new TypeConstraint(body, Tuples.flatTupleOf(var_connection), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_connection, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new Equality(body, var__virtual_2_, var_destination);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
