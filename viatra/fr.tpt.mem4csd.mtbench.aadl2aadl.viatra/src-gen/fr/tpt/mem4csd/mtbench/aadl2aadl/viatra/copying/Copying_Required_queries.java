/**
 * Generated from platform:/resource/fr.tpt.mem4csd.mtbench.aadl2aadl.viatra/src/fr/tpt/mem4csd/mtbench/aadl2aadl/viatra/copying/Copying_Required_queries.vql
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying;

import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copy_component;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copy_connection;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copy_connectionref;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copy_feature;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copy_system;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Is_in_trace_for_copy;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedPatternGroup;

/**
 * A pattern group formed of all public patterns defined in Copying_Required_queries.vql.
 * 
 * <p>Use the static instance as any {@link interface org.eclipse.viatra.query.runtime.api.IQueryGroup}, to conveniently prepare
 * a VIATRA Query engine for matching all patterns originally defined in file Copying_Required_queries.vql,
 * in order to achieve better performance than one-by-one on-demand matcher initialization.
 * 
 * <p> From package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying, the group contains the definition of the following patterns: <ul>
 * <li>copy_system</li>
 * <li>copy_component</li>
 * <li>copy_feature</li>
 * <li>copy_connection</li>
 * <li>copy_connectionref</li>
 * <li>is_in_trace_for_copy</li>
 * </ul>
 * 
 * @see IQueryGroup
 * 
 */
@SuppressWarnings("all")
public final class Copying_Required_queries extends BaseGeneratedPatternGroup {
  /**
   * Access the pattern group.
   * 
   * @return the singleton instance of the group
   * @throws ViatraQueryRuntimeException if there was an error loading the generated code of pattern specifications
   * 
   */
  public static Copying_Required_queries instance() {
    if (INSTANCE == null) {
        INSTANCE = new Copying_Required_queries();
    }
    return INSTANCE;
  }
  
  private static Copying_Required_queries INSTANCE;
  
  private Copying_Required_queries() {
    querySpecifications.add(Copy_system.instance());
    querySpecifications.add(Copy_component.instance());
    querySpecifications.add(Copy_feature.instance());
    querySpecifications.add(Copy_connection.instance());
    querySpecifications.add(Copy_connectionref.instance());
    querySpecifications.add(Is_in_trace_for_copy.instance());
  }
  
  public Copy_system getCopy_system() {
    return Copy_system.instance();
  }
  
  public Copy_system.Matcher getCopy_system(final ViatraQueryEngine engine) {
    return Copy_system.Matcher.on(engine);
  }
  
  public Copy_component getCopy_component() {
    return Copy_component.instance();
  }
  
  public Copy_component.Matcher getCopy_component(final ViatraQueryEngine engine) {
    return Copy_component.Matcher.on(engine);
  }
  
  public Copy_feature getCopy_feature() {
    return Copy_feature.instance();
  }
  
  public Copy_feature.Matcher getCopy_feature(final ViatraQueryEngine engine) {
    return Copy_feature.Matcher.on(engine);
  }
  
  public Copy_connection getCopy_connection() {
    return Copy_connection.instance();
  }
  
  public Copy_connection.Matcher getCopy_connection(final ViatraQueryEngine engine) {
    return Copy_connection.Matcher.on(engine);
  }
  
  public Copy_connectionref getCopy_connectionref() {
    return Copy_connectionref.instance();
  }
  
  public Copy_connectionref.Matcher getCopy_connectionref(final ViatraQueryEngine engine) {
    return Copy_connectionref.Matcher.on(engine);
  }
  
  public Is_in_trace_for_copy getIs_in_trace_for_copy() {
    return Is_in_trace_for_copy.instance();
  }
  
  public Is_in_trace_for_copy.Matcher getIs_in_trace_for_copy(final ViatraQueryEngine engine) {
    return Is_in_trace_for_copy.Matcher.on(engine);
  }
}
