/**
 * Generated from platform:/resource/fr.tpt.mem4csd.mtbench.aadl2aadl.viatra/src/fr/tpt/mem4csd/mtbench/aadl2aadl/viatra/copying/Copying_Required_queries.vql
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying;

import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Is_in_trace_for_copy;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ComponentInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         pattern
 *         copy_component(component : ComponentInstance, componentref : ComponentInstance, subcomponent : ComponentInstance) {
 *         	find is_in_trace_for_copy(_, _, component, componentref);
 *         	ComponentInstance.componentInstance(component, subcomponent);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Copy_component extends BaseGeneratedEMFQuerySpecification<Copy_component.Matcher> {
  /**
   * Pattern-specific match representation of the fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_component pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fComponent;
    
    private ComponentInstance fComponentref;
    
    private ComponentInstance fSubcomponent;
    
    private static List<String> parameterNames = makeImmutableList("component", "componentref", "subcomponent");
    
    private Match(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      this.fComponent = pComponent;
      this.fComponentref = pComponentref;
      this.fSubcomponent = pSubcomponent;
    }
    
    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "component": return this.fComponent;
          case "componentref": return this.fComponentref;
          case "subcomponent": return this.fSubcomponent;
          default: return null;
      }
    }
    
    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fComponent;
          case 1: return this.fComponentref;
          case 2: return this.fSubcomponent;
          default: return null;
      }
    }
    
    public ComponentInstance getComponent() {
      return this.fComponent;
    }
    
    public ComponentInstance getComponentref() {
      return this.fComponentref;
    }
    
    public ComponentInstance getSubcomponent() {
      return this.fSubcomponent;
    }
    
    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("component".equals(parameterName) ) {
          this.fComponent = (ComponentInstance) newValue;
          return true;
      }
      if ("componentref".equals(parameterName) ) {
          this.fComponentref = (ComponentInstance) newValue;
          return true;
      }
      if ("subcomponent".equals(parameterName) ) {
          this.fSubcomponent = (ComponentInstance) newValue;
          return true;
      }
      return false;
    }
    
    public void setComponent(final ComponentInstance pComponent) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fComponent = pComponent;
    }
    
    public void setComponentref(final ComponentInstance pComponentref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fComponentref = pComponentref;
    }
    
    public void setSubcomponent(final ComponentInstance pSubcomponent) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSubcomponent = pSubcomponent;
    }
    
    @Override
    public String patternName() {
      return "fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_component";
    }
    
    @Override
    public List<String> parameterNames() {
      return Copy_component.Match.parameterNames;
    }
    
    @Override
    public Object[] toArray() {
      return new Object[]{fComponent, fComponentref, fSubcomponent};
    }
    
    @Override
    public Copy_component.Match toImmutable() {
      return isMutable() ? newMatch(fComponent, fComponentref, fSubcomponent) : this;
    }
    
    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"component\"=" + prettyPrintValue(fComponent) + ", ");
      result.append("\"componentref\"=" + prettyPrintValue(fComponentref) + ", ");
      result.append("\"subcomponent\"=" + prettyPrintValue(fSubcomponent));
      return result.toString();
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(fComponent, fComponentref, fSubcomponent);
    }
    
    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Copy_component.Match)) {
          Copy_component.Match other = (Copy_component.Match) obj;
          return Objects.equals(fComponent, other.fComponent) && Objects.equals(fComponentref, other.fComponentref) && Objects.equals(fSubcomponent, other.fSubcomponent);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }
    
    @Override
    public Copy_component specification() {
      return Copy_component.instance();
    }
    
    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Copy_component.Match newEmptyMatch() {
      return new Mutable(null, null, null);
    }
    
    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Copy_component.Match newMutableMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      return new Mutable(pComponent, pComponentref, pSubcomponent);
    }
    
    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Copy_component.Match newMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      return new Immutable(pComponent, pComponentref, pSubcomponent);
    }
    
    private static final class Mutable extends Copy_component.Match {
      Mutable(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
        super(pComponent, pComponentref, pSubcomponent);
      }
      
      @Override
      public boolean isMutable() {
        return true;
      }
    }
    
    private static final class Immutable extends Copy_component.Match {
      Immutable(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
        super(pComponent, pComponentref, pSubcomponent);
      }
      
      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }
  
  /**
   * Generated pattern matcher API of the fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_component pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * pattern
   * copy_component(component : ComponentInstance, componentref : ComponentInstance, subcomponent : ComponentInstance) {
   * 	find is_in_trace_for_copy(_, _, component, componentref);
   * 	ComponentInstance.componentInstance(component, subcomponent);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Copy_component
   * 
   */
  public static class Matcher extends BaseMatcher<Copy_component.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Copy_component.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }
    
    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Copy_component.Matcher create() {
      return new Matcher();
    }
    
    private static final int POSITION_COMPONENT = 0;
    
    private static final int POSITION_COMPONENTREF = 1;
    
    private static final int POSITION_SUBCOMPONENT = 2;
    
    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Copy_component.Matcher.class);
    
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }
    
    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Copy_component.Match> getAllMatches(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      return rawStreamAllMatches(new Object[]{pComponent, pComponentref, pSubcomponent}).collect(Collectors.toSet());
    }
    
    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Copy_component.Match> streamAllMatches(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      return rawStreamAllMatches(new Object[]{pComponent, pComponentref, pSubcomponent});
    }
    
    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Copy_component.Match> getOneArbitraryMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      return rawGetOneArbitraryMatch(new Object[]{pComponent, pComponentref, pSubcomponent});
    }
    
    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      return rawHasMatch(new Object[]{pComponent, pComponentref, pSubcomponent});
    }
    
    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      return rawCountMatches(new Object[]{pComponent, pComponentref, pSubcomponent});
    }
    
    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent, final Consumer<? super Copy_component.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pComponent, pComponentref, pSubcomponent}, processor);
    }
    
    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pComponentref the fixed value of pattern parameter componentref, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Copy_component.Match newMatch(final ComponentInstance pComponent, final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      return Copy_component.Match.newMatch(pComponent, pComponentref, pSubcomponent);
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcomponent(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPONENT, parameters).map(ComponentInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponent() {
      return rawStreamAllValuesOfcomponent(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponent() {
      return rawStreamAllValuesOfcomponent(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponent(final Copy_component.Match partialMatch) {
      return rawStreamAllValuesOfcomponent(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponent(final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      return rawStreamAllValuesOfcomponent(new Object[]{null, pComponentref, pSubcomponent});
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponent(final Copy_component.Match partialMatch) {
      return rawStreamAllValuesOfcomponent(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponent(final ComponentInstance pComponentref, final ComponentInstance pSubcomponent) {
      return rawStreamAllValuesOfcomponent(new Object[]{null, pComponentref, pSubcomponent}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcomponentref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPONENTREF, parameters).map(ComponentInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponentref() {
      return rawStreamAllValuesOfcomponentref(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponentref() {
      return rawStreamAllValuesOfcomponentref(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponentref(final Copy_component.Match partialMatch) {
      return rawStreamAllValuesOfcomponentref(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponentref(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      return rawStreamAllValuesOfcomponentref(new Object[]{pComponent, null, pSubcomponent});
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponentref(final Copy_component.Match partialMatch) {
      return rawStreamAllValuesOfcomponentref(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for componentref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponentref(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      return rawStreamAllValuesOfcomponentref(new Object[]{pComponent, null, pSubcomponent}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfsubcomponent(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SUBCOMPONENT, parameters).map(ComponentInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsubcomponent() {
      return rawStreamAllValuesOfsubcomponent(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsubcomponent() {
      return rawStreamAllValuesOfsubcomponent(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsubcomponent(final Copy_component.Match partialMatch) {
      return rawStreamAllValuesOfsubcomponent(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsubcomponent(final ComponentInstance pComponent, final ComponentInstance pComponentref) {
      return rawStreamAllValuesOfsubcomponent(new Object[]{pComponent, pComponentref, null});
    }
    
    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsubcomponent(final Copy_component.Match partialMatch) {
      return rawStreamAllValuesOfsubcomponent(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsubcomponent(final ComponentInstance pComponent, final ComponentInstance pComponentref) {
      return rawStreamAllValuesOfsubcomponent(new Object[]{pComponent, pComponentref, null}).collect(Collectors.toSet());
    }
    
    @Override
    protected Copy_component.Match tupleToMatch(final Tuple t) {
      try {
          return Copy_component.Match.newMatch((ComponentInstance) t.get(POSITION_COMPONENT), (ComponentInstance) t.get(POSITION_COMPONENTREF), (ComponentInstance) t.get(POSITION_SUBCOMPONENT));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }
    
    @Override
    protected Copy_component.Match arrayToMatch(final Object[] match) {
      try {
          return Copy_component.Match.newMatch((ComponentInstance) match[POSITION_COMPONENT], (ComponentInstance) match[POSITION_COMPONENTREF], (ComponentInstance) match[POSITION_SUBCOMPONENT]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }
    
    @Override
    protected Copy_component.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Copy_component.Match.newMutableMatch((ComponentInstance) match[POSITION_COMPONENT], (ComponentInstance) match[POSITION_COMPONENTREF], (ComponentInstance) match[POSITION_SUBCOMPONENT]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }
    
    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Copy_component.Matcher> querySpecification() {
      return Copy_component.instance();
    }
  }
  
  private Copy_component() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Copy_component instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }
  
  @Override
  protected Copy_component.Matcher instantiate(final ViatraQueryEngine engine) {
    return Copy_component.Matcher.on(engine);
  }
  
  @Override
  public Copy_component.Matcher instantiate() {
    return Copy_component.Matcher.create();
  }
  
  @Override
  public Copy_component.Match newEmptyMatch() {
    return Copy_component.Match.newEmptyMatch();
  }
  
  @Override
  public Copy_component.Match newMatch(final Object... parameters) {
    return Copy_component.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0], (org.osate.aadl2.instance.ComponentInstance) parameters[1], (org.osate.aadl2.instance.ComponentInstance) parameters[2]);
  }
  
  /**
   * Inner class allowing the singleton instance of {@link Copy_component} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Copy_component#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Copy_component INSTANCE = new Copy_component();
    
    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();
    
    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Copy_component.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    private final PParameter parameter_component = new PParameter("component", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);
    
    private final PParameter parameter_componentref = new PParameter("componentref", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);
    
    private final PParameter parameter_subcomponent = new PParameter("subcomponent", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);
    
    private final List<PParameter> parameters = Arrays.asList(parameter_component, parameter_componentref, parameter_subcomponent);
    
    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }
    
    @Override
    public String getFullyQualifiedName() {
      return "fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_component";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("component","componentref","subcomponent");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_component = body.getOrCreateVariableByName("component");
          PVariable var_componentref = body.getOrCreateVariableByName("componentref");
          PVariable var_subcomponent = body.getOrCreateVariableByName("subcomponent");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          PVariable var___1_ = body.getOrCreateVariableByName("_<1>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_component), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_componentref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_subcomponent), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_component, parameter_component),
             new ExportedParameter(body, var_componentref, parameter_componentref),
             new ExportedParameter(body, var_subcomponent, parameter_subcomponent)
          ));
          // 	find is_in_trace_for_copy(_, _, component, componentref)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___0_, var___1_, var_component, var_componentref), Is_in_trace_for_copy.instance().getInternalQueryRepresentation());
          // 	ComponentInstance.componentInstance(component, subcomponent)
          new TypeConstraint(body, Tuples.flatTupleOf(var_component), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_component, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "componentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new Equality(body, var__virtual_0_, var_subcomponent);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
