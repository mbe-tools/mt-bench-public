/**
 * Generated from platform:/resource/fr.tpt.mem4csd.mtbench.aadl2aadl.viatra/src/fr/tpt/mem4csd/mtbench/aadl2aadl/viatra/copying/Copying_Required_queries.vql
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying;

import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTraceSpec;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.SystemInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         pattern
 *         copy_system(aadl2aadlref : Aadl2AadlTraceSpec, system : SystemInstance, systemRef : SystemInstance) {
 *         	Aadl2AadlTraceSpec.leftSystem(aadl2aadlref, system);
 *         	Aadl2AadlTraceSpec.rightSystem(aadl2aadlref, systemRef);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Copy_system extends BaseGeneratedEMFQuerySpecification<Copy_system.Matcher> {
  /**
   * Pattern-specific match representation of the fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_system pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private Aadl2AadlTraceSpec fAadl2aadlref;
    
    private SystemInstance fSystem;
    
    private SystemInstance fSystemRef;
    
    private static List<String> parameterNames = makeImmutableList("aadl2aadlref", "system", "systemRef");
    
    private Match(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
      this.fAadl2aadlref = pAadl2aadlref;
      this.fSystem = pSystem;
      this.fSystemRef = pSystemRef;
    }
    
    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "aadl2aadlref": return this.fAadl2aadlref;
          case "system": return this.fSystem;
          case "systemRef": return this.fSystemRef;
          default: return null;
      }
    }
    
    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fAadl2aadlref;
          case 1: return this.fSystem;
          case 2: return this.fSystemRef;
          default: return null;
      }
    }
    
    public Aadl2AadlTraceSpec getAadl2aadlref() {
      return this.fAadl2aadlref;
    }
    
    public SystemInstance getSystem() {
      return this.fSystem;
    }
    
    public SystemInstance getSystemRef() {
      return this.fSystemRef;
    }
    
    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("aadl2aadlref".equals(parameterName) ) {
          this.fAadl2aadlref = (Aadl2AadlTraceSpec) newValue;
          return true;
      }
      if ("system".equals(parameterName) ) {
          this.fSystem = (SystemInstance) newValue;
          return true;
      }
      if ("systemRef".equals(parameterName) ) {
          this.fSystemRef = (SystemInstance) newValue;
          return true;
      }
      return false;
    }
    
    public void setAadl2aadlref(final Aadl2AadlTraceSpec pAadl2aadlref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fAadl2aadlref = pAadl2aadlref;
    }
    
    public void setSystem(final SystemInstance pSystem) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSystem = pSystem;
    }
    
    public void setSystemRef(final SystemInstance pSystemRef) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSystemRef = pSystemRef;
    }
    
    @Override
    public String patternName() {
      return "fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_system";
    }
    
    @Override
    public List<String> parameterNames() {
      return Copy_system.Match.parameterNames;
    }
    
    @Override
    public Object[] toArray() {
      return new Object[]{fAadl2aadlref, fSystem, fSystemRef};
    }
    
    @Override
    public Copy_system.Match toImmutable() {
      return isMutable() ? newMatch(fAadl2aadlref, fSystem, fSystemRef) : this;
    }
    
    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"aadl2aadlref\"=" + prettyPrintValue(fAadl2aadlref) + ", ");
      result.append("\"system\"=" + prettyPrintValue(fSystem) + ", ");
      result.append("\"systemRef\"=" + prettyPrintValue(fSystemRef));
      return result.toString();
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(fAadl2aadlref, fSystem, fSystemRef);
    }
    
    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Copy_system.Match)) {
          Copy_system.Match other = (Copy_system.Match) obj;
          return Objects.equals(fAadl2aadlref, other.fAadl2aadlref) && Objects.equals(fSystem, other.fSystem) && Objects.equals(fSystemRef, other.fSystemRef);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }
    
    @Override
    public Copy_system specification() {
      return Copy_system.instance();
    }
    
    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Copy_system.Match newEmptyMatch() {
      return new Mutable(null, null, null);
    }
    
    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pSystem the fixed value of pattern parameter system, or null if not bound.
     * @param pSystemRef the fixed value of pattern parameter systemRef, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Copy_system.Match newMutableMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
      return new Mutable(pAadl2aadlref, pSystem, pSystemRef);
    }
    
    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pSystem the fixed value of pattern parameter system, or null if not bound.
     * @param pSystemRef the fixed value of pattern parameter systemRef, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Copy_system.Match newMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
      return new Immutable(pAadl2aadlref, pSystem, pSystemRef);
    }
    
    private static final class Mutable extends Copy_system.Match {
      Mutable(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
        super(pAadl2aadlref, pSystem, pSystemRef);
      }
      
      @Override
      public boolean isMutable() {
        return true;
      }
    }
    
    private static final class Immutable extends Copy_system.Match {
      Immutable(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
        super(pAadl2aadlref, pSystem, pSystemRef);
      }
      
      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }
  
  /**
   * Generated pattern matcher API of the fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_system pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * pattern
   * copy_system(aadl2aadlref : Aadl2AadlTraceSpec, system : SystemInstance, systemRef : SystemInstance) {
   * 	Aadl2AadlTraceSpec.leftSystem(aadl2aadlref, system);
   * 	Aadl2AadlTraceSpec.rightSystem(aadl2aadlref, systemRef);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Copy_system
   * 
   */
  public static class Matcher extends BaseMatcher<Copy_system.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Copy_system.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }
    
    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Copy_system.Matcher create() {
      return new Matcher();
    }
    
    private static final int POSITION_AADL2AADLREF = 0;
    
    private static final int POSITION_SYSTEM = 1;
    
    private static final int POSITION_SYSTEMREF = 2;
    
    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Copy_system.Matcher.class);
    
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }
    
    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pSystem the fixed value of pattern parameter system, or null if not bound.
     * @param pSystemRef the fixed value of pattern parameter systemRef, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Copy_system.Match> getAllMatches(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
      return rawStreamAllMatches(new Object[]{pAadl2aadlref, pSystem, pSystemRef}).collect(Collectors.toSet());
    }
    
    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pSystem the fixed value of pattern parameter system, or null if not bound.
     * @param pSystemRef the fixed value of pattern parameter systemRef, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Copy_system.Match> streamAllMatches(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
      return rawStreamAllMatches(new Object[]{pAadl2aadlref, pSystem, pSystemRef});
    }
    
    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pSystem the fixed value of pattern parameter system, or null if not bound.
     * @param pSystemRef the fixed value of pattern parameter systemRef, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Copy_system.Match> getOneArbitraryMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
      return rawGetOneArbitraryMatch(new Object[]{pAadl2aadlref, pSystem, pSystemRef});
    }
    
    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pSystem the fixed value of pattern parameter system, or null if not bound.
     * @param pSystemRef the fixed value of pattern parameter systemRef, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
      return rawHasMatch(new Object[]{pAadl2aadlref, pSystem, pSystemRef});
    }
    
    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pSystem the fixed value of pattern parameter system, or null if not bound.
     * @param pSystemRef the fixed value of pattern parameter systemRef, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
      return rawCountMatches(new Object[]{pAadl2aadlref, pSystem, pSystemRef});
    }
    
    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pSystem the fixed value of pattern parameter system, or null if not bound.
     * @param pSystemRef the fixed value of pattern parameter systemRef, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef, final Consumer<? super Copy_system.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pAadl2aadlref, pSystem, pSystemRef}, processor);
    }
    
    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pAadl2aadlref the fixed value of pattern parameter aadl2aadlref, or null if not bound.
     * @param pSystem the fixed value of pattern parameter system, or null if not bound.
     * @param pSystemRef the fixed value of pattern parameter systemRef, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Copy_system.Match newMatch(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem, final SystemInstance pSystemRef) {
      return Copy_system.Match.newMatch(pAadl2aadlref, pSystem, pSystemRef);
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Aadl2AadlTraceSpec> rawStreamAllValuesOfaadl2aadlref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_AADL2AADLREF, parameters).map(Aadl2AadlTraceSpec.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aadl2AadlTraceSpec> getAllValuesOfaadl2aadlref() {
      return rawStreamAllValuesOfaadl2aadlref(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Aadl2AadlTraceSpec> streamAllValuesOfaadl2aadlref() {
      return rawStreamAllValuesOfaadl2aadlref(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aadl2AadlTraceSpec> streamAllValuesOfaadl2aadlref(final Copy_system.Match partialMatch) {
      return rawStreamAllValuesOfaadl2aadlref(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aadl2AadlTraceSpec> streamAllValuesOfaadl2aadlref(final SystemInstance pSystem, final SystemInstance pSystemRef) {
      return rawStreamAllValuesOfaadl2aadlref(new Object[]{null, pSystem, pSystemRef});
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aadl2AadlTraceSpec> getAllValuesOfaadl2aadlref(final Copy_system.Match partialMatch) {
      return rawStreamAllValuesOfaadl2aadlref(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for aadl2aadlref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aadl2AadlTraceSpec> getAllValuesOfaadl2aadlref(final SystemInstance pSystem, final SystemInstance pSystemRef) {
      return rawStreamAllValuesOfaadl2aadlref(new Object[]{null, pSystem, pSystemRef}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for system.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<SystemInstance> rawStreamAllValuesOfsystem(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SYSTEM, parameters).map(SystemInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for system.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<SystemInstance> getAllValuesOfsystem() {
      return rawStreamAllValuesOfsystem(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for system.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<SystemInstance> streamAllValuesOfsystem() {
      return rawStreamAllValuesOfsystem(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for system.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<SystemInstance> streamAllValuesOfsystem(final Copy_system.Match partialMatch) {
      return rawStreamAllValuesOfsystem(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for system.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<SystemInstance> streamAllValuesOfsystem(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystemRef) {
      return rawStreamAllValuesOfsystem(new Object[]{pAadl2aadlref, null, pSystemRef});
    }
    
    /**
     * Retrieve the set of values that occur in matches for system.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<SystemInstance> getAllValuesOfsystem(final Copy_system.Match partialMatch) {
      return rawStreamAllValuesOfsystem(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for system.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<SystemInstance> getAllValuesOfsystem(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystemRef) {
      return rawStreamAllValuesOfsystem(new Object[]{pAadl2aadlref, null, pSystemRef}).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for systemRef.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<SystemInstance> rawStreamAllValuesOfsystemRef(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SYSTEMREF, parameters).map(SystemInstance.class::cast);
    }
    
    /**
     * Retrieve the set of values that occur in matches for systemRef.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<SystemInstance> getAllValuesOfsystemRef() {
      return rawStreamAllValuesOfsystemRef(emptyArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for systemRef.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<SystemInstance> streamAllValuesOfsystemRef() {
      return rawStreamAllValuesOfsystemRef(emptyArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for systemRef.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<SystemInstance> streamAllValuesOfsystemRef(final Copy_system.Match partialMatch) {
      return rawStreamAllValuesOfsystemRef(partialMatch.toArray());
    }
    
    /**
     * Retrieve the set of values that occur in matches for systemRef.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<SystemInstance> streamAllValuesOfsystemRef(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem) {
      return rawStreamAllValuesOfsystemRef(new Object[]{pAadl2aadlref, pSystem, null});
    }
    
    /**
     * Retrieve the set of values that occur in matches for systemRef.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<SystemInstance> getAllValuesOfsystemRef(final Copy_system.Match partialMatch) {
      return rawStreamAllValuesOfsystemRef(partialMatch.toArray()).collect(Collectors.toSet());
    }
    
    /**
     * Retrieve the set of values that occur in matches for systemRef.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<SystemInstance> getAllValuesOfsystemRef(final Aadl2AadlTraceSpec pAadl2aadlref, final SystemInstance pSystem) {
      return rawStreamAllValuesOfsystemRef(new Object[]{pAadl2aadlref, pSystem, null}).collect(Collectors.toSet());
    }
    
    @Override
    protected Copy_system.Match tupleToMatch(final Tuple t) {
      try {
          return Copy_system.Match.newMatch((Aadl2AadlTraceSpec) t.get(POSITION_AADL2AADLREF), (SystemInstance) t.get(POSITION_SYSTEM), (SystemInstance) t.get(POSITION_SYSTEMREF));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }
    
    @Override
    protected Copy_system.Match arrayToMatch(final Object[] match) {
      try {
          return Copy_system.Match.newMatch((Aadl2AadlTraceSpec) match[POSITION_AADL2AADLREF], (SystemInstance) match[POSITION_SYSTEM], (SystemInstance) match[POSITION_SYSTEMREF]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }
    
    @Override
    protected Copy_system.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Copy_system.Match.newMutableMatch((Aadl2AadlTraceSpec) match[POSITION_AADL2AADLREF], (SystemInstance) match[POSITION_SYSTEM], (SystemInstance) match[POSITION_SYSTEMREF]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }
    
    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Copy_system.Matcher> querySpecification() {
      return Copy_system.instance();
    }
  }
  
  private Copy_system() {
    super(GeneratedPQuery.INSTANCE);
  }
  
  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Copy_system instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }
  
  @Override
  protected Copy_system.Matcher instantiate(final ViatraQueryEngine engine) {
    return Copy_system.Matcher.on(engine);
  }
  
  @Override
  public Copy_system.Matcher instantiate() {
    return Copy_system.Matcher.create();
  }
  
  @Override
  public Copy_system.Match newEmptyMatch() {
    return Copy_system.Match.newEmptyMatch();
  }
  
  @Override
  public Copy_system.Match newMatch(final Object... parameters) {
    return Copy_system.Match.newMatch((fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTraceSpec) parameters[0], (org.osate.aadl2.instance.SystemInstance) parameters[1], (org.osate.aadl2.instance.SystemInstance) parameters[2]);
  }
  
  /**
   * Inner class allowing the singleton instance of {@link Copy_system} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Copy_system#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Copy_system INSTANCE = new Copy_system();
    
    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();
    
    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }
  
  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Copy_system.GeneratedPQuery INSTANCE = new GeneratedPQuery();
    
    private final PParameter parameter_aadl2aadlref = new PParameter("aadl2aadlref", "fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTraceSpec", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTraceSpec")), PParameterDirection.INOUT);
    
    private final PParameter parameter_system = new PParameter("system", "org.osate.aadl2.instance.SystemInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "SystemInstance")), PParameterDirection.INOUT);
    
    private final PParameter parameter_systemRef = new PParameter("systemRef", "org.osate.aadl2.instance.SystemInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "SystemInstance")), PParameterDirection.INOUT);
    
    private final List<PParameter> parameters = Arrays.asList(parameter_aadl2aadlref, parameter_system, parameter_systemRef);
    
    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }
    
    @Override
    public String getFullyQualifiedName() {
      return "fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.copy_system";
    }
    
    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("aadl2aadlref","system","systemRef");
    }
    
    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }
    
    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_aadl2aadlref = body.getOrCreateVariableByName("aadl2aadlref");
          PVariable var_system = body.getOrCreateVariableByName("system");
          PVariable var_systemRef = body.getOrCreateVariableByName("systemRef");
          new TypeConstraint(body, Tuples.flatTupleOf(var_aadl2aadlref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTraceSpec")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_system), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "SystemInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_systemRef), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "SystemInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_aadl2aadlref, parameter_aadl2aadlref),
             new ExportedParameter(body, var_system, parameter_system),
             new ExportedParameter(body, var_systemRef, parameter_systemRef)
          ));
          // 	Aadl2AadlTraceSpec.leftSystem(aadl2aadlref, system)
          new TypeConstraint(body, Tuples.flatTupleOf(var_aadl2aadlref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTraceSpec")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_aadl2aadlref, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTraceSpec", "leftSystem")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "SystemInstance")));
          new Equality(body, var__virtual_0_, var_system);
          // 	Aadl2AadlTraceSpec.rightSystem(aadl2aadlref, systemRef)
          new TypeConstraint(body, Tuples.flatTupleOf(var_aadl2aadlref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTraceSpec")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_aadl2aadlref, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.tpt.mem4csd.mtbench.aadl2aadl.trace/aadl2aadl", "Aadl2AadlTraceSpec", "rightSystem")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "SystemInstance")));
          new Equality(body, var__virtual_1_, var_systemRef);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
