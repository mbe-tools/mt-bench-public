package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra;

import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTraceSpec;
import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2aadlPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.viatra.query.runtime.api.IQueryGroup;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.IModelManipulations;
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.SimpleModelManipulations;
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRuleFactory;
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.osate.aadl2.Aadl2Package;
import org.osate.aadl2.instance.InstancePackage;

@SuppressWarnings("all")
public abstract class AbstractTransformation {
  @Extension
  public EventDrivenTransformationRuleFactory ruleFactory = new EventDrivenTransformationRuleFactory();
  
  @Extension
  public IQueryGroup queries;
  
  /**
   * EMF metamodels
   */
  @Extension
  public InstancePackage instPackage = InstancePackage.eINSTANCE;
  
  @Extension
  public Aadl2Package aadl2Package = Aadl2Package.eINSTANCE;
  
  @Extension
  public Aadl2aadlPackage trace = Aadl2aadlPackage.eINSTANCE;
  
  @Extension
  public IModelManipulations manipulation;
  
  public EventDrivenTransformation transformation;
  
  public ViatraQueryEngine engine;
  
  public Resource resource;
  
  public Aadl2AadlTraceSpec aadl2aadl;
  
  private boolean initialized = false;
  
  public boolean initialize(final Aadl2AadlTraceSpec aadl2aadl, final ViatraQueryEngine engine) {
    boolean _xifexpression = false;
    if ((!this.initialized)) {
      boolean _xblockexpression = false;
      {
        this.aadl2aadl = aadl2aadl;
        this.engine = engine;
        this.resource = aadl2aadl.getRightSystem().eResource();
        this.queries = this.setQueries();
        this.queries.prepare(engine);
        SimpleModelManipulations _simpleModelManipulations = new SimpleModelManipulations(engine);
        this.manipulation = _simpleModelManipulations;
        this.transformation = this.createTransformation(engine).build();
        _xblockexpression = this.initialized = true;
      }
      _xifexpression = _xblockexpression;
    }
    return _xifexpression;
  }
  
  public abstract EventDrivenTransformation.EventDrivenTransformationBuilder createTransformation(final ViatraQueryEngine engine);
  
  public abstract IQueryGroup setQueries();
  
  public void execute() {
    this.transformation.getExecutionSchema().startUnscheduledExecution();
  }
  
  public void dispose() {
    if ((this.transformation != null)) {
      this.transformation.getExecutionSchema().dispose();
      this.transformation = null;
    }
    return;
  }
}
