package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying;

import com.google.common.base.Objects;
import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTrace;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.AbstractTransformation;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.MScheduler;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copy_component;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copy_connection;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copy_connectionref;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copy_feature;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copy_system;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Copying_Required_queries;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.Is_in_trace_for_copy;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.viatra.query.runtime.api.IQueryGroup;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.transformation.evm.specific.Lifecycles;
import org.eclipse.viatra.transformation.evm.specific.crud.CRUDActivationStateEnum;
import org.eclipse.viatra.transformation.evm.specific.resolver.InvertedDisappearancePriorityConflictResolver;
import org.eclipse.viatra.transformation.runtime.emf.rules.EventDrivenTransformationRuleGroup;
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRule;
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRuleFactory;
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.SystemInstance;

@SuppressWarnings("all")
public class CopyingAadl2aadlTransformation extends AbstractTransformation {
  @Extension
  private EventDrivenTransformationRuleFactory ruleFactory = new EventDrivenTransformationRuleFactory();
  
  /**
   * VIATRA Query Pattern group
   */
  @Extension
  private Copying_Required_queries required_queries = Copying_Required_queries.instance();
  
  public EventDrivenTransformationRuleGroup getRules() {
    return new EventDrivenTransformationRuleGroup(
      this.system2system, 
      this.component2component, 
      this.feature2feature, 
      this.connection2connection, 
      this.connectionRef2connectionRef);
  }
  
  @Override
  public EventDrivenTransformation.EventDrivenTransformationBuilder createTransformation(final ViatraQueryEngine engine) {
    final InvertedDisappearancePriorityConflictResolver fixedPriorityResolver = new InvertedDisappearancePriorityConflictResolver();
    fixedPriorityResolver.setPriority(this.system2system.getRuleSpecification(), 1);
    fixedPriorityResolver.setPriority(this.component2component.getRuleSpecification(), 50);
    fixedPriorityResolver.setPriority(this.feature2feature.getRuleSpecification(), 200);
    fixedPriorityResolver.setPriority(this.connection2connection.getRuleSpecification(), 300);
    fixedPriorityResolver.setPriority(this.connectionRef2connectionRef.getRuleSpecification(), 300);
    final EventDrivenTransformation.EventDrivenTransformationBuilder builder = EventDrivenTransformation.forEngine(engine).setConflictResolver(fixedPriorityResolver).addRules(this.getRules());
    MScheduler.MSchedulerFactory _mSchedulerFactory = new MScheduler.MSchedulerFactory();
    builder.setSchedulerFactory(_mSchedulerFactory);
    return builder;
  }
  
  private final EventDrivenTransformationRule<Copy_system.Match, Copy_system.Matcher> system2system = this.ruleFactory.<Copy_system.Match, Copy_system.Matcher>createRule(Copy_system.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<Copy_system.Match>) (Copy_system.Match it) -> {
    try {
      this.manipulation.set(it.getSystemRef(), this.aadl2Package.getNamedElement_Name(), it.getSystem().getName());
      this.manipulation.set(it.getSystemRef(), this.instPackage.getComponentInstance_Category(), it.getSystem().getCategory());
      final SystemInstance source_system = it.getSystem();
      final SystemInstance target_system = it.getSystemRef();
      EObject _createChild = this.manipulation.createChild(this.aadl2aadl, this.trace.getAadl2AadlTraceSpec_Traces(), this.trace.getAadl2AadlTrace());
      final Procedure1<EObject> _function = (EObject it_1) -> {
        try {
          this.manipulation.addTo(it_1, this.trace.getAadl2AadlTrace_LeftInstance(), source_system);
          this.manipulation.addTo(it_1, this.trace.getAadl2AadlTrace_RightInstance(), target_system);
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      };
      ObjectExtensions.<EObject>operator_doubleArrow(_createChild, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<Copy_system.Match>) (Copy_system.Match it) -> {
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<Copy_system.Match>) (Copy_system.Match it) -> {
  })).addLifeCycle(
    Lifecycles.getDefault(true, true)).build();
  
  private final EventDrivenTransformationRule<Copy_component.Match, Copy_component.Matcher> component2component = this.ruleFactory.<Copy_component.Match, Copy_component.Matcher>createRule(Copy_component.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<Copy_component.Match>) (Copy_component.Match it) -> {
    try {
      final ComponentInstance leftsubcomponent = it.getSubcomponent();
      final EObject rightsubcomponent = this.manipulation.createChild(it.getComponentref(), this.instPackage.getComponentInstance_ComponentInstance(), this.instPackage.getComponentInstance());
      this.manipulation.set(rightsubcomponent, this.aadl2Package.getNamedElement_Name(), leftsubcomponent.getName());
      this.manipulation.set(rightsubcomponent, this.instPackage.getComponentInstance_Category(), leftsubcomponent.getCategory());
      EObject _createChild = this.manipulation.createChild(this.aadl2aadl, this.trace.getAadl2AadlTraceSpec_Traces(), this.trace.getAadl2AadlTrace());
      final Procedure1<EObject> _function = (EObject it_1) -> {
        try {
          this.manipulation.addTo(it_1, this.trace.getAadl2AadlTrace_LeftInstance(), leftsubcomponent);
          this.manipulation.addTo(it_1, this.trace.getAadl2AadlTrace_RightInstance(), rightsubcomponent);
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      };
      ObjectExtensions.<EObject>operator_doubleArrow(_createChild, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<Copy_component.Match>) (Copy_component.Match it) -> {
    try {
      final Is_in_trace_for_copy.Match traceMatch = this.required_queries.getIs_in_trace_for_copy(this.engine).getOneArbitraryMatch(this.aadl2aadl, null, it.getSubcomponent(), null).get();
      final String new_name = traceMatch.getAadlElement().getName();
      String _name = traceMatch.getAadlrefElement().getName();
      boolean _notEquals = (!Objects.equal(_name, new_name));
      if (_notEquals) {
        this.manipulation.set(traceMatch.getAadlrefElement(), this.aadl2Package.getNamedElement_Name(), new_name);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<Copy_component.Match>) (Copy_component.Match it) -> {
    try {
      final Is_in_trace_for_copy.Match traceMatch = this.required_queries.getIs_in_trace_for_copy(this.engine).getOneArbitraryMatch(this.aadl2aadl, null, it.getSubcomponent(), null).get();
      this.manipulation.remove(traceMatch.getAadlrefElement());
      this.manipulation.remove(this.aadl2aadl, this.trace.getAadl2AadlTraceSpec_Traces(), traceMatch.getTrace());
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();
  
  private final EventDrivenTransformationRule<Copy_feature.Match, Copy_feature.Matcher> feature2feature = this.ruleFactory.<Copy_feature.Match, Copy_feature.Matcher>createRule(Copy_feature.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<Copy_feature.Match>) (Copy_feature.Match it) -> {
    try {
      final FeatureInstance leftfeature = it.getFeature();
      final EObject rightfeature = this.manipulation.createChild(it.getComponentref(), this.instPackage.getComponentInstance_FeatureInstance(), this.instPackage.getFeatureInstance());
      this.manipulation.set(rightfeature, this.aadl2Package.getNamedElement_Name(), leftfeature.getName());
      this.manipulation.set(rightfeature, this.instPackage.getFeatureInstance_Category(), leftfeature.getCategory());
      this.manipulation.set(rightfeature, this.instPackage.getFeatureInstance_Direction(), leftfeature.getDirection());
      EObject _createChild = this.manipulation.createChild(this.aadl2aadl, this.trace.getAadl2AadlTraceSpec_Traces(), this.trace.getAadl2AadlTrace());
      final Procedure1<EObject> _function = (EObject it_1) -> {
        try {
          this.manipulation.addTo(it_1, this.trace.getAadl2AadlTrace_LeftInstance(), leftfeature);
          this.manipulation.addTo(it_1, this.trace.getAadl2AadlTrace_RightInstance(), rightfeature);
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      };
      ObjectExtensions.<EObject>operator_doubleArrow(_createChild, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<Copy_feature.Match>) (Copy_feature.Match it) -> {
    try {
      final Is_in_trace_for_copy.Match traceMatch = this.required_queries.getIs_in_trace_for_copy(this.engine).getOneArbitraryMatch(this.aadl2aadl, null, it.getFeature(), null).get();
      final String new_name = traceMatch.getAadlElement().getName();
      String _name = traceMatch.getAadlrefElement().getName();
      boolean _notEquals = (!Objects.equal(_name, new_name));
      if (_notEquals) {
        this.manipulation.set(traceMatch.getAadlrefElement(), this.aadl2Package.getNamedElement_Name(), new_name);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<Copy_feature.Match>) (Copy_feature.Match it) -> {
    try {
      final Is_in_trace_for_copy.Match traceMatch = this.required_queries.getIs_in_trace_for_copy(this.engine).getOneArbitraryMatch(this.aadl2aadl, null, it.getFeature(), null).get();
      this.manipulation.remove(traceMatch.getAadlrefElement());
      this.manipulation.remove(this.aadl2aadl, this.trace.getAadl2AadlTraceSpec_Traces(), traceMatch.getTrace());
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();
  
  private final EventDrivenTransformationRule<Copy_connection.Match, Copy_connection.Matcher> connection2connection = this.ruleFactory.<Copy_connection.Match, Copy_connection.Matcher>createRule(Copy_connection.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<Copy_connection.Match>) (Copy_connection.Match it) -> {
    try {
      final ConnectionInstance connectionleft = it.getConnection();
      final EObject connectionright = this.manipulation.createChild(it.getComponentref(), 
        this.instPackage.getComponentInstance_ConnectionInstance(), 
        this.instPackage.getConnectionInstance());
      this.manipulation.set(connectionright, this.aadl2Package.getNamedElement_Name(), it.getConnection().getName());
      this.manipulation.set(connectionright, this.instPackage.getConnectionInstance_Kind(), it.getConnection().getKind());
      this.manipulation.set(connectionright, this.instPackage.getConnectionInstance_Complete(), Boolean.valueOf(it.getConnection().isComplete()));
      this.manipulation.set(connectionright, this.instPackage.getConnectionInstance_Source(), it.getSourceref());
      this.manipulation.set(connectionright, this.instPackage.getConnectionInstance_Destination(), it.getDestinationref());
      EObject _createChild = this.manipulation.createChild(this.aadl2aadl, this.trace.getAadl2AadlTraceSpec_Traces(), this.trace.getAadl2AadlTrace());
      final Procedure1<EObject> _function = (EObject it_1) -> {
        try {
          this.manipulation.addTo(it_1, this.trace.getAadl2AadlTrace_LeftInstance(), connectionleft);
          this.manipulation.addTo(it_1, this.trace.getAadl2AadlTrace_RightInstance(), connectionright);
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      };
      ObjectExtensions.<EObject>operator_doubleArrow(_createChild, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<Copy_connection.Match>) (Copy_connection.Match it) -> {
    try {
      final Is_in_trace_for_copy.Match traceMatch = this.required_queries.getIs_in_trace_for_copy(this.engine).getOneArbitraryMatch(this.aadl2aadl, null, it.getConnection(), null).get();
      final String new_name = traceMatch.getAadlElement().getName();
      String _name = traceMatch.getAadlrefElement().getName();
      boolean _notEquals = (!Objects.equal(_name, new_name));
      if (_notEquals) {
        this.manipulation.set(traceMatch.getAadlrefElement(), this.aadl2Package.getNamedElement_Name(), new_name);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<Copy_connection.Match>) (Copy_connection.Match it) -> {
    try {
      final Set<Aadl2AadlTrace> traceMatch = this.required_queries.getIs_in_trace_for_copy(this.engine).getAllValuesOftrace(null, it.getConnection(), null);
      for (int i = 0; (i < ((Object[])Conversions.unwrapArray(traceMatch, Object.class)).length); i++) {
        {
          Aadl2AadlTrace _get = ((Aadl2AadlTrace[])Conversions.unwrapArray(traceMatch, Aadl2AadlTrace.class))[i];
          final Aadl2AadlTrace trace = ((Aadl2AadlTrace) _get);
          final InstanceObject element = IterableExtensions.<InstanceObject>head(trace.getRightInstance());
          this.manipulation.remove(element);
          this.manipulation.remove(trace);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();
  
  private final EventDrivenTransformationRule<Copy_connectionref.Match, Copy_connectionref.Matcher> connectionRef2connectionRef = this.ruleFactory.<Copy_connectionref.Match, Copy_connectionref.Matcher>createRule(Copy_connectionref.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<Copy_connectionref.Match>) (Copy_connectionref.Match it) -> {
    try {
      final EObject connectionRefright = this.manipulation.createChild(it.getConnectionref(), 
        this.instPackage.getConnectionInstance_ConnectionReference(), 
        this.instPackage.getConnectionReference());
      this.manipulation.set(connectionRefright, this.instPackage.getConnectionReference_Source(), it.getSourceref());
      this.manipulation.set(connectionRefright, this.instPackage.getConnectionReference_Destination(), it.getDestinationref());
      this.manipulation.set(connectionRefright, this.instPackage.getConnectionReference_Context(), it.getContextRref());
      final ConnectionReference connectionRefleft = it.getConnectionR();
      EObject _createChild = this.manipulation.createChild(this.aadl2aadl, this.trace.getAadl2AadlTraceSpec_Traces(), this.trace.getAadl2AadlTrace());
      final Procedure1<EObject> _function = (EObject it_1) -> {
        try {
          this.manipulation.addTo(it_1, this.trace.getAadl2AadlTrace_LeftInstance(), connectionRefleft);
          this.manipulation.addTo(it_1, this.trace.getAadl2AadlTrace_RightInstance(), connectionRefright);
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      };
      ObjectExtensions.<EObject>operator_doubleArrow(_createChild, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<Copy_connectionref.Match>) (Copy_connectionref.Match it) -> {
    try {
      final Is_in_trace_for_copy.Match traceMatch = this.required_queries.getIs_in_trace_for_copy(this.engine).getOneArbitraryMatch(this.aadl2aadl, null, it.getConnectionR(), null).get();
      final String new_name = traceMatch.getAadlElement().getName();
      String _name = traceMatch.getAadlrefElement().getName();
      boolean _notEquals = (!Objects.equal(_name, new_name));
      if (_notEquals) {
        this.manipulation.set(traceMatch.getAadlrefElement(), this.aadl2Package.getNamedElement_Name(), new_name);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<Copy_connectionref.Match>) (Copy_connectionref.Match it) -> {
    try {
      final Is_in_trace_for_copy.Match traceMatch = this.required_queries.getIs_in_trace_for_copy(this.engine).getOneArbitraryMatch(this.aadl2aadl, null, it.getConnectionR(), null).get();
      this.manipulation.remove(traceMatch.getAadlrefElement());
      this.manipulation.remove(this.aadl2aadl, this.trace.getAadl2AadlTraceSpec_Traces(), traceMatch.getTrace());
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();
  
  @Override
  public IQueryGroup setQueries() {
    return this.required_queries;
  }
}
