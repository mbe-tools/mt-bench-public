package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra

import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTraceSpec
import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2aadlPackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.IModelManipulations
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.SimpleModelManipulations
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRuleFactory
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation.EventDrivenTransformationBuilder
import org.osate.aadl2.instance.InstancePackage
import org.osate.aadl2.Aadl2Package
import org.eclipse.viatra.query.runtime.api.IQueryGroup

abstract class AbstractTransformation {

	public extension EventDrivenTransformationRuleFactory ruleFactory = new EventDrivenTransformationRuleFactory
	public extension IQueryGroup queries

	/** EMF metamodels **/
	public extension InstancePackage instPackage = InstancePackage::eINSTANCE
	public extension Aadl2Package aadl2Package = Aadl2Package::eINSTANCE
	public extension Aadl2aadlPackage trace = Aadl2aadlPackage::eINSTANCE

	public extension IModelManipulations manipulation

	public EventDrivenTransformation transformation
	public ViatraQueryEngine engine
	public Resource resource
	public Aadl2AadlTraceSpec aadl2aadl

	var initialized = false;

	def initialize(
		Aadl2AadlTraceSpec aadl2aadl,
		ViatraQueryEngine engine
	) {
		if (!initialized) {
			this.aadl2aadl = aadl2aadl
			this.engine = engine
			resource = aadl2aadl.rightSystem.eResource
			queries = setQueries()
			prepare(engine)
			this.manipulation = new SimpleModelManipulations(engine)

			transformation = createTransformation(engine).build

			initialized = true
		}
	}

	def EventDrivenTransformationBuilder createTransformation(ViatraQueryEngine engine);
	def IQueryGroup setQueries();
	
	def execute() {

		transformation.executionSchema.startUnscheduledExecution

	}
	def dispose() {
		if (transformation !== null) {
			transformation.executionSchema.dispose
			transformation = null
		}

		return
	}

}
