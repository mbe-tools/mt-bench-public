package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra

import fr.tpt.mem4csd.mtbench.aadl2aadl.trace.aadl2aadl.Aadl2AadlTrace
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.MScheduler.MSchedulerFactory
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine
import org.eclipse.viatra.transformation.evm.specific.Lifecycles
import org.eclipse.viatra.transformation.evm.specific.crud.CRUDActivationStateEnum
import org.eclipse.viatra.transformation.evm.specific.resolver.InvertedDisappearancePriorityConflictResolver
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation.EventDrivenTransformationBuilder
import org.osate.aadl2.ComponentCategory
import org.osate.aadl2.DirectionType
import org.osate.aadl2.instance.ConnectionKind
import org.osate.aadl2.instance.FeatureCategory
import org.osate.aadl2.instance.FeatureInstance

class Aadl2aadlTransformation extends AbstractTransformation {
	extension Required_queries required_queries = Required_queries.instance

	val system2system = createRule(Find_system.instance).action(CRUDActivationStateEnum.CREATED) [
		systemRef.set(namedElement_Name, system.name)
		systemRef.set(componentInstance_Category, ComponentCategory.SYSTEM)
		val source_system = system
		val target_system = systemRef

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, source_system)
			addTo(aadl2AadlTrace_RightInstance, target_system)
		]

	].action(CRUDActivationStateEnum.UPDATED)[].action(CRUDActivationStateEnum.DELETED)[].addLifeCycle(
		Lifecycles.getDefault(true, true)).build

	val component2component = createRule(Find_component.instance).action(CRUDActivationStateEnum.CREATED) [
		val leftsubcomponent = subcomponent
		val rightsubcomponent = componentref.createChild(componentInstance_ComponentInstance, componentInstance)
		rightsubcomponent.set(namedElement_Name, leftsubcomponent.name)
		rightsubcomponent.set(componentInstance_Category, leftsubcomponent.category)

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, leftsubcomponent)
			addTo(aadl2AadlTrace_RightInstance, rightsubcomponent)
		]
	].action(CRUDActivationStateEnum.UPDATED) [
		val traceMatch = engine.is_in_trace.getOneArbitraryMatch(aadl2aadl, null, subcomponent, null).get

		val new_name = traceMatch.aadlElement.name
		if (traceMatch.aadlrefElement.name != new_name) {
			traceMatch.aadlrefElement.set(namedElement_Name, new_name)
		}

	].action(CRUDActivationStateEnum.DELETED) [
		val traceMatch = engine.is_in_trace.getOneArbitraryMatch(aadl2aadl, null, subcomponent, null).get
		traceMatch.aadlrefElement.remove
		aadl2aadl.remove(aadl2AadlTraceSpec_Traces, traceMatch.trace)

	].addLifeCycle(Lifecycles.getDefault(true, true)).build

	val feature2feature = createRule(Find_feature.instance).action(CRUDActivationStateEnum.CREATED) [
		val leftfeature = feature
		val rightfeature = componentref.createChild(componentInstance_FeatureInstance, featureInstance)
		rightfeature.set(namedElement_Name, leftfeature.name)
		rightfeature.set(featureInstance_Category, leftfeature.category)
		rightfeature.set(featureInstance_Direction, leftfeature.direction)

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, leftfeature)
			addTo(aadl2AadlTrace_RightInstance, rightfeature)
		]
	].action(CRUDActivationStateEnum.UPDATED) [
		val traceMatch = engine.is_in_trace.getOneArbitraryMatch(aadl2aadl, null, feature, null).get

		val new_name = traceMatch.aadlElement.name
		if (traceMatch.aadlrefElement.name != new_name) {
			traceMatch.aadlrefElement.set(namedElement_Name, new_name)
		}

	].action(CRUDActivationStateEnum.DELETED) [
		val traceMatch = engine.is_in_trace.getOneArbitraryMatch(aadl2aadl, null, feature, null).get
		traceMatch.aadlrefElement.remove
		aadl2aadl.remove(aadl2AadlTraceSpec_Traces, traceMatch.trace)

	].addLifeCycle(Lifecycles.getDefault(true, true)).build

	val portconnection2dataaccessAtprocess = createRule(Find_portconnection_process.instance).action(
		CRUDActivationStateEnum.CREATED) [

		val dataInstance = processref.createChild(componentInstance_ComponentInstance, componentInstance)
		dataInstance.set(componentInstance_Category, ComponentCategory.DATA)
		dataInstance.set(namedElement_Name, portconnection.name)

		val connection_out = processref.createChild(componentInstance_ConnectionInstance, connectionInstance)
		connection_out.set(connectionInstance_Kind, ConnectionKind.ACCESS_CONNECTION)
		connection_out.set(connectionInstance_Complete, true)

		val connection_in = processref.createChild(componentInstance_ConnectionInstance, connectionInstance)
		connection_in.set(connectionInstance_Kind, ConnectionKind.ACCESS_CONNECTION)
		connection_in.set(connectionInstance_Complete, true)

		val feature_out_match = engine.is_source.getOneArbitraryMatch(aadl2aadl, null, featuresource, null, null, null)
		val feature_in_match = engine.is_destination.getOneArbitraryMatch(aadl2aadl, null, featuredestination, null,
			null, null)
		var FeatureInstance feature_out
		var FeatureInstance feature_in
		val featuresource = featuresource
		val featuredestination = featuredestination
		val portconnection = portconnection

		connection_out.set(namedElement_Name, "1_" + portconnection.name)
		connection_in.set(namedElement_Name, "2_" + portconnection.name)

		if (feature_out_match.isPresent) {
			feature_out = feature_out_match.get.sourceref as FeatureInstance

		} else {
			feature_out = threadsourceref.createChild(componentInstance_FeatureInstance,
				featureInstance) as FeatureInstance
			feature_out.set(namedElement_Name, featuresource.name)
			feature_out.set(featureInstance_Direction, DirectionType.OUT)
			feature_out.set(featureInstance_Category, FeatureCategory.DATA_ACCESS)
		}

		if (feature_in_match.isPresent) {
			feature_in = feature_in_match.get.destinationref as FeatureInstance
		} else {
			feature_in = threaddestinationref.createChild(componentInstance_FeatureInstance,
				featureInstance) as FeatureInstance
			feature_in.set(namedElement_Name, featuredestination.name)
			feature_in.set(featureInstance_Direction, DirectionType.IN)
			feature_in.set(featureInstance_Category, FeatureCategory.DATA_ACCESS)
		}

		val connref_in = connection_in.createChild(connectionInstance_ConnectionReference, connectionReference)
		connref_in.set(connectionReference_Context, processref)
		connref_in.set(connectionReference_Source, dataInstance)
		connref_in.set(connectionReference_Destination, feature_in)
		connection_in.set(connectionInstance_Source, dataInstance)
		connection_in.set(connectionInstance_Destination, feature_in)

		val connref_out = connection_out.createChild(connectionInstance_ConnectionReference, connectionReference)
		connref_out.set(connectionReference_Context, processref)
		connref_out.set(connectionReference_Source, feature_out)
		connref_out.set(connectionReference_Destination, dataInstance)
		connection_out.set(connectionInstance_Source, feature_out)
		connection_out.set(connectionInstance_Destination, dataInstance)

		val featuresourceleft = feature_out
		val featuredestinationleft = feature_in

		// Traces
		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, portconnection)
			addTo(aadl2AadlTrace_RightInstance, dataInstance)
		]

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, portconnection)
			addTo(aadl2AadlTrace_RightInstance, connection_in)
		]
		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, portconnection)
			addTo(aadl2AadlTrace_RightInstance, connection_out)
		]

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, featuresource)
			addTo(aadl2AadlTrace_RightInstance, featuresourceleft)
		]
		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, featuredestination)
			addTo(aadl2AadlTrace_RightInstance, featuredestinationleft)
		]

	].action(CRUDActivationStateEnum.UPDATED) [
		val traceMatch = engine.is_in_trace.getAllValuesOftrace(null, portconnection, null)

		for (var i = 0; i < traceMatch.length; i++) {
			val trace = traceMatch.get(i) as Aadl2AadlTrace
			val element = trace.leftInstance.head
			val new_name = element.name
			if (trace.rightInstance.head.name != new_name) {
				trace.rightInstance.head.set(namedElement_Name, new_name)
			}
		}

	].action(CRUDActivationStateEnum.DELETED) [
		val traceMatch = engine.is_in_trace.getAllValuesOftrace(null, portconnection, null)
		for (var i = 0; i < traceMatch.length; i++) {
			val trace = traceMatch.get(i) as Aadl2AadlTrace
			val element = trace.rightInstance.head
			element.remove
			trace.remove
		}

	].addLifeCycle(Lifecycles.getDefault(true, true)).build

	val portConnection2DataAccessatSystem = createRule(Find_portconnection_system.instance).action(
		CRUDActivationStateEnum.CREATED) [
		val newportconnection = portconnection

		val dataInstance1 = processsourceref.createChild(componentInstance_ComponentInstance, componentInstance)
		dataInstance1.set(componentInstance_Category, ComponentCategory.DATA)
		dataInstance1.set(namedElement_Name, portconnection.name)

		val connection_out = processsourceref.createChild(componentInstance_ConnectionInstance, connectionInstance)
		connection_out.set(connectionInstance_Kind, ConnectionKind.ACCESS_CONNECTION)
		connection_out.set(namedElement_Name, portconnection.name)
		connection_out.set(connectionInstance_Complete, true)

		var FeatureInstance feature_out
		// check if the function has already been transformed 
		val feature_out_match = engine.is_source.getOneArbitraryMatch(aadl2aadl, null, featuresource, null, null, null)
		if (feature_out_match.isPresent) {
			feature_out = feature_out_match.get.sourceref as FeatureInstance
		} else {
			feature_out = threadsourceref.createChild(componentInstance_FeatureInstance,
				featureInstance) as FeatureInstance
			feature_out.set(namedElement_Name, featuresource.name)
			feature_out.set(featureInstance_Direction, DirectionType.OUT)
			feature_out.set(featureInstance_Category, FeatureCategory.DATA_ACCESS)
		}
		connection_out.set(connectionInstance_Source, feature_out)
		connection_out.set(connectionInstance_Destination, dataInstance1)
		// ...
		val feature_in_match = engine.is_destination.getOneArbitraryMatch(aadl2aadl, null, featuredestination, null,
			null, null)
		var FeatureInstance feature_in
		val dataInstance2 = processdestinationref.createChild(componentInstance_ComponentInstance, componentInstance)
		dataInstance2.set(componentInstance_Category, ComponentCategory.DATA)
		dataInstance2.set(namedElement_Name, portconnection.name)

		val connection_in = processdestinationref.createChild(componentInstance_ConnectionInstance, connectionInstance)
		connection_in.set(connectionInstance_Kind, ConnectionKind.ACCESS_CONNECTION)
		connection_in.set(namedElement_Name, portconnection.name)
		connection_in.set(connectionInstance_Complete, true)

		if (feature_in_match.isPresent) {
			feature_in = feature_in_match.get.destinationref as FeatureInstance
		} else {
			feature_in = threaddestinationref.createChild(componentInstance_FeatureInstance,
				featureInstance) as FeatureInstance
			feature_in.set(namedElement_Name, featuredestination.name)
			feature_in.set(featureInstance_Direction, DirectionType.IN)
			feature_in.set(featureInstance_Category, FeatureCategory.DATA_ACCESS)
		}

		val connref_in = connection_in.createChild(connectionInstance_ConnectionReference, connectionReference)
		connref_in.set(connectionReference_Context, processdestinationref)
		connref_in.set(connectionReference_Source, dataInstance2)
		connref_in.set(connectionReference_Destination, feature_in)

		connection_in.set(connectionInstance_Source, dataInstance2)
		connection_in.set(connectionInstance_Destination, feature_in)

		val connref_out = connection_out.createChild(connectionInstance_ConnectionReference, connectionReference)
		connref_out.set(connectionReference_Context, processsourceref)
		connref_out.set(connectionReference_Source, feature_out)
		connref_out.set(connectionReference_Destination, dataInstance1)

		// Traces
		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, newportconnection)
			addTo(aadl2AadlTrace_RightInstance, dataInstance1)
		]

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, newportconnection)
			addTo(aadl2AadlTrace_RightInstance, dataInstance2)
		]

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, newportconnection)
			addTo(aadl2AadlTrace_RightInstance, connection_in)
		]
		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, newportconnection)
			addTo(aadl2AadlTrace_RightInstance, connection_out)
		]

		val featuredestinationright = feature_in
		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, newportconnection)
			addTo(aadl2AadlTrace_RightInstance, featuredestinationright)
		]
		val featuresourceright = feature_out

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, newportconnection)
			addTo(aadl2AadlTrace_RightInstance, featuresourceright)
		]

	].action(CRUDActivationStateEnum.UPDATED) [
		val traceMatch = engine.is_in_trace.getAllValuesOftrace(null, portconnection, null)
		for (var i = 0; i < traceMatch.length; i++) {
			val trace = traceMatch.get(i) as Aadl2AadlTrace
			val element = trace.leftInstance.head
			val refelement = trace.rightInstance.head
			if ((refelement.eClass.name.equals("ComponentInstance")) ||
				(refelement.eClass.name.equals("ConnectionInstance"))) {
				val new_name = element.name
				if (refelement.name != new_name) {
					refelement.set(namedElement_Name, new_name)
				}
			}
		}
	].action(CRUDActivationStateEnum.DELETED) [
		val traceMatch = engine.is_in_trace.getAllValuesOftrace(aadl2aadl, portconnection, null)
		for (var i = 0; i < traceMatch.length; i++) {
			val trace = traceMatch.get(i) as Aadl2AadlTrace
			val element = trace.rightInstance.head
			if (element.eClass.name.equals("FeatureInstance")) {
				val feature_match = engine.is_in_trace.getAllValuesOftrace(null, null, element)
				if (feature_match.length > 1) {
					trace.remove
				} else {
					element.remove
					trace.remove
				}
			} else {
				element.remove
				trace.remove
			}
		}

	].addLifeCycle(Lifecycles.getDefault(true, true)).build

	val connectionRef2connectionRef = createRule(Find_connectionref.instance).action(CRUDActivationStateEnum.CREATED) [

		val connectionRefright = connectionref.createChild(
			connectionInstance_ConnectionReference,
			connectionReference
		)
		connectionRefright.set(namedElement_Name, connectionR.name)
		connectionRefright.set(connectionReference_Source, source)
		connectionRefright.set(connectionReference_Destination, destinationref)
		connectionRefright.set(connectionReference_Context, contextRref)

		val connectionRefleft = connectionR

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, connectionRefleft)
			addTo(aadl2AadlTrace_RightInstance, connectionRefright)
		]

	].action(CRUDActivationStateEnum.UPDATED) [
		val traceMatch = engine.is_in_trace.getOneArbitraryMatch(aadl2aadl, null, connectionR, null).get
		val new_name = traceMatch.aadlElement.name
		if (traceMatch.aadlrefElement.name != new_name) {
			traceMatch.aadlrefElement.set(namedElement_Name, new_name)
		}

	].action(CRUDActivationStateEnum.DELETED) [
		val traceMatch = engine.is_in_trace.getOneArbitraryMatch(aadl2aadl, null, connectionR, null).get
		traceMatch.aadlrefElement.remove
		aadl2aadl.remove(aadl2AadlTraceSpec_Traces, traceMatch.trace)

	].addLifeCycle(Lifecycles.getDefault(true, true)).build

	val connection2connection = createRule(Find_otherconnection.instance).action(CRUDActivationStateEnum.CREATED) [
		val connectionright = componentref.createChild(
			componentInstance_ConnectionInstance,
			connectionInstance
		)
		connectionright.set(namedElement_Name, connection.name)
		connectionright.set(connectionInstance_Kind, connection.kind)
		connectionright.set(connectionInstance_Complete, connection.complete)

		connectionright.set(connectionInstance_Source, sourceref)
		connectionright.set(connectionInstance_Destination, destinationref)

		val connectionleft = connection

		aadl2aadl.createChild(aadl2AadlTraceSpec_Traces, aadl2AadlTrace) => [
			addTo(aadl2AadlTrace_LeftInstance, connectionleft)
			addTo(aadl2AadlTrace_RightInstance, connectionright)
		]

	].action(CRUDActivationStateEnum.UPDATED) [
		val traceMatch = engine.is_in_trace.getOneArbitraryMatch(aadl2aadl, null, connection, null).get
		val new_name = traceMatch.aadlElement.name
		if (traceMatch.aadlrefElement.name != new_name) {
			traceMatch.aadlrefElement.set(namedElement_Name, new_name)
		}

	].action(CRUDActivationStateEnum.DELETED) [
		val traceMatch = engine.is_in_trace.getOneArbitraryMatch(aadl2aadl, null, connection, null).get
		traceMatch.aadlrefElement.remove
		aadl2aadl.remove(aadl2AadlTraceSpec_Traces, traceMatch.trace)

	].addLifeCycle(Lifecycles.getDefault(true, true)).build

	override EventDrivenTransformationBuilder createTransformation(ViatraQueryEngine engine) {
		val fixedPriorityResolver = new InvertedDisappearancePriorityConflictResolver
		fixedPriorityResolver.setPriority(system2system.ruleSpecification, 1)
		fixedPriorityResolver.setPriority(component2component.ruleSpecification, 1)
		fixedPriorityResolver.setPriority(feature2feature.ruleSpecification, 1)
		fixedPriorityResolver.setPriority(portConnection2DataAccessatSystem.ruleSpecification, 10)
		fixedPriorityResolver.setPriority(portconnection2dataaccessAtprocess.ruleSpecification, 10)
		fixedPriorityResolver.setPriority(connection2connection.ruleSpecification, 10)
		fixedPriorityResolver.setPriority(connectionRef2connectionRef.ruleSpecification, 20)

		val builder = EventDrivenTransformation.forEngine(engine).setConflictResolver(fixedPriorityResolver).addRule(
			system2system).addRule(component2component).addRule(feature2feature).addRule(
			portconnection2dataaccessAtprocess).addRule(portConnection2DataAccessatSystem).addRule(
			connection2connection).addRule(connectionRef2connectionRef)

		builder.schedulerFactory = new MSchedulerFactory()

		return builder;
	}

	override setQueries() {
		return required_queries
	}

}
