package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.tests;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.Aadl2aadlTransformation;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.AbstractTransformation;

public class RefinementBatch extends AbstractViatraScenario {

	public RefinementBatch() {
		super();
	}

	@Override
	AbstractTransformation getSpecification() {
		return new Aadl2aadlTransformation();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.BATCH;
	}
	
	@Override
	String getExtension() {
		return BenchmarkConstants.REFINEMENT_BATCH_SUFFIX;
	}
}
