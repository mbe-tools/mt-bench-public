package fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.tests;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.AbstractTransformation;
import fr.tpt.mem4csd.mtbench.aadl2aadl.viatra.copying.CopyingAadl2aadlTransformation;

public class CopyingAddition extends AbstractViatraScenario {

	public CopyingAddition() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.ADDITION;
	}

	@Override
	AbstractTransformation getSpecification() {
		return new CopyingAadl2aadlTransformation();
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.COPY_ADD_SUFFIX;
	}

}
