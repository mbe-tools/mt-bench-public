/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.impl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.Aadl2aadlPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.CorrConnection2DataAccess;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Connection2 Data Access</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrConnection2DataAccessImpl extends CorrFeature2FeatureImpl implements CorrConnection2DataAccess {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrConnection2DataAccessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Aadl2aadlPackage.Literals.CORR_CONNECTION2_DATA_ACCESS;
	}

} //CorrConnection2DataAccessImpl
