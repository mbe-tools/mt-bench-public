/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.impl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Aadl2aadlFactoryImpl extends EFactoryImpl implements Aadl2aadlFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Aadl2aadlFactory init() {
		try {
			Aadl2aadlFactory theAadl2aadlFactory = (Aadl2aadlFactory) EPackage.Registry.INSTANCE
					.getEFactory(Aadl2aadlPackage.eNS_URI);
			if (theAadl2aadlFactory != null) {
				return theAadl2aadlFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Aadl2aadlFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aadl2aadlFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case Aadl2aadlPackage.CORR_AXIOM:
			return createCorrAxiom();
		case Aadl2aadlPackage.CORR_COMPONENT2_COMPONENT:
			return createCorrComponent2Component();
		case Aadl2aadlPackage.CORR_CONNECTION2_DATA_ACCESS:
			return createCorrConnection2DataAccess();
		case Aadl2aadlPackage.CORR_REF2_REF:
			return createCorrRef2Ref();
		case Aadl2aadlPackage.CORR_CONN2_CONN:
			return createCorrConn2Conn();
		case Aadl2aadlPackage.CORR_CONNECTION2_DATA_ACCESS_SYS:
			return createCorrConnection2DataAccessSys();
		case Aadl2aadlPackage.CORR_FEATURE2_FEATURE:
			return createCorrFeature2Feature();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrAxiom createCorrAxiom() {
		CorrAxiomImpl corrAxiom = new CorrAxiomImpl();
		return corrAxiom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrComponent2Component createCorrComponent2Component() {
		CorrComponent2ComponentImpl corrComponent2Component = new CorrComponent2ComponentImpl();
		return corrComponent2Component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrConnection2DataAccess createCorrConnection2DataAccess() {
		CorrConnection2DataAccessImpl corrConnection2DataAccess = new CorrConnection2DataAccessImpl();
		return corrConnection2DataAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrRef2Ref createCorrRef2Ref() {
		CorrRef2RefImpl corrRef2Ref = new CorrRef2RefImpl();
		return corrRef2Ref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrConn2Conn createCorrConn2Conn() {
		CorrConn2ConnImpl corrConn2Conn = new CorrConn2ConnImpl();
		return corrConn2Conn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrConnection2DataAccessSys createCorrConnection2DataAccessSys() {
		CorrConnection2DataAccessSysImpl corrConnection2DataAccessSys = new CorrConnection2DataAccessSysImpl();
		return corrConnection2DataAccessSys;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrFeature2Feature createCorrFeature2Feature() {
		CorrFeature2FeatureImpl corrFeature2Feature = new CorrFeature2FeatureImpl();
		return corrFeature2Feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aadl2aadlPackage getAadl2aadlPackage() {
		return (Aadl2aadlPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Aadl2aadlPackage getPackage() {
		return Aadl2aadlPackage.eINSTANCE;
	}

} //Aadl2aadlFactoryImpl
