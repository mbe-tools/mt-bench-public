/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Ref2 Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.Aadl2aadlPackage#getCorrRef2Ref()
 * @model
 * @generated
 */
public interface CorrRef2Ref extends CorrFeature2Feature {
} // CorrRef2Ref
