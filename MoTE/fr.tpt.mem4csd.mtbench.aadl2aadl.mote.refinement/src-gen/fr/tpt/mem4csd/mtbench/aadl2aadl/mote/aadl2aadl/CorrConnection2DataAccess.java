/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Connection2 Data Access</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.Aadl2aadlPackage#getCorrConnection2DataAccess()
 * @model
 * @generated
 */
public interface CorrConnection2DataAccess extends CorrFeature2Feature {
} // CorrConnection2DataAccess
