/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated;

import de.mdelab.mltgg.mote2.sdm.SdmOperationalTGG;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>aadl2aadl Operational TGG</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage#getaadl2aadlOperationalTGG()
 * @model
 * @generated
 */
public interface aadl2aadlOperationalTGG extends SdmOperationalTGG {
} // aadl2aadlOperationalTGG
