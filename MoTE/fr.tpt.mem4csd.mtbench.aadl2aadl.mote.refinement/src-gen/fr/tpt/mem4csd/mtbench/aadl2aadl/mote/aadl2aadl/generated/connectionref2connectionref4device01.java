/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated;

import de.mdelab.mltgg.mote2.operationalTGG.OperationalRuleGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>connectionref2connectionref4device01</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage#getconnectionref2connectionref4device01()
 * @model
 * @generated
 */
public interface connectionref2connectionref4device01 extends OperationalRuleGroup {
} // connectionref2connectionref4device01
