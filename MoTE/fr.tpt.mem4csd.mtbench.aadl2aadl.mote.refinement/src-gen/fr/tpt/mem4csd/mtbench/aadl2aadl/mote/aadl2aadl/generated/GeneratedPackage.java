/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated;

import de.mdelab.mltgg.mote2.operationalTGG.OperationalTGGPackage;

import de.mdelab.mltgg.mote2.sdm.SdmPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedFactory
 * @model kind="package"
 * @generated
 */
public interface GeneratedPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "generated";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://mem4csd.telecom-paris.fr/mtbench.aadl2aadl/generated";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "aadl2aadl.generated";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GeneratedPackage eINSTANCE = fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.aadl2aadlOperationalTGGImpl <em>aadl2aadl Operational TGG</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.aadl2aadlOperationalTGGImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getaadl2aadlOperationalTGG()
	 * @generated
	 */
	int AADL2AADL_OPERATIONAL_TGG = 0;

	/**
	 * The feature id for the '<em><b>Operational Axiom Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG__OPERATIONAL_AXIOM_GROUP = SdmPackage.SDM_OPERATIONAL_TGG__OPERATIONAL_AXIOM_GROUP;

	/**
	 * The feature id for the '<em><b>Operational Rule Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG__OPERATIONAL_RULE_GROUPS = SdmPackage.SDM_OPERATIONAL_TGG__OPERATIONAL_RULE_GROUPS;

	/**
	 * The feature id for the '<em><b>Tgg Engine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG__TGG_ENGINE = SdmPackage.SDM_OPERATIONAL_TGG__TGG_ENGINE;

	/**
	 * The feature id for the '<em><b>Tgg ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG__TGG_ID = SdmPackage.SDM_OPERATIONAL_TGG__TGG_ID;

	/**
	 * The feature id for the '<em><b>Unidirectional References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG__UNIDIRECTIONAL_REFERENCES = SdmPackage.SDM_OPERATIONAL_TGG__UNIDIRECTIONAL_REFERENCES;

	/**
	 * The feature id for the '<em><b>Interpreter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG__INTERPRETER = SdmPackage.SDM_OPERATIONAL_TGG__INTERPRETER;

	/**
	 * The number of structural features of the '<em>aadl2aadl Operational TGG</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG_FEATURE_COUNT = SdmPackage.SDM_OPERATIONAL_TGG_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Transformation Started</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG___TRANSFORMATION_STARTED = SdmPackage.SDM_OPERATIONAL_TGG___TRANSFORMATION_STARTED;

	/**
	 * The operation id for the '<em>Transformation Finished</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG___TRANSFORMATION_FINISHED = SdmPackage.SDM_OPERATIONAL_TGG___TRANSFORMATION_FINISHED;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG___INIT = SdmPackage.SDM_OPERATIONAL_TGG___INIT;

	/**
	 * The operation id for the '<em>Get Operational Mapping Group</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG___GET_OPERATIONAL_MAPPING_GROUP__STRING = SdmPackage.SDM_OPERATIONAL_TGG___GET_OPERATIONAL_MAPPING_GROUP__STRING;

	/**
	 * The operation id for the '<em>Execute Activity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG___EXECUTE_ACTIVITY__ACTIVITY_EMAP = SdmPackage.SDM_OPERATIONAL_TGG___EXECUTE_ACTIVITY__ACTIVITY_EMAP;

	/**
	 * The number of operations of the '<em>aadl2aadl Operational TGG</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AADL2AADL_OPERATIONAL_TGG_OPERATION_COUNT = SdmPackage.SDM_OPERATIONAL_TGG_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.moteAxiomImpl <em>mote Axiom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.moteAxiomImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getmoteAxiom()
	 * @generated
	 */
	int MOTE_AXIOM = 1;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Axioms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM__AXIOMS = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP__AXIOMS;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP__OPERATIONAL_TGG;

	/**
	 * The number of structural features of the '<em>mote Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>mote Axiom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_AXIOM_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device10Impl <em>connectionref2connectionref4device10</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device10Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref4device10()
	 * @generated
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10 = 2;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>connectionref2connectionref4device10</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>connectionref2connectionref4device10</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.portconnection2portconnection4deviceDestinationImpl <em>portconnection2portconnection4device Destination</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.portconnection2portconnection4deviceDestinationImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getportconnection2portconnection4deviceDestination()
	 * @generated
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION = 3;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>portconnection2portconnection4device Destination</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>portconnection2portconnection4device Destination</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device01Impl <em>connectionref2connectionref4device01</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device01Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref4device01()
	 * @generated
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01 = 4;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>connectionref2connectionref4device01</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>connectionref2connectionref4device01</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.feature2featureImpl <em>feature2feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.feature2featureImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getfeature2feature()
	 * @generated
	 */
	int FEATURE2FEATURE = 5;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>feature2feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>feature2feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess01Impl <em>Port Connection2 Data Access01</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess01Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccess01()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS01 = 6;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access01</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access01</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionrefImpl <em>connectionref2connectionref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionrefImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref()
	 * @generated
	 */
	int CONNECTIONREF2CONNECTIONREF = 7;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>connectionref2connectionref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>connectionref2connectionref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys01Impl <em>Port Connection2 Data Access Sys01</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys01Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccessSys01()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01 = 8;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access Sys01</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access Sys01</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device11Impl <em>connectionref2connectionref4device11</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device11Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref4device11()
	 * @generated
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11 = 9;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>connectionref2connectionref4device11</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>connectionref2connectionref4device11</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys00Impl <em>Port Connection2 Data Access Sys00</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys00Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccessSys00()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00 = 10;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access Sys00</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access Sys00</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess11Impl <em>Port Connection2 Data Access11</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess11Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccess11()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS11 = 11;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access11</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access11</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess10Impl <em>Port Connection2 Data Access10</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess10Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccess10()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS10 = 12;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access10</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access10</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.portconnection2portconnection4deviceSourceImpl <em>portconnection2portconnection4device Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.portconnection2portconnection4deviceSourceImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getportconnection2portconnection4deviceSource()
	 * @generated
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE = 13;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>portconnection2portconnection4device Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>portconnection2portconnection4device Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connection2connectionImpl <em>connection2connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connection2connectionImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getconnection2connection()
	 * @generated
	 */
	int CONNECTION2CONNECTION = 14;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>connection2connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>connection2connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys11Impl <em>Port Connection2 Data Access Sys11</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys11Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccessSys11()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11 = 15;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access Sys11</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access Sys11</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess00Impl <em>Port Connection2 Data Access00</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess00Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccess00()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS00 = 16;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access00</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access00</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.subcomponent2subcomponentImpl <em>subcomponent2subcomponent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.subcomponent2subcomponentImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getsubcomponent2subcomponent()
	 * @generated
	 */
	int SUBCOMPONENT2SUBCOMPONENT = 17;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>subcomponent2subcomponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>subcomponent2subcomponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys10Impl <em>Port Connection2 Data Access Sys10</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys10Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccessSys10()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10 = 18;

	/**
	 * The feature id for the '<em><b>Rule Group ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10__RULE_GROUP_ID = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULE_GROUP_ID;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10__RULES = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__RULES;

	/**
	 * The feature id for the '<em><b>Operational TGG</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10__OPERATIONAL_TGG = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__OPERATIONAL_TGG;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE_GROUP__ENABLED;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access Sys10</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_FEATURE_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Get Operational Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10___GET_OPERATIONAL_MAPPING__STRING = OperationalTGGPackage.OPERATIONAL_RULE_GROUP___GET_OPERATIONAL_MAPPING__STRING;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access Sys10</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_GROUP_OPERATION_COUNT
			+ 0;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.moteAxiom_r0Impl <em>mote Axiom r0</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.moteAxiom_r0Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getmoteAxiom_r0()
	 * @generated
	 */
	int MOTE_AXIOM_R0 = 19;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_AXIOM__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__RULE_ID = OperationalTGGPackage.OPERATIONAL_AXIOM__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Axiom Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__OPERATIONAL_AXIOM_GROUP = OperationalTGGPackage.OPERATIONAL_AXIOM__OPERATIONAL_AXIOM_GROUP;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>mote Axiom r0</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_AXIOM_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_AXIOM___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___ADD_ELEMENT__EMAP_ELIST_ELIST = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___TRANSFORM_FORWARD__ELIST_ELIST_BOOLEAN = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___TRANSFORM_MAPPING__ELIST_ELIST_BOOLEAN = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___TRANSFORM_BACKWARD__ELIST_ELIST_BOOLEAN = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___SYNCHRONIZE_FORWARD__ELIST_ELIST_TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0___SYNCHRONIZE_BACKWARD__ELIST_ELIST_TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT
			+ 6;

	/**
	 * The number of operations of the '<em>mote Axiom r0</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTE_AXIOM_R0_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_AXIOM_OPERATION_COUNT + 7;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess01_r401Impl <em>Port Connection2 Data Access01 r401</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess01_r401Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401 = 20;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access01 r401</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access01 r401</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS01_R401_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connection2connection_r90Impl <em>connection2connection r90</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connection2connection_r90Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getconnection2connection_r90()
	 * @generated
	 */
	int CONNECTION2CONNECTION_R90 = 21;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>connection2connection r90</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>connection2connection r90</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION2CONNECTION_R90_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.portconnection2portconnection4deviceDestination_r90DDImpl <em>portconnection2portconnection4device Destination r90 DD</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.portconnection2portconnection4deviceDestination_r90DDImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD = 22;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>portconnection2portconnection4device Destination r90 DD</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>portconnection2portconnection4device Destination r90 DD</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_DESTINATION_R90_DD_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess11_r411Impl <em>Port Connection2 Data Access11 r411</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess11_r411Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411 = 23;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access11 r411</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access11 r411</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS11_R411_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys10_r810Impl <em>Port Connection2 Data Access Sys10 r810</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys10_r810Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810 = 24;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access Sys10 r810</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access Sys10 r810</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS10_R810_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys01_r801Impl <em>Port Connection2 Data Access Sys01 r801</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys01_r801Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801 = 25;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access Sys01 r801</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access Sys01 r801</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS01_R801_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device10_r61ffd10Impl <em>connectionref2connectionref4device10 r61ffd10</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device10_r61ffd10Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10 = 26;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>connectionref2connectionref4device10 r61ffd10</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>connectionref2connectionref4device10 r61ffd10</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE10_R61FFD10_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.subcomponent2subcomponent_r2Impl <em>subcomponent2subcomponent r2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.subcomponent2subcomponent_r2Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2 = 27;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>subcomponent2subcomponent r2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>subcomponent2subcomponent r2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBCOMPONENT2SUBCOMPONENT_R2_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess00_r400Impl <em>Port Connection2 Data Access00 r400</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess00_r400Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400 = 28;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access00 r400</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access00 r400</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS00_R400_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys00_r800Impl <em>Port Connection2 Data Access Sys00 r800</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys00_r800Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800 = 29;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access Sys00 r800</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access Sys00 r800</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS00_R800_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref_r61ffImpl <em>connectionref2connectionref r61ff</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref_r61ffImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF = 30;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>connectionref2connectionref r61ff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>connectionref2connectionref r61ff</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF_R61FF_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device11_r61ffd11Impl <em>connectionref2connectionref4device11 r61ffd11</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device11_r61ffd11Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11 = 31;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>connectionref2connectionref4device11 r61ffd11</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>connectionref2connectionref4device11 r61ffd11</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE11_R61FFD11_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess10_r410Impl <em>Port Connection2 Data Access10 r410</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccess10_r410Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410 = 32;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access10 r410</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access10 r410</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS10_R410_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.feature2feature_r3Impl <em>feature2feature r3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.feature2feature_r3Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getfeature2feature_r3()
	 * @generated
	 */
	int FEATURE2FEATURE_R3 = 33;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>feature2feature r3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>feature2feature r3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2FEATURE_R3_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT + 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys11_r811Impl <em>Port Connection2 Data Access Sys11 r811</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.PortConnection2DataAccessSys11_r811Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811 = 34;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>Port Connection2 Data Access Sys11 r811</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>Port Connection2 Data Access Sys11 r811</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTION2_DATA_ACCESS_SYS11_R811_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device01_r61ffd01Impl <em>connectionref2connectionref4device01 r61ffd01</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.connectionref2connectionref4device01_r61ffd01Impl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01 = 35;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>connectionref2connectionref4device01 r61ffd01</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>connectionref2connectionref4device01 r61ffd01</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTIONREF2CONNECTIONREF4DEVICE01_R61FFD01_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * The meta object id for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.portconnection2portconnection4deviceSource_r90DSImpl <em>portconnection2portconnection4device Source r90 DS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.portconnection2portconnection4deviceSource_r90DSImpl
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl.GeneratedPackageImpl#getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS = 36;

	/**
	 * The feature id for the '<em><b>Created TGG Nodes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__CREATED_TGG_NODES = OperationalTGGPackage.OPERATIONAL_RULE__CREATED_TGG_NODES;

	/**
	 * The feature id for the '<em><b>Rule ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__RULE_ID = OperationalTGGPackage.OPERATIONAL_RULE__RULE_ID;

	/**
	 * The feature id for the '<em><b>Operational Rule Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__OPERATIONAL_RULE_GROUP = OperationalTGGPackage.OPERATIONAL_RULE__OPERATIONAL_RULE_GROUP;

	/**
	 * The feature id for the '<em><b>Preferred Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__PREFERRED_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__PREFERRED_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>All Input Corr Node Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__ALL_INPUT_CORR_NODE_TYPES = OperationalTGGPackage.OPERATIONAL_RULE__ALL_INPUT_CORR_NODE_TYPES;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__ENABLED = OperationalTGGPackage.OPERATIONAL_RULE__ENABLED;

	/**
	 * The feature id for the '<em><b>Add Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__ADD_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Move Element Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__MOVE_ELEMENT_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Change Attribute Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__CHANGE_ATTRIBUTE_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Transform Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__TRANSFORM_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Transform Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__TRANSFORM_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Transform Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__TRANSFORM_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Conflict Check Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__CONFLICT_CHECK_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Conflict Check Mapping Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__CONFLICT_CHECK_MAPPING_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Conflict Check Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__CONFLICT_CHECK_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Synchronize Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__SYNCHRONIZE_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Synchronize Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__SYNCHRONIZE_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Repair Forward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__REPAIR_FORWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Repair Backward Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS__REPAIR_BACKWARD_ACTIVITY = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 12;

	/**
	 * The number of structural features of the '<em>portconnection2portconnection4device Source r90 DS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS_FEATURE_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_FEATURE_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>Delete Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___DELETE_ELEMENT__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE___DELETE_ELEMENT__TGGNODE;

	/**
	 * The operation id for the '<em>Add Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___ADD_ELEMENT__EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Change Attribute Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Move Element</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Transform Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Transform Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Transform Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Conflict Check Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___CONFLICT_CHECK_FORWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Conflict Check Mapping</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___CONFLICT_CHECK_MAPPING__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Conflict Check Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___CONFLICT_CHECK_BACKWARD__TGGNODE = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Synchronize Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Synchronize Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Repair Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___REPAIR_FORWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Repair Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS___REPAIR_BACKWARD__TGGNODE_BOOLEAN = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 12;

	/**
	 * The number of operations of the '<em>portconnection2portconnection4device Source r90 DS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORTCONNECTION2PORTCONNECTION4DEVICE_SOURCE_R90_DS_OPERATION_COUNT = OperationalTGGPackage.OPERATIONAL_RULE_OPERATION_COUNT
			+ 13;

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.aadl2aadlOperationalTGG <em>aadl2aadl Operational TGG</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>aadl2aadl Operational TGG</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.aadl2aadlOperationalTGG
	 * @generated
	 */
	EClass getaadl2aadlOperationalTGG();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom <em>mote Axiom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>mote Axiom</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom
	 * @generated
	 */
	EClass getmoteAxiom();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10 <em>connectionref2connectionref4device10</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connectionref2connectionref4device10</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10
	 * @generated
	 */
	EClass getconnectionref2connectionref4device10();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination <em>portconnection2portconnection4device Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>portconnection2portconnection4device Destination</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination
	 * @generated
	 */
	EClass getportconnection2portconnection4deviceDestination();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01 <em>connectionref2connectionref4device01</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connectionref2connectionref4device01</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01
	 * @generated
	 */
	EClass getconnectionref2connectionref4device01();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature <em>feature2feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>feature2feature</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature
	 * @generated
	 */
	EClass getfeature2feature();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01 <em>Port Connection2 Data Access01</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access01</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01
	 * @generated
	 */
	EClass getPortConnection2DataAccess01();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref <em>connectionref2connectionref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connectionref2connectionref</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref
	 * @generated
	 */
	EClass getconnectionref2connectionref();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01 <em>Port Connection2 Data Access Sys01</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access Sys01</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01
	 * @generated
	 */
	EClass getPortConnection2DataAccessSys01();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11 <em>connectionref2connectionref4device11</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connectionref2connectionref4device11</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11
	 * @generated
	 */
	EClass getconnectionref2connectionref4device11();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00 <em>Port Connection2 Data Access Sys00</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access Sys00</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00
	 * @generated
	 */
	EClass getPortConnection2DataAccessSys00();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11 <em>Port Connection2 Data Access11</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access11</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11
	 * @generated
	 */
	EClass getPortConnection2DataAccess11();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10 <em>Port Connection2 Data Access10</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access10</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10
	 * @generated
	 */
	EClass getPortConnection2DataAccess10();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource <em>portconnection2portconnection4device Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>portconnection2portconnection4device Source</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource
	 * @generated
	 */
	EClass getportconnection2portconnection4deviceSource();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection <em>connection2connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connection2connection</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection
	 * @generated
	 */
	EClass getconnection2connection();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11 <em>Port Connection2 Data Access Sys11</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access Sys11</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11
	 * @generated
	 */
	EClass getPortConnection2DataAccessSys11();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00 <em>Port Connection2 Data Access00</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access00</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00
	 * @generated
	 */
	EClass getPortConnection2DataAccess00();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent <em>subcomponent2subcomponent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>subcomponent2subcomponent</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent
	 * @generated
	 */
	EClass getsubcomponent2subcomponent();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10 <em>Port Connection2 Data Access Sys10</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access Sys10</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10
	 * @generated
	 */
	EClass getPortConnection2DataAccessSys10();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0 <em>mote Axiom r0</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>mote Axiom r0</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0
	 * @generated
	 */
	EClass getmoteAxiom_r0();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getAddElementActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getMoveElementActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getChangeAttributeActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getTransformForwardActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getTransformMappingActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getTransformBackwardActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getSynchronizeForwardActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#getSynchronizeBackwardActivity()
	 * @see #getmoteAxiom_r0()
	 * @generated
	 */
	EReference getmoteAxiom_r0_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#addElement(org.eclipse.emf.common.util.EMap, org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#addElement(org.eclipse.emf.common.util.EMap, org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__AddElement__EMap_EList_EList();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#transformForward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#transformForward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__TransformForward__EList_EList_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#transformMapping(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#transformMapping(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__TransformMapping__EList_EList_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#transformBackward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#transformBackward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, boolean)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__TransformBackward__EList_EList_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#synchronizeForward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#synchronizeForward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__SynchronizeForward__EList_EList_TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#synchronizeBackward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.moteAxiom_r0#synchronizeBackward(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList, de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getmoteAxiom_r0__SynchronizeBackward__EList_EList_TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401 <em>Port Connection2 Data Access01 r401</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access01 r401</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401
	 * @generated
	 */
	EClass getPortConnection2DataAccess01_r401();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getAddElementActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getMoveElementActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getChangeAttributeActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getTransformForwardActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getTransformMappingActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getTransformBackwardActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getConflictCheckForwardActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getConflictCheckMappingActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getConflictCheckBackwardActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getSynchronizeForwardActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getSynchronizeBackwardActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getRepairForwardActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#getRepairBackwardActivity()
	 * @see #getPortConnection2DataAccess01_r401()
	 * @generated
	 */
	EReference getPortConnection2DataAccess01_r401_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01_r401#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess01_r401__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90 <em>connection2connection r90</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connection2connection r90</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90
	 * @generated
	 */
	EClass getconnection2connection_r90();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getAddElementActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getMoveElementActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getChangeAttributeActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getTransformForwardActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getTransformMappingActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getTransformBackwardActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getConflictCheckForwardActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getConflictCheckMappingActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getConflictCheckBackwardActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getSynchronizeForwardActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getSynchronizeBackwardActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getRepairForwardActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#getRepairBackwardActivity()
	 * @see #getconnection2connection_r90()
	 * @generated
	 */
	EReference getconnection2connection_r90_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnection2connection_r90__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnection2connection_r90__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnection2connection_r90__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnection2connection_r90__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnection2connection_r90__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnection2connection_r90__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connection2connection_r90#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnection2connection_r90__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD <em>portconnection2portconnection4device Destination r90 DD</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>portconnection2portconnection4device Destination r90 DD</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD
	 * @generated
	 */
	EClass getportconnection2portconnection4deviceDestination_r90DD();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getAddElementActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getMoveElementActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getChangeAttributeActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getTransformForwardActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getTransformMappingActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getTransformBackwardActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getConflictCheckForwardActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getConflictCheckMappingActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getConflictCheckBackwardActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getSynchronizeForwardActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getSynchronizeBackwardActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getRepairForwardActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#getRepairBackwardActivity()
	 * @see #getportconnection2portconnection4deviceDestination_r90DD()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceDestination_r90DD_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceDestination_r90DD#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceDestination_r90DD__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411 <em>Port Connection2 Data Access11 r411</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access11 r411</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411
	 * @generated
	 */
	EClass getPortConnection2DataAccess11_r411();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getAddElementActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getMoveElementActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getChangeAttributeActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getTransformForwardActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getTransformMappingActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getTransformBackwardActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getConflictCheckForwardActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getConflictCheckMappingActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getConflictCheckBackwardActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getSynchronizeForwardActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getSynchronizeBackwardActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getRepairForwardActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#getRepairBackwardActivity()
	 * @see #getPortConnection2DataAccess11_r411()
	 * @generated
	 */
	EReference getPortConnection2DataAccess11_r411_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess11_r411#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess11_r411__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810 <em>Port Connection2 Data Access Sys10 r810</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access Sys10 r810</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810
	 * @generated
	 */
	EClass getPortConnection2DataAccessSys10_r810();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getAddElementActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getMoveElementActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getChangeAttributeActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getTransformForwardActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getTransformMappingActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getTransformBackwardActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getConflictCheckForwardActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getConflictCheckMappingActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getConflictCheckBackwardActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getSynchronizeForwardActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getSynchronizeBackwardActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getRepairForwardActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#getRepairBackwardActivity()
	 * @see #getPortConnection2DataAccessSys10_r810()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys10_r810_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys10_r810#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys10_r810__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801 <em>Port Connection2 Data Access Sys01 r801</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access Sys01 r801</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801
	 * @generated
	 */
	EClass getPortConnection2DataAccessSys01_r801();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getAddElementActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getMoveElementActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getChangeAttributeActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getTransformForwardActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getTransformMappingActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getTransformBackwardActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getConflictCheckForwardActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getConflictCheckMappingActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getConflictCheckBackwardActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getSynchronizeForwardActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getSynchronizeBackwardActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getRepairForwardActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#getRepairBackwardActivity()
	 * @see #getPortConnection2DataAccessSys01_r801()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys01_r801_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys01_r801#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys01_r801__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10 <em>connectionref2connectionref4device10 r61ffd10</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connectionref2connectionref4device10 r61ffd10</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10
	 * @generated
	 */
	EClass getconnectionref2connectionref4device10_r61ffd10();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getAddElementActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getMoveElementActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getChangeAttributeActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getTransformForwardActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getTransformMappingActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getTransformBackwardActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getConflictCheckForwardActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getConflictCheckMappingActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getConflictCheckBackwardActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getSynchronizeForwardActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getSynchronizeBackwardActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getRepairForwardActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#getRepairBackwardActivity()
	 * @see #getconnectionref2connectionref4device10_r61ffd10()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device10_r61ffd10_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device10_r61ffd10#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device10_r61ffd10__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2 <em>subcomponent2subcomponent r2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>subcomponent2subcomponent r2</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2
	 * @generated
	 */
	EClass getsubcomponent2subcomponent_r2();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getAddElementActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getMoveElementActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getChangeAttributeActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getTransformForwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getTransformMappingActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getTransformBackwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getConflictCheckForwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getConflictCheckMappingActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getConflictCheckBackwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getSynchronizeForwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getSynchronizeBackwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getRepairForwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#getRepairBackwardActivity()
	 * @see #getsubcomponent2subcomponent_r2()
	 * @generated
	 */
	EReference getsubcomponent2subcomponent_r2_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.subcomponent2subcomponent_r2#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getsubcomponent2subcomponent_r2__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400 <em>Port Connection2 Data Access00 r400</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access00 r400</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400
	 * @generated
	 */
	EClass getPortConnection2DataAccess00_r400();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getAddElementActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getMoveElementActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getChangeAttributeActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getTransformForwardActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getTransformMappingActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getTransformBackwardActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getConflictCheckForwardActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getConflictCheckMappingActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getConflictCheckBackwardActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getSynchronizeForwardActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getSynchronizeBackwardActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getRepairForwardActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#getRepairBackwardActivity()
	 * @see #getPortConnection2DataAccess00_r400()
	 * @generated
	 */
	EReference getPortConnection2DataAccess00_r400_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess00_r400#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess00_r400__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800 <em>Port Connection2 Data Access Sys00 r800</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access Sys00 r800</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800
	 * @generated
	 */
	EClass getPortConnection2DataAccessSys00_r800();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getAddElementActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getMoveElementActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getChangeAttributeActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getTransformForwardActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getTransformMappingActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getTransformBackwardActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getConflictCheckForwardActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getConflictCheckMappingActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getConflictCheckBackwardActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getSynchronizeForwardActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getSynchronizeBackwardActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getRepairForwardActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#getRepairBackwardActivity()
	 * @see #getPortConnection2DataAccessSys00_r800()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys00_r800_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys00_r800#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys00_r800__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff <em>connectionref2connectionref r61ff</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connectionref2connectionref r61ff</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff
	 * @generated
	 */
	EClass getconnectionref2connectionref_r61ff();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getAddElementActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getMoveElementActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getChangeAttributeActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getTransformForwardActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getTransformMappingActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getTransformBackwardActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getConflictCheckForwardActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getConflictCheckMappingActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getConflictCheckBackwardActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getSynchronizeForwardActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getSynchronizeBackwardActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getRepairForwardActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#getRepairBackwardActivity()
	 * @see #getconnectionref2connectionref_r61ff()
	 * @generated
	 */
	EReference getconnectionref2connectionref_r61ff_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref_r61ff#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref_r61ff__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11 <em>connectionref2connectionref4device11 r61ffd11</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connectionref2connectionref4device11 r61ffd11</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11
	 * @generated
	 */
	EClass getconnectionref2connectionref4device11_r61ffd11();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getAddElementActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getMoveElementActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getChangeAttributeActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getTransformForwardActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getTransformMappingActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getTransformBackwardActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getConflictCheckForwardActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getConflictCheckMappingActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getConflictCheckBackwardActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getSynchronizeForwardActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getSynchronizeBackwardActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getRepairForwardActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#getRepairBackwardActivity()
	 * @see #getconnectionref2connectionref4device11_r61ffd11()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device11_r61ffd11_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device11_r61ffd11#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device11_r61ffd11__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410 <em>Port Connection2 Data Access10 r410</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access10 r410</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410
	 * @generated
	 */
	EClass getPortConnection2DataAccess10_r410();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getAddElementActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getMoveElementActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getChangeAttributeActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getTransformForwardActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getTransformMappingActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getTransformBackwardActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getConflictCheckForwardActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getConflictCheckMappingActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getConflictCheckBackwardActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getSynchronizeForwardActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getSynchronizeBackwardActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getRepairForwardActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#getRepairBackwardActivity()
	 * @see #getPortConnection2DataAccess10_r410()
	 * @generated
	 */
	EReference getPortConnection2DataAccess10_r410_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess10_r410#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccess10_r410__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3 <em>feature2feature r3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>feature2feature r3</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3
	 * @generated
	 */
	EClass getfeature2feature_r3();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getAddElementActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getMoveElementActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getChangeAttributeActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getTransformForwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getTransformMappingActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getTransformBackwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getConflictCheckForwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getConflictCheckMappingActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getConflictCheckBackwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getSynchronizeForwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getSynchronizeBackwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getRepairForwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#getRepairBackwardActivity()
	 * @see #getfeature2feature_r3()
	 * @generated
	 */
	EReference getfeature2feature_r3_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getfeature2feature_r3__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getfeature2feature_r3__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getfeature2feature_r3__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getfeature2feature_r3__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getfeature2feature_r3__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getfeature2feature_r3__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.feature2feature_r3#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getfeature2feature_r3__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811 <em>Port Connection2 Data Access Sys11 r811</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connection2 Data Access Sys11 r811</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811
	 * @generated
	 */
	EClass getPortConnection2DataAccessSys11_r811();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getAddElementActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getMoveElementActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getChangeAttributeActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getTransformForwardActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getTransformMappingActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getTransformBackwardActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getConflictCheckForwardActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getConflictCheckMappingActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getConflictCheckBackwardActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getSynchronizeForwardActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getSynchronizeBackwardActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getRepairForwardActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#getRepairBackwardActivity()
	 * @see #getPortConnection2DataAccessSys11_r811()
	 * @generated
	 */
	EReference getPortConnection2DataAccessSys11_r811_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccessSys11_r811#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getPortConnection2DataAccessSys11_r811__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01 <em>connectionref2connectionref4device01 r61ffd01</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>connectionref2connectionref4device01 r61ffd01</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01
	 * @generated
	 */
	EClass getconnectionref2connectionref4device01_r61ffd01();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getAddElementActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getMoveElementActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getChangeAttributeActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getTransformForwardActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getTransformMappingActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getTransformBackwardActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getConflictCheckForwardActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getConflictCheckMappingActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getConflictCheckBackwardActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getSynchronizeForwardActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getSynchronizeBackwardActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getRepairForwardActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#getRepairBackwardActivity()
	 * @see #getconnectionref2connectionref4device01_r61ffd01()
	 * @generated
	 */
	EReference getconnectionref2connectionref4device01_r61ffd01_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.connectionref2connectionref4device01_r61ffd01#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getconnectionref2connectionref4device01_r61ffd01__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for class '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS <em>portconnection2portconnection4device Source r90 DS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>portconnection2portconnection4device Source r90 DS</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS
	 * @generated
	 */
	EClass getportconnection2portconnection4deviceSource_r90DS();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getAddElementActivity <em>Add Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Add Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getAddElementActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_AddElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getMoveElementActivity <em>Move Element Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Move Element Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getMoveElementActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_MoveElementActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getChangeAttributeActivity <em>Change Attribute Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Change Attribute Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getChangeAttributeActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_ChangeAttributeActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getTransformForwardActivity <em>Transform Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getTransformForwardActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_TransformForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getTransformMappingActivity <em>Transform Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getTransformMappingActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_TransformMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getTransformBackwardActivity <em>Transform Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Transform Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getTransformBackwardActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_TransformBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getConflictCheckForwardActivity <em>Conflict Check Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getConflictCheckForwardActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_ConflictCheckForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getConflictCheckMappingActivity <em>Conflict Check Mapping Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Mapping Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getConflictCheckMappingActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_ConflictCheckMappingActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getConflictCheckBackwardActivity <em>Conflict Check Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Check Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getConflictCheckBackwardActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_ConflictCheckBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getSynchronizeForwardActivity <em>Synchronize Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getSynchronizeForwardActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_SynchronizeForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getSynchronizeBackwardActivity <em>Synchronize Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Synchronize Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getSynchronizeBackwardActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_SynchronizeBackwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getRepairForwardActivity <em>Repair Forward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Forward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getRepairForwardActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_RepairForwardActivity();

	/**
	 * Returns the meta object for the reference '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getRepairBackwardActivity <em>Repair Backward Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Repair Backward Activity</em>'.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#getRepairBackwardActivity()
	 * @see #getportconnection2portconnection4deviceSource_r90DS()
	 * @generated
	 */
	EReference getportconnection2portconnection4deviceSource_r90DS_RepairBackwardActivity();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#addElement(org.eclipse.emf.common.util.EMap) <em>Add Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#addElement(org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__AddElement__EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap) <em>Change Attribute Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Attribute Values</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#changeAttributeValues(de.mdelab.mltgg.mote2.TGGNode, org.eclipse.emf.common.util.EMap)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__ChangeAttributeValues__TGGNode_EMap();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode) <em>Move Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move Element</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#moveElement(de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode, de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__MoveElement__TGGNode_TGGNode_TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#transformForward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__TransformForward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#transformMapping(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__TransformMapping__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean) <em>Transform Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#transformBackward(de.mdelab.mltgg.mote2.TGGNode, boolean, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__TransformBackward__TGGNode_boolean_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#conflictCheckForward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__ConflictCheckForward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Mapping</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Mapping</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#conflictCheckMapping(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__ConflictCheckMapping__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode) <em>Conflict Check Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Conflict Check Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#conflictCheckBackward(de.mdelab.mltgg.mote2.TGGNode)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__ConflictCheckBackward__TGGNode();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#synchronizeForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__SynchronizeForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Synchronize Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Synchronize Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#synchronizeBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__SynchronizeBackward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Forward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#repairForward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__RepairForward__TGGNode_boolean();

	/**
	 * Returns the meta object for the '{@link fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean) <em>Repair Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Repair Backward</em>' operation.
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.portconnection2portconnection4deviceSource_r90DS#repairBackward(de.mdelab.mltgg.mote2.TGGNode, boolean)
	 * @generated
	 */
	EOperation getportconnection2portconnection4deviceSource_r90DS__RepairBackward__TGGNode_boolean();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GeneratedFactory getGeneratedFactory();

} //GeneratedPackage
