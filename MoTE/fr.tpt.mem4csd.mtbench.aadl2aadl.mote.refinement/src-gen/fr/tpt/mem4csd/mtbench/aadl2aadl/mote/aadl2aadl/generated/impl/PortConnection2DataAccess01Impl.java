/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.impl;

import de.mdelab.mltgg.mote2.operationalTGG.impl.OperationalRuleGroupImpl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.GeneratedPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.aadl2aadl.generated.PortConnection2DataAccess01;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Connection2 Data Access01</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PortConnection2DataAccess01Impl extends OperationalRuleGroupImpl implements PortConnection2DataAccess01 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortConnection2DataAccess01Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GeneratedPackage.eINSTANCE.getPortConnection2DataAccess01();
	}

} //PortConnection2DataAccess01Impl
