/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated;

import de.mdelab.mltgg.mote2.operationalTGG.OperationalRuleGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>feature2feature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.GeneratedPackage#getfeature2feature()
 * @model
 * @generated
 */
public interface feature2feature extends OperationalRuleGroup {
} // feature2feature
