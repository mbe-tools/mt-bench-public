/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.impl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrConnection2DataAccessSys;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Connection2 Data Access Sys</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrConnection2DataAccessSysImpl extends CorrFeature2FeatureImpl implements CorrConnection2DataAccessSys {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrConnection2DataAccessSysImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyingPackage.Literals.CORR_CONNECTION2_DATA_ACCESS_SYS;
	}

} //CorrConnection2DataAccessSysImpl
