/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl;

import de.mdelab.mlcallactions.MlcallactionsPackage;

import de.mdelab.mlcore.MlcorePackage;

import de.mdelab.mlexpressions.MlexpressionsPackage;

import de.mdelab.mlsdm.MlsdmPackage;

import de.mdelab.mlstorypatterns.MlstorypatternsPackage;

import de.mdelab.mltgg.mote2.Mote2Package;

import de.mdelab.mltgg.mote2.helpers.HelpersPackage;

import de.mdelab.mltgg.mote2.operationalTGG.OperationalTGGPackage;

import de.mdelab.mltgg.mote2.sdm.SdmPackage;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.GeneratedFactory;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.GeneratedPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connection2connection_r90p;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.connectionref2connectionref_r60ff;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.copyingOperationalTGG;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.feature2feature_r3;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.moteAxiom_r0;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.subcomponent2subcomponent_r2;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.impl.CopyingPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GeneratedPackageImpl extends EPackageImpl implements GeneratedPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass copyingOperationalTGGEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moteAxiomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connection2connectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionref2connectionrefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass feature2featureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subcomponent2subcomponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moteAxiom_r0EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectionref2connectionref_r60ffEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass feature2feature_r3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subcomponent2subcomponent_r2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connection2connection_r90pEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.GeneratedPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GeneratedPackageImpl() {
		super(eNS_URI, GeneratedFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link GeneratedPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GeneratedPackage init() {
		if (isInited)
			return (GeneratedPackage) EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredGeneratedPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		GeneratedPackageImpl theGeneratedPackage = registeredGeneratedPackage instanceof GeneratedPackageImpl
				? (GeneratedPackageImpl) registeredGeneratedPackage
				: new GeneratedPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Mote2Package.eINSTANCE.eClass();
		MlcorePackage.eINSTANCE.eClass();
		SdmPackage.eINSTANCE.eClass();
		MlsdmPackage.eINSTANCE.eClass();
		MlexpressionsPackage.eINSTANCE.eClass();
		MlstorypatternsPackage.eINSTANCE.eClass();
		MlcallactionsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(CopyingPackage.eNS_URI);
		CopyingPackageImpl theCopyingPackage = (CopyingPackageImpl) (registeredPackage instanceof CopyingPackageImpl
				? registeredPackage
				: CopyingPackage.eINSTANCE);

		// Create package meta-data objects
		theGeneratedPackage.createPackageContents();
		theCopyingPackage.createPackageContents();

		// Initialize created meta-data
		theGeneratedPackage.initializePackageContents();
		theCopyingPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGeneratedPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GeneratedPackage.eNS_URI, theGeneratedPackage);
		return theGeneratedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getcopyingOperationalTGG() {
		return copyingOperationalTGGEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getmoteAxiom() {
		return moteAxiomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnection2connection() {
		return connection2connectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnectionref2connectionref() {
		return connectionref2connectionrefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getfeature2feature() {
		return feature2featureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getsubcomponent2subcomponent() {
		return subcomponent2subcomponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getmoteAxiom_r0() {
		return moteAxiom_r0EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_AddElementActivity() {
		return (EReference) moteAxiom_r0EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_MoveElementActivity() {
		return (EReference) moteAxiom_r0EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_ChangeAttributeActivity() {
		return (EReference) moteAxiom_r0EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_TransformForwardActivity() {
		return (EReference) moteAxiom_r0EClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_TransformMappingActivity() {
		return (EReference) moteAxiom_r0EClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_TransformBackwardActivity() {
		return (EReference) moteAxiom_r0EClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_SynchronizeForwardActivity() {
		return (EReference) moteAxiom_r0EClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getmoteAxiom_r0_SynchronizeBackwardActivity() {
		return (EReference) moteAxiom_r0EClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__AddElement__EMap_EList_EList() {
		return moteAxiom_r0EClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__ChangeAttributeValues__TGGNode_EMap() {
		return moteAxiom_r0EClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__TransformForward__EList_EList_boolean() {
		return moteAxiom_r0EClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__TransformMapping__EList_EList_boolean() {
		return moteAxiom_r0EClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__TransformBackward__EList_EList_boolean() {
		return moteAxiom_r0EClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__SynchronizeForward__EList_EList_TGGNode_boolean() {
		return moteAxiom_r0EClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getmoteAxiom_r0__SynchronizeBackward__EList_EList_TGGNode_boolean() {
		return moteAxiom_r0EClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnectionref2connectionref_r60ff() {
		return connectionref2connectionref_r60ffEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_AddElementActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_MoveElementActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_ChangeAttributeActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_TransformForwardActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_TransformMappingActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_TransformBackwardActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_ConflictCheckForwardActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_ConflictCheckMappingActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_ConflictCheckBackwardActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_SynchronizeForwardActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_SynchronizeBackwardActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_RepairForwardActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnectionref2connectionref_r60ff_RepairBackwardActivity() {
		return (EReference) connectionref2connectionref_r60ffEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__AddElement__EMap() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__ChangeAttributeValues__TGGNode_EMap() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__MoveElement__TGGNode_TGGNode_TGGNode() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__TransformForward__TGGNode_boolean_boolean() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__TransformMapping__TGGNode_boolean_boolean() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__TransformBackward__TGGNode_boolean_boolean() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__ConflictCheckForward__TGGNode() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__ConflictCheckMapping__TGGNode() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__ConflictCheckBackward__TGGNode() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__SynchronizeForward__TGGNode_boolean() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__SynchronizeBackward__TGGNode_boolean() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__RepairForward__TGGNode_boolean() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnectionref2connectionref_r60ff__RepairBackward__TGGNode_boolean() {
		return connectionref2connectionref_r60ffEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getfeature2feature_r3() {
		return feature2feature_r3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_AddElementActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_MoveElementActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_ChangeAttributeActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_TransformForwardActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_TransformMappingActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_TransformBackwardActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_ConflictCheckForwardActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_ConflictCheckMappingActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_ConflictCheckBackwardActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_SynchronizeForwardActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_SynchronizeBackwardActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_RepairForwardActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getfeature2feature_r3_RepairBackwardActivity() {
		return (EReference) feature2feature_r3EClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__AddElement__EMap() {
		return feature2feature_r3EClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__ChangeAttributeValues__TGGNode_EMap() {
		return feature2feature_r3EClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__MoveElement__TGGNode_TGGNode_TGGNode() {
		return feature2feature_r3EClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__TransformForward__TGGNode_boolean_boolean() {
		return feature2feature_r3EClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__TransformMapping__TGGNode_boolean_boolean() {
		return feature2feature_r3EClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__TransformBackward__TGGNode_boolean_boolean() {
		return feature2feature_r3EClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__ConflictCheckForward__TGGNode() {
		return feature2feature_r3EClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__ConflictCheckMapping__TGGNode() {
		return feature2feature_r3EClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__ConflictCheckBackward__TGGNode() {
		return feature2feature_r3EClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__SynchronizeForward__TGGNode_boolean() {
		return feature2feature_r3EClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__SynchronizeBackward__TGGNode_boolean() {
		return feature2feature_r3EClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__RepairForward__TGGNode_boolean() {
		return feature2feature_r3EClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getfeature2feature_r3__RepairBackward__TGGNode_boolean() {
		return feature2feature_r3EClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getsubcomponent2subcomponent_r2() {
		return subcomponent2subcomponent_r2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_AddElementActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_MoveElementActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_ChangeAttributeActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_TransformForwardActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_TransformMappingActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_TransformBackwardActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_ConflictCheckForwardActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_ConflictCheckMappingActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_ConflictCheckBackwardActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_SynchronizeForwardActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_SynchronizeBackwardActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_RepairForwardActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getsubcomponent2subcomponent_r2_RepairBackwardActivity() {
		return (EReference) subcomponent2subcomponent_r2EClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__AddElement__EMap() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__ChangeAttributeValues__TGGNode_EMap() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__MoveElement__TGGNode_TGGNode_TGGNode() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__TransformForward__TGGNode_boolean_boolean() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__TransformMapping__TGGNode_boolean_boolean() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__TransformBackward__TGGNode_boolean_boolean() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__ConflictCheckForward__TGGNode() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__ConflictCheckMapping__TGGNode() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__ConflictCheckBackward__TGGNode() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__SynchronizeForward__TGGNode_boolean() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__SynchronizeBackward__TGGNode_boolean() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__RepairForward__TGGNode_boolean() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getsubcomponent2subcomponent_r2__RepairBackward__TGGNode_boolean() {
		return subcomponent2subcomponent_r2EClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getconnection2connection_r90p() {
		return connection2connection_r90pEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_AddElementActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_MoveElementActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_ChangeAttributeActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_TransformForwardActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_TransformMappingActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_TransformBackwardActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_ConflictCheckForwardActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_ConflictCheckMappingActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_ConflictCheckBackwardActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_SynchronizeForwardActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_SynchronizeBackwardActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_RepairForwardActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getconnection2connection_r90p_RepairBackwardActivity() {
		return (EReference) connection2connection_r90pEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__AddElement__EMap() {
		return connection2connection_r90pEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__ChangeAttributeValues__TGGNode_EMap() {
		return connection2connection_r90pEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__MoveElement__TGGNode_TGGNode_TGGNode() {
		return connection2connection_r90pEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__TransformForward__TGGNode_boolean_boolean() {
		return connection2connection_r90pEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__TransformMapping__TGGNode_boolean_boolean() {
		return connection2connection_r90pEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__TransformBackward__TGGNode_boolean_boolean() {
		return connection2connection_r90pEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__ConflictCheckForward__TGGNode() {
		return connection2connection_r90pEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__ConflictCheckMapping__TGGNode() {
		return connection2connection_r90pEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__ConflictCheckBackward__TGGNode() {
		return connection2connection_r90pEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__SynchronizeForward__TGGNode_boolean() {
		return connection2connection_r90pEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__SynchronizeBackward__TGGNode_boolean() {
		return connection2connection_r90pEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__RepairForward__TGGNode_boolean() {
		return connection2connection_r90pEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getconnection2connection_r90p__RepairBackward__TGGNode_boolean() {
		return connection2connection_r90pEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneratedFactory getGeneratedFactory() {
		return (GeneratedFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		copyingOperationalTGGEClass = createEClass(COPYING_OPERATIONAL_TGG);

		moteAxiomEClass = createEClass(MOTE_AXIOM);

		connection2connectionEClass = createEClass(CONNECTION2CONNECTION);

		connectionref2connectionrefEClass = createEClass(CONNECTIONREF2CONNECTIONREF);

		feature2featureEClass = createEClass(FEATURE2FEATURE);

		subcomponent2subcomponentEClass = createEClass(SUBCOMPONENT2SUBCOMPONENT);

		moteAxiom_r0EClass = createEClass(MOTE_AXIOM_R0);
		createEReference(moteAxiom_r0EClass, MOTE_AXIOM_R0__ADD_ELEMENT_ACTIVITY);
		createEReference(moteAxiom_r0EClass, MOTE_AXIOM_R0__MOVE_ELEMENT_ACTIVITY);
		createEReference(moteAxiom_r0EClass, MOTE_AXIOM_R0__CHANGE_ATTRIBUTE_ACTIVITY);
		createEReference(moteAxiom_r0EClass, MOTE_AXIOM_R0__TRANSFORM_FORWARD_ACTIVITY);
		createEReference(moteAxiom_r0EClass, MOTE_AXIOM_R0__TRANSFORM_MAPPING_ACTIVITY);
		createEReference(moteAxiom_r0EClass, MOTE_AXIOM_R0__TRANSFORM_BACKWARD_ACTIVITY);
		createEReference(moteAxiom_r0EClass, MOTE_AXIOM_R0__SYNCHRONIZE_FORWARD_ACTIVITY);
		createEReference(moteAxiom_r0EClass, MOTE_AXIOM_R0__SYNCHRONIZE_BACKWARD_ACTIVITY);
		createEOperation(moteAxiom_r0EClass, MOTE_AXIOM_R0___ADD_ELEMENT__EMAP_ELIST_ELIST);
		createEOperation(moteAxiom_r0EClass, MOTE_AXIOM_R0___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP);
		createEOperation(moteAxiom_r0EClass, MOTE_AXIOM_R0___TRANSFORM_FORWARD__ELIST_ELIST_BOOLEAN);
		createEOperation(moteAxiom_r0EClass, MOTE_AXIOM_R0___TRANSFORM_MAPPING__ELIST_ELIST_BOOLEAN);
		createEOperation(moteAxiom_r0EClass, MOTE_AXIOM_R0___TRANSFORM_BACKWARD__ELIST_ELIST_BOOLEAN);
		createEOperation(moteAxiom_r0EClass, MOTE_AXIOM_R0___SYNCHRONIZE_FORWARD__ELIST_ELIST_TGGNODE_BOOLEAN);
		createEOperation(moteAxiom_r0EClass, MOTE_AXIOM_R0___SYNCHRONIZE_BACKWARD__ELIST_ELIST_TGGNODE_BOOLEAN);

		connectionref2connectionref_r60ffEClass = createEClass(CONNECTIONREF2CONNECTIONREF_R60FF);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__ADD_ELEMENT_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__MOVE_ELEMENT_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__CHANGE_ATTRIBUTE_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__TRANSFORM_FORWARD_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__TRANSFORM_MAPPING_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__TRANSFORM_BACKWARD_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__CONFLICT_CHECK_FORWARD_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__CONFLICT_CHECK_MAPPING_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__CONFLICT_CHECK_BACKWARD_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__SYNCHRONIZE_FORWARD_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__SYNCHRONIZE_BACKWARD_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__REPAIR_FORWARD_ACTIVITY);
		createEReference(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF__REPAIR_BACKWARD_ACTIVITY);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___ADD_ELEMENT__EMAP);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___CONFLICT_CHECK_FORWARD__TGGNODE);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___CONFLICT_CHECK_MAPPING__TGGNODE);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___CONFLICT_CHECK_BACKWARD__TGGNODE);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___REPAIR_FORWARD__TGGNODE_BOOLEAN);
		createEOperation(connectionref2connectionref_r60ffEClass,
				CONNECTIONREF2CONNECTIONREF_R60FF___REPAIR_BACKWARD__TGGNODE_BOOLEAN);

		feature2feature_r3EClass = createEClass(FEATURE2FEATURE_R3);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__ADD_ELEMENT_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__MOVE_ELEMENT_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__CHANGE_ATTRIBUTE_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__TRANSFORM_FORWARD_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__TRANSFORM_MAPPING_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__TRANSFORM_BACKWARD_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__CONFLICT_CHECK_FORWARD_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__CONFLICT_CHECK_MAPPING_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__CONFLICT_CHECK_BACKWARD_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__SYNCHRONIZE_FORWARD_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__SYNCHRONIZE_BACKWARD_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__REPAIR_FORWARD_ACTIVITY);
		createEReference(feature2feature_r3EClass, FEATURE2FEATURE_R3__REPAIR_BACKWARD_ACTIVITY);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___ADD_ELEMENT__EMAP);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___CONFLICT_CHECK_FORWARD__TGGNODE);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___CONFLICT_CHECK_MAPPING__TGGNODE);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___CONFLICT_CHECK_BACKWARD__TGGNODE);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___REPAIR_FORWARD__TGGNODE_BOOLEAN);
		createEOperation(feature2feature_r3EClass, FEATURE2FEATURE_R3___REPAIR_BACKWARD__TGGNODE_BOOLEAN);

		subcomponent2subcomponent_r2EClass = createEClass(SUBCOMPONENT2SUBCOMPONENT_R2);
		createEReference(subcomponent2subcomponent_r2EClass, SUBCOMPONENT2SUBCOMPONENT_R2__ADD_ELEMENT_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass, SUBCOMPONENT2SUBCOMPONENT_R2__MOVE_ELEMENT_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass, SUBCOMPONENT2SUBCOMPONENT_R2__CHANGE_ATTRIBUTE_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass, SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_FORWARD_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass, SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_MAPPING_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass, SUBCOMPONENT2SUBCOMPONENT_R2__TRANSFORM_BACKWARD_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_FORWARD_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_MAPPING_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2__CONFLICT_CHECK_BACKWARD_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2__SYNCHRONIZE_FORWARD_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2__SYNCHRONIZE_BACKWARD_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass, SUBCOMPONENT2SUBCOMPONENT_R2__REPAIR_FORWARD_ACTIVITY);
		createEReference(subcomponent2subcomponent_r2EClass, SUBCOMPONENT2SUBCOMPONENT_R2__REPAIR_BACKWARD_ACTIVITY);
		createEOperation(subcomponent2subcomponent_r2EClass, SUBCOMPONENT2SUBCOMPONENT_R2___ADD_ELEMENT__EMAP);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_FORWARD__TGGNODE);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_MAPPING__TGGNODE);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___CONFLICT_CHECK_BACKWARD__TGGNODE);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___REPAIR_FORWARD__TGGNODE_BOOLEAN);
		createEOperation(subcomponent2subcomponent_r2EClass,
				SUBCOMPONENT2SUBCOMPONENT_R2___REPAIR_BACKWARD__TGGNODE_BOOLEAN);

		connection2connection_r90pEClass = createEClass(CONNECTION2CONNECTION_R90P);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__ADD_ELEMENT_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__MOVE_ELEMENT_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__CHANGE_ATTRIBUTE_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__TRANSFORM_FORWARD_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__TRANSFORM_MAPPING_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__TRANSFORM_BACKWARD_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__CONFLICT_CHECK_FORWARD_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__CONFLICT_CHECK_MAPPING_ACTIVITY);
		createEReference(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P__CONFLICT_CHECK_BACKWARD_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__SYNCHRONIZE_FORWARD_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__SYNCHRONIZE_BACKWARD_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__REPAIR_FORWARD_ACTIVITY);
		createEReference(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P__REPAIR_BACKWARD_ACTIVITY);
		createEOperation(connection2connection_r90pEClass, CONNECTION2CONNECTION_R90P___ADD_ELEMENT__EMAP);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___CHANGE_ATTRIBUTE_VALUES__TGGNODE_EMAP);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___MOVE_ELEMENT__TGGNODE_TGGNODE_TGGNODE);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___TRANSFORM_FORWARD__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___TRANSFORM_MAPPING__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___TRANSFORM_BACKWARD__TGGNODE_BOOLEAN_BOOLEAN);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___CONFLICT_CHECK_FORWARD__TGGNODE);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___CONFLICT_CHECK_MAPPING__TGGNODE);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___CONFLICT_CHECK_BACKWARD__TGGNODE);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___SYNCHRONIZE_FORWARD__TGGNODE_BOOLEAN);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___SYNCHRONIZE_BACKWARD__TGGNODE_BOOLEAN);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___REPAIR_FORWARD__TGGNODE_BOOLEAN);
		createEOperation(connection2connection_r90pEClass,
				CONNECTION2CONNECTION_R90P___REPAIR_BACKWARD__TGGNODE_BOOLEAN);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SdmPackage theSdmPackage = (SdmPackage) EPackage.Registry.INSTANCE.getEPackage(SdmPackage.eNS_URI);
		OperationalTGGPackage theOperationalTGGPackage = (OperationalTGGPackage) EPackage.Registry.INSTANCE
				.getEPackage(OperationalTGGPackage.eNS_URI);
		MlsdmPackage theMlsdmPackage = (MlsdmPackage) EPackage.Registry.INSTANCE.getEPackage(MlsdmPackage.eNS_URI);
		Mote2Package theMote2Package = (Mote2Package) EPackage.Registry.INSTANCE.getEPackage(Mote2Package.eNS_URI);
		HelpersPackage theHelpersPackage = (HelpersPackage) EPackage.Registry.INSTANCE
				.getEPackage(HelpersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		copyingOperationalTGGEClass.getESuperTypes().add(theSdmPackage.getSdmOperationalTGG());
		moteAxiomEClass.getESuperTypes().add(theOperationalTGGPackage.getOperationalAxiomGroup());
		connection2connectionEClass.getESuperTypes().add(theOperationalTGGPackage.getOperationalRuleGroup());
		connectionref2connectionrefEClass.getESuperTypes().add(theOperationalTGGPackage.getOperationalRuleGroup());
		feature2featureEClass.getESuperTypes().add(theOperationalTGGPackage.getOperationalRuleGroup());
		subcomponent2subcomponentEClass.getESuperTypes().add(theOperationalTGGPackage.getOperationalRuleGroup());
		moteAxiom_r0EClass.getESuperTypes().add(theOperationalTGGPackage.getOperationalAxiom());
		connectionref2connectionref_r60ffEClass.getESuperTypes().add(theOperationalTGGPackage.getOperationalRule());
		feature2feature_r3EClass.getESuperTypes().add(theOperationalTGGPackage.getOperationalRule());
		subcomponent2subcomponent_r2EClass.getESuperTypes().add(theOperationalTGGPackage.getOperationalRule());
		connection2connection_r90pEClass.getESuperTypes().add(theOperationalTGGPackage.getOperationalRule());

		// Initialize classes, features, and operations; add parameters
		initEClass(copyingOperationalTGGEClass, copyingOperationalTGG.class, "copyingOperationalTGG", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(moteAxiomEClass, moteAxiom.class, "moteAxiom", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(connection2connectionEClass, connection2connection.class, "connection2connection", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(connectionref2connectionrefEClass, connectionref2connectionref.class, "connectionref2connectionref",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(feature2featureEClass, feature2feature.class, "feature2feature", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(subcomponent2subcomponentEClass, subcomponent2subcomponent.class, "subcomponent2subcomponent",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(moteAxiom_r0EClass, moteAxiom_r0.class, "moteAxiom_r0", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getmoteAxiom_r0_AddElementActivity(), theMlsdmPackage.getActivity(), null, "addElementActivity",
				null, 1, 1, moteAxiom_r0.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getmoteAxiom_r0_MoveElementActivity(), theMlsdmPackage.getActivity(), null,
				"moveElementActivity", null, 1, 1, moteAxiom_r0.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getmoteAxiom_r0_ChangeAttributeActivity(), theMlsdmPackage.getActivity(), null,
				"changeAttributeActivity", null, 1, 1, moteAxiom_r0.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getmoteAxiom_r0_TransformForwardActivity(), theMlsdmPackage.getActivity(), null,
				"transformForwardActivity", null, 1, 1, moteAxiom_r0.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getmoteAxiom_r0_TransformMappingActivity(), theMlsdmPackage.getActivity(), null,
				"transformMappingActivity", null, 1, 1, moteAxiom_r0.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getmoteAxiom_r0_TransformBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"transformBackwardActivity", null, 1, 1, moteAxiom_r0.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getmoteAxiom_r0_SynchronizeForwardActivity(), theMlsdmPackage.getActivity(), null,
				"synchronizeForwardActivity", null, 1, 1, moteAxiom_r0.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getmoteAxiom_r0_SynchronizeBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"synchronizeBackwardActivity", null, 1, 1, moteAxiom_r0.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getmoteAxiom_r0__AddElement__EMap_EList_EList(), theMote2Package.getTGGNode(),
				"addElement", 1, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(theHelpersPackage.getMapEntry());
		EGenericType g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "parameters", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "leftInputElements", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "rightInputElements", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getmoteAxiom_r0__ChangeAttributeValues__TGGNode_EMap(), ecorePackage.getEBoolean(),
				"changeAttributeValues", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "correspondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "ruleParameters", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getmoteAxiom_r0__TransformForward__EList_EList_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "leftInputElements", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "rightInputElements", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getmoteAxiom_r0__TransformMapping__EList_EList_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformMapping", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "leftInputElements", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "rightInputElements", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getmoteAxiom_r0__TransformBackward__EList_EList_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "leftInputElements", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "rightInputElements", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getmoteAxiom_r0__SynchronizeForward__EList_EList_TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "synchronizeForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "leftInputElements", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "rightInputElements", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getmoteAxiom_r0__SynchronizeBackward__EList_EList_TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "synchronizeBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "leftInputElements", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "rightInputElements", 1, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		initEClass(connectionref2connectionref_r60ffEClass, connectionref2connectionref_r60ff.class,
				"connectionref2connectionref_r60ff", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getconnectionref2connectionref_r60ff_AddElementActivity(), theMlsdmPackage.getActivity(), null,
				"addElementActivity", null, 1, 1, connectionref2connectionref_r60ff.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_MoveElementActivity(), theMlsdmPackage.getActivity(), null,
				"moveElementActivity", null, 1, 1, connectionref2connectionref_r60ff.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_ChangeAttributeActivity(), theMlsdmPackage.getActivity(),
				null, "changeAttributeActivity", null, 1, 1, connectionref2connectionref_r60ff.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_TransformForwardActivity(), theMlsdmPackage.getActivity(),
				null, "transformForwardActivity", null, 1, 1, connectionref2connectionref_r60ff.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_TransformMappingActivity(), theMlsdmPackage.getActivity(),
				null, "transformMappingActivity", null, 1, 1, connectionref2connectionref_r60ff.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_TransformBackwardActivity(), theMlsdmPackage.getActivity(),
				null, "transformBackwardActivity", null, 1, 1, connectionref2connectionref_r60ff.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_ConflictCheckForwardActivity(),
				theMlsdmPackage.getActivity(), null, "conflictCheckForwardActivity", null, 1, 1,
				connectionref2connectionref_r60ff.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_ConflictCheckMappingActivity(),
				theMlsdmPackage.getActivity(), null, "conflictCheckMappingActivity", null, 1, 1,
				connectionref2connectionref_r60ff.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_ConflictCheckBackwardActivity(),
				theMlsdmPackage.getActivity(), null, "conflictCheckBackwardActivity", null, 1, 1,
				connectionref2connectionref_r60ff.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_SynchronizeForwardActivity(), theMlsdmPackage.getActivity(),
				null, "synchronizeForwardActivity", null, 1, 1, connectionref2connectionref_r60ff.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_SynchronizeBackwardActivity(),
				theMlsdmPackage.getActivity(), null, "synchronizeBackwardActivity", null, 1, 1,
				connectionref2connectionref_r60ff.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_RepairForwardActivity(), theMlsdmPackage.getActivity(),
				null, "repairForwardActivity", null, 1, 1, connectionref2connectionref_r60ff.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getconnectionref2connectionref_r60ff_RepairBackwardActivity(), theMlsdmPackage.getActivity(),
				null, "repairBackwardActivity", null, 1, 1, connectionref2connectionref_r60ff.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		op = initEOperation(getconnectionref2connectionref_r60ff__AddElement__EMap(), theMote2Package.getTGGNode(),
				"addElement", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "parameters", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__ChangeAttributeValues__TGGNode_EMap(),
				ecorePackage.getEBoolean(), "changeAttributeValues", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "correspondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "ruleParameters", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__MoveElement__TGGNode_TGGNode_TGGNode(),
				ecorePackage.getEBoolean(), "moveElement", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "correspondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "oldPreviousCorrespondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "newPreviousCorrespondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__TransformForward__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__TransformMapping__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformMapping", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__TransformBackward__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__ConflictCheckForward__TGGNode(),
				ecorePackage.getEObject(), "conflictCheckForward", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__ConflictCheckMapping__TGGNode(),
				ecorePackage.getEObject(), "conflictCheckMapping", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__ConflictCheckBackward__TGGNode(),
				ecorePackage.getEObject(), "conflictCheckBackward", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__SynchronizeForward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "synchronizeForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__SynchronizeBackward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "synchronizeBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__RepairForward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "repairForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnectionref2connectionref_r60ff__RepairBackward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "repairBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		initEClass(feature2feature_r3EClass, feature2feature_r3.class, "feature2feature_r3", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getfeature2feature_r3_AddElementActivity(), theMlsdmPackage.getActivity(), null,
				"addElementActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_MoveElementActivity(), theMlsdmPackage.getActivity(), null,
				"moveElementActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_ChangeAttributeActivity(), theMlsdmPackage.getActivity(), null,
				"changeAttributeActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_TransformForwardActivity(), theMlsdmPackage.getActivity(), null,
				"transformForwardActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_TransformMappingActivity(), theMlsdmPackage.getActivity(), null,
				"transformMappingActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_TransformBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"transformBackwardActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_ConflictCheckForwardActivity(), theMlsdmPackage.getActivity(), null,
				"conflictCheckForwardActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_ConflictCheckMappingActivity(), theMlsdmPackage.getActivity(), null,
				"conflictCheckMappingActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_ConflictCheckBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"conflictCheckBackwardActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_SynchronizeForwardActivity(), theMlsdmPackage.getActivity(), null,
				"synchronizeForwardActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_SynchronizeBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"synchronizeBackwardActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_RepairForwardActivity(), theMlsdmPackage.getActivity(), null,
				"repairForwardActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getfeature2feature_r3_RepairBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"repairBackwardActivity", null, 1, 1, feature2feature_r3.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getfeature2feature_r3__AddElement__EMap(), theMote2Package.getTGGNode(), "addElement", 1, 1,
				IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "parameters", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__ChangeAttributeValues__TGGNode_EMap(), ecorePackage.getEBoolean(),
				"changeAttributeValues", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "correspondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "ruleParameters", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__MoveElement__TGGNode_TGGNode_TGGNode(), ecorePackage.getEBoolean(),
				"moveElement", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "correspondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "oldPreviousCorrespondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "newPreviousCorrespondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__TransformForward__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__TransformMapping__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformMapping", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__TransformBackward__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__ConflictCheckForward__TGGNode(), ecorePackage.getEObject(),
				"conflictCheckForward", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__ConflictCheckMapping__TGGNode(), ecorePackage.getEObject(),
				"conflictCheckMapping", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__ConflictCheckBackward__TGGNode(), ecorePackage.getEObject(),
				"conflictCheckBackward", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__SynchronizeForward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "synchronizeForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__SynchronizeBackward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "synchronizeBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__RepairForward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "repairForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getfeature2feature_r3__RepairBackward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "repairBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		initEClass(subcomponent2subcomponent_r2EClass, subcomponent2subcomponent_r2.class,
				"subcomponent2subcomponent_r2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getsubcomponent2subcomponent_r2_AddElementActivity(), theMlsdmPackage.getActivity(), null,
				"addElementActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_MoveElementActivity(), theMlsdmPackage.getActivity(), null,
				"moveElementActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_ChangeAttributeActivity(), theMlsdmPackage.getActivity(), null,
				"changeAttributeActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_TransformForwardActivity(), theMlsdmPackage.getActivity(), null,
				"transformForwardActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_TransformMappingActivity(), theMlsdmPackage.getActivity(), null,
				"transformMappingActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_TransformBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"transformBackwardActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_ConflictCheckForwardActivity(), theMlsdmPackage.getActivity(),
				null, "conflictCheckForwardActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_ConflictCheckMappingActivity(), theMlsdmPackage.getActivity(),
				null, "conflictCheckMappingActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_ConflictCheckBackwardActivity(), theMlsdmPackage.getActivity(),
				null, "conflictCheckBackwardActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_SynchronizeForwardActivity(), theMlsdmPackage.getActivity(),
				null, "synchronizeForwardActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_SynchronizeBackwardActivity(), theMlsdmPackage.getActivity(),
				null, "synchronizeBackwardActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_RepairForwardActivity(), theMlsdmPackage.getActivity(), null,
				"repairForwardActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getsubcomponent2subcomponent_r2_RepairBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"repairBackwardActivity", null, 1, 1, subcomponent2subcomponent_r2.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getsubcomponent2subcomponent_r2__AddElement__EMap(), theMote2Package.getTGGNode(),
				"addElement", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "parameters", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__ChangeAttributeValues__TGGNode_EMap(),
				ecorePackage.getEBoolean(), "changeAttributeValues", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "correspondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "ruleParameters", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__MoveElement__TGGNode_TGGNode_TGGNode(),
				ecorePackage.getEBoolean(), "moveElement", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "correspondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "oldPreviousCorrespondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "newPreviousCorrespondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__TransformForward__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__TransformMapping__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformMapping", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__TransformBackward__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__ConflictCheckForward__TGGNode(), ecorePackage.getEObject(),
				"conflictCheckForward", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__ConflictCheckMapping__TGGNode(), ecorePackage.getEObject(),
				"conflictCheckMapping", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__ConflictCheckBackward__TGGNode(),
				ecorePackage.getEObject(), "conflictCheckBackward", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__SynchronizeForward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "synchronizeForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__SynchronizeBackward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "synchronizeBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__RepairForward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "repairForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getsubcomponent2subcomponent_r2__RepairBackward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "repairBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		initEClass(connection2connection_r90pEClass, connection2connection_r90p.class, "connection2connection_r90p",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getconnection2connection_r90p_AddElementActivity(), theMlsdmPackage.getActivity(), null,
				"addElementActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnection2connection_r90p_MoveElementActivity(), theMlsdmPackage.getActivity(), null,
				"moveElementActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnection2connection_r90p_ChangeAttributeActivity(), theMlsdmPackage.getActivity(), null,
				"changeAttributeActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnection2connection_r90p_TransformForwardActivity(), theMlsdmPackage.getActivity(), null,
				"transformForwardActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnection2connection_r90p_TransformMappingActivity(), theMlsdmPackage.getActivity(), null,
				"transformMappingActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnection2connection_r90p_TransformBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"transformBackwardActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnection2connection_r90p_ConflictCheckForwardActivity(), theMlsdmPackage.getActivity(),
				null, "conflictCheckForwardActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getconnection2connection_r90p_ConflictCheckMappingActivity(), theMlsdmPackage.getActivity(),
				null, "conflictCheckMappingActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getconnection2connection_r90p_ConflictCheckBackwardActivity(), theMlsdmPackage.getActivity(),
				null, "conflictCheckBackwardActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getconnection2connection_r90p_SynchronizeForwardActivity(), theMlsdmPackage.getActivity(), null,
				"synchronizeForwardActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnection2connection_r90p_SynchronizeBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"synchronizeBackwardActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getconnection2connection_r90p_RepairForwardActivity(), theMlsdmPackage.getActivity(), null,
				"repairForwardActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getconnection2connection_r90p_RepairBackwardActivity(), theMlsdmPackage.getActivity(), null,
				"repairBackwardActivity", null, 1, 1, connection2connection_r90p.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getconnection2connection_r90p__AddElement__EMap(), theMote2Package.getTGGNode(),
				"addElement", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "parameters", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__ChangeAttributeValues__TGGNode_EMap(),
				ecorePackage.getEBoolean(), "changeAttributeValues", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "correspondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(theHelpersPackage.getMapEntry());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "ruleParameters", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__MoveElement__TGGNode_TGGNode_TGGNode(),
				ecorePackage.getEBoolean(), "moveElement", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "correspondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "oldPreviousCorrespondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "newPreviousCorrespondenceNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__TransformForward__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__TransformMapping__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformMapping", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__TransformBackward__TGGNode_boolean_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "transformBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "considerAllLhsCorrNodes", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__ConflictCheckForward__TGGNode(), ecorePackage.getEObject(),
				"conflictCheckForward", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__ConflictCheckMapping__TGGNode(), ecorePackage.getEObject(),
				"conflictCheckMapping", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__ConflictCheckBackward__TGGNode(), ecorePackage.getEObject(),
				"conflictCheckBackward", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__SynchronizeForward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "synchronizeForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__SynchronizeBackward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "synchronizeBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__RepairForward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "repairForward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());

		op = initEOperation(getconnection2connection_r90p__RepairBackward__TGGNode_boolean(),
				theOperationalTGGPackage.getErrorCodeEnum(), "repairBackward", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMote2Package.getTGGNode(), "inputTggNode", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "checkAttributeFormulae", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEException(op, theHelpersPackage.getTransformationException());
	}

} //GeneratedPackageImpl
