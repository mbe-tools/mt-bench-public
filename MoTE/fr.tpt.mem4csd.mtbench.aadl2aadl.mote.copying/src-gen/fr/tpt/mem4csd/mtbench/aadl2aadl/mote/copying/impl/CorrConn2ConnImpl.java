/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.impl;

import de.mdelab.mltgg.mote2.impl.TGGNodeImpl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrConn2Conn;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Corr Conn2 Conn</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CorrConn2ConnImpl extends TGGNodeImpl implements CorrConn2Conn {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CorrConn2ConnImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyingPackage.Literals.CORR_CONN2_CONN;
	}

} //CorrConn2ConnImpl
