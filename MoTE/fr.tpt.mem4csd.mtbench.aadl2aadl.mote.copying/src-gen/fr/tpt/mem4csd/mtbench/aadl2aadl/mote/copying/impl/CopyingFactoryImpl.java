/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.impl;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CopyingFactoryImpl extends EFactoryImpl implements CopyingFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CopyingFactory init() {
		try {
			CopyingFactory theCopyingFactory = (CopyingFactory) EPackage.Registry.INSTANCE
					.getEFactory(CopyingPackage.eNS_URI);
			if (theCopyingFactory != null) {
				return theCopyingFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CopyingFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyingFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case CopyingPackage.CORR_AXIOM:
			return createCorrAxiom();
		case CopyingPackage.CORR_COMPONENT2_COMPONENT:
			return createCorrComponent2Component();
		case CopyingPackage.CORR_CONNECTION2_DATA_ACCESS:
			return createCorrConnection2DataAccess();
		case CopyingPackage.CORR_REF2_REF:
			return createCorrRef2Ref();
		case CopyingPackage.CORR_CONN2_CONN:
			return createCorrConn2Conn();
		case CopyingPackage.CORR_CONNECTION2_DATA_ACCESS_SYS:
			return createCorrConnection2DataAccessSys();
		case CopyingPackage.CORR_FEATURE2_FEATURE:
			return createCorrFeature2Feature();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrAxiom createCorrAxiom() {
		CorrAxiomImpl corrAxiom = new CorrAxiomImpl();
		return corrAxiom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrComponent2Component createCorrComponent2Component() {
		CorrComponent2ComponentImpl corrComponent2Component = new CorrComponent2ComponentImpl();
		return corrComponent2Component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrConnection2DataAccess createCorrConnection2DataAccess() {
		CorrConnection2DataAccessImpl corrConnection2DataAccess = new CorrConnection2DataAccessImpl();
		return corrConnection2DataAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrRef2Ref createCorrRef2Ref() {
		CorrRef2RefImpl corrRef2Ref = new CorrRef2RefImpl();
		return corrRef2Ref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrConn2Conn createCorrConn2Conn() {
		CorrConn2ConnImpl corrConn2Conn = new CorrConn2ConnImpl();
		return corrConn2Conn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrConnection2DataAccessSys createCorrConnection2DataAccessSys() {
		CorrConnection2DataAccessSysImpl corrConnection2DataAccessSys = new CorrConnection2DataAccessSysImpl();
		return corrConnection2DataAccessSys;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CorrFeature2Feature createCorrFeature2Feature() {
		CorrFeature2FeatureImpl corrFeature2Feature = new CorrFeature2FeatureImpl();
		return corrFeature2Feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyingPackage getCopyingPackage() {
		return (CopyingPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CopyingPackage getPackage() {
		return CopyingPackage.eINSTANCE;
	}

} //CopyingFactoryImpl
