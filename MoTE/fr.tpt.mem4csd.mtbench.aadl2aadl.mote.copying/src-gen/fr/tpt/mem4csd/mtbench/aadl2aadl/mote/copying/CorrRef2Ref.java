/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying;

import de.mdelab.mltgg.mote2.TGGNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Ref2 Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage#getCorrRef2Ref()
 * @model
 * @generated
 */
public interface CorrRef2Ref extends TGGNode {
} // CorrRef2Ref
