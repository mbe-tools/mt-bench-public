/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.util;

import de.mdelab.mltgg.mote2.operationalTGG.OperationalAxiom;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalAxiomGroup;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalMapping;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalMappingGroup;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalRule;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalRuleGroup;
import de.mdelab.mltgg.mote2.operationalTGG.OperationalTGG;

import de.mdelab.mltgg.mote2.sdm.SdmOperationalTGG;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.GeneratedPackage
 * @generated
 */
public class GeneratedSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static GeneratedPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneratedSwitch() {
		if (modelPackage == null) {
			modelPackage = GeneratedPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case GeneratedPackage.COPYING_OPERATIONAL_TGG: {
			copyingOperationalTGG copyingOperationalTGG = (copyingOperationalTGG) theEObject;
			T result = casecopyingOperationalTGG(copyingOperationalTGG);
			if (result == null)
				result = caseSdmOperationalTGG(copyingOperationalTGG);
			if (result == null)
				result = caseOperationalTGG(copyingOperationalTGG);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.MOTE_AXIOM: {
			moteAxiom moteAxiom = (moteAxiom) theEObject;
			T result = casemoteAxiom(moteAxiom);
			if (result == null)
				result = caseOperationalAxiomGroup(moteAxiom);
			if (result == null)
				result = caseOperationalMappingGroup(moteAxiom);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTION2CONNECTION: {
			connection2connection connection2connection = (connection2connection) theEObject;
			T result = caseconnection2connection(connection2connection);
			if (result == null)
				result = caseOperationalRuleGroup(connection2connection);
			if (result == null)
				result = caseOperationalMappingGroup(connection2connection);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF: {
			connectionref2connectionref connectionref2connectionref = (connectionref2connectionref) theEObject;
			T result = caseconnectionref2connectionref(connectionref2connectionref);
			if (result == null)
				result = caseOperationalRuleGroup(connectionref2connectionref);
			if (result == null)
				result = caseOperationalMappingGroup(connectionref2connectionref);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.FEATURE2FEATURE: {
			feature2feature feature2feature = (feature2feature) theEObject;
			T result = casefeature2feature(feature2feature);
			if (result == null)
				result = caseOperationalRuleGroup(feature2feature);
			if (result == null)
				result = caseOperationalMappingGroup(feature2feature);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.SUBCOMPONENT2SUBCOMPONENT: {
			subcomponent2subcomponent subcomponent2subcomponent = (subcomponent2subcomponent) theEObject;
			T result = casesubcomponent2subcomponent(subcomponent2subcomponent);
			if (result == null)
				result = caseOperationalRuleGroup(subcomponent2subcomponent);
			if (result == null)
				result = caseOperationalMappingGroup(subcomponent2subcomponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.MOTE_AXIOM_R0: {
			moteAxiom_r0 moteAxiom_r0 = (moteAxiom_r0) theEObject;
			T result = casemoteAxiom_r0(moteAxiom_r0);
			if (result == null)
				result = caseOperationalAxiom(moteAxiom_r0);
			if (result == null)
				result = caseOperationalMapping(moteAxiom_r0);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTIONREF2CONNECTIONREF_R60FF: {
			connectionref2connectionref_r60ff connectionref2connectionref_r60ff = (connectionref2connectionref_r60ff) theEObject;
			T result = caseconnectionref2connectionref_r60ff(connectionref2connectionref_r60ff);
			if (result == null)
				result = caseOperationalRule(connectionref2connectionref_r60ff);
			if (result == null)
				result = caseOperationalMapping(connectionref2connectionref_r60ff);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.FEATURE2FEATURE_R3: {
			feature2feature_r3 feature2feature_r3 = (feature2feature_r3) theEObject;
			T result = casefeature2feature_r3(feature2feature_r3);
			if (result == null)
				result = caseOperationalRule(feature2feature_r3);
			if (result == null)
				result = caseOperationalMapping(feature2feature_r3);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.SUBCOMPONENT2SUBCOMPONENT_R2: {
			subcomponent2subcomponent_r2 subcomponent2subcomponent_r2 = (subcomponent2subcomponent_r2) theEObject;
			T result = casesubcomponent2subcomponent_r2(subcomponent2subcomponent_r2);
			if (result == null)
				result = caseOperationalRule(subcomponent2subcomponent_r2);
			if (result == null)
				result = caseOperationalMapping(subcomponent2subcomponent_r2);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case GeneratedPackage.CONNECTION2CONNECTION_R90P: {
			connection2connection_r90p connection2connection_r90p = (connection2connection_r90p) theEObject;
			T result = caseconnection2connection_r90p(connection2connection_r90p);
			if (result == null)
				result = caseOperationalRule(connection2connection_r90p);
			if (result == null)
				result = caseOperationalMapping(connection2connection_r90p);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>copying Operational TGG</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>copying Operational TGG</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casecopyingOperationalTGG(copyingOperationalTGG object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>mote Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>mote Axiom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casemoteAxiom(moteAxiom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connection2connection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connection2connection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnection2connection(connection2connection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connectionref2connectionref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connectionref2connectionref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnectionref2connectionref(connectionref2connectionref object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>feature2feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>feature2feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casefeature2feature(feature2feature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>subcomponent2subcomponent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>subcomponent2subcomponent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casesubcomponent2subcomponent(subcomponent2subcomponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>mote Axiom r0</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>mote Axiom r0</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casemoteAxiom_r0(moteAxiom_r0 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connectionref2connectionref r60ff</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connectionref2connectionref r60ff</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnectionref2connectionref_r60ff(connectionref2connectionref_r60ff object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>feature2feature r3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>feature2feature r3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casefeature2feature_r3(feature2feature_r3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>subcomponent2subcomponent r2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>subcomponent2subcomponent r2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casesubcomponent2subcomponent_r2(subcomponent2subcomponent_r2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>connection2connection r90p</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>connection2connection r90p</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseconnection2connection_r90p(connection2connection_r90p object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational TGG</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational TGG</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalTGG(OperationalTGG object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational TGG</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational TGG</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSdmOperationalTGG(SdmOperationalTGG object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Mapping Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Mapping Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalMappingGroup(OperationalMappingGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Axiom Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Axiom Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalAxiomGroup(OperationalAxiomGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Rule Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Rule Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalRuleGroup(OperationalRuleGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalMapping(OperationalMapping object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Axiom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Axiom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalAxiom(OperationalAxiom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operational Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operational Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalRule(OperationalRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //GeneratedSwitch
