/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Corr Connection2 Data Access Sys</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage#getCorrConnection2DataAccessSys()
 * @model
 * @generated
 */
public interface CorrConnection2DataAccessSys extends CorrFeature2Feature {
} // CorrConnection2DataAccessSys
