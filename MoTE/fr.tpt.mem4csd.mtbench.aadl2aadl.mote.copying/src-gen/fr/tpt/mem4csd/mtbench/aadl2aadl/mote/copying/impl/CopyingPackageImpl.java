/**
 */
package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.impl;

import de.mdelab.mlcallactions.MlcallactionsPackage;

import de.mdelab.mlcore.MlcorePackage;

import de.mdelab.mlexpressions.MlexpressionsPackage;

import de.mdelab.mlsdm.MlsdmPackage;

import de.mdelab.mlstorypatterns.MlstorypatternsPackage;

import de.mdelab.mltgg.mote2.Mote2Package;

import de.mdelab.mltgg.mote2.sdm.SdmPackage;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingFactory;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrAxiom;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrComponent2Component;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrConn2Conn;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrConnection2DataAccess;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrConnection2DataAccessSys;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrFeature2Feature;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CorrRef2Ref;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.GeneratedPackage;

import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.generated.impl.GeneratedPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CopyingPackageImpl extends EPackageImpl implements CopyingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrAxiomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrComponent2ComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrConnection2DataAccessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrRef2RefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrConn2ConnEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrConnection2DataAccessSysEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass corrFeature2FeatureEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CopyingPackageImpl() {
		super(eNS_URI, CopyingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link CopyingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CopyingPackage init() {
		if (isInited)
			return (CopyingPackage) EPackage.Registry.INSTANCE.getEPackage(CopyingPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredCopyingPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		CopyingPackageImpl theCopyingPackage = registeredCopyingPackage instanceof CopyingPackageImpl
				? (CopyingPackageImpl) registeredCopyingPackage
				: new CopyingPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Mote2Package.eINSTANCE.eClass();
		MlcorePackage.eINSTANCE.eClass();
		SdmPackage.eINSTANCE.eClass();
		MlsdmPackage.eINSTANCE.eClass();
		MlexpressionsPackage.eINSTANCE.eClass();
		MlstorypatternsPackage.eINSTANCE.eClass();
		MlcallactionsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(GeneratedPackage.eNS_URI);
		GeneratedPackageImpl theGeneratedPackage = (GeneratedPackageImpl) (registeredPackage instanceof GeneratedPackageImpl
				? registeredPackage
				: GeneratedPackage.eINSTANCE);

		// Create package meta-data objects
		theCopyingPackage.createPackageContents();
		theGeneratedPackage.createPackageContents();

		// Initialize created meta-data
		theCopyingPackage.initializePackageContents();
		theGeneratedPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCopyingPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CopyingPackage.eNS_URI, theCopyingPackage);
		return theCopyingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrAxiom() {
		return corrAxiomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrComponent2Component() {
		return corrComponent2ComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrConnection2DataAccess() {
		return corrConnection2DataAccessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrRef2Ref() {
		return corrRef2RefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrConn2Conn() {
		return corrConn2ConnEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrConnection2DataAccessSys() {
		return corrConnection2DataAccessSysEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrFeature2Feature() {
		return corrFeature2FeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyingFactory getCopyingFactory() {
		return (CopyingFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		corrAxiomEClass = createEClass(CORR_AXIOM);

		corrComponent2ComponentEClass = createEClass(CORR_COMPONENT2_COMPONENT);

		corrConnection2DataAccessEClass = createEClass(CORR_CONNECTION2_DATA_ACCESS);

		corrRef2RefEClass = createEClass(CORR_REF2_REF);

		corrConn2ConnEClass = createEClass(CORR_CONN2_CONN);

		corrConnection2DataAccessSysEClass = createEClass(CORR_CONNECTION2_DATA_ACCESS_SYS);

		corrFeature2FeatureEClass = createEClass(CORR_FEATURE2_FEATURE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GeneratedPackage theGeneratedPackage = (GeneratedPackage) EPackage.Registry.INSTANCE
				.getEPackage(GeneratedPackage.eNS_URI);
		Mote2Package theMote2Package = (Mote2Package) EPackage.Registry.INSTANCE.getEPackage(Mote2Package.eNS_URI);

		// Add subpackages
		getESubpackages().add(theGeneratedPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		corrAxiomEClass.getESuperTypes().add(this.getCorrComponent2Component());
		corrComponent2ComponentEClass.getESuperTypes().add(this.getCorrFeature2Feature());
		corrConnection2DataAccessEClass.getESuperTypes().add(this.getCorrFeature2Feature());
		corrRef2RefEClass.getESuperTypes().add(theMote2Package.getTGGNode());
		corrConn2ConnEClass.getESuperTypes().add(theMote2Package.getTGGNode());
		corrConnection2DataAccessSysEClass.getESuperTypes().add(this.getCorrFeature2Feature());
		corrFeature2FeatureEClass.getESuperTypes().add(theMote2Package.getTGGNode());

		// Initialize classes, features, and operations; add parameters
		initEClass(corrAxiomEClass, CorrAxiom.class, "CorrAxiom", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(corrComponent2ComponentEClass, CorrComponent2Component.class, "CorrComponent2Component",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(corrConnection2DataAccessEClass, CorrConnection2DataAccess.class, "CorrConnection2DataAccess",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(corrRef2RefEClass, CorrRef2Ref.class, "CorrRef2Ref", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(corrConn2ConnEClass, CorrConn2Conn.class, "CorrConn2Conn", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(corrConnection2DataAccessSysEClass, CorrConnection2DataAccessSys.class,
				"CorrConnection2DataAccessSys", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(corrFeature2FeatureEClass, CorrFeature2Feature.class, "CorrFeature2Feature", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //CopyingPackageImpl
