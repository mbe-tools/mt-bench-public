package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class RefinementAddition extends AbstractMoteRefinementScenario {

	public RefinementAddition() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.ADDITION;
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.REFINEMENT_ADD_SUFFIX;
	}

}
