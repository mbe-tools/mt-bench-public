package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.AbstractScenario;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.TestSuiteAbstract;

public class TestSuiteRefinement extends TestSuiteAbstract {

	public TestSuiteRefinement() {
		super();
	}

	public static void main(String[] args) {
		runAllTests( args, new TestSuiteRefinement() );
	}

	@Override
	protected String getTestSuiteName() {
		return "MoTE-Refinement";
	}

	@Override
	protected AbstractScenario getScenario(ScenarioKind scenarioKind) {
		switch ( scenarioKind ) {
			case ADDITION:
				return new RefinementAddition();
			
			case UPDATE_ATT:
				return new RefinementUpdateAttribute();
	
			case UPDATE_REF:
				return new RefinementUpdateReference();
	
			case DELETE:
				return new RefinementDeletion();
	
			case BATCH:
				return new RefinementBatch();
				
			default:
				throw new IllegalArgumentException( "Unknown scenario kind: '" + scenarioKind + "'." );
		}
	}
}