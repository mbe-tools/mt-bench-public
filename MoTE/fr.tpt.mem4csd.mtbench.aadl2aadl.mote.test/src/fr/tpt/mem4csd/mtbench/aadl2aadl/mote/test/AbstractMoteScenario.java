package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.mdelab.mlcallactions.MlcallactionsPackage;
import de.mdelab.mlcore.MlcorePackage;
import de.mdelab.mlexpressions.MlexpressionsPackage;
import de.mdelab.mlsdm.MlsdmPackage;
import de.mdelab.mlstorypatterns.MlstorypatternsPackage;
import de.mdelab.mltgg.mote2.Mote2Package;
import de.mdelab.mltgg.mote2.TransformationDirectionEnum;
import de.mdelab.mltgg.mote2.sdm.MoTE2Sdm;
import de.mdelab.mltgg.mote2.sdm.SdmOperationalTGG;
import de.mdelab.mltgg.mote2.sdm.SdmPackage;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.AbstractScenario;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.ModelModif;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.ResourcesUtil;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.Result;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public abstract class AbstractMoteScenario extends AbstractScenario {

	protected AbstractMoteScenario() {
		super();
	}

	@Override
	protected void setup() {
		MlcorePackage.eINSTANCE.eClass();
		MlexpressionsPackage.eINSTANCE.eClass();
		MlcallactionsPackage.eINSTANCE.eClass();
		MlstorypatternsPackage.eINSTANCE.eClass();
		MlsdmPackage.eINSTANCE.eClass();
		Mote2Package.eINSTANCE.eClass();
		SdmPackage.eINSTANCE.eClass();

		super.setup();
	}

	protected abstract MoTE2Sdm createEngine();

	protected MoTE2Sdm createEngine(final String tggUriPath) {
		final MoTE2Sdm engine = new MoTE2Sdm();
		final URI opTggResUri = ResourcesUtil.createURI(tggUriPath, "xmi");
		final Resource opTggRes = ResourcesUtil.createResourceSet().getResource(opTggResUri, true);
		final SdmOperationalTGG operationalTgg = (SdmOperationalTGG) opTggRes.getContents().get(0);
		EcoreUtil.resolveAll(opTggRes);
		engine.initRules(operationalTgg);

		return engine;
	}

	protected long[] scenarioSteps(ScenarioKind scenarioKind, Resource leftSystem, ModelModif modif, int runtimes,
			MoTE2Sdm engine) {
		long t0, t1;
		long[] runs = new long[runtimes];

		if (scenarioKind != ScenarioKind.BATCH) {
			engine.transform(TransformationDirectionEnum.FORWARD, false, false, false, false, null);
		}

		for (int i = 0; i < runtimes; i++) {
			modif.applyModif(scenarioKind,leftSystem);
			
			t0 = System.nanoTime();
			engine.transform(TransformationDirectionEnum.FORWARD, true, false, false, false, null);
			t1 = System.nanoTime();
			runs[i] = t1 - t0;
		}
		return runs;
	}

	abstract ScenarioKind getScenario();

	abstract String getExtension();

	@Override
	public Resource executeScenario(final int testNumber, final Result res, final String inputModelName)
			throws Exception {
		ModelModif modif = new ModelModif();
		long[] runs = new long[res.getRuntimes()];
		long t0, t1;
		t0 = System.nanoTime();
		final MoTE2Sdm engine = createEngine();
		final ResourceSet resourceSet = ResourcesUtil.createResourceSet();
		final Resource aadlResourceleft = ResourcesUtil.loadAadlInstanceInputResource(inputModelName, resourceSet);
		final URI createdResourceUri = ResourcesUtil.createAadlInstanceOutputURI(inputModelName + getExtension());
		final Resource aadlResourceright = resourceSet.createResource(createdResourceUri);

		System.out.println(
				"Test " + testNumber + " " + aadlResourceright.getURI().lastSegment() + " Performing transformation:");

		engine.initModels(aadlResourceleft.getContents(), aadlResourceright.getContents());
		t1 = System.nanoTime();
		
		System.out.println("Test " + testNumber + " Initialization in " + Long.toString(t1 - t0));
		
		runs = scenarioSteps(getScenario(), aadlResourceleft, modif, res.getRuntimes(), engine);

		res.setExecutionTimes(inputModelName, testNumber, res.getMedian(runs));
		System.out.println("Test " + testNumber + " in " + res.getMedian(runs) + " ns");

		aadlResourceright.getContents().addAll(engine.getRightInputElements());
		aadlResourceright.save(null);

		System.out.println("Transformation finished.");

		return aadlResourceright;
	}
}