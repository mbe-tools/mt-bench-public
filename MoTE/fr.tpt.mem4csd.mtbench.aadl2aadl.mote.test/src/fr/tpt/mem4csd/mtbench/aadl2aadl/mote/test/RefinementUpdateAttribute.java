package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class RefinementUpdateAttribute extends AbstractMoteRefinementScenario {

	public RefinementUpdateAttribute() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.UPDATE_ATT;
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.REFINEMENT_UPDATE_ATT_SUFFIX;
	}

}