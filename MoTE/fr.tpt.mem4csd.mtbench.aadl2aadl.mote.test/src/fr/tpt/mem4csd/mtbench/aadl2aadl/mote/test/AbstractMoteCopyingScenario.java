package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import de.mdelab.mltgg.mote2.sdm.MoTE2Sdm;
import fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying.CopyingPackage;

public abstract class AbstractMoteCopyingScenario extends AbstractMoteScenario {

	protected AbstractMoteCopyingScenario() {
		super();
	}

	@Override
	protected void setup() {
		super.setup();
		
		CopyingPackage.eINSTANCE.eClass();
	}

	@Override
	protected MoTE2Sdm createEngine() {
		return super.createEngine( "../fr.tpt.mem4csd.mtbench.aadl2aadl.mote.copying/model-gen/config" );
	}
}
