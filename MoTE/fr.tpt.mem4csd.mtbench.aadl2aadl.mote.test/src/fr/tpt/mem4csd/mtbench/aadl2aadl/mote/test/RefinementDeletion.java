package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class RefinementDeletion extends AbstractMoteRefinementScenario {

	public RefinementDeletion() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.DELETE;
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.REFINEMENT_DELETION_SUFFIX;
	}

}