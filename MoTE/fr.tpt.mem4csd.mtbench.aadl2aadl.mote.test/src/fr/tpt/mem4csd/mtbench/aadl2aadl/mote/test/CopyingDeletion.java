package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class CopyingDeletion extends AbstractMoteCopyingScenario {

	public CopyingDeletion() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.DELETE;
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.COPY_DELETION_SUFFIX;
	}

}
