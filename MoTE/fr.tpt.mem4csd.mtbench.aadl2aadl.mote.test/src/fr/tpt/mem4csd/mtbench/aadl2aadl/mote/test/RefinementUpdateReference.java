package fr.tpt.mem4csd.mtbench.aadl2aadl.mote.test;

import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants;
import fr.tpt.mem4csd.mtbench.aadl2aadl.tests.BenchmarkConstants.ScenarioKind;

public class RefinementUpdateReference extends AbstractMoteRefinementScenario {

	public RefinementUpdateReference() {
		super();
	}

	@Override
	ScenarioKind getScenario() {
		return ScenarioKind.UPDATE_REF;
	}

	@Override
	String getExtension() {
		return BenchmarkConstants.REFINEMENT_UPDATE_REF_SUFFIX;
	}


}
